package consegne;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import config.Config;
import provvedimento.Provvedimento;

public class ConsegneReader {

	List<Provvedimento> consegne = new ArrayList<Provvedimento>();
	
	
	
	public List<String> readConsegnati(){
		List<String> consegnati = new ArrayList<String>();
		
		File folderConsegnati = new File(Config.folderCONSEGNATI);
		File[] listOfFiles = folderConsegnati.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()  && file.getName().startsWith("consegna") && file.getName().endsWith("txt")) {
				String distretto = file.getName().split("-")[1];
				String dataConsegna = file.getName().split("-")[2];
				dataConsegna = dataConsegna.substring(0,dataConsegna.indexOf(".")).trim();
				
//				System.err.println("distretto: "+distretto);
//				System.err.println("dataConsegna: "+dataConsegna);
				
				try {
					BufferedReader reader = new BufferedReader( new FileReader(file));
					String line  = null;

					while( ( line = reader.readLine() ) != null) {
						String idConsegna = line.trim();
						//System.err.println("\t"+idConsegna);
						consegnati.add(dataConsegna+"\t"+idConsegna);
					}
				}catch(Exception x) {
					x.printStackTrace();
				}
			}
		}
		
		return consegnati;
	}
	
	
	public List<String> readScartati(){
		
		List<String> scartati = new ArrayList<String>();
		
		File folderScartati = new File(Config.folderSCARTATI);
		File[] listOfFiles = folderScartati.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()  && file.getName().startsWith("scart") && file.getName().endsWith("txt")) {
				
				System.err.println("Scartati");

				
				try {
					BufferedReader reader = new BufferedReader( new FileReader(file));
					String line  = null;

					while( ( line = reader.readLine() ) != null) {
						
						// SKIP COMMENTS
						if(line.startsWith("#"))
							continue;
						
						String idScartato = line.trim();
						System.err.println("\t"+idScartato);
						scartati.add(idScartato);
					}
				}catch(Exception x) {
					x.printStackTrace();
				}
			}
		}
		return scartati;
	}
	
	
	
	public List<String> readRisposte(){
		List<String> risposte = new ArrayList<String>();
		
		File folderRisposte = new File(Config.folderRISPOSTE);
		String[] listOfFolders = folderRisposte.list();
		
		
		for (String dirName : listOfFolders) {
			File distrettoFolder = new File(Config.folderRISPOSTE+"/"+dirName); 
			if (distrettoFolder.isDirectory()  && !distrettoFolder.getName().startsWith("old")) {
				String distretto = distrettoFolder.getName().toUpperCase();
				
				String[] listOfItems = distrettoFolder.list();
				
				for (String item : listOfItems) {
					if(!item.startsWith(".")) {
						File fileItem = new File(item);
						String idItem = fileItem.getName();
						if(idItem.length()>11) {

							String idProvv = idItem.substring(0,2).toUpperCase()
									+":"+idItem.substring(2,4).toUpperCase()
									+":"+idItem.substring(4,7).toUpperCase()
									+":"+idItem.substring(7,11).toUpperCase()
									+":"+idItem.substring(11);

							File itemFolder = new File(Config.folderRISPOSTE+"/"+dirName+"/"+idItem);
							String[] listOfContents = itemFolder.list();
							String replied = "NON_TROVATO";
							String path = "-";
							String nota ="";
							for (String content : listOfContents) {
								File file = new File(content);
								//if(file.getName().startsWith("document")) {
								if(file.getName().endsWith("pdf")) {
									replied = "TROVATO";
									path = "file://"+Config.folderRISPOSTE+"/"+dirName+"/"+idItem+"/"+file.getName();
								}
								if(file.getName().startsWith("note")) {
									try {
										Scanner sc = new Scanner(new File(Config.folderRISPOSTE+"/"+dirName+"/"+idItem+"/"+file.getName()));
										while(sc.hasNextLine()){
											nota = sc.nextLine();                     
										}
									}catch(Exception e) {
										e.printStackTrace();
									}
									if(nota.trim().toLowerCase().startsWith("digitare eventuali note"))
										nota ="";
									if(nota.trim().length()>0)
										replied+="_NOTA";
									else
										replied+="_NOTA_VUOTA";
								}

							}

							risposte.add(distretto+"\t"+idProvv+"\t"+replied+"\t"+path+"\t"+nota);
						}
						else {
							System.err.println("++++++++++++WARNING: non canonical id for reply - "+idItem);
							//System.err.println(distretto+"\t"+idProvv+"\t"+replied+"\t"+path+"\t"+nota);
						}
					}
				}
			}
		}
		
		return risposte;
	}
	
	
}
