package wkiDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fonte.Fonte;
import provvedimento.Provvedimento;

public class WKISpogliDataReader {
	
	
	List<Provvedimento> provvedimenti = new ArrayList<Provvedimento>();
	
	
	
	public boolean readWKISpogliTSV(String tsvFilePath) {
		

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
	
				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
							
				String idITTIG ="";
				String distretto ="";
				String provv_year="";
				String esito = "";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				
				
				
				//0 idITTIG	
				//1 isEstremoCompleto	
				//2 isEditManuale	
//				3 distretto	
//				4 anno	
//				5 tipo_ufficio	
//				6 localita_ufficio	
//				7 tipo_provvedimento	
//				8 settore	
//				9 provv_data	
//				10 provv_numero	
//				11 provv_sez	
//				12 giudici	
//				13 parti	
//				14 num_registro	
//				15 estremo_provv
				
				idITTIG = fields[0].trim();
//				estremoCompleto = fields[1].trim();
				esito = fields[1].trim();
				distretto = fields[3].trim();
				provv_year = fields[4].trim();
				tipo_ufficio  = fields[5].trim();
				localita_ufficio = fields[6].trim();
				tipoDoc = fields[7].trim();
				area = fields[8].trim();
				provv_data = fields[9].trim();  // trasforma in aaaa-mm-gg
				provv_numero = fields[10].trim();
				provv_sezione = fields[11].trim();
				giudici = fields[12].trim();
				parti = fields[13].trim();
				num_registro = fields[14].trim();
				provv_estremo = fields[15].trim();
						
						
				Provvedimento p = new Provvedimento();
		
			
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
		
				p.setEditManuale(true);
				p.setEstremoCompletoEdit(esito.toLowerCase().startsWith("x")?true:false);
				p.setEstremoCompletoAuto(false);

				p.setPersistentID(idITTIG);
				p.setDocType(tipoDoc);
				p.setDistrettoCorteAppello(distretto);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(provv_data);
				p.setCourt_section(provv_sezione); 				
				p.setEstremoText(provv_estremo);
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);				
			
				provvedimenti.add(p);			
				
			
				
			}
			System.err.println("processed "+i+" WKI Spogli rows");

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}

	
	
	
	
	public List<Provvedimento> getProvvedimenti() {
		return this.provvedimenti;
	}
	

}
