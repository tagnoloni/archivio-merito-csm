package wkiDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import config.Config;
import fonte.Fonte;
import provvedimento.Provvedimento;
import util.DistrettiCA;
import util.Util;

public class WKIDataReader {



	HashMap<String,Provvedimento> provvedimenti = new HashMap<String,Provvedimento>();
	HashMap<String,Fonte> uniqueFonti = new HashMap<String,Fonte>();

	

	HashMap<String,List<String>> lookupComuni = new HashMap<String,List<String>>();
	HashMap<String,String> lookupDistrettiCA = new HashMap<String,String>();
	
	int idProvvedimento =1;



	

	public boolean readWKITSV(String tsvFilePath) {

	
		// deduplica 1 (provvedimenti con lo stesso estremoProvvedimento e lo stesso idContributo -- non aggiungere)
		// deduplica 2 (provvedimenti con lo stesso estremoProvvedimento e idContributo diverso -- crea Fonti multiple)
		
		// completa con distretto CA; IDPermanente

		
		List<Provvedimento> provvedimentiWKIList = new ArrayList<Provvedimento>();

	
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

		
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				
				String[] fields = line.split("\t");

				
				// ITEM WKI
//				System.out.println(pagina+Config.SEP
//						+idProvvedimentoRequest+Config.SEP
//						+provv.getProvv_year()+Config.SEP
//						+provv.getGeo_name()+Config.SEP
//						+provv.getAuthority()+Config.SEP
//						+provv.getCourt_section()+Config.SEP
//						+provv.getDocType()+Config.SEP
//						+provv.getProvv_date()+Config.SEP
//						+provv.getProvv_number()+Config.SEP
//						+estremoProvvedimento+Config.SEP
//						+parti+Config.SEP
//						+linkProvvedimento+Config.SEP
//						+idScheda+Config.SEP
				
//						+titolo+Config.SEP
//						+rivista+Config.SEP
//						+autori+Config.SEP
//						+cleanAutori+Config.SEP
//						+estremoRivista+Config.SEP
//						+cleanEstremo+Config.SEP
//						+annoContributo+Config.SEP
//						+volumeContributo+Config.SEP
//						+pagineContributo+Config.SEP
//						+linkScheda+Config.SEP
//						+classificazione+Config.SEP
//						+commentedWith+Config.SEP
//						+provenance);
				
				
				String paginaWKI = fields[0].trim();
				String idProvvedimentoWKI = fields[1].trim();
				String provv_year = fields[2].trim();
				String localita_ufficio = fields[3].trim();
				String tipo_ufficio  = fields[4].trim();
				String provv_sezione = fields[5].trim();
				String provv_tipoDoc = fields[6].trim();
				String provv_date = fields[7].trim();  
				String provv_numero = fields[8].trim();
				String provv_estremo = fields[9].trim();
				String provv_parti = fields[10].trim();
				String provv_url = fields[11].trim();
				String idContributoWKI = fields[12].trim();
				String contrib_titolo = fields[13].trim();
				String contrib_rivista = fields[14].trim();
				// skip autori originale [15]
				String contrib_autori = fields[16].trim();
				// skip fonte originale [15]
				String contrib_fonte = fields[18].trim(); 
				String contrib_anno = fields[19].trim();
				String contrib_volume = fields[20].trim();
				String contrib_pagine = fields[21].trim();
				String contrib_url = fields[22].trim();
				String classificazioniTOP = fields[23].trim();
				String commentedWith = fields[24].trim();
				String provenance = translateProvenance(fields[25].trim());


				
				Provvedimento p = new Provvedimento();
								
				p.setScrapedPageNumber(paginaWKI);
				p.setIdProvvedimento(idProvvedimentoWKI);  
				p.setProvv_year(provv_year);
				p.setGeo_name(localita_ufficio);
				p.setAuthority(tipo_ufficio);
				p.setCourt_section(provv_sezione);
				p.setDocType(provv_tipoDoc);
				p.setProvv_date(provv_date);
				p.setProvv_number(provv_numero);
				p.setEstremoText(provv_estremo);
				p.setParti(provv_parti);
				p.setUrl(provv_url);
				p.setCommentedWith(commentedWith);
				p.setProvenance(provenance);
				
				// CALCOLATI: 
				DistrettiCA distretti = new DistrettiCA();
				p = distretti.setDistrettoAppello(p);
				p.setPersistentID(Util.computeNewID(p));
				
				
				Fonte f = new Fonte();
				f.setIdContributo(idContributoWKI);		
				f.setTitoloContributo(contrib_titolo);
				f.setRivista(contrib_rivista);
				f.setAutori(contrib_autori);
				f.setDataFascicolo(contrib_anno);  
				f.setNumFascicolo(contrib_volume);
				f.setPagIniziale(contrib_pagine);
				f.setUrl(contrib_url);
				f.setSistematici(new String[]{classificazioniTOP});
				f.setEditore("Wolters Kluwer Italia s.r.l.");
				f.setFindableIn("WKI");
				f.setEstremoText(contrib_fonte);     	
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
				uniqueFonti.put(idContributoWKI, f);
				
				provvedimentiWKIList.add(p);
				
				
				
//				String found ="";
//				if((found = findEstremoInProvvedimenti(estremo))==null) {
//					provvedimenti.put(""+idProvvedimento, p);
//				}else {
//					// provvedimento commentato in più contributi; gli aggiungo solo il contributo
//					Fonte newFonte = p.getFonti().get(0);
//					provvedimenti.get(found).addFonte(newFonte);
//				}
				
				
			}
			System.err.println("processed "+i+" rows");
			
			
			deduplicateAndMergeFonti(provvedimentiWKIList);


		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	
	
	
    private void deduplicateAndMergeFonti(List<Provvedimento> provvedimentiWKIList) {
    	
    	System.err.println("BEFORE provvedimentiWKIList "+provvedimentiWKIList.size());
    	
    	// obb:
    	// RIMUOVERE provvedimenti duplicati con stesso id Contributo  (sono restituiti due volte da query diverse - es. queryTRIB e queryAPP)
    	
    	// ACCORPARE provvedimenti commentati in piu fonti   1 provvedimento : N Fonti
    	// metterli nella mappa dei provvedimenti 
    	
    	// la lista provvedimentiWKIList è 1 provvedimento :1 fonte 
    	
    	for(Provvedimento provv:provvedimentiWKIList) {
    		// provvedimento già preso
    		if(provvedimenti.get(provv.getPersistentID())!=null) {
    			
    			// provvedimento già presente
    			Provvedimento existingProvv = provvedimenti.get(provv.getPersistentID());
    			
    			String existingContributi ="";
    			for(Fonte f:existingProvv.getFonti()) {
    				existingContributi+=f.getIdContributo()+" ";
    			}
    			
    			if(!existingContributi.contains(provv.getFonti().get(0).getIdContributo()))
    				existingProvv.addFonte(provv.getFonti().get(0));
    			
    		}else {
    			provvedimenti.put(provv.getPersistentID(), provv);
    		}
    	}
    	
    	System.err.println("AFTER: provvedimenti MAP "+provvedimenti.size());
    	
    }


	public boolean printExplodedProvvedimenti() {

			
		
		
		String header ="persistentID"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"pageNum"+Config.SEP
				+"estremo_completo"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
				+"giudici"+Config.SEP
				+"parti"+Config.SEP
				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"idContributo"+Config.SEP
				+"titolo"+Config.SEP
				+"autori"+Config.SEP
				+"url_contributo"+Config.SEP
				+"rivista"+Config.SEP
				+"editore"+Config.SEP
				+"findable_IN"+Config.SEP
				+"fonte"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		System.err.println(header);
	

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printExplodedProvvedimentoCSV();
//			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
//				continue;
//			
//			if(p.getProvv_year()==null )
//				p.printGennusoCSV_1_3();
//			else{
//				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
//					p.printGennusoCSV_1_3();
//			}
		}
		return true;
	}
	
	
	public List<Provvedimento> getProvvedimenti() {
		List<Provvedimento> filtered_provv_list = new ArrayList<Provvedimento>();
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			

			if(p.getProvv_year()==null )
				filtered_provv_list.add(p);
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					filtered_provv_list.add(p);
			}
			
		}	
		return filtered_provv_list;
	}


	private String translateProvenance(String provenance) {
		switch(provenance) {
		case "DeJure-Giurisp. commentata-Appello-2014-2019": return Config.DEJURE_COMM_APP;
		case "DeJure-Giurisp. commentata-Tribunali-2014-2019": return Config.DEJURE_COMM_TRIB;
		case "DeJure-Note a sentenza-Appello-2014-2019": return Config.DEJURE_NAS_APP;
		case "DeJure-Note a sentenza-Tribunali-2014-2019": return Config.DEJURE_NAS_TRIB;
		case "WKI_NAS_APPELLO_2016_2019": return Config.WKI_NAS_APP;
		case "WKI_NAS_TRIBUNALI_2016_2019": return Config.WKI_NAS_TRIB;
		default: return "";
		}
	}
	
	
	
}

