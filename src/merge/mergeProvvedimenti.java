package merge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fonte.Fonte;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import provvedimento.Provvedimento;
import util.Util;


public class mergeProvvedimenti {


	List<Provvedimento> provv_deJure = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_deJure_Giur_Comm = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_deJure_NAS = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_deJureUnique = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_deJureUnique1N = new ArrayList<Provvedimento>();

	List<Provvedimento> provv_Spogli = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_Spogli_WKI = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_EditManuale = new ArrayList<Provvedimento>();

	
	List<Provvedimento> provv_Dogi = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_WKI = new ArrayList<Provvedimento>();
	List<Provvedimento> provv_Merge = new ArrayList<Provvedimento>();
	
	HashMap<String,Provvedimento> provv_Merge_Map = new HashMap<String,Provvedimento>();
	

	
	List<Provvedimento> residual_deJure = new ArrayList<Provvedimento>();
	List<Provvedimento> residual_WKI = new ArrayList<Provvedimento>();
	
	List<Fonte> uniqueFontiDeJure = new ArrayList<Fonte>();




	public List<Provvedimento> getProvv_Merge() {
		return provv_Merge;
	}
	
	public HashMap<String,Provvedimento> getProvv_Merge_Map() {
		return provv_Merge_Map;
	}
	
	public void setProvv_Merge_Map(HashMap<String,Provvedimento> provvMap) {
		this.provv_Merge_Map = provvMap;
	}
	
	public void setProvv_Merge(List<Provvedimento> provv_Merge) {
		this.provv_Merge = provv_Merge;
	}
	public List<Provvedimento> getProvv_deJure() {
		return provv_deJure;
	}
	public void setProvv_deJure(List<Provvedimento> provv_deJure) {
		this.provv_deJure = provv_deJure;
	}
	public List<Provvedimento> getProvv_Dogi() {
		return provv_Dogi;
	}
	public void setProvv_Dogi(List<Provvedimento> provv_Dogi) {
		this.provv_Dogi = provv_Dogi;
	}
	public List<Provvedimento> getProvv_WKI() {
		return provv_WKI;
	}
	public void setProvv_WKI(List<Provvedimento> provv_WKI) {
		this.provv_WKI = provv_WKI;
	}

	public void setSpogliManualiDeJure(List<Provvedimento> edited_provv) {
		this.provv_Spogli = edited_provv;
	}
	

	public void setSpogliManualiWKI(List<Provvedimento> edited_provv) {
		this.provv_Spogli_WKI = edited_provv;
	}
	
	
	public void setEditManuali(List<Provvedimento> edited_provv) {
		this.provv_EditManuale = edited_provv;
	}
	
	public void updateIsEstremoCompleto() {
		for(Provvedimento provv : provv_Merge) {
			provv.setEstremoCompleto(provv.isEstremoCompletoAuto() || provv.isEstremoCompletoEdit());
		}
	}

	
	public void updateIsEstremoCompletoMap() {
		
		for(Provvedimento provv : provv_Merge_Map.values()) {
			
			//provv.setEstremoCompleto(provv.isEstremoCompletoAuto() || provv.isEstremoCompletoEdit());
			provv.setEstremoCompleto(false);
		
			
			if(provv.getProvv_number()!=null && provv.getProvv_number().trim().length()>0)
				provv.setEstremoCompleto(true);
			
			if(provv.getNumRG()!=null && provv.getNumRG().trim().length()>0)
				provv.setEstremoCompleto(true);
			
			if(provv.getGiudici()!=null && provv.getGiudici().trim().length()>0)
				provv.setEstremoCompleto(true);
			
			if(!provv.isEstremoCompleto()) {
				if(provv.isEstremoCompletoAuto() || provv.isEstremoCompletoEdit())
					provv.setNota("FIXME - estremoCompleto");
			}
			
					
		}
		
	}
	
	
	public void normalizeLocalitaUfficio() {
		

				
		for(Provvedimento provv : provv_Merge_Map.values()) {
			
			
			String geoName = provv.getGeo_name();
			geoName = Util.normalizeLocalitaUfficio(geoName);
			provv.setGeo_name(geoName);
					
		}
		
	}
	
	
	
	
	public void resetIsEditManualeMap() {
		
		for(Provvedimento provv : provv_Merge_Map.values()) {
			
			provv.setEditManuale(false);
						
		}
		
	}
	
	public void forceAREAbySection() {
		
		for(Provvedimento provv : provv_Merge_Map.values()) {
			
			
			if(provv.getCourt_section().trim().equalsIgnoreCase("L") || provv.getCourt_section().trim().equalsIgnoreCase("lav."))
				provv.setGuessed_subject("LAVORO");
			
			if(provv.getCourt_section().trim().equalsIgnoreCase("fallimentare"))
				provv.setGuessed_subject("FALLIMENTARE");
			
			
			if(provv.getGuessed_subject().trim().equalsIgnoreCase("INDUSTRIALE") || provv.getGuessed_subject().trim().equalsIgnoreCase("IMPRESA"))
				provv.setGuessed_subject("CIVILE");
			
				
		}
		
	}
	
	
	public void removeScartatiFromProvvedimentiMap(List<String> scartati) {
		
		for(String idScartato : scartati) {	
			if(provv_Merge_Map.get(idScartato)!=null) {
				provv_Merge_Map.remove(idScartato);
			}else {
				System.err.println("+++WARNING Provv da SCARTARE "+idScartato+" NON TROVATO in MAPPA");
			}	
		}
	}
	
	
	
	public void flagProvvedimentoConsegnato(List<String> consegnati) {
		
		
		for(String itemConsegnato : consegnati) {
			String idConsegnato = itemConsegnato.split("\t")[1];
			String dataConsegna = itemConsegnato.split("\t")[0];

			if(provv_Merge_Map.get(idConsegnato)!=null) {
				provv_Merge_Map.get(idConsegnato).setProvvedimentoConsegnato(true);
				provv_Merge_Map.get(idConsegnato).setDataConsegnaProvvedimento(dataConsegna);
			}else {
				System.err.println("+++WARNING Provv CONSEGNATO "+idConsegnato+" NON TROVATO in MAPPA");
			}	
		}	
	}
	
	
	public void flagProvvedimentoTrovato(List<String> risposte) {
		
		// risposte.add(distretto+"\t"+idProvv+"\t"+replied+"\t"+path+"\t"+nota);
		
		for(String itemRisposta : risposte) {
			String idTrovato = itemRisposta.split("\t")[1];
			String isTrovato = itemRisposta.split("\t")[2];
			String localFilePath = itemRisposta.split("\t")[3];
			String notaRisposta ="";
			if(itemRisposta.split("\t").length>4)
				notaRisposta = itemRisposta.split("\t")[4];
			

			if(provv_Merge_Map.get(idTrovato)!=null) {
				provv_Merge_Map.get(idTrovato).setProvvedimentoTrovato(isTrovato);
				if(isTrovato.startsWith("TROVATO"))
					provv_Merge_Map.get(idTrovato).setLocalFilePath(localFilePath);
				if(notaRisposta.trim().length()>1)
					provv_Merge_Map.get(idTrovato).setNotaRispostaRichiesta(notaRisposta);
			}else {
				System.err.println("+++WARNING Provv TROVATO "+idTrovato+" NON TROVATO in MAPPA");
			}	
		}	
	}
	
	
	public void generaTestoEstremoUniformeMap() {
		
		for(Provvedimento provv : provv_Merge_Map.values()) {
			if(provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST."))
				provv.setEstremoUniforme(provv.getEstremoText());
			else {
				provv.setEstremoUniforme(Util.getEstremoUniforme(provv));
			}
		}
		
	}
	
	
	
	private boolean isProvvinDeJureCommentata(Provvedimento find_provv) {

		String titolo_contrib = find_provv.getFonti().get(0).getTitoloContributo().trim(); 
		for(Provvedimento provv : provv_deJure_Giur_Comm) {
			
			int matchratio = FuzzySearch.ratio(titolo_contrib, provv.getFonti().get(0).getTitoloContributo().trim());
			
			if(matchratio>90 && (provv.getProvv_date().equalsIgnoreCase(find_provv.getProvv_date()))) {

				//				if(!provv.getProvv_date().equalsIgnoreCase(find_provv.getProvv_date())) {
				//					System.err.println(provv.getEstremoText()+" -- "+find_provv.getEstremoText());
				//					System.err.println(provv.getProvv_date()+" -- "+find_provv.getProvv_date());
				//
				//				}

				return true;
			}
		}
		return false;
	}
	
	
	private boolean isProvvinDeJureUnique(Provvedimento find_provv) {

		String titolo_contrib = find_provv.getFonti().get(0).getTitoloContributo().trim(); 
		for(Provvedimento provv : provv_deJureUnique) {
			
			int matchratio = FuzzySearch.ratio(titolo_contrib, provv.getFonti().get(0).getTitoloContributo().trim());
			
			if(matchratio>90 && (provv.getProvv_date().equalsIgnoreCase(find_provv.getProvv_date()))) {

				//				if(!provv.getProvv_date().equalsIgnoreCase(find_provv.getProvv_date())) {
				//					System.err.println(provv.getEstremoText()+" -- "+find_provv.getEstremoText());
				//					System.err.println(provv.getProvv_date()+" -- "+find_provv.getProvv_date());
				//
				//				}

				return true;
			}
		}
		return false;
	}

	
	private Provvedimento isProvvinDeJureUnique1N(Provvedimento find_provv) {

		for(Provvedimento provv : provv_deJureUnique1N) {
			
			if(provv.getEstremoText().trim().equalsIgnoreCase(find_provv.getEstremoText().trim())) {
				return provv;
			}
			
			
		}
		return null;
	}
	
	
	
	private HashMap<String,Provvedimento> arrayMergeProvvToMap(){
		
		for(Provvedimento provv : provv_Merge) {
			
			if(!provv_Merge_Map.containsKey(provv.getPersistentID()))
				provv_Merge_Map.put(provv.getPersistentID(), provv);
			else
				System.err.println("*** WARNING - duplicate ID "+provv.getPersistentID()+" in arrayMergeProvvToMap");
			
		}
		System.err.println("*** PROVV_MERGE_MAP SIZE  "+provv_Merge_Map.size());

		return provv_Merge_Map;
	}
	
	
	public void addSelezioneLexadoc(HashMap<String,Provvedimento> selezione) {

		for(String provvId : selezione.keySet()) {
			
			if(!provv_Merge_Map.containsKey(provvId))
				provv_Merge_Map.put(provvId, selezione.get(provvId));
			else
				System.err.println("*** WARNING - MERGE SELEZIONE LEXADOC duplicate ID "+provvId+" in mergeMap");
			
		}
		System.err.println("*** PROVV_MERGE_MAP SIZE  "+provv_Merge_Map.size());

	}
	
	public void addProvvedimentiCOST(HashMap<String,Provvedimento> selezione) {

		for(String provvId : selezione.keySet()) {
			
			if(!provv_Merge_Map.containsKey(provvId))
				provv_Merge_Map.put(provvId, selezione.get(provvId));
			else
				System.err.println("*** WARNING - MERGE PROVVEDIMENTI COST duplicate ID "+provvId+" in mergeMap");
			
		}
		System.err.println("*** PROVV_MERGE_MAP SIZE  "+provv_Merge_Map.size());

	}
	
	
	private void printSameAS(Provvedimento provv) {
		System.err.println("ID: "+provv.getPersistentID()+
				" tipoDoc:"+provv.getDocType()+
				" area "+provv.getGuessed_subject()+
				/*" authority: "+provv_subject.getAuthority()+*/
				" number: "+provv.getProvv_number()+
				" sezione: "+provv.getCourt_section()+
				" num_RG: "+provv.getNumRG()+
				" giudici: "+provv.getGiudici()+
				" parti: "+provv.getParti()+
				" estremo: "+provv.getEstremoText()
				);
	}
	
	
	private void clean_Merge_Map(String SAME_AS_PATH) {


		File same_as_TSV = new File(SAME_AS_PATH);
		if(!same_as_TSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+same_as_TSV);
		}


		String line  = null;

		try{
			BufferedReader reader = new BufferedReader( new FileReader(same_as_TSV));

			while( ( line = reader.readLine() ) != null) {

				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0) {
					System.err.println("WARNING length 0 "+line);
					continue;
				}

				line = line.replaceAll("( )+"," ").trim(); 
				String[] fields = line.split("\t");
				String subject = fields[0].trim();

				Provvedimento provv_subject = provv_Merge_Map.get(subject);
				if(provv_subject!=null) {
					for(int k=1; k<fields.length;k++) {
						Provvedimento provv_duplicate = provv_Merge_Map.get(fields[k].trim());
						if(provv_duplicate!=null) {
							provv_Merge_Map.remove(provv_duplicate.getPersistentID());
						}
					}
				}
			}

		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	public void mergeSAME_AS(String SAME_AS_PATH) {
		
		
		// TRASFORMA Array di provvedimenti in mappa id:Provvedimento
		arrayMergeProvvToMap();
		
		File same_as_TSV = new File(SAME_AS_PATH);
		if(!same_as_TSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+same_as_TSV);
		}
		
	
		String line  = null;
		int i=0;

		try{
			BufferedReader reader = new BufferedReader( new FileReader(same_as_TSV));
					
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;

				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0) {
					System.err.println("WARNING length 0 "+line);
					continue;
				}
				
				// SKIP COMMENTS
				if(line.startsWith("#")) {
					continue;
				}
			
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				String[] fields = line.split("\t");
				String subject = fields[0].trim();
				
				Provvedimento provv_subject = provv_Merge_Map.get(subject);
				
//				if(provv_subject!=null) {
//					printSameAS(provv_subject);
//				}else {
//					System.err.println("+++++ ERROR cannot find provv_subject "+subject);
//				}
				
				ArrayList<Provvedimento> same_as_object = new ArrayList<Provvedimento>();
				for(int k=1; k<fields.length;k++) {
					Provvedimento provv_duplicate = provv_Merge_Map.get(fields[k].trim());
					if(provv_duplicate!=null) {
						//printSameAS(provv_duplicate);
						same_as_object.add(provv_duplicate);
					}else {
						System.err.println("+++++ ERROR cannot find provv_duplicate "+fields[k]);
						// NON è un problema - sono spariti precedentemente accorpando fonte DeJure 1:N oppure accorpando con provv Dogi
					}
				}
				
				if(provv_subject!=null & same_as_object.size()>=1) {
					provv_subject = merge_same_as_provv(provv_subject,same_as_object);
					printSameAS(provv_subject);
					System.err.println();

					// TODO  
					// 1) fai il merge delle fonti fra provv_subject e lista dei same_As (in prima approx posso prendere tutte le fonti, anche duplic)
					// 2) ?? merge delle provenance?? degli attributi di spoglio ??
					// 3) poi elimina i same_as dalla mappa e aggiorna provv_subject
					
				}
				
				
				System.err.println("**");
							
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		// elimina i provvedimenti duplicati
		clean_Merge_Map(SAME_AS_PATH);
		
		System.err.println(i+"  ROWS FOUND IN SAME_AS");
		
	}
	
	
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public  <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
	    Comparator<K> valueComparator =  new Comparator<K>() {
	        public int compare(K k1, K k2) {
	            int compare = map.get(k2).compareTo(map.get(k1));
	            if (compare == 0) return 1;
	            else return compare;
	        }
	    };
	    Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
	    sortedByValues.putAll(map);
	    //return sortedByValues;
	    return new LinkedHashMap<K,V>(sortedByValues); 
	}
	
	
	
	
	private Map<String,Integer> createFreqValues(ArrayList<String> values){
		
		Map<String,Integer> freqTable = new HashMap<String,Integer>();
		for(String value:values) {
			if(value.length()>0) {
				
				Integer occurences = freqTable.get(value);
				if(occurences == null) {
					freqTable.put(value, new Integer(1));
				} else {
					freqTable.put(value, new Integer(occurences+1));
				}		
			}
		}
		return sortByValues(freqTable);
	}
	
	
	
	
	private String averageValuesGiudiciParti(ArrayList<String> values) {
		
		// PRENDE SEMPLICEMENTE LA STRINGA PIU LUNGA
		
		String selected="";
		
		for(String value:values) {
			if(value.trim().length()>0 && value.length()>selected.trim().length())
				selected = value;		
		}
		return selected;
			
	}
	
	
	
	
	private String averageValuesTipoDocAreaNumber(ArrayList<String> values) {
		
		// lista ordinata per frequenza di valori unici (escluso vuoto)
		Map<String,Integer> freqTable = createFreqValues(values); 
		
		// se mappa vuota allora value - vuoto
		// sennò prendi il più frequente (e non vuoto)
		// se ci sono più valori a pari merito di freq prendili tutti
		// poi dipende dai casi; per le parti e i giudici prendi la stringa più lunga
		 
		// PRENDI IL PRIMO NON VUOTO ed eventualmente la Stringa più lunga
		
		String selected="";
		
		if(freqTable.keySet().size()==0)
			return "";
		
		int maxFreq = 0;
		int freq = 0;
		String currentValue = "";
		Iterator<String> freqTableIter = freqTable.keySet().iterator();
		while(freqTableIter.hasNext()) {
			currentValue = freqTableIter.next().toString();
			freq = freqTable.get(currentValue);
			if(freq > maxFreq) {
				selected = currentValue;
				maxFreq = freq;
			}else if(freq == maxFreq) {
				selected = selected+"#"+currentValue;
			}else  // if(freq<maxFreq)
			    return selected;
		}
	
		return selected;
		

	}
	
	private String normalizeSezione(String sezione) {
		sezione = sezione.trim();
		if(sezione.equalsIgnoreCase("I"))
			return "1";
		if(sezione.equalsIgnoreCase("II"))
			return "2";
		if(sezione.equalsIgnoreCase("III"))
			return "3";
		if(sezione.equalsIgnoreCase("IV"))
			return "4";
		if(sezione.equalsIgnoreCase("V"))
			return "5";
		if(sezione.equalsIgnoreCase("VI"))
			return "6";
		if(sezione.equalsIgnoreCase("VII"))
			return "7";
		if(sezione.equalsIgnoreCase("VIII"))
			return "8";
		if(sezione.equalsIgnoreCase("IX"))
			return "9";
		if(sezione.equalsIgnoreCase("X"))
			return "10";
		if(sezione.toLowerCase().startsWith("lav"))
			return "L";
		return sezione;
	}
	
	private Provvedimento  merge_same_as_provv(Provvedimento provv_subject,ArrayList<Provvedimento> same_as_object) {
		
		System.err.println();
		System.err.println("+++++++++++ "+provv_subject.getPersistentID());
		// aggiorna le feature del subject con tutte quelle (degli) object
		
		ArrayList<String> tipoDocList = new ArrayList<String>();
		ArrayList<String> areaList = new ArrayList<String>();
		ArrayList<String> numberList = new ArrayList<String>();
		ArrayList<String> sezioneList = new ArrayList<String>();
		ArrayList<String> giudiciList = new ArrayList<String>();
		ArrayList<String> partiList = new ArrayList<String>();
		ArrayList<String> numRGList = new ArrayList<String>();


		
		String tipoDoc = provv_subject.getDocType();
		tipoDocList.add(tipoDoc);
		System.err.print("TIPO_DOC "+tipoDoc +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getDocType().trim().length()>0) {
				tipoDocList.add(provv.getDocType());
				System.err.print(provv.getDocType()+"  ");
			}
		}
		String selectedTipoDoc = averageValuesTipoDocAreaNumber(tipoDocList);
		provv_subject.setDocType(selectedTipoDoc);
		System.err.println("*"+selectedTipoDoc);
		
		
		String provenance = provv_subject.getProvenance();
		System.err.print("PROVENANCE "+provenance +"  --->  ");
		String selectedProvenance = provenance+"-SAME_AS";
		for(Provvedimento provv:same_as_object) {
			if(provv.getProvenance().trim().length()>0) {
				if(provv.getProvenance().indexOf("-")!=-1)
					selectedProvenance=selectedProvenance+"-"+provv.getProvenance().substring(0, provv.getProvenance().indexOf("-"));
				else
					selectedProvenance=selectedProvenance+"-"+provv.getProvenance();
			}
		}
		if(selectedProvenance.endsWith("-"))
			selectedProvenance=selectedProvenance.substring(0,selectedProvenance.length()-1);
		provv_subject.setProvenance(selectedProvenance);
		System.err.println("*"+selectedProvenance);
		
		
		boolean isEditManuale = provv_subject.isEditManuale();
		boolean isEsitoEdit = provv_subject.isEsitoEdit();
		
		for(Provvedimento provv:same_as_object) {
			if(provv.isEditManuale()) 
				isEditManuale = provv.isEditManuale();
			if(provv.isEsitoEdit()) 
				isEsitoEdit = provv.isEsitoEdit();
				
		}
		provv_subject.setEditManuale(isEditManuale);
		provv_subject.setEsitoEdit(isEsitoEdit);

		
		
		String area = provv_subject.getGuessed_subject();
		if(!area.trim().equalsIgnoreCase("N.D."))
			areaList.add(area);
		System.err.print("AREA "+area +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getGuessed_subject().trim().length()>0 && !provv.getGuessed_subject().trim().equalsIgnoreCase("N.D.")) {
				System.err.print(provv.getGuessed_subject()+"  ");
				areaList.add(provv.getGuessed_subject());
			}
		}
		String selectedArea = averageValuesTipoDocAreaNumber(areaList);
		provv_subject.setGuessed_subject(selectedArea);
		System.err.println("*"+selectedArea);
		
		String number = provv_subject.getProvv_number();
		numberList.add(number);
		System.err.print("NUMERO "+number +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getProvv_number().trim().length()>0) {
				System.err.print(provv.getProvv_number()+"  ");
				numberList.add(provv.getProvv_number());
			}
		}
		String selectedNumber = averageValuesTipoDocAreaNumber(numberList);
		
		// FIXME    N.B - fix manuale ambiguità (numero sbagliato)
		if(provv_subject.getPersistentID().equalsIgnoreCase("IT:PA:TRB:2014:1852830552"))
			selectedNumber ="1173";
		
		provv_subject.setProvv_number(selectedNumber);
		System.err.println("*"+selectedNumber);


		
		
		String sezione = provv_subject.getCourt_section();
		sezioneList.add(normalizeSezione(sezione));
		System.err.print("SEZIONE "+sezione +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getCourt_section().trim().length()>0) {
				System.err.print(provv.getCourt_section()+"  ");
				sezioneList.add(normalizeSezione(provv.getCourt_section()));
			}
		}
		String selectedSezione = averageValuesTipoDocAreaNumber(sezioneList);
		provv_subject.setCourt_section(selectedSezione);
		System.err.println("*"+selectedSezione);

		
		
		String giudici = provv_subject.getGiudici();
		giudiciList.add(giudici);
		System.err.print("GIUDICI "+giudici +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getGiudici().trim().length()>0) {
				System.err.print(provv.getGiudici()+"  ");
				giudiciList.add(provv.getGiudici());
			}
		}
		String selectedGiudici = averageValuesGiudiciParti(giudiciList);
		provv_subject.setGiudici(selectedGiudici);
		System.err.println("*"+selectedGiudici);

		
		String parti = provv_subject.getParti();
		partiList.add(parti);
		System.err.print("PARTI "+parti +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getParti().trim().length()>0) {
				System.err.print(provv.getParti()+"  ");
				partiList.add(provv.getParti());
			}
		}
		String selectedParti = averageValuesGiudiciParti(partiList);
		provv_subject.setParti(selectedParti);
		System.err.println("*"+selectedParti);

		
		
		String numRG = provv_subject.getNumRG();
		numRGList.add(numRG);
		System.err.print("Num_RG "+numRG +"  --->  ");
		for(Provvedimento provv:same_as_object) {
			if(provv.getNumRG().trim().length()>0) {
				System.err.print(provv.getNumRG()+"  ");
				numRGList.add(provv.getNumRG());
			}
		}
		String selectedNumRG = averageValuesTipoDocAreaNumber(numRGList);
		provv_subject.setNumRG(selectedNumRG);
		System.err.println("*"+selectedNumRG);
		
		
		// MERGE URL PROVVEDIMENTO
		String selectedUrlProvv = provv_subject.getUrl();
		for(Provvedimento provv:same_as_object) {
			if(provv.getUrl().trim().length()>0) {
				if(!selectedUrlProvv.contains(provv.getUrl().trim()))
					selectedUrlProvv = selectedUrlProvv+" | "+provv.getUrl().trim();
			}
		}
		selectedUrlProvv=selectedUrlProvv.trim();
		if(selectedUrlProvv.startsWith("|"))
			selectedUrlProvv=selectedUrlProvv.substring(1).trim();
		if(selectedUrlProvv.endsWith("|"))
			selectedUrlProvv=selectedUrlProvv.substring(0,selectedUrlProvv.length()-1).trim();
		provv_subject.setUrl(selectedUrlProvv);
		System.err.println("*"+selectedUrlProvv);
		
		// FIXME 20/3 merge FONTI provvedimenti SAME_AS 
		// AGGIUNGO LE FONTI DI TUTTI I PROVVEDIMENTI SAME_AS (per il momento senza nessun filtraggio..)
		// Non vanno aggiunte le fonti duplicate.. 
		List<Fonte> previousFonti = provv_subject.getFonti();
		for(Provvedimento provv:same_as_object) {
			// SKIP provv_SUBJECT
			if(!provv.getPersistentID().trim().equals(provv_subject.getPersistentID().trim())) {
				for(Fonte f:provv.getFonti()) {
					// se la fonte f non è già fra fonti di provv_subject .. 
					if(isFonteDeJureinFontiDogi(f,provv_subject)==null)
						previousFonti.add(f);
				}
			}
		}
		provv_subject.setFonti(previousFonti);

		
		
//		for(Fonte fonteDeJure:provvDeJure.getFonti()) {
//
//			Fonte fonteDogi = isFonteDeJureinFontiDogi(fonteDeJure,provvDogi);
//			if(fonteDogi!=null)
//				fonteDogi.setUrl(fonteDeJure.getUrl());
//			else
//				provvDogi.addFonte(fonteDeJure);
//
//		}
//		// poi aggiungi nel provv Dogi tutte le fonti DeJure che non ci sono già	
//		return provvDogi;
		
		
		return provv_subject;
		
		
	}
	
	public void mergeEditedInfoDeJure() {
		
		for(Provvedimento editedProvv:provv_Spogli) {
			Provvedimento found = findSpoglioinMergeList(editedProvv);
			// FLAG SPOGLIO
			if(found!=null) {
				found.setEditManuale(editedProvv.isEditManuale());
				found.setEsitoEdit(editedProvv.isEsitoEdit());
				found.setEstremoCompletoEdit(editedProvv.isEstremoCompletoEdit());
				found.setEstremoCompletoAuto(editedProvv.isEstremoCompletoAuto());


				if(editedProvv.isEditManuale()) {
					
					// se le edited info sono non vuote, rimpiazza le info esistenti	
					
					if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0)
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
					
					if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
					if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0)
						found.setGiudici(editedProvv.getGiudici());
					if(editedProvv.getParti()!=null && editedProvv.getParti().trim().length()>0)
						found.setParti(editedProvv.getParti());
					found.setNumRG(editedProvv.getNumRG());
					if(editedProvv.getCourt_section()!=null && editedProvv.getCourt_section().trim().length()>0)
						found.setCourt_section(editedProvv.getCourt_section());
					if(editedProvv.getProvv_number()!=null && editedProvv.getProvv_number().trim().length()>0)
						found.setProvv_number(editedProvv.getProvv_number());
					
//					System.err.println("BEFORE Estremo "+found.getEstremoText());
//					System.err.println("BEFORE TipoDoc "+found.getDocType());
//					System.err.println("BEFORE Area "+found.getLegalArea());
//					System.err.println("BEFORE Giudici "+found.getGiudici());
//					System.err.println("BEFORE Parti "+found.getParti());
//					System.err.println("BEFORE Registro "+found.getNumRG());
//					System.err.println("BEFORE NumeroProvv "+found.getProvv_number());
//					System.err.println("BEFORE Sezione "+found.getCourt_section());
//					System.err.println("AFTER Estremo "+editedProvv.getEstremoText());
//					System.err.println("AFTER TipoDoc "+editedProvv.getDocType());
//					System.err.println("AFTER Area "+editedProvv.getLegalArea());
//					System.err.println("AFTER Giudici "+editedProvv.getGiudici());
//					System.err.println("AFTER Parti "+editedProvv.getParti());
//					System.err.println("AFTER Registro "+editedProvv.getNumRG());
//					System.err.println("AFTER NumeroProvv "+editedProvv.getProvv_number());
//					System.err.println("AFTER Sezione "+editedProvv.getCourt_section());

					
					// aggiorna certe info (se vuote o diverse?) 
					// Tipodoc / Area / Giudici / Parti / NumRG / Numero / Sezione
				}
				
				
			}else {
				//System.err.println("++++ merge spogli: CANNOT FIND "+editedProvv.getEstremoText() +" in MERGE LIST ++++");
			}
			
		}
		
		
	}
	

	
	// merge edited INFO SPOGLI MANUALI (e.g. clean #) 
	// va fatto sulla mappa provv_merge_map
	public void mergeEditedInfoSpogliManuali() {
		
		for(Provvedimento editedProvv:provv_EditManuale) {
			
			
			Provvedimento found = provv_Merge_Map.get(editedProvv.getPersistentID().trim());
			
			if(found!=null) {
				
				found.setEditManuale(editedProvv.isEditManuale());
				found.setEstremoCompletoEdit(editedProvv.isEstremoCompletoEdit());
				found.setEstremoCompletoAuto(editedProvv.isEstremoCompletoAuto());
				
		
				// se le edited info sono non vuote, rimpiazza le info esistenti	
					
				if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0) {
					if(!encodeTipoDoc(editedProvv.getDocType()).equalsIgnoreCase(found.getDocType()))
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
				}
				
				if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
					if(!editedProvv.getGuessed_subject().equalsIgnoreCase(found.getGuessed_subject())) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
				}
				
				if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0) {
					if(!editedProvv.getGiudici().equalsIgnoreCase(found.getGiudici()))
						found.setGiudici(editedProvv.getGiudici());
				}
				
				if(editedProvv.getParti()!=null /*&& editedProvv.getParti().trim().length()>0*/) {
					if(!editedProvv.getParti().equalsIgnoreCase(found.getParti()))
						found.setParti(editedProvv.getParti());
				}
				
				found.setNumRG(editedProvv.getNumRG());
				
				if(editedProvv.getCourt_section()!=null && editedProvv.getCourt_section().trim().length()>0)
					found.setCourt_section(editedProvv.getCourt_section());
				
				//if(editedProvv.getProvv_number()!=null && editedProvv.getProvv_number().trim().length()>0)
					found.setProvv_number(editedProvv.getProvv_number());
				
				if(editedProvv.getProvv_date()!=null && editedProvv.getProvv_date().trim().length()>0) {
					if(!found.getProvv_date().trim().equalsIgnoreCase(editedProvv.getProvv_date().trim()))
						found.setProvv_date(editedProvv.getProvv_date().trim());
				}
				
				// provv_url
				if(editedProvv.getUrl()!=null && editedProvv.getUrl().trim().length()>0) {
					if(!found.getUrl().trim().equalsIgnoreCase(editedProvv.getUrl().trim()))
						found.setUrl(editedProvv.getUrl().trim());
				}
				
				if(editedProvv.getGeo_name()!=null && editedProvv.getGeo_name().trim().length()>0) {
					if(!found.getGeo_name().trim().equalsIgnoreCase(editedProvv.getGeo_name().trim()))
						found.setGeo_name(editedProvv.getGeo_name().trim());
				}
				
				
			}	
			else {
				System.err.println("**** WARNING provvedimento NOT FOUND -- "+editedProvv.getPersistentID());
			}
			
		}
	}
	
	private String[] getClassificazioniDefinitive(String vociClassificazione) {
		if(vociClassificazione.contains("#"))
			return vociClassificazione.split("#");
		else
			return new String[]{vociClassificazione};
	}
	
	
	
	public void mergeEditedInfoConsegneDefinitive() {

		for(Provvedimento editedProvv:provv_EditManuale) {


			Provvedimento found = provv_Merge_Map.get(editedProvv.getPersistentID().trim());

			if(found!=null) {
				
				// se le edited info sono non vuote, rimpiazza le info esistenti	

				if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0) {
					if(!encodeTipoDoc(editedProvv.getDocType()).equalsIgnoreCase(found.getDocType()))
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
				}

				if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
					if(!editedProvv.getGuessed_subject().equalsIgnoreCase(found.getGuessed_subject())) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
				}
	

				if(editedProvv.getParti()!=null /*&& editedProvv.getParti().trim().length()>0*/) {
					if(!editedProvv.getParti().equalsIgnoreCase(found.getParti()))
						found.setParti(editedProvv.getParti());
				}

				// LO SETTA ANCHE SE VUOTO (ovvero se è stato cancellato in fase di EDIT)
				
				found.setRestituireDefinitivo(editedProvv.isRestituireDefinitivo());
				
				found.setNumRG(editedProvv.getNumRG());
				
				found.setGiudici(editedProvv.getGiudici());

				found.setCourt_section(editedProvv.getCourt_section());
				
				found.setProvv_number(editedProvv.getProvv_number());
						
				found.setClassificazioniCSMFinal(editedProvv.getClassificazioniCSMFinal());
				
				found.setDataDecisione(editedProvv.getDataDecisione());
				
				found.setDataPubblicazione(editedProvv.getDataPubblicazione());
				
				found.setFilePath(editedProvv.getFilePath());
				
				if(editedProvv.getFonti()!=null && editedProvv.getFonti().size()>0) {
					if(editedProvv.getFonti().get(0)!=null && editedProvv.getFonti().get(0).getEstremoText().trim().length()==0) {
						String contrib_fonte = "DeJure, Giuffrè Francis Lefebvre";
						editedProvv.getFonti().get(0).setRivista(contrib_fonte);
						editedProvv.getFonti().get(0).setTitoloContributo("");
						editedProvv.getFonti().get(0).setEstremoText(contrib_fonte); 
						editedProvv.getFonti().get(0).setSistematici(new String[]{""});
						editedProvv.getFonti().get(0).setDescrittori(new String[]{""});

						ArrayList<Fonte> newFonti = new ArrayList<Fonte>();
						newFonti.add(editedProvv.getFonti().get(0));
						found.setFonti(newFonti);
					}

				}

				// SGANCIA FONTE E METTINE UNA VUOTA

				if(editedProvv.getProvv_date()!=null && editedProvv.getProvv_date().trim().length()>0) {
					if(!found.getProvv_date().trim().equalsIgnoreCase(editedProvv.getProvv_date().trim()))
						found.setProvv_date(editedProvv.getProvv_date().trim());
				}

				// provv_url
				if(editedProvv.getUrl()!=null && editedProvv.getUrl().trim().length()>0) {
					if(!found.getUrl().trim().equalsIgnoreCase(editedProvv.getUrl().trim()))
						found.setUrl(editedProvv.getUrl().trim());
				}

				if(editedProvv.getGeo_name()!=null && editedProvv.getGeo_name().trim().length()>0) {
					if(!found.getGeo_name().trim().equalsIgnoreCase(editedProvv.getGeo_name().trim()))
						found.setGeo_name(editedProvv.getGeo_name().trim());
				}


			}	
			else {
				System.err.println("+++++ WARNING provvedimento DEFINITIVO NOT FOUND in MERGE -- "+editedProvv.getPersistentID());
			}

		}
	}
	
	
	public void mergeEditedInfoOrdinanzeCOST() {
		
		for(Provvedimento editedProvv:provv_EditManuale) {
			
			
			Provvedimento found = provv_Merge_Map.get(editedProvv.getPersistentID().trim());
			
			if(found!=null) {
				
			
				
				if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
					if(!editedProvv.getGuessed_subject().equalsIgnoreCase(found.getGuessed_subject())) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
				}
				
				if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0) {
					if(!editedProvv.getGiudici().equalsIgnoreCase(found.getGiudici()))
						found.setGiudici(editedProvv.getGiudici());
				}
				
				
				
			}	
			else {
				System.err.println("**** WARNING provvedimento NOT FOUND -- "+editedProvv.getPersistentID());
			}
			
		}
	}
	
	public void mergeEditedInfoSpogliManualiLEXADOC() {

		for(Provvedimento editedProvv:provv_EditManuale) {

			
			boolean write = false; //editedProvv.getGuessed_subject().contains("PENALE"); 


			Provvedimento found = provv_Merge_Map.get(editedProvv.getPersistentID().trim());

			if(found!=null) {

				found.setEditManuale(true);
				

				// se le edited info sono non vuote, rimpiazza le info esistenti	

				if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0) {
					if(!encodeTipoDoc(editedProvv.getDocType()).equalsIgnoreCase(found.getDocType())) {
						if(write)
							System.out.println("TIPODOC"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getDocType()+"\t"+"AFTER:"+editedProvv.getDocType());
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
						
					}
					
				}

				if(editedProvv.getGuessed_subject()!=null && editedProvv.getGuessed_subject().trim().length()>0) {
					if(!editedProvv.getGuessed_subject().equalsIgnoreCase(found.getGuessed_subject())) {
						if(write)
							System.out.println("AREA"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getGuessed_subject()+"\t"+"AFTER:"+editedProvv.getGuessed_subject());
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
				}

				if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0) {
					if(!editedProvv.getGiudici().equalsIgnoreCase(found.getGiudici())) {
						if(write)
							System.out.println("GIUDICI"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getGiudici()+"\t"+"AFTER:"+editedProvv.getGiudici());
						found.setGiudici(editedProvv.getGiudici());
					}
				}

				if(editedProvv.getParti()!=null /*&& editedProvv.getParti().trim().length()>0*/) {
					if(!editedProvv.getParti().equalsIgnoreCase(found.getParti()))
						found.setParti(editedProvv.getParti());
				}

				found.setNumRG(editedProvv.getNumRG());

				if(editedProvv.getCourt_section()!=null && editedProvv.getCourt_section().trim().length()>0) {
					if(write)
						System.out.println("SEZ"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getCourt_section()+"\t"+"AFTER:"+editedProvv.getCourt_section());
					found.setCourt_section(editedProvv.getCourt_section());
				}

				if(editedProvv.getProvv_number()!=null && editedProvv.getProvv_number().trim().length()>0) {
					if(!editedProvv.getProvv_number().equalsIgnoreCase(found.getProvv_number())) {
						if(write)
							System.out.println("NUM"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getProvv_number()+"\t"+"AFTER:"+editedProvv.getProvv_number());
						found.setProvv_number(editedProvv.getProvv_number());
					}
				}
				
				// WARNING nei provv in mappa le classificazioniCSM non ce le ho messe
				// LE HO MESSE IN DEJURE CLASSE e poi fatto tutto il merge
				if(editedProvv.getClassificazioniCSM()!=null && editedProvv.getClassificazioniCSM().trim().length()>0){
					if(!editedProvv.getClassificazioniCSM().equalsIgnoreCase(found.getClassificazioniCSM())) {
						if(write)
							System.out.println("CLASSI"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getClassificazioniCSM()+"\t"+"AFTER:"+editedProvv.getClassificazioniCSM());
						found.setClassificazioniCSM(editedProvv.getClassificazioniCSM());
					}
					// QUI SETTA FRA LE CLASSIFICAZIONI DEFINITIVE LE CLASSI CSM CIVILI 
					// e LE CLASSI PENALI DEJure DEGLI SPOGLI LEXADOC 
					found.setClassificazioniCSMFinal(getClassificazioniDefinitive(editedProvv.getClassificazioniCSM()));
				}
				

				if(editedProvv.getProvv_date()!=null && editedProvv.getProvv_date().trim().length()>0) {
					if(!found.getProvv_date().trim().equalsIgnoreCase(editedProvv.getProvv_date().trim())) {
						if(write)
							System.out.println("DATE"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getProvv_date()+"\t"+"AFTER:"+editedProvv.getProvv_date());
						found.setProvv_date(editedProvv.getProvv_date().trim());
					}
				}

				// provv_url
				if(editedProvv.getUrl()!=null && editedProvv.getUrl().trim().length()>0) {
					if(!found.getUrl().trim().equalsIgnoreCase(editedProvv.getUrl().trim())) {
						if(write)
							System.out.println("URL"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getUrl()+"\t"+"AFTER:"+editedProvv.getUrl());
						found.setUrl(editedProvv.getUrl().trim());
					}
				}

				if(editedProvv.getGeo_name()!=null && editedProvv.getGeo_name().trim().length()>0) {
					if(!found.getGeo_name().trim().equalsIgnoreCase(editedProvv.getGeo_name().trim())) {
						if(write)
							System.out.println("GEO"+"\t"+found.getPersistentID()+"\t"+"BEFORE:"+found.getGeo_name()+"\t"+"AFTER:"+editedProvv.getGeo_name());
						found.setGeo_name(editedProvv.getGeo_name().trim());
					}
				}


			}	
			else {
				System.err.println("**** WARNING provvedimento LEXADOC NOT FOUND in MERGE -- "+editedProvv.getPersistentID());
			}

		}
	}
	
	
	// merge edited INFO WKI va fatto sulla mappa provv_merge_map
	// merge delle info da Spoglio WKI in provvedimenti_merge (provv_Merge_Map ?)
	public void mergeEditedInfoWKI() {
		
		for(Provvedimento editedProvv:provv_Spogli_WKI) {
			Provvedimento found = findSpoglioWKIinMergeMap(editedProvv);
			// FLAG SPOGLIO
			if(found!=null) {
				found.setEditManuale(editedProvv.isEditManuale());
				found.setEstremoCompletoEdit(editedProvv.isEstremoCompletoEdit());
				found.setEstremoCompletoAuto(editedProvv.isEstremoCompletoAuto());


				if(editedProvv.isEditManuale()) {
					
					// se le edited info sono non vuote, rimpiazza le info esistenti	
					
					if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0)
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
					
					if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
					if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0)
						found.setGiudici(editedProvv.getGiudici());
					if(editedProvv.getParti()!=null && editedProvv.getParti().trim().length()>0)
						found.setParti(editedProvv.getParti());
					found.setNumRG(editedProvv.getNumRG());
					if(editedProvv.getCourt_section()!=null && editedProvv.getCourt_section().trim().length()>0)
						found.setCourt_section(editedProvv.getCourt_section());
					if(editedProvv.getProvv_number()!=null && editedProvv.getProvv_number().trim().length()>0)
						found.setProvv_number(editedProvv.getProvv_number());
					if(editedProvv.getProvv_date()!=null && editedProvv.getProvv_date().trim().length()>0) {
						if(!found.getProvv_date().trim().equalsIgnoreCase(editedProvv.getProvv_date().trim()))
							found.setProvv_date(editedProvv.getProvv_date().trim());
					}
					
//					System.err.println("BEFORE Estremo "+found.getEstremoText());
//					System.err.println("BEFORE TipoDoc "+found.getDocType());
//					System.err.println("BEFORE Area "+found.getLegalArea());
//					System.err.println("BEFORE Giudici "+found.getGiudici());
//					System.err.println("BEFORE Parti "+found.getParti());
//					System.err.println("BEFORE Registro "+found.getNumRG());
//					System.err.println("BEFORE NumeroProvv "+found.getProvv_number());
//					System.err.println("BEFORE Sezione "+found.getCourt_section());
//					System.err.println("AFTER Estremo "+editedProvv.getEstremoText());
//					System.err.println("AFTER TipoDoc "+editedProvv.getDocType());
//					System.err.println("AFTER Area "+editedProvv.getLegalArea());
//					System.err.println("AFTER Giudici "+editedProvv.getGiudici());
//					System.err.println("AFTER Parti "+editedProvv.getParti());
//					System.err.println("AFTER Registro "+editedProvv.getNumRG());
//					System.err.println("AFTER NumeroProvv "+editedProvv.getProvv_number());
//					System.err.println("AFTER Sezione "+editedProvv.getCourt_section());

				}
				
				
			}else {
				//System.err.println("++++ merge spogli: CANNOT FIND "+editedProvv.getEstremoText() +" in MERGE LIST ++++");
			}
			
		}
		
		
	}
	
	private String encodeTipoDoc(String tipoDoc) {
		
		String ret ="";
		switch(tipoDoc.toLowerCase()) {
		case "sentenza": 
			ret="JUDGMENT";
			break;
		case "ordinanza": 
			ret="ORDER";
			break;
		case "decreto":
			ret="DECREE";
			break;
		default:
			ret = tipoDoc;
	}
		
		return ret;
	}
	
	
	// ricerca Spoglio in MERGE-LIST
	private Provvedimento findSpoglioinMergeList(Provvedimento findMe) {

		// se findME ha persistentID trova il provvedimento tramite id.. 
		// (vale per lo spoglio DeJureNAS_TRIB)
		if(findMe.getPersistentID()!=null && findMe.getPersistentID().trim().length()>0) {
			for(Provvedimento provv : provv_Merge) {
				if(provv.getPersistentID().trim().equalsIgnoreCase(findMe.getPersistentID().trim())){
					return provv;
				}
			}
			// FIXME indaga provvedimenti rimossi per duplicazione
			System.err.println("+++++ MERGE SPOGLI by ID : cannot find  "+findMe.getPersistentID() );
			return null;

		}else {
			String find_authority = findMe.getAuthority()!=null?findMe.getAuthority():"";
			//MATCH SU
			// 1) titolo contributo close match (una delle fonti WKI con una delle fonti Dogi)
			// 2) Authority Provvedimento (Tribunale / Corte Appello etc.)
			// 3) Anno Provvedimento
			// 4) Città

			int threshold = 80;

			for(Provvedimento provv : provv_Merge) {

				String authority = provv.getAuthority()!=null?provv.getAuthority():"";

				int matchratioTitolo = fuzzyMatchTitoliContribWKIvsDogi(findMe.getFonti(), provv.getFonti());

				if(matchratioTitolo >threshold/*titoloContribDogi.toLowerCase().indexOf(find_titolo_contrib.toLowerCase())!=-1*/
						&& (provv.getProvv_year().equalsIgnoreCase(findMe.getProvv_year()))
						&& (provv.getGeo_name().equalsIgnoreCase(findMe.getGeo_name()))
						&& (authority.equalsIgnoreCase(find_authority))) {

					return provv;
				}else {
					//System.err.println("NON matching match ratio "+matchratioTitolo);
				}
			}
			return null;
		}
	}

	
		// ricerca Spoglio in MERGE-MAP
		private Provvedimento findSpoglioWKIinMergeMap(Provvedimento findMe) {

			if(findMe.getPersistentID()!=null && findMe.getPersistentID().trim().length()>0) {
				return provv_Merge_Map.get(findMe.getPersistentID().trim());
			
			}
			return null;
		}
	
	
	
	public void mergeProvvedimentiALL() {

				// provv_deJure contiene tutti e 4 i tipi : commentata/NAS  Trib/App
		
		
				// FIXME  DeJure ha ancora 40 duplicati
				//		DEJURE_COMM_TRIB	20
				//		DEJURE_NAS_TRIB	14
				//		DEJURE_NAS_APP	4
				//		DEJURE_COMM_APP	2

				////////////
				// 1)
				// separa provv_deJure NAS e GiurispComm
				////////////
		
		
				for(Provvedimento provv : provv_deJure) {
					if(provv.getProvenance().contains("DEJURE_COMM"))
						provv_deJure_Giur_Comm.add(provv);
					else if(provv.getProvenance().contains("DEJURE_NAS"))
						provv_deJure_NAS.add(provv);
				}
				
				System.out.println("deJure: "+provv_deJure.size()+" commentata: "+provv_deJure_Giur_Comm.size()+" note a sent: "+provv_deJure_NAS.size());
		
				
				////////////
				// 2)
				// provv_deJureUnique = [note a sentenza]-commentata + commentata
				///////////
				
				for(Provvedimento provv : provv_deJure_NAS) {
					if(!isProvvinDeJureCommentata(provv)) {
						if(!isProvvinDeJureUnique(provv)) {
							provv_deJureUnique.add(provv);
						}
					}
					//se non è fra le commentate aggiungila alla lista		
				}
				for(Provvedimento provv : provv_deJure_Giur_Comm) {
					if(!isProvvinDeJureUnique(provv)) {
						provv_deJureUnique.add(provv);
					}
				}
				System.out.println("deJureUnique: "+provv_deJureUnique.size());
		
				
				/////////////////////////////
				// FIXME 
				// 2.5)
				// 8/3 
				// da provvedimenti DeJure 1:1 a 1:N
				// se un provvedimento DeJure è anche in Dogi prendi solo quello Dogi e arricchiscilo con info DeJure
				// elimina DeJure dalla lista
				/////////////////////////////
				
				int duplicated=0;
				for(Provvedimento provv : provv_deJureUnique) {
					Provvedimento foundProvv = isProvvinDeJureUnique1N(provv);
					if(foundProvv==null) {
						provv_deJureUnique1N.add(provv);
					}else {
						foundProvv.getFonti().add(provv.getFonti().get(0));
						duplicated++;
						System.err.println("+++++++++++ Duplicated "+provv.getPersistentID()+" DEJURE_PROVV removed ");
					}					
				}
				
				System.err.println("+++++++++++ "+duplicated+"  DEJURE_PROVV removed --> 1:N");
				
				/////////////////////////////
				// 3)
				// se un provvedimento DeJure è anche in Dogi prendi solo quello Dogi e arricchiscilo con info DeJure
				// elimina DeJure dalla lista
				/////////////////////////////
				
				System.out.println("provv DOGI before: "+provv_Dogi.size());
				Provvedimento foundInDogi =null;
				for(Provvedimento provv : provv_deJureUnique1N) {
					// FIXME 1-N
					if((foundInDogi=isProvvDeJureinDogiList(provv))!=null) {
						
						// !!! MERGE CON INFO PROVVEDIMENTO DOGI
						// Update provenance ?? 
						// copia: linkProvvedimento
						// aggiorna Provenance
						// aggiungi sezione se non c'è
						// copia: linkContributo in Fonte
						foundInDogi.setUrl(provv.getUrl());
						if(foundInDogi.getCourt_section()==null || foundInDogi.getCourt_section().trim().length()==0)
							foundInDogi.setCourt_section(provv.getCourt_section());
						
						
						// UPDATE PROVENANCE
						if(!foundInDogi.getProvenance().contains(provv.getProvenance()))
							foundInDogi.setProvenance(foundInDogi.getProvenance()+"-"+provv.getProvenance());
						
						// FIXME 1-N
						foundInDogi=mergeFontiDogiDeJure(provv,foundInDogi);
						
						System.err.println("+++++++++++ "+provv.getPersistentID()+"  DEJURE_PROVV removed --> found in Dogi");

						
					}else {
						// provvedimento DeJure NON in Dogi
						residual_deJure.add(provv);
					}
				}	
			


		////////////
		// 4)
		// se un provvedimento WKI è anche in Dogi prendi solo quello Dogi e arricchiscilo con info WKI
		// elimina WKI dalla lista		
		////////////

		Provvedimento found_WKI_InDogi =null;
		for(Provvedimento provv : provv_WKI) {
			if((found_WKI_InDogi=isProvvWKIinDogiList(provv))!=null) {

				// !!! MERGE CON INFO PROVVEDIMENTO DOGI
				// Update provenance ?? 
				// copia: linkProvvedimento
				// aggiorna Provenance
				// aggiungi Parti
				// aggiungi sezione se non c'è
				// copia: linkContributo in Fonte
				if(found_WKI_InDogi.getUrl()==null || found_WKI_InDogi.getUrl().trim().length()==0) {
					found_WKI_InDogi.setUrl(provv.getUrl());
				}
				found_WKI_InDogi.setParti(provv.getParti());
				
				// UPDATE SEZIONE
				if(found_WKI_InDogi.getCourt_section()==null || found_WKI_InDogi.getCourt_section().trim().length()==0)
					found_WKI_InDogi.setCourt_section(provv.getCourt_section());
				
				// UPDATE PROVENANCE
				if(!found_WKI_InDogi.getProvenance().contains(provv.getProvenance()))
					found_WKI_InDogi.setProvenance(found_WKI_InDogi.getProvenance()+"-"+provv.getProvenance());

				found_WKI_InDogi=mergeFontiDogiWKI(provv,found_WKI_InDogi);

				//				LOG - Matching DeJure - Dogi				
				//				System.err.println("FIND WKI in DOGI - match ");
				//				System.err.println("DOGI:");
				//				found_WKI_InDogi.printGennusoCSV_1_3();
				//				System.err.println("WKI:");
				//				System.err.print("\t");
				//				provv.printGennusoCSV_1_3();
				//				System.err.println("");

			
			}else {
				residual_WKI.add(provv);
			}
		}	


		//////////////////////////////////////////////////////////////////
		//
		//
		//////////////////////////////////////////////////////////////////

		System.out.println("provv DeJure before: "+provv_deJure.size());
		System.out.println("provv WKI before: "+provv_WKI.size());
		System.out.println("provv DOGI after ALL: "+provv_Dogi.size());
		System.out.println("residual WKI after ALL: "+residual_WKI.size());
		System.out.println("residual Dejure after ALL: "+residual_deJure.size());



		for(Provvedimento provv : provv_Dogi) {
			provv_Merge.add(provv);
		}
		// aggiungi i restanti WKI
		for(Provvedimento provv : residual_WKI) {
			provv_Merge.add(provv);
		}

		for(Provvedimento provv : residual_deJure) {
			provv_Merge.add(provv);
		}

		System.out.println("MERGE provv after ALL: "+provv_Merge.size());
		System.out.println();


	}

	
	
	

	private Provvedimento mergeFontiDogiDeJure(Provvedimento provvDeJure, Provvedimento provvDogi) {
		
		for(Fonte fonteDeJure:provvDeJure.getFonti()) {

			Fonte fonteDogi = isFonteDeJureinFontiDogi(fonteDeJure,provvDogi);
			if(fonteDogi!=null)
				fonteDogi.setUrl(fonteDeJure.getUrl());
			else
				provvDogi.addFonte(fonteDeJure);

		}
		// poi aggiungi nel provv Dogi tutte le fonti DeJure che non ci sono già	
		return provvDogi;
		
		
// 		ERA: versione 1:1
//		for(Fonte fonte:provvDogi.getFonti()) {
//			String titoloDeJure = provvDeJure.getFonti().get(0).getTitoloContributo();
//			int matchratio = FuzzySearch.ratio(titoloDeJure.toLowerCase(), fonte.getTitoloContributo().toLowerCase());
//
//			if(matchratio>90) {
//				fonte.setUrl(provvDeJure.getFonti().get(0).getUrl());
//				fonte.setDataFascicolo(provvDeJure.getFonti().get(0).getDataFascicolo());
//				break;
//			}
//
//		}
//		return provvDogi;
		
	}





	private Provvedimento mergeFontiDogiWKI(Provvedimento provvWKI, Provvedimento provvDogi) {

		// dovrebbe prendersi anche tutte le altre fonti di WKI che non ci sono già


		for(Fonte fonteWKI:provvWKI.getFonti()) {

			Fonte fonteDogi = isFonteWKIinFontiDogi(fonteWKI,provvDogi);
			if(fonteDogi!=null)
				fonteDogi.setUrl(fonteWKI.getUrl());
			else
				provvDogi.addFonte(fonteWKI);

		}
		// poi aggiungi nel provv Dogi tutte le fonti WKI che non ci sono già	
		return provvDogi;
	}


	private Fonte isFonteWKIinFontiDogi(Fonte fonteWKI, Provvedimento provvDogi) {

		for(Fonte fonte:provvDogi.getFonti()) {

			String titoloWKI = fonteWKI.getTitoloContributo();
			int matchratio = FuzzySearch.ratio(titoloWKI.toLowerCase(), fonte.getTitoloContributo().toLowerCase());
			if(titoloWKI.toLowerCase().contains(fonte.getTitoloContributo().toLowerCase()))
				matchratio+=10;
			if(matchratio>80) 
				return fonte;

		}
		return null;
	}

	
	private Fonte isFonteDeJureinFontiDogi(Fonte fonteDeJure, Provvedimento provvDogi) {
		
		for(Fonte fonte:provvDogi.getFonti()) {

			String titoloDeJure = fonteDeJure.getTitoloContributo();
			int matchratio = FuzzySearch.ratio(titoloDeJure.toLowerCase(), fonte.getTitoloContributo().toLowerCase());
			if(titoloDeJure.toLowerCase().contains(fonte.getTitoloContributo().toLowerCase()))
				matchratio+=10;
			if(matchratio>80) 
				return fonte;

		}
		return null;
	}


	// ricerca provvedimento WKI in DOGI-LIST
	private Provvedimento isProvvWKIinDogiList(Provvedimento findMe) {


		//			ESEMPIO DUPLICATO 
		//				
		//				provv WKI
		//				Trib. Napoli Sez. VII Ordinanza, 14 luglio 2016
		//				PARTITI POLITICI - «È LA POLITICA, BELLEZZA!»
		//				Maria Vita De Giorgi
		//				
		//				provv DOGI
		//				ord. Tribunale Napoli 14 luglio 2016
		//				E' la politica, bellezza!
		//				De Giorgi Maria Vita



		String find_authority = findMe.getAuthority()!=null?findMe.getAuthority():"";


		//MATCH SU
		// 1) titolo contributo close match (una delle fonti WKI con una delle fonti Dogi)
		// 2) Authority Provvedimento (Tribunale / Corte Appello etc.)
		// 3) Anno Provvedimento
		// 4) Città



		int threshold = 80;

		for(Provvedimento provv : provv_Dogi) {


			String authority = provv.getAuthority()!=null?provv.getAuthority():"";


			int matchratioTitolo = fuzzyMatchTitoliContribWKIvsDogi(findMe.getFonti(), provv.getFonti());
			//FuzzySearch.ratio(titoloContribDogi.toLowerCase(),find_titolo_contrib.toLowerCase());

			if(matchratioTitolo >threshold/*titoloContribDogi.toLowerCase().indexOf(find_titolo_contrib.toLowerCase())!=-1*/
					&& (provv.getProvv_year().equalsIgnoreCase(findMe.getProvv_year()))
					&& (provv.getGeo_name().equalsIgnoreCase(findMe.getGeo_name()))
					&& (authority.equalsIgnoreCase(find_authority))) {

				//System.err.print(""+matchratioTitolo+"\t");
				return provv;
			}else {
				//System.err.println("NON matching match ratio "+matchratioTitolo);
			}
		}

		return null;
	}



	// ricerca provvedimento DeJure in DOGI-LIST
	private Provvedimento isProvvDeJureinDogiList(Provvedimento findMe) {
		
		String find_authority = findMe.getAuthority()!=null?findMe.getAuthority():"";

		//MATCH SU
		// 1) titolo contributo
		// 2) Authority Provvedimento (Tribunale / Corte Appello etc.)
		// 3) Anno Provvedimento
		// 4) Città

		int threshold = 80;

		for(Provvedimento provv : provv_Dogi) {

			String authority = provv.getAuthority()!=null?provv.getAuthority():"";			
			
			int matchratioTitolo = fuzzyMatchTitoliContribDeJurevsDogi(findMe.getFonti(), provv.getFonti());

			if(matchratioTitolo >threshold
					&& (provv.getProvv_year().equalsIgnoreCase(findMe.getProvv_year()))
					&& (provv.getGeo_name().equalsIgnoreCase(findMe.getGeo_name()))
					&& (authority.equalsIgnoreCase(find_authority))) {

				//System.err.print(""+matchratioTitolo+"\t");
				return provv;
			}else {
				//System.err.println("NON matching match ratio "+matchratioTitolo);
			}
		}
		return null;

	}


	int fuzzyMatchTitoloContrib(String titoloDeJure, List<Fonte> fonti ) {
		int maxmatchratio =0;
		int matchratio;
		for(Fonte fonte:fonti) {
			matchratio = FuzzySearch.ratio(titoloDeJure.toLowerCase(), fonte.getTitoloContributo().toLowerCase());
			if(matchratio > maxmatchratio)
				maxmatchratio = matchratio;
		}
		return maxmatchratio;
	}


	int fuzzyMatchTitoliContribWKIvsDogi(List<Fonte> fontiWKI, List<Fonte> fontiDogi ) {
		int maxmatchratio =0;
		int matchratio;

		for(Fonte fonteWKI:fontiWKI) {
			String titoloWKI = fonteWKI.getTitoloContributo();
			for(Fonte fonteDogi:fontiDogi) {
				matchratio = FuzzySearch.ratio(titoloWKI.toLowerCase(), fonteDogi.getTitoloContributo().toLowerCase());
				// BONUS SE il titolo Dogi è contenuto nel titolo WKI
				if(titoloWKI.toLowerCase().contains(fonteDogi.getTitoloContributo().toLowerCase()))
					matchratio+=10;
				if(matchratio > maxmatchratio)
					maxmatchratio = matchratio;
			}
		}
		return maxmatchratio;
	}
	
	int fuzzyMatchTitoliContribDeJurevsDogi(List<Fonte> fontiDeJure, List<Fonte> fontiDogi ) {
		int maxmatchratio =0;
		int matchratio;

		for(Fonte fonteDeJure:fontiDeJure) {
			String titoloDeJure = fonteDeJure.getTitoloContributo();
			for(Fonte fonteDogi:fontiDogi) {
				matchratio = FuzzySearch.ratio(titoloDeJure.toLowerCase(), fonteDogi.getTitoloContributo().toLowerCase());
				// BONUS SE il titolo Dogi è contenuto nel titolo WKI
				if(titoloDeJure.toLowerCase().contains(fonteDogi.getTitoloContributo().toLowerCase()))
					matchratio+=10;
				if(matchratio > maxmatchratio)
					maxmatchratio = matchratio;
			}
		}
		return maxmatchratio;
	}


}
