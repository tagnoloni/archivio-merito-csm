package fonte;

import java.io.Serializable;

import util.Util;


public class Fonte implements Serializable {
	
	private static final long serialVersionUID = -628789983459288036L;

	
	private String idContributo;
	private String titoloContributo;
	private String rivista;
	private String editore;
	private String findableIn;
	private String nomeAutore;
	private String cognomeAutore;
	private String autori;
	private String dataFascicolo;
	private String pagIniziale;
	private String numFascicolo;
	private String ISSN;
	private String grandeVoceDogi;   // String[]
	private String estremoText;   
	private String url ="";   

	private String deJureTestoMassima ="";

	private String[] descrittori;  // String[]
	private String[] sistematici;  // String[]
	private String[] fonti_Jur_Leg;    // String[]
	
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getEstremoText() {
		return estremoText;
	}
	public void setEstremoText(String estremoText) {
		this.estremoText = estremoText;
	}


	public String getFindableIn() {
		return findableIn;
	}
	public void setFindableIn(String findableIn) {
		this.findableIn = findableIn;
	}
	
	
	public String getISSN() {
		return ISSN;
	}
	public void setISSN(String iSSN) {
		ISSN = iSSN;
	}
	public String[] getDescrittori() {
		return descrittori;
	}
	public void setDescrittori(String[] descrittori) {
		this.descrittori = descrittori;
	}
	public String[] getSistematici() {
		return sistematici;
	}
	public void setSistematici(String[] sistematici) {
		this.sistematici = sistematici;
	}
	public String[] getFonti_Jur_Leg() {
		return fonti_Jur_Leg;
	}
	public void setFonti_Jur_Leg(String[] fonti_Jur_Leg) {
		this.fonti_Jur_Leg = fonti_Jur_Leg;
	}
	public String getIdContributo() {
		return idContributo;
	}
	public void setIdContributo(String idContributo) {
		this.idContributo = idContributo;
	}
	public String getTitoloContributo() {
		return titoloContributo;
	}
	public void setTitoloContributo(String titoloContributo) {
		this.titoloContributo = titoloContributo;
	}
	public String getRivista() {
		return rivista;
	}
	public void setRivista(String rivista) {
		this.rivista = Util.normalizeTitoloRivista(rivista);
	}
	public String getEditore() {
		return editore;
	}
	public void setEditore(String editore) {
		if(editore.equalsIgnoreCase("Giuffrè"))
			editore = "Giuffrè Francis Lefebvre";
		this.editore = editore;
	}
	public String getNomeAutore() {
		return nomeAutore;
	}
	public void setNomeAutore(String nomeAutore) {
		this.nomeAutore = nomeAutore;
	}
	public String getCognomeAutore() {
		return cognomeAutore;
	}
	public void setCognomeAutore(String cognomeAutore) {
		this.cognomeAutore = cognomeAutore;
	}
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}
	public String getDataFascicolo() {
		return dataFascicolo;
	}
	public void setDataFascicolo(String dataFascicolo) {
		this.dataFascicolo = dataFascicolo;
	}
	public String getPagIniziale() {
		return pagIniziale;
	}
	public void setPagIniziale(String pagIniziale) {
		this.pagIniziale = pagIniziale;
	}
	public String getNumFascicolo() {
		return numFascicolo;
	}
	public void setNumFascicolo(String numFascicolo) {
		this.numFascicolo = numFascicolo;
	}
	public String getGrandeVoceDogi() {
		return grandeVoceDogi;
	}
	public void setGrandeVoceDogi(String grandeVoceDogi) {
		this.grandeVoceDogi = grandeVoceDogi;
	}
	
	public String getDeJureTestoMassima() {
		return deJureTestoMassima;
	}
	public void setDeJureTestoMassima(String deJureTestoMassima) {
		this.deJureTestoMassima = deJureTestoMassima;
	}

	// FIXME
	
//	errore in annoRivista per Giurisprudenza commentata DeJure
//	Per alienare un bene costituito in fondo patrimoniale l’autorizzazione giudiziale è necessaria 
//	quando vi sono figli minori, Riv. Ilfamiliarista.it, Giuffrè, 19 F, nota di SCARFÒ FRANCESCO
//	
//	Genitore pubblica sui social network foto e notizie del figlio minore: interviene d'ufficio 
//	il Giudice, Riv. Ilfamiliarista.it, Giuffrè, 12 M, nota di CESARO GRAZIA OFELIA
	
//  es. rivista/portale on line
//  App. Milano, sez. V, 16 novembre 2017, n. 4793, ilfamiliarista.it, Giuffrè Francis Lefebvre, 
// 	15 gennaio 2018, nota di M. ROVACCHI 
//	(essenzialmente, di un portale on line si riporta il giorno di pubblicazione)	
	
	public String printCitazioneFonte(){
		//Riv.  Notariato, Giuffrè Francis Lefebvre, 2018,  01, 163, nota di  P.  BOERO (laddove 2018 = anno rivista, 01 = numero fascicolo, 163 = numero di pagina)


		
		// PROVVEDIMENTI DA SCRAPE DEJURE
		if(this.getRivista()!=null && this.getRivista().length()>0 
				&& this.getEditore()==null 
				&& this.getDataFascicolo()==null 
				&& this.getNumFascicolo()==null 
				&& this.getAutori()==null)
					return this.getTitoloContributo()+", "+this.getRivista();
		
		String dataContrib = this.getDataFascicolo();

		String annoRivista = this.getDataFascicolo();
		if(this.getDataFascicolo()!=null && this.getDataFascicolo().length()>=4)
			annoRivista = this.getDataFascicolo().substring(0,4);

		String ret="";

		ret+=this.getTitoloContributo()+", ";

		if(this.getRivista()!=null && this.getRivista().trim().length()>0) {
			if(!this.getRivista().toLowerCase().startsWith("riv") && !this.getRivista().toLowerCase().startsWith("redazione")
					&& !this.getRivista().toLowerCase().contains("gazzetta"))
				ret+="Riv. ";
			ret+=this.getRivista()+", ";
			ret+=this.getEditore()!=null?this.getEditore()+", ":"";
			if(!Util.isRivistaTelematica(this.getRivista().trim())) {
				ret+=annoRivista+", ";
				if(this.getNumFascicolo()!=null && this.getNumFascicolo().length()>0)
					ret+=this.getNumFascicolo()+", ";
				if(this.getPagIniziale()!=null && this.getPagIniziale().length()>0)
					ret+=this.getPagIniziale()+", ";
			}else {
				ret+=dataContrib.trim().length()>0?dataContrib.trim()+", ":"";
			}
		}
		if(this.getAutori()!=null) {
			ret+="nota di ";
			ret+=this.getAutori().toUpperCase().replaceAll(" \\| ", ", ");
		}

		return "\""+ret+"\"";
	}
	
	
	public String printFonteGennuso(){
		
		// PROVVEDIMENTI DA SCRAPE DEJURE
		if(this.getRivista()!=null && this.getRivista().length()>0  && this.getDataFascicolo()==null && this.getNumFascicolo()==null && this.getAutori()==null)
			return this.getRivista();
		
		String dataContrib = "";
		String annoRivista = "";
		
		if(this.getDataFascicolo()!=null) {
			dataContrib = this.getDataFascicolo();
			annoRivista = this.getDataFascicolo();
			if(this.getDataFascicolo().length()>=4)
				annoRivista = this.getDataFascicolo().substring(0,4);
		}
			

		String ret="";

		if(this.getRivista()!=null && this.getRivista().trim().length()>0) {
			if(!this.getRivista().toLowerCase().startsWith("riv") && !this.getRivista().toLowerCase().startsWith("redazione"))
				ret+="Riv. ";
			ret+=this.getRivista()+", ";
			if(this.getEditore()!=null && this.getEditore().trim().length()>0)
				ret+=this.getEditore()+", ";
			
			if(!Util.isRivistaTelematica(this.getRivista().trim())) {
				ret+=annoRivista+", ";
				if(this.getNumFascicolo()!=null && this.getNumFascicolo().length()>0)
					ret+=this.getNumFascicolo()+", ";
				if(this.getPagIniziale()!=null && this.getPagIniziale().length()>0)
					ret+=this.getPagIniziale()+", ";
			}else {
				ret+=dataContrib+", ";
			}
		}
		
		if(this.getAutori()!=null) {
			ret+="nota di ";
			ret+=this.getAutori().toUpperCase().replaceAll(" \\| ", ", ");
		}
		
		return "\""+ret+"\"";
	}
	
	
	
//	Array descrittori;
//	Array Sistematici;
//	Array fonti;

}
