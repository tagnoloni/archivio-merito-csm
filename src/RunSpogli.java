import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import config.Config;
import config.ConfigSpogli;
import provvedimento.Provvedimento;
import provvedimento.ProvvedimentoFinal;
import spogliFinale.EditSpogliDataReader;
import spogliFinale.MergeEditSpogli;
import spogliFinale.SpogliFinale;




public class RunSpogli {


	public static void main (String[] args) {
		
		HashMap<String,ProvvedimentoFinal> provv_Map_Final = spoglioProvvedimenti();
		//printMapFinal(provv_Map_Final);
		
		provv_Map_Final = mergeSpogliManuali(provv_Map_Final);
		
		
		//printTabellaSpoglioFinale(provv_Map_Final);
		
		
		// REPLACE CON STAMPA TABELLA NEL FORMATO DI CONSEGNA
		//printTabellaSpogliatiFinale(provv_Map_Final);
		//printTabellaConsegnaFinale_PRE_CONSEGNA(provv_Map_Final);
		printTabellaConsegnaFinale(provv_Map_Final);

	}
	
	private static HashMap<String,ProvvedimentoFinal> mergeSpogliManuali(HashMap<String,ProvvedimentoFinal> provv_Map_Final) {
		
				SpogliFinale sF = new SpogliFinale();
				HashMap<String,String> PENLookup;
				HashMap<String,String> CIVLookup;
				HashMap<String,String> PROCIVLookup;

				CIVLookup = sF.readDescrittoriLookupTSV(ConfigSpogli.CIV_lookup);
				PENLookup= sF.readDescrittoriLookupTSV(ConfigSpogli.PEN_lookup);
				PROCIVLookup = sF.readDescrittoriLookupTSV(ConfigSpogli.PROCIV_lookup);
				
				EditSpogliDataReader editSpoglidr = new EditSpogliDataReader();
				editSpoglidr.setCIVLookup(CIVLookup);
				editSpoglidr.setPENLookup(PENLookup);
				editSpoglidr.setPROCIVLookup(PROCIVLookup);
				
				MergeEditSpogli mergeEditSpogli = new MergeEditSpogli();
				mergeEditSpogli.setProvv_Map_Final(provv_Map_Final);
				
				//QUI merge spogli manuali finali  
			
				
//				// SPOGLIO 1 - ANCONA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_ANCONA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - BARI
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_BARI);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - BOLOGNA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_BOLOGNA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - BRESCIA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_BRESCIA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - CALTANISSETTA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_CALTANISSETTA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - FIRENZE
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_FIRENZE);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - GENOVA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_GENOVA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - LAQUILA
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_LAQUILA);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - TRIESTE
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_TRIESTE);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
//				
//				// SPOGLIO 1 - PALERMO
//				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_1_PALERMO);
//				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
//				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				
				// ======  FINE merge Spogli ====== //
				

				// SPOGLIO 2 - POTENZA
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_POTENZA);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - MILANO
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_MILANO);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - CATANIA
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_CATANIA);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - VENEZIA
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_VENEZIA);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - SALERNO
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_SALERNO);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - PERUGIA
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_PERUGIA);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - TRENTO
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_TRENTO);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - CAGLIARI
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_CAGLIARI);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - NAPOLI
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_NAPOLI);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
				// SPOGLIO 2 - LECCE
				editSpoglidr.readEditSpogliDefinitiviTSV(ConfigSpogli.SPOGLI_2_LECCE);
				mergeEditSpogli.setProvv_EditManuale(editSpoglidr.getProvvedimentiFinali());
				mergeEditSpogli.mergeEditedInfoSpogliManuali();
				
			
				
				
				
				mergeEditSpogli.generaTestoEstremoUniformeFinal();
				
				return mergeEditSpogli.getProvv_Map_Final();
	}

	
	private static void printTabellaSpoglioFinale(HashMap<String,ProvvedimentoFinal> provv_Map_Final ) {

		System.err.println();
		System.err.println("****************");
		System.err.println();
		System.err.println(ConfigSpogli.HEADER_SINGLE_ROW_PROVV_SPOGLIO_FINALE);
		for(ProvvedimentoFinal provvF : provv_Map_Final.values()) {
			//if(provvF.isProvvedimentoConsegnato())
			// ATTENZIONE (stampa solo estremi completi e scarta Giudice di Pace)
			if(provvF.isEstremoCompleto() && (provvF.getAuthority()==null || !provvF.getAuthority().contains("GPC")))
				provvF.printSingleRowTabellaSpogliFinale();
		}

	}
	
	
	private static void printTabellaConsegnaFinale(HashMap<String,ProvvedimentoFinal> provv_Map_Final ) {
		
		//		SPOGLIO GIUDICI (commento Mandalari)		
		//		-  il campo giudici è unico mentre è prevista la suddivisione nei campi presidente e relatore/estensore.  
		//		In 33 casi, sui 74 proposti nel campione, è presente sia il  presidente sia il relatore/estensore  che occorre 
		//		inserire separatamente nei rispettivi campi. In tutti gli altri casi proposti, dove compare un solo nominativo 
		//		(giudice monocratico), si ritiene che debba essere adottata la pratica di inserirlo sia nel campo presidente 
		//		sia nel campo relatore/estensore;  



		System.err.println();
		System.err.println("****************");
		System.err.println();
		System.err.println(ConfigSpogli.HEADER_GENNUSO_FINAL_RESTITUZIONE);
		for(ProvvedimentoFinal provvF : provv_Map_Final.values()) {
			//if(provvF.isProvvedimentoConsegnato())
			// ATTENZIONE (stampa solo estremi completi e scarta Giudice di Pace)
			if(provvF.isSpogliato() && provvF.isRestituireDefinitivo())
				provvF.printSingleRowProvvedimentoCONSEGNA_FINALE();
		}

	}
	
	
	
	private static void printTabellaConsegnaFinale_PRE_CONSEGNA(HashMap<String,ProvvedimentoFinal> provv_Map_Final ) {
		
		//		SPOGLIO GIUDICI (commento Mandalari)		
		//		-  il campo giudici è unico mentre è prevista la suddivisione nei campi presidente e relatore/estensore.  
		//		In 33 casi, sui 74 proposti nel campione, è presente sia il  presidente sia il relatore/estensore  che occorre 
		//		inserire separatamente nei rispettivi campi. In tutti gli altri casi proposti, dove compare un solo nominativo 
		//		(giudice monocratico), si ritiene che debba essere adottata la pratica di inserirlo sia nel campo presidente 
		//		sia nel campo relatore/estensore;  



		System.err.println();
		System.err.println("****************");
		System.err.println();
		//System.err.println(ConfigSpogli.HEADER_GENNUSO_FINAL_RESTITUZIONE);
		for(ProvvedimentoFinal provvF : provv_Map_Final.values()) {
			//if(provvF.isProvvedimentoConsegnato())
			// ATTENZIONE (stampa solo estremi completi e scarta Giudice di Pace)
			if(provvF.isSpogliato())
				provvF.printSingleRowProvvedimentoCONSEGNA_FINALE_PRE_CONSEGNA();
		}

	}
	
	

	
	private static void printMapFinal(HashMap<String,ProvvedimentoFinal> provv_Map_Final ) {
	
		
		System.err.println();
		System.err.println("****************");
		System.err.println();
		System.err.println(ConfigSpogli.HEADER_SINGLE_ROW_PROVV_RESTITUZIONE_ESTESO);
		for(ProvvedimentoFinal provvF : provv_Map_Final.values()) {
			//if(provvF.isProvvedimentoConsegnato())
			// ATTENZIONE (stampa solo estremi completi e scarta Giudice di Pace)
			if(provvF.isEstremoCompleto() && (provvF.getAuthority()==null || !provvF.getAuthority().contains("GPC")))
				provvF.printSingleRowProvvedimentoFinal();
		}

	}
	
	private static HashMap<String,ProvvedimentoFinal> spoglioProvvedimenti() {


		HashMap<String,Provvedimento> serializedProvvMap = null;


		File prov_Map_serial = new File(Config.serializedProvvMap);


		if(prov_Map_serial.exists()) {

			try {
				serializedProvvMap = readProvvMapFromFile(Config.serializedProvvMap);
			}catch(Exception ex) {
				ex.printStackTrace();
			}

		}else {   // MAPPA NON ESISTE SERIALIZZATA
			System.err.println("+++++++++WARNING  - mappa serializzata NON TROVATA ");
		}


		//===============================================
		//	SPOGLI_FINALE - attribuzione classificazioni 
		//===============================================

		SpogliFinale sF = new SpogliFinale();
		sF.setProvv_Merge_Map(serializedProvvMap);

		HashMap<String,String> PENLookup;
		HashMap<String,String> CIVLookup;
		HashMap<String,String> PROCIVLookup;

		CIVLookup = sF.readDescrittoriLookupTSV(ConfigSpogli.CIV_lookup);
		PENLookup= sF.readDescrittoriLookupTSV(ConfigSpogli.PEN_lookup);
		PROCIVLookup = sF.readDescrittoriLookupTSV(ConfigSpogli.PROCIV_lookup);

		sF.setCIVLookup(CIVLookup);
		sF.setPENLookup(PENLookup);
		sF.setPROCIVLookup(PROCIVLookup);


		HashMap<String,ArrayList<String>> allineaCIV;
		HashMap<String,ArrayList<String>> allineaPEN;
		HashMap<String,ArrayList<String>> allineaPEN_DJ;


		allineaCIV = sF.readAllineamentiTSV(ConfigSpogli.ALLINEA_CIV);
		allineaPEN = sF.readAllineamentiTSV(ConfigSpogli.ALLINEA_PEN);
		allineaPEN_DJ = sF.readAllineamentiTSV(ConfigSpogli.ALLINEA_PEN_DEJURE);

		sF.setAllineaCIV(allineaCIV);
		sF.setAllineaPEN(allineaPEN);
		sF.setAllineaPEN_DJ(allineaPEN_DJ);

		// SET CLASSIFICAZIONI FINALI via ALLINEAMENTO (e copia mappa ProvvMerge --> ProvvFinal)
		sF.setClassificazioniFinali_MergeMap();
		// AGGIORNA ALTRE info su mappa Provvedimenti Finali (url provvedimento Pdf, normalizzazione sezioni)
		sF.updateProvvMapFinal();
		

		return sF.getProvv_Map_Final();

	}
	


	private static HashMap<String, Provvedimento> readProvvMapFromFile(String filePath) throws IOException{
	
		ObjectInputStream objectinputstream = null;
		try {
			FileInputStream streamIn = new FileInputStream(filePath);
			objectinputstream = new ObjectInputStream(streamIn);
			HashMap<String, Provvedimento> readMap = (HashMap<String, Provvedimento>) objectinputstream.readObject();
			return readMap;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(objectinputstream != null){
				objectinputstream .close();
			} 
		}
		return null;
	}


	
	
}







