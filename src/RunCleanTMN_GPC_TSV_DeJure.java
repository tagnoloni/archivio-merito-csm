import config.Config;
import deJureDataReader.DeJureTMN_GPC_TSVDataReader;


public class RunCleanTMN_GPC_TSV_DeJure {


	public static void main (String[] args) {
	
		runCleanTMN_GPC_TSV_DeJure();
		//	runDeJure();
		//	runDeJureSpogli();
			
		
	}
	

	
	private static void runCleanTMN_GPC_TSV_DeJure() {
		DeJureTMN_GPC_TSVDataReader djTMN_GPC_TSVdr = new DeJureTMN_GPC_TSVDataReader();

		System.out.println("-- START READ PENALE TSV --");
		
		

		
		String tsvFilePath=Config.DATA_INPUT_DEJURE_TMN_GPC_TSV;

		djTMN_GPC_TSVdr.readTMN_GPC_TSV(tsvFilePath);	
		//djPenaledr.printPenaliDeJureExploded();
		djTMN_GPC_TSVdr.printTMN_GPC_TSV_DeJureSingleRow();

		
	}
		
	
	
}



