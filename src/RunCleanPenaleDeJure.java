import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;

import config.Config;
import deJureDataReader.DeJureCivileDataReader;
import deJureDataReader.DeJurePenaleDataReader;
import provvedimento.Provvedimento;


public class RunCleanPenaleDeJure {


	public static void main (String[] args) {
	
		runCleanPenaleDeJure();
				
	}
	

	private static void runCleanPenaleDeJure() {
		DeJurePenaleDataReader djPenaledr = new DeJurePenaleDataReader();

		System.out.println("-- START READ CIVILE TSV --");
		

		// LOAD DATASET PROVVEDIMENTI PRINCIPALE (mergeProvvMAP)
		HashMap<String,Provvedimento> datasetMerge = null;
		File prov_Map_serial = new File(Config.serializedProvvMap);
		if(!prov_Map_serial.exists()) {
			System.out.println("DATASET provvedimenti PRINCIPALE non trovato ");
		}else {
			try {
				datasetMerge = readProvvMapFromFile(Config.serializedProvvMap);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		

	
		//String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1_2_3;
		String tsvFilePath=Config.DATA_INPUT_DEJURE_PENALE_TRIB_APP_ASSISE;

		djPenaledr.readDeJurePenaleLexaDocTSV(tsvFilePath);	
		
	
		djPenaledr.alphaOrderForMultipleCat();
		

		// CERCA I PROVVEDIMENTI SCRAPATI da DEJURE nel dataset principale & CHECK se già presi
		if(datasetMerge!=null) {
			djPenaledr.flagDuplicate(datasetMerge);
			//djCiviledr.flagProbablyDuplicate(serializedProvvMap);
		}
		
		// FORMATO SPECIFICO scrape Dejure
		djPenaledr.printCivileDeJureSingleRow();
		
		// FORMATO COMUNE ALTRI RECORD
		//djPenaledr.printCivileDeJureGennusoStandard();
		//djPenaledr.printCivileDeJureSingleRowStandard();
		//djPenaledr.printCivileDeJureSingleRowOmeka();


		
	}
	
	
		
	private static HashMap<String, Provvedimento> readProvvMapFromFile(String filePath) throws IOException{
		
		ObjectInputStream objectinputstream = null;
		try {
		    FileInputStream streamIn = new FileInputStream(filePath);
		    objectinputstream = new ObjectInputStream(streamIn);
		    HashMap<String, Provvedimento> readMap = (HashMap<String, Provvedimento>) objectinputstream.readObject();
			return readMap;
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if(objectinputstream != null){
		        objectinputstream .close();
		    } 
		}
		return null;
	}
	
}



