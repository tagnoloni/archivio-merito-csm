package editManualiDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import fonte.Fonte;
import provvedimento.Provvedimento;

public class EditManualiDataReader {
	
	
	List<Provvedimento> provvedimenti;
	
	
	
	
	public boolean readEditConsegneDefinitiveTSV(String tsvFilePath) {
		
		provvedimenti = new ArrayList<Provvedimento>();

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
	
				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
					
							
				String idITTIG ="";
				String restituireDefinitivo; 
				String distretto ="";
				String provv_year="";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String data_decisione="";
				String data_pubblicazione ="";
				String voci_classificazione ="";
				String fonte = "";
				String filePath ="";
				String provv_sezione = "";
				String provv_numero = "";
				
//				0 idITTIG	
//				1 *restituire_definitivo	
//				2 id_Provv	
//				3 isEstremoCompleto	
//				4 isEditManuale	
//				5 isProvvedimentoConsegnato	
//				6 dataConsegnaProvvedimento	
//				7 isProvvedimentoTrovato	
//				8 distretto	
//				9 anno	
//				10 tipo_ufficio	
//				11 localita_ufficio	
//				12 tipo_provvedimento	
//				13 settore	
//				14 provv_data	
//				15 *provv_numero	
//				16 *provv_sez	
//				17 *giudici	
//				18 parti	
//				19 num_registro	
//				20 *data_decisione	
//				21 *data_pubblicazione	
//				22 *voci_classificazione	
//				23 estremo_uniforme	
//				24 estremo_provv	
//				25 url_provv	
//				26 fonte	
//				27 url_contributo	
//				28 rivista	
//				29 editore	
//				30 findable_IN	
//				31 classificazioneTOP	
//				32 classificazione	
//				33 nota	
//				34 provenance	
//				35 localFilePath	
//				36 notaRisposta	
//				37 versione
				
				
				idITTIG = fields[0].trim();
				restituireDefinitivo = fields[1].trim();
				distretto = fields[8].trim();
				provv_year = fields[9].trim();
				tipo_ufficio  = fields[10].trim();
				localita_ufficio = fields[11].trim();
				tipoDoc = fields[12].trim();
				area = fields[13].trim();
				provv_data = fields[14].trim();  
				provv_numero = fields[15].trim();
				provv_sezione = fields[16].trim();
				giudici = fields[17].trim();
				parti = fields[18].trim();
				num_registro = fields[19].trim();
				data_decisione = fields[20].trim();
				data_pubblicazione = fields[21].trim();
				voci_classificazione = fields[22].trim();
				fonte = fields[26].trim();
				filePath = fields[35].trim();
				
				if(filePath.indexOf("csm")!=-1)
					filePath=filePath.substring(filePath.indexOf("csm")+3, filePath.length());
						
				Provvedimento p = new Provvedimento();
		
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("tribunale dei minori"))
					tipo_ufficio = "IT_TMN";
				else if(tipo_ufficio.equalsIgnoreCase("giudice di pace"))
					tipo_ufficio = "IT_GPC";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
		
				p.setRestituireDefinitivo(restituireDefinitivo.trim().toLowerCase().startsWith("s")?true:false);
				
				p.setPersistentID(idITTIG);
				p.setDocType(tipoDoc);
				p.setDistrettoCorteAppello(distretto);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(provv_data);
				p.setCourt_section(provv_sezione); 				
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);	
				
				p.setGiudici(giudici);
				p.setDataDecisione(data_decisione);
				p.setDataPubblicazione(data_pubblicazione);
				p.setFilePath(filePath);
				p.setClassificazioniCSMFinal(getClassificazioniDefinitive(voci_classificazione));

				if(fonte.trim().length()==0) {
					System.out.println("----- EDIT FINALE - Sganciare fonte");
					Fonte f = new Fonte();
					f.setEstremoText("");
					p.addFonte(f);
				}
			
				provvedimenti.add(p);			
				
			}
			System.err.println("+++CONSEGNE DEFINITIVE - ADDED "+i);

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}
	
	
	private String[] getClassificazioniDefinitive(String vociClassificazione) {
		if(vociClassificazione.contains("#"))
			return vociClassificazione.split("#");
		else
			return new String[]{vociClassificazione};
	}
	
	
	public boolean readEditManualiTSV(String tsvFilePath) {
		
		provvedimenti = new ArrayList<Provvedimento>();

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
	
				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
							
				String idITTIG ="";
				String distretto ="";
				String provv_year="";
				String estremoCompleto = "";
				String editManuale = "";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				String provv_url = "";

	
				
				idITTIG = fields[0].trim();
				estremoCompleto = fields[2].trim();
				editManuale = fields[3].trim();
				distretto = fields[7].trim();
				provv_year = fields[8].trim();
				tipo_ufficio  = fields[9].trim();
				localita_ufficio = fields[10].trim();
				tipoDoc = fields[11].trim();
				area = fields[12].trim();
				provv_data = fields[13].trim();  // trasforma in aaaa-mm-gg
				provv_numero = fields[14].trim();
				provv_sezione = fields[15].trim();
				giudici = fields[16].trim();
				parti = fields[17].trim();
				num_registro = fields[18].trim();
				provv_estremo = fields[20].trim();
				provv_url = fields[21].trim();

						
						
				Provvedimento p = new Provvedimento();
		
			
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("tribunale dei minori"))
					tipo_ufficio = "IT_TMN";
				else if(tipo_ufficio.equalsIgnoreCase("giudice di pace"))
					tipo_ufficio = "IT_GPC";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
		
				// FIXME 6 maggio - check qui integrazioni precedenti
				// nelle integrazioni precedenti sono marcati come true/false e quindi trascurate
				// quando non ha importanza il flag isEditManuale:  tenerlo vuoto 
				// negli spogli da rivista con la X 
				p.setEditManuale(editManuale.trim().toLowerCase().startsWith("x")?true:false);
				
				p.setEstremoCompletoEdit(estremoCompleto.trim().toLowerCase().startsWith("true")?true:false);
				p.setEstremoCompletoAuto(false);

				p.setPersistentID(idITTIG);
				p.setDocType(tipoDoc);
				p.setDistrettoCorteAppello(distretto);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(provv_data);
				p.setCourt_section(provv_sezione); 				
				p.setEstremoText(provv_estremo);
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);	
				p.setUrl(provv_url);
			
				provvedimenti.add(p);			
				
			
				
			}
			System.err.println("+++EDIT MANUALI - ADDED "+i+" edit manuali clean # rows");

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}
	
	
	public boolean readEditManualiCOSTTSV(String tsvFilePath) {

		provvedimenti = new ArrayList<Provvedimento>();

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		String line  = null;

		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));

			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;

				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;

				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 

				String[] fields = line.split("\t");


				String idITTIG ="";
				String distretto ="";
				String provv_year="";
				String estremoCompleto = "";
				String editManuale = "";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				String provv_url = "";



				idITTIG = fields[0].trim();
				estremoCompleto = fields[2].trim();
				editManuale = fields[3].trim();
				distretto = fields[7].trim();
				provv_year = fields[8].trim();
				tipo_ufficio  = fields[9].trim();
				localita_ufficio = fields[10].trim();
				tipoDoc = fields[11].trim();
				area = fields[13].trim();
				provv_data = fields[14].trim();  // trasforma in aaaa-mm-gg
				provv_numero = fields[15].trim();
				provv_sezione = fields[16].trim();
				giudici = fields[17].trim();
				parti = fields[18].trim();
				num_registro = fields[19].trim();
				provv_estremo = fields[20].trim();
				provv_url = fields[22].trim();



				Provvedimento p = new Provvedimento();


				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("tribunale dei minori"))
					tipo_ufficio = "IT_TMN";
				else if(tipo_ufficio.equalsIgnoreCase("giudice di pace"))
					tipo_ufficio = "IT_GPC";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";


				// FIXME 6 maggio - check qui integrazioni precedenti
				// nelle integrazioni precedenti sono marcati come true/false e quindi trascurate
				// quando non ha importanza il flag isEditManuale:  tenerlo vuoto 
				// negli spogli da rivista con la X 
				p.setEditManuale(editManuale.trim().toLowerCase().startsWith("x")?true:false);

				p.setEstremoCompletoEdit(estremoCompleto.trim().toLowerCase().startsWith("true")?true:false);
				p.setEstremoCompletoAuto(false);

				p.setPersistentID(idITTIG);
				p.setDocType(tipoDoc);
				p.setDistrettoCorteAppello(distretto);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(provv_data);
				p.setCourt_section(provv_sezione); 				
				p.setEstremoText(provv_estremo);
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);	
				p.setUrl(provv_url);

				provvedimenti.add(p);			



			}
			System.err.println("+++EDIT MANUALI - ADDED "+i+" edit manuali clean # rows");

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}
	
	public boolean readEditManualiTSVLexadoc(String tsvFilePath) {
		
		provvedimenti = new ArrayList<Provvedimento>();

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
	
				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
							
				String idITTIG ="";
				String classificazioneCassazione ="";
				String distretto ="";
				String provv_year="";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				String provv_url = "";

			

				
				idITTIG = fields[0].trim();
				classificazioneCassazione = fields[4].trim();
				distretto = fields[5].trim();
				provv_year = fields[6].trim();
				tipo_ufficio  = fields[7].trim();
				localita_ufficio = fields[8].trim();
				tipoDoc = fields[9].trim();
				area = fields[10].trim();
				provv_data = fields[11].trim();  // trasforma in aaaa-mm-gg
				provv_numero = fields[13].trim();
				provv_sezione = fields[12].trim();
				giudici = fields[14].trim();
				parti = fields[15].trim();
				num_registro = fields[16].trim();
				provv_estremo = fields[17].trim();
				provv_url = fields[18].trim();

						
						
				Provvedimento p = new Provvedimento();
		
			
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("tribunale dei minori"))
					tipo_ufficio = "IT_TMN";
				else if(tipo_ufficio.equalsIgnoreCase("giudice di pace"))
					tipo_ufficio = "IT_GPC";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
		
				

				p.setPersistentID(idITTIG);
				p.setClassificazioniCSM(classificazioneCassazione);
				p.setDocType(tipoDoc);
				p.setDistrettoCorteAppello(distretto);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(provv_data);
				p.setCourt_section(provv_sezione); 				
				p.setEstremoText(provv_estremo);
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);	
				p.setUrl(provv_url);
			
				provvedimenti.add(p);			
				
			
				
			}
			System.err.println("+++EDIT MANUALI - ADDED "+i+" edit manuali clean # rows");

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}
	

	
	
	
	
	public List<Provvedimento> getProvvedimenti() {
		return this.provvedimenti;
	}
	

}
