package config; 

public class Config {

//  28-06-2018
//	public static String DATA_INPUT_NOTE = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/EDIT-CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-AUTORI-json.tsv";
//	
//	public static String DATA_INPUT_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-SISTEMATICI-json.tsv";
//
//	public static String DATA_INPUT_DESCRITTORI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-DESCRITTORI-json.tsv";
//
//	public static String DATA_INPUT_FONTI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-29-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-FONTI-json.tsv";
//
//	public static String TAB_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/tabSistematici-json.tsv";

	public static String DATASET_VERSION ="2.0";
	public static String serializedProvvMap = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/provvMap2_0.ser";


	public static String SEP = "\t";
	public static String MULTI_VALUE_SEP = "#"; //"; ""#"; // "|";
	
	
//  25-10-2018	
	

//	public static String DATA_INPUT_NOTE = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-25-10-source-tsv/CSM-Dogi-25-10-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-AUTORI-json.tsv";
//	
//	public static String DATA_INPUT_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-25-10-source-tsv/CSM-Dogi-25-10-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-SISTEMATICI-json.tsv";
//
//	public static String DATA_INPUT_DESCRITTORI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-25-10-source-tsv/CSM-Dogi-25-10-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-DESCRITTORI-json.tsv";
//
//	public static String DATA_INPUT_FONTI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-25-10-source-tsv/CSM-Dogi-25-10-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-FONTI-json.tsv";
//
//	public static String TAB_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-25-10-source-tsv/tabSistematici-json.tsv";

	
	public static String DATA_ORDINANZE_COST = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/OrdinanzeCOST/Ordinanze-promovimento-2014-2019-GU-COST-3giugno2019.tsv";
	
	public static String CACHE_ORDINANZE_COST = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/src/ordinanzeCostDataReader/CACHE";
//	29-01-2019	
	
	public static String DATA_INPUT_NOTE = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-29-01-source-tsv/CSM-Dogi-29-01-2019-Note-a-sentenza-dal-2014-con-filtro-testuale-AUTORI-json.tsv";
	
	public static String DATA_INPUT_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-29-01-source-tsv/CSM-Dogi-29-01-2019-Note-a-sentenza-dal-2014-con-filtro-testuale-SISTEMATICI-json.tsv";

	public static String DATA_INPUT_DESCRITTORI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-29-01-source-tsv/CSM-Dogi-29-01-2019-Note-a-sentenza-dal-2014-con-filtro-testuale-DESCRITTORI-json.tsv";

	public static String DATA_INPUT_FONTI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-29-01-source-tsv/CSM-Dogi-29-01-2019-Note-a-sentenza-dal-2014-con-filtro-testuale-FONTI-json.tsv";

	public static String TAB_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-29-01-source-tsv/tabSistematici-json.tsv";
	
	
	public static String TAB_COMUNI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/elenco-comuni-italiani.tsv";

	
	public static String TAB_DISTRETTI_CA = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/distretti-CA.tsv";

	public static String TAB_DISTRETTI_CA_FIX = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/distretti-CA-fix.tsv";

	
	public static String TAB_FINDABILITY_EDITORI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/editori-findability.tsv";
	
	
	
	//public static String SEP2 = "\t";


	public static String TSV_PREFIX ="_ - ";
	
	
	public static String SPOGLI_VENANZI ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/spogli_venanzi.txt";
	public static String SPOGLI_VENANZI_FAILED ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/spogli_venanzi_failed.txt";

	
	/////////////////////////
	//   CONSEGNE METADATI DEFINITIVI
	/////////////////////////

	public static String DATA_CONSEGNA_DEFINITIVA_1 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/RISPOSTE_DEFINITIVE/consegna-risposte-CSM_1-78.tsv";

	public static String EDIT_ORDINANZE_COST ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/EDIT_Ordinanze_COST.tsv";
	/////////////////////////
	//   SCRAPE DE JURE
	/////////////////////////
	
	// LEXADOC
	
	
	public static String FOLDER_SELEZIONE_LEXADOC_CIVILE ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/SELEZIONE_LEXADOC_CIVILE";
	public static String FOLDER_SELEZIONE_LEXADOC_PENALE ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/SELEZIONE_LEXADOC_PENALE";
	public static String FOLDER_SELEZIONE_LEXADOC_LAVORO ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/SELEZIONE_LEXADOC_LAVORO";

	
	public static String EDIT_CONSEGNA_LEXADOC_1 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/consegnaLEXADOC1-20190527-1125.tsv";
	public static String EDIT_CONSEGNA_LEXADOC_2_PENALE ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/consegnaLEXADOC2-PENALE-20190614-1550.tsv";
	public static String EDIT_CONSEGNA_LEXADOC_3 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/consegnaLEXADOC3-20190627-2300.tsv";
	public static String EDIT_CONSEGNA_LEXADOC_LAVORO ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/consegnaLEXADOC4-Lavoro-20190628-526.tsv";

	
	public static String DATA_INPUT_DEJURE_LEXADOC_1 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-LEXADOC-1.tsv";
	public static String DATA_INPUT_DEJURE_LEXADOC_2 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-LEXADOC-2.tsv";
	public static String DATA_INPUT_DEJURE_LEXADOC_3 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-LEXADOC-3.tsv";
	public static String DATA_INPUT_DEJURE_LEXADOC_4 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-LAVORO-LEXADOC-4.tsv";

	
	public static String DATA_INPUT_DEJURE_LEXADOC_1_2 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-LEXADOC-1-2.tsv";
	public static String DATA_INPUT_DEJURE_LEXADOC_1_2_3 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-LEXADOC-1-2-3.tsv";

	
	public static String DATA_INPUT_DEJURE_COMMENTATA_APPELLO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-GIURISP-COMMENTATA-APPELLO.tsv";
	public static String DATA_INPUT_DEJURE_COMMENTATA_TRIBUNALI = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-GIURISP-COMMENTATA-TRIBUNALI.tsv";

	public static String DATA_INPUT_DEJURE_NOTE_SENTENZA_APPELLO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-NOTE-SENTENZA-APPELLO.tsv";
	public static String DATA_INPUT_DEJURE_NOTE_SENTENZA_TRIBUNALI = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-NOTE-SENTENZA-TRIBUNALI.tsv";

	
	public static String DATA_INPUT_DEJURE_PENALE_TRIB_APP_ASSISE ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-PENALE-trib-app-assise.tsv";
	
	public static String DATA_INPUT_DEJURE_CIVILE_FAMIGLIA ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-FAMIGLIA.tsv";
	
	public static String DATA_INPUT_DEJURE_CIVILE_IMMOBILI_CONDOMINIO ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-IMMOBILI-CONDOMINIO.tsv";
	
	public static String DATA_INPUT_DEJURE_CIVILE_DANNO_RESPONSABILITA ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-CIVILE-DANNO-RESPONSABILITA.tsv";
	
	public static String DATA_INPUT_DEJURE_TMN_GPC_TSV ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/DeJure-sources-19-01-2019/Dejure-scrape-TMN-GPC-TSV.tsv";
	
	public static String DATA_SPOGLI_DEJURE_NOTE_SENTENZA_APPELLO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/spogli-DeJure/142-nas-appello.tsv";

	public static String DATA_SPOGLI_DEJURE_NOTE_SENTENZA_TRIBUNALI = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/spogli-DeJure/557-nas-tribunali.tsv";

	public static String DATA_SPOGLI_DEJURE_COMMENTATA_APPELLO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/spogli-DeJure/81-commentata-appello.tsv";

	public static String DATA_SPOGLI_DEJURE_COMMENTATA_TRIBUNALI = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/spogli-DeJure/487-commentata-tribu.tsv";

	
	public static String DATA_INPUT_WKI_NOTE_SENTENZA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/WKI-sources-28-01-2019/WKI-NOTE-SENTENZA-2016-2019.tsv";
	
	public static String DATA_SPOGLI_WKI_1160 = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/spogli-WKI/1160-spogli-WKI.tsv";
	
	public static String EDIT_MANUALI_CLEAN1 = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/spogli-clean#.tsv";
	
	public static String EDIT_MANUALI_CLEAN2 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/fix-revisione-consegna-29-aprile.tsv";

	public static String SPOGLI_DOGI_HOPELESS_1 = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/Hopeless_DOGI_fatti_20190506.tsv";
	
	public static String SPOGLI_GIUFFRE_VENANZI_1 = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/spogliVenanzi_TABLE_clean_20190506.tsv";
	
	public static String SPOGLI_DOGI_HOPELESS_2 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/spogli-fatti-20190507.tsv";
	
	public static String SPOGLI_HOPELESS_WKI_17_5 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/Fatti-Integrati20190517.tsv";

	
	public static String SPOGLI_HOPELESS_6_6 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/Hopeless_FATTI-integrati6giugno2019.tsv";

	public static String SPOGLI_WKI_6_6 ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/WKI2016-246FATTI-integrati_6giugno.tsv";
	
	
	
	public static String EDIT_MANUALI_FIX_CONSEGNA_27_MAGGIO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/edit-manuali/fix-revisione-consegna-1_7-27-maggio.tsv";

	
	public static String DATA_SAME_AS = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/SAME_AS/SAME_AS_2019-03-18.tsv";
	

	public static String DATA_INPUT_OLD = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data_old/Note-Merito-CSM-Dogi-22-06-2018.tsv";
	
	public static String headerProvvedimento = "doc-principali"+SEP+"anno" + SEP + "eli-provvedimento" + SEP + "categoria-GU" + SEP + "estremi-provv" + SEP + "emettitore" + SEP + "titolo-provv" + SEP + "data-pubb-provv"+  SEP + "redazionale-provv"+SEP+"pagina-GU"+SEP+"riferimento-GU"+SEP+"urlProvvedimento";
	public static String headerFascicolo = "eli-GU"+SEP+"serie-GU"+SEP+"nome-serie-GU"+SEP+"titolo-GU"+SEP+"num-GU"+SEP+"data-pubb-GU";
	
	public static String header = headerProvvedimento+SEP+headerFascicolo;
	
	
	public static String folderCONSEGNATI ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/CONSEGNATI";
	
	public static String folderSCARTATI ="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/SCARTATI";

	public static String folderRISPOSTE = "/Users/tommaso/Work/Convenzione-MERITO-CSM/RISPOSTE";
		

	
	public static String WKI_NAS_TRIB ="WKI_NAS_TRIB";
	public static String WKI_NAS_APP="WKI_NAS_APP";
	public static String DEJURE_NAS_APP="DEJURE_NAS_APP";
	public static String DEJURE_NAS_TRIB="DEJURE_NAS_TRIB";
	public static String DEJURE_COMM_APP="DEJURE_COMM_APP";
	public static String DEJURE_COMM_TRIB="DEJURE_COMM_TRIB";
	
	

		
	public static String HEADER_GENNUSO ="idITTIG"+SEP
										+"isEstremoCompleto"+SEP
										//+"isEstremoCompletoAUTO"+SEP
										//+"isEstremoCompletoEDIT"+SEP
										+"isProvvedimentoConsegnato"+SEP
										+"isProvvedimentoTrovato"+SEP
										+"distretto"+SEP
										+"anno"+SEP
										+"tipo_ufficio"+SEP
										+"localita_ufficio"+SEP
										+"tipo_provvedimento"+SEP
										+"settore"+SEP
										+"provv_data"+SEP
										+"provv_numero"+SEP
										+"provv_sez"+SEP
										+"giudici"+SEP
										+"parti"+SEP
										+"num_registro"+SEP
										+"estremo_provv"+SEP
										+"titolo_contributo"+SEP
										+"fonte"+SEP
										+"nota"+SEP
										+"provenienza"+SEP
										+"versione";
	

	
	
	
	
	
	public static String HEADER_GENNUSO_FINAL ="idITTIG"+SEP
			+"restituire_definitivo"+SEP
			+"isEstremoCompleto"+SEP
			//+"isEstremoCompletoAUTO"+SEP
			//+"isEstremoCompletoEDIT"+SEP
			+"isProvvedimentoConsegnato"+SEP
			+"isProvvedimentoTrovato"+SEP
			+"distretto"+SEP
			+"anno"+SEP
			+"tipo_ufficio"+SEP
			+"localita_ufficio"+SEP
			+"tipo_provvedimento"+SEP
			+"settore"+SEP
			/*+"provv_data"+SEP*/
			+"data_decisione"+SEP
			+"data_pubblicazione"+SEP
			+"provv_numero"+SEP
			+"provv_sez"+SEP
			+"giudici"+SEP
			/*+"parti"+SEP*/
			+"num_registro"+SEP
			+"voci_classificazione"+SEP
			+"estremo_provv"+SEP
			+"titolo_contributo"+SEP
			+"fonte"+SEP
			+"nota"+SEP
			+"provenienza"+SEP
			+"versione"+SEP
			+"filePath"+SEP
			+"eli"+SEP
			+"eli_GU";

	

	
	
	public static String HEADER_SINGLE_ROW_PROVV_FINAL ="idITTIG"+SEP
			 +"restituire_definitivo"+SEP
			 +"id_Provv"+SEP
			 +"isEstremoCompleto"+SEP
			 +"isEditManuale"+SEP
			 +"isProvvedimentoConsegnato"+SEP
			 +"dataConsegnaProvvedimento"+SEP
			 +"isProvvedimentoTrovato"+SEP
			 //+"isEsitoEdit"+SEP
			 //+"isEstremoCompletoAUTO"+SEP
			 //+"isEstremoCompletoEDIT"+SEP
			 //+"pagina"+SEP
			 +"distretto"+SEP
			 +"anno"+SEP
			 +"tipo_ufficio"+SEP
			 +"localita_ufficio"+SEP
			 +"tipo_provvedimento"+SEP
			 +"settore"+SEP
			 +"provv_data"+SEP
			 +"provv_numero"+SEP
			 +"provv_sez"+SEP
			 +"giudici"+SEP
			 +"parti"+SEP
			 +"num_registro"+SEP
			 +"data_decisione"+SEP
			 +"data_pubblicazione"+SEP
			 +"voci_classificazione"+SEP
			 +"estremo_uniforme"+SEP
			 +"estremo_provv"+SEP
			 +"url_provv"+SEP
			 +"fonte"+SEP
			 +"url_contributo"+SEP
			 +"rivista"+SEP
			 +"editore"+SEP
			 +"findable_IN"+SEP
			 +"classificazioneTOP"+SEP
			 +"classificazione"+SEP
			 +"nota"+SEP
			 +"provenance"+SEP
			 +"localFilePath"+SEP
			 +"filePath"+SEP
			 +"notaRisposta"+SEP
			 +"versione";
	
	public static String HEADER_SINGLE_ROW_PROVV ="idITTIG"+SEP
												 +"id_Provv"+SEP
												 +"isEstremoCompleto"+SEP
												 +"isEditManuale"+SEP
												 +"isProvvedimentoConsegnato"+SEP
												 +"dataConsegnaProvvedimento"+SEP
												 +"isProvvedimentoTrovato"+SEP
												 //+"isEsitoEdit"+SEP
												 //+"isEstremoCompletoAUTO"+SEP
												 //+"isEstremoCompletoEDIT"+SEP
												 //+"pagina"+SEP
												 +"distretto"+SEP
												 +"anno"+SEP
												 +"tipo_ufficio"+SEP
												 +"localita_ufficio"+SEP
												 +"tipo_provvedimento"+SEP
												 +"settore"+SEP
												 +"provv_data"+SEP
												 +"provv_numero"+SEP
												 +"provv_sez"+SEP
												 +"giudici"+SEP
												 +"parti"+SEP
												 +"num_registro"+SEP
												 +"estremo_uniforme"+SEP
												 +"estremo_provv"+SEP
												 +"url_provv"+SEP
												 +"fonte"+SEP
												 +"url_contributo"+SEP
												 +"rivista"+SEP
												 +"editore"+SEP
												 +"findable_IN"+SEP
												 +"classificazioneTOP"+SEP
												 +"classificazione"+SEP
												 +"nota"+SEP
												 +"provenance"+SEP
												 +"localFilePath"+SEP
												 +"notaRisposta"+SEP
												 +"voci_classificazione_CSM"+SEP
												 +"versione";
	
	public static String HEADER_EXPLODED_PROVV ="idITTIG"+SEP
			 								   +"id_Provv"+SEP
											   +"isEstremoCompleto"+SEP
											   +"pagina"+SEP
											   +"distretto"+SEP
											   +"anno"+SEP
											   +"tipo_ufficio"+SEP
											   +"localita_ufficio"+SEP
											   +"tipo_provvedimento"+SEP
											   +"settore"+SEP
											   +"provv_data"+SEP
											   +"provv_numero"+SEP
											   +"provv_sez"+SEP
											   +"giudici"+SEP
											   +"parti"+SEP
											   +"num_registro"+SEP
											   +"estremo_uniforme"+SEP
											   +"estremo_provv"+SEP
											   +"url_provv"+SEP
											   +"idContributo"+SEP
											   +"fonte"+SEP
											   +"url_contributo"+SEP
											   +"titolo_contributo"+SEP
											   +"autori"+SEP
											   +"rivista"+SEP
											   +"editore"+SEP
											   +"findable_IN"+SEP
											   +"classificazioneTOP"+SEP
											   +"nota"+SEP
											   +"provenance"+SEP
											   +"versione";
		

	public static String HEADER_OMEKA =  "dcterms:identifier"+Config.SEP
							+"dcterms:conformsTo"+Config.SEP // estremoCompleto
							+"bibo:court"+Config.SEP     	// distretto
							+"dcterms:issued"+Config.SEP 	// anno_provv
							+"dcterms:creator"+Config.SEP	// tipo_ufficio
							+"dcterms:coverage"+Config.SEP	// localita_ufficio
							+"dcterms:type"+Config.SEP		// tipo_doc
							+"dcterms:title"+Config.SEP		// estremo_text
							+"dcterms:subject"+Config.SEP	// area
							+"dcterms:date"+Config.SEP      // data_provv
							+"bibo:number"+Config.SEP		// numero_provv
							+"bibo:section"+Config.SEP      // sezione
							+"dcterms:contributor"+Config.SEP // giudici
							+"dcterms:hasPart"+Config.SEP     // parti
							+"bibo:coden"+Config.SEP		// numRG
							+"bibo:uri"+Config.SEP			// url_provvedimento
							+"dcterms:bibliographicCitation"+Config.SEP	// fonte
							+"bibo:editor"+Config.SEP 	// rivista
							+"dcterms:publisher"+Config.SEP	// editore
							+"bibo:content"+SEP
							+"dcterms:provenance"+Config.SEP	//provenance Gennuso
							+"dcterms:hasVersion"; 	// dataset_version
	
	public static String HEADER_OMEKA_1_8 =  "dcterms:identifier"+Config.SEP
			+"dcterms:conformsTo"+Config.SEP // estremoCompleto
			+"bibo:court"+Config.SEP     	// distretto
			+"dcterms:issued"+Config.SEP 	// anno_provv
			+"dcterms:creator"+Config.SEP	// tipo_ufficio
			+"dcterms:coverage"+Config.SEP	// localita_ufficio
			+"dcterms:type"+Config.SEP		// tipo_doc
			+"dcterms:title"+Config.SEP		// estremo_text
			+"dcterms:subject"+Config.SEP	// area
			+"dcterms:date"+Config.SEP      // data_provv
			+"bibo:number"+Config.SEP		// numero_provv
			+"bibo:section"+Config.SEP      // sezione
			+"dcterms:contributor"+Config.SEP // giudici
			//+"dcterms:hasPart"+Config.SEP     // parti
			+"bibo:coden"+Config.SEP		// numRG
			+"bibo:uri"+Config.SEP			// url_provvedimento
			+"dcterms:bibliographicCitation"+Config.SEP	// fonte
			+"bibo:editor"+Config.SEP 	// rivista
			+"dcterms:publisher"+Config.SEP	// editore
			+"bibo:content"+SEP
			+"dcterms:tableOfContents"+SEP
			+"dcterms:dateSubmitted"+SEP
			+"dcterms:provenance"+Config.SEP	//provenance Gennuso
			+"dcterms:hasVersion"; 	// dataset_version
	
	// COME AGGIORNARE?
	// - voci classificazione CSM
	// - data consegna richiesta acquisizione
	// 
	
//	String HEADER_OMEKA =  "dcterms:identifier"+Config.SEP
	// dcterms:conformsTo  +isEstremoCompleto
//	+"bibo:court"+Config.SEP     	// distretto
//	+"dcterms:issued"+Config.SEP 	// anno_provv
//	+"dcterms:creator"+Config.SEP	// tipo_ufficio
//	+"dcterms:coverage"+Config.SEP	// localita_ufficio
//	+"dcterms:type"+Config.SEP		// tipo_doc
//	+"dcterms:title"+Config.SEP		// estremo_text
//	+"dcterms:subject"+Config.SEP	// area
	// + dcterms:date // data estesa
	// + bibo:number
	// + bibo:section
	// + dcterms:contributor  // giudici
	// + dcterms:hasPart   // parti
	// + bibo:coden // numRG
	// + bibo:uri
//	+"dcterms:bibliographicCitation"+Config.SEP	// fonte
	//	+bibo:editor  // rivista
//	+"dcterms:publisher"+Config.SEP	// editore
//	+"dcterms:provenance"+Config.SEP	//provenance Gennuso
//	+"dcterms:hasVersion"; 	// dataset_version
	
	
	// altre flag? 
	// estremo_completo S/N ?
	// provv_richiesto/consegnato ? 
	// etc

		
}
