package config; 

public class ConfigSpogli {

//  28-06-2018
//	public static String DATA_INPUT_NOTE = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/EDIT-CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-AUTORI-json.tsv";
//	
//	public static String DATA_INPUT_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-SISTEMATICI-json.tsv";
//
//	public static String DATA_INPUT_DESCRITTORI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-28-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-DESCRITTORI-json.tsv";
//
//	public static String DATA_INPUT_FONTI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/CSM-Dogi-29-06-2018-Note-a-sentenza-dal-2014-con-filtro-testuale-FONTI-json.tsv";
//
//	public static String TAB_SISTEMATICI = "/Users/tommaso/Work/workspaces/ITTIG-lab/archivio-merito-csm/data/DOGI-CSM-28-06-source-tsv/tabSistematici-json.tsv";

	public static String DATASET_VERSION ="2.0";
	public static String serializedProvvMap = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/data/provvMap2_0.ser";


	public static String SEP = "\t";
	public static String MULTI_VALUE_SEP = "#"; //"; ""#"; // "|";
	
	public static String PEN_lookup = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/PEN-lookup.tsv";
	public static String CIV_lookup = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/CIV-lookup.tsv";
	public static String PROCIV_lookup = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/PROCIV-lookup.tsv";
	
	public static String ALLINEA_CIV = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/ALLINEAMENTO-Classificazioni-CIVILE.tsv";
	public static String ALLINEA_PEN = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/ALLINEAMENTO-Classificazioni-PENALE.tsv";
	public static String ALLINEA_PEN_DEJURE = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/ALLINEAMENTO-Classificazioni-PENALE_DeJure.tsv";
	

	
	public static String NASH_PREFIX = "http://nash.ittig.cnr.it/risposte-csm/";


	// 	1) visualizzazione FINALE estesa

	public static String HEADER_SINGLE_ROW_PROVV_RESTITUZIONE_ESTESO ="idITTIG"+SEP
			//+"idITTIG_fix"+SEP
			+"isEstremoCompleto"+SEP
			+"isProvvedimentoTrovato"+SEP
			+"distretto"+SEP
			+"anno"+SEP
			+"tipo_ufficio"+SEP
			+"localita_ufficio"+SEP
			+"tipo_provvedimento"+SEP
			+"settore"+SEP
			+"provv_data"+SEP
			+"provv_numero"+SEP
			+"provv_sez"+SEP
			+"giudici"+SEP
			+"parti"+SEP
			+"num_registro"+SEP
			+"estremo_uniforme"+SEP
			+"url_provv"+SEP
			+"fonte"+SEP
			+"url_contributo"+SEP
			+"rivista"+SEP
			+"editore"+SEP
			+"nota"+SEP
			+"provenance"+SEP
			+"localFilePath"+SEP
			+"notaRisposta"+SEP
			+"classificazioneTOP"+SEP
			+"voci_classificazione_CSM"+SEP
			+"classificazione_FINALE"+SEP
			+"versione";

	
	
	public static String HEADER_SINGLE_ROW_PROVV_SPOGLIO_FINALE ="idITTIG"+SEP
			 +"restituire_definitivo"+SEP
			 +"nota_spoglio_ITTIG"+SEP
			 //+"spogliato"+SEP
			 +"dataConsegnaProvvedimento"+SEP
			 +"isProvvedimentoTrovato"+SEP
			 +"notaRisposta"+SEP
			 +"estremo_uniforme"+SEP
			 +"local_pdf"+SEP
			 +"titolo_contributo"+SEP
			 +"fonte"+SEP
			 +"distretto"+SEP
			 +"anno"+SEP
			 +"tipo_ufficio"+SEP
			 +"localita_ufficio"+SEP
			 +"tipo_provvedimento"+SEP
			 +"settore"+SEP
			 +"provv_sez"+SEP
			 +"provv_numero"+SEP
			 +"num_registro"+SEP
			 +"provv_data"+SEP
			 +"*data_decisione"+SEP
			 +"*data_pubblicazione"+SEP
			 +"giudici"+SEP
			 +"*presidente"+SEP
			 +"*relatore_estensore"+SEP
			 +"*voci_classificazione_finale"+SEP
			 +"descrittoriTOP"+SEP
			 +"classiCSM"+SEP
			 +"url_provv"+SEP
			 +"url_contributo"+SEP
			 +"rivista"+SEP
			 +"editore"+SEP
			 +"provenance"+SEP
			 +"versione";
	
	
	


	
	
	public static String HEADER_GENNUSO_FINAL_RESTITUZIONE ="idITTIG"+SEP
//			+"restituire_definitivo"+SEP
//			+"isEstremoCompleto"+SEP
//			+"isProvvedimentoConsegnato"+SEP
//			+"isProvvedimentoTrovato"+SEP
			+"distretto"+SEP
			+"anno"+SEP
			+"tipo_ufficio"+SEP
			+"localita_ufficio"+SEP
			+"tipo_provvedimento"+SEP
			+"settore"+SEP
			/*+"provv_data"+SEP*/
			+"data_decisione"+SEP
			+"data_pubblicazione"+SEP
			+"provv_numero"+SEP
			+"num_registro"+SEP
			+"provv_sez"+SEP
			+"presidente"+SEP
			+"relatore_estensore"+SEP
			+"voci_classificazione"+SEP
			+"estremo_provv"+SEP
			+"titolo_contributo"+SEP
			+"fonte"+SEP
			//+"nota"+SEP
			+"provenienza"+SEP
			+"versione"+SEP
			+"filePath";
//			+SEP
//			+"eli"+SEP
//			+"eli_GU";

	
	public static String SPOGLI_1_ANCONA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_ANCONA(40)_5settembre.tsv";
	public static String SPOGLI_1_BARI = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_BARI(273)_5settembre.tsv";
	public static String SPOGLI_1_BOLOGNA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_BOLOGNA(240)_5settembre.tsv";
	public static String SPOGLI_1_BRESCIA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_BRESCIA(119)_28agosto.tsv";
	public static String SPOGLI_1_CALTANISSETTA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_CALTANISSETTA(30)_28agosto.tsv";
	public static String SPOGLI_1_FIRENZE = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_FIRENZE(470)_5settembre.tsv";
	public static String SPOGLI_1_GENOVA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_GENOVA(97)_5settembre.tsv";
	public static String SPOGLI_1_LAQUILA = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_LAQUILA(115)_5settembre.tsv";
	public static String SPOGLI_1_TRIESTE = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE-tabella_spogli_TRIESTE(130)_28agosto.tsv";
	public static String SPOGLI_1_PALERMO = "/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_tabella_spogli_PALERMO(213)_5settembre.tsv";

	public static String SPOGLI_2_POTENZA="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_POTENZA(77)_5settembre.tsv";
	public static String SPOGLI_2_MILANO="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_MILANO(29)_5settembre.tsv";
	public static String SPOGLI_2_CATANIA="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_CATANIA(18)_5settembre.tsv";
	public static String SPOGLI_2_VENEZIA="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_VENEZIA(272)_5settembre.tsv";
	public static String SPOGLI_2_SALERNO="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_SALERNO(23)_5settembre.tsv";
	public static String SPOGLI_2_PERUGIA="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_PERUGIA(112)_5settembre.tsv";
	public static String SPOGLI_2_TRENTO="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_TRENTO(57)_5settembre.tsv";
	public static String SPOGLI_2_CAGLIARI="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_CAGLIARI(69)_5settembre.tsv";
	public static String SPOGLI_2_NAPOLI="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_NAPOLI(307)_5settembre.tsv";
	public static String SPOGLI_2_LECCE="/Users/tommaso/Work/workspaces/ITTIG-LAB/archivio-merito-csm/dataSpogli/spogli_finali/DONE_2_tabella_spogli_LECCE(34)_5settembre.tsv";




	
}
