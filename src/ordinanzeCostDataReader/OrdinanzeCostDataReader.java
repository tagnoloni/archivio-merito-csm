package ordinanzeCostDataReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import config.Config;
import fonte.Fonte;
import provvedimento.Provvedimento;
import util.DistrettiCA;
import util.Util;

public class OrdinanzeCostDataReader {



	HashMap<String,Provvedimento> provvedimenti = new HashMap<String,Provvedimento>();

	
	
	
	int idProvvedimento =1;

	
	
	
	private String encodeTipoDoc(String tipoDoc) {

		String ret ="";
		switch(tipoDoc.toLowerCase()) {
		case "sentenza": 
			ret="JUDGMENT";
			break;
		case "ordinanza": 
			ret="ORDER";
			break;
		case "decreto":
			ret="DECREE";
			break;
		default:
			ret = tipoDoc;
		}

		return ret;
	}
	
	public boolean readOrdinanzeCostTSV(String tsvFilePath) {
		
		
		boolean online = false;
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

			
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				//SKIP HEADER
				if(line.startsWith("eli-provv"))
					continue;
				
				String[] fields = line.split("\t");
		
				
				String eli_provvedimento = fields[0].trim();
				String estremi_provv  = fields[1].trim();
				String data_ordinanza = fields[2].trim();
				String titolo_provv_gazzetta = fields[3].trim().replaceAll("\\s+", " ");
				
				String redazionale_provv = fields[5].trim();  // trasforma in aaaa-mm-gg
				String eli_GU = fields[6].trim();
				String eli_GU_pdf = fields[7].trim();
				String nome_serie_GU = fields[8].trim();
				String fascicolo_GU = fields[9].trim();
				String numeroGU = fields[10].trim();
				String data_pubb_GU = fields[11].trim();
				String guessedSubject = getGuessedSubject(titolo_provv_gazzetta);
				String giudici ="";
				//https://www.gazzettaufficiale.it/atto/vediMenuHTML?atto.dataPubblicazioneGazzetta=2014-06-18&atto.codiceRedazionale=14C00140&tipoSerie=corte_costituzionale&tipoVigenza=originario
				
				String urlFullText = "https://www.gazzettaufficiale.it/atto/vediMenuHTML?atto.dataPubblicazioneGazzetta="+data_pubb_GU+"&atto.codiceRedazionale="+redazionale_provv+"&tipoSerie=corte_costituzionale&tipoVigenza=originario";
				
				String classificazioni = "";
				if(online) {
					cacheFullText(urlFullText,  redazionale_provv); 
					//classificazioni = getClassificazioniFullTextLIVE(urlFullText);
				}else {
					classificazioni = getClassificazioniFullTextLOCAL(redazionale_provv);
					giudici= getGiudiciFromFullTextLOCAL(redazionale_provv);
				}
				
				String estremoText =cutEstremoText(titolo_provv_gazzetta);
				
//				String classificazioni = "";
//				String titolo_provv_gazzetta_2 = titolo_provv_gazzetta;
//				try{
//					classificazioni = getTitolettiCOST(titolo_provv_gazzetta);
//				}catch(Exception e) {
//					 titolo_provv_gazzetta_2 = titolo_provv_gazzetta_2.substring(titolo_provv_gazzetta_2.indexOf("-")+1, titolo_provv_gazzetta_2.length());
//					 try {
//						 classificazioni = getTitolettiCOST(titolo_provv_gazzetta_2);
//					 }catch(Exception ex) {
//						 
//					 }
//				}
				

				// AGGIUNGERE CAMPI PER SPOGLIO: estremo completo, giudici, parti, numRG
				Provvedimento p = new Provvedimento();
				if(estremoText.contains("Appello"))
					estremoText=estremoText.replace("Appello", "appello");
				p.setEstremoDogi(estremoText);  // QUI FA CHIAMATA A LINKOLN
				
				String normalizedDate = textToDate(data_ordinanza);
				String authority_Code = p.getAuthority();
				if(estremoText.toLowerCase().contains("sorveglianza"))
					authority_Code = "IT_TSV";
				if(estremoText.toLowerCase().contains("minorenn"))
					authority_Code = "IT_TMN";

				
				p.setIdProvvedimento(redazionale_provv); 
				p.setDocType("ORDINANZA RIMESSIONE COST.");
				p.setEstremoText(estremoText);
				
				String localita_ufficio = p.getGeo_name();
				if(localita_ufficio.trim().length()>0)
					p.setGeo_name(localita_ufficio);
				else {
					p.setGeo_name(getLocalitaUfficioFromEstremo(estremoText));
				}
				
				
//				SEZIONE e NUMERO non disponibili				
//				p.setCourt_section(provv_sezione);
//				p.setProvv_number(provv_numero);
				
			//	DATE TUTTE UGUALI	
				p.setProvv_date(normalizedDate);
				p.setDataDecisione(normalizedDate);
				p.setDataPubblicazione(normalizedDate);
				
				p.setProvv_year(normalizedDate.substring(0, 4));
				
				p.setAuthority(authority_Code);
				p.setUrl(eli_provvedimento);
				p.setGuessed_subject(guessedSubject);		
				p.setProvenance("GAZZETTA_UFFICIALE");
				DistrettiCA distretti = new DistrettiCA();
				p = distretti.setDistrettoAppello(p);
				p.setPersistentID(Util.computeNewID_COST(p));
				
				String contrib_fonte = "Gazzetta Ufficiale "+nome_serie_GU+" "+fascicolo_GU;
				//GU 1a Serie Speciale - Corte Costituzionale n.40 del 24-9-2014
				
//				ArrayList<String> titoletti = new ArrayList<String>();
//			       titoletti.add(classificazioni);
			    
				p.setClassificazioniCSMFinal(getClassificazioniTOP(classificazioni));

				p.setGiudici(giudici);
				
				//p.setNota(eli_provvedimento);
				
				p.setFilePath(urlFullText);
				p.setUrl(eli_provvedimento);
				p.setEli_GU_Pdf(eli_GU_pdf);
			
				Fonte f = new Fonte();
				f.setIdContributo(redazionale_provv);					
				f.setTitoloContributo("");
//				
//				// PROVVEDIMENTI NON MASSIMATI
//				if(contrib_fonte.trim().length()==0)
//					contrib_fonte = "Dejure, Giuffrè";
//				
				f.setRivista(contrib_fonte);
//			
//				f.setFindableIn("GFL");
//				f.setEstremoText(contrib_fonte);     	
				f.setUrl(eli_GU);
				f.setEditore("");
	
				
				// SE NON LE METTO NEI SISTEMATICI POI ME LE CANCELLA QUANDO FA NORMALIZZAZIONE
				//p.setClassificazioniTOP(getClassificazioni(classificazioni));
				//f.setSistematici(getClassificazioni(classificazioni).toArray(new String[0]));
				//f.setDescrittori(new String[]{});

				p.addFonte(f);
			
			
				if(provvedimenti.get(p.getPersistentID())==null)
					provvedimenti.put(p.getPersistentID(), p);
				else {
					System.out.println("++++ DUP "+p.getPersistentID());
				}
				
			}
			System.err.println("processed "+i+" rows");

		}catch(Exception e){
			e.printStackTrace();
		}

	
		return true;
			
	}


	
	private String[] getClassificazioniTOP(String titoletti){
		
		ArrayList<String> classificazioniTOP = new ArrayList<String> ();
		
		String[] titolettiSplit = titoletti.split(" - ");
		for(int i=0; i<2;i++) {
			classificazioniTOP.add(titolettiSplit[i].trim().replaceAll("\\s+", " "));
		}
		
		return classificazioniTOP.toArray(new String[0]);
	}
	
	
	private ArrayList<String> getClassificazioni(String titoletti){
		
		ArrayList<String> classificazioniTOP = new ArrayList<String> ();
		
		String[] titolettiSplit = titoletti.split(" - ");
		for(int i=0; i<titolettiSplit.length;i++) {
			classificazioniTOP.add(titolettiSplit[i].trim().replaceAll("\\s+", " "));
		}
		
		
		return classificazioniTOP;
	}

	private void cacheFullText(String urlFullText, String id) throws Exception{

		int millis =10000;
		URL url = new URL(urlFullText);
		Document fullText = Jsoup.parse(url,millis);	

		// FIXME qui salva la pagina parsata da JSOUP - bisogna salvare l'html raw non parsato
		savePage(Config.CACHE_ORDINANZE_COST+"/"+id+".html", fullText.html());


	}

	
	private String getClassificazioniFullTextLIVE(String urlFullText) throws Exception{
		
		int millis =10000;
		URL url = new URL(urlFullText);
		Document doc = Jsoup.parse(url,millis);	

		Element pre = doc.select("pre").first();

		String text = pre.text();
		
		String[] split = text.split("\n \n");
		//System.out.println(split.length);
		String classificazioni_rif = split[1];
		
		String[] classificazioniSplit = classificazioni_rif.split(" \n-");
		String classificazioni = classificazioniSplit[0];
		//System.out.println(classificazioni);

		return classificazioni.replaceAll("\n", " ");
	}
	
	
	private String getClassificazioniFullTextLOCAL(String id) throws Exception{
		
		File page = new File(Config.CACHE_ORDINANZE_COST+"/"+id+".html");
		Document doc = Jsoup.parse(page, "UTF-8", "http://www.gazzettaufficiale.it");	

		
		//Document doc = Jsoup.parse(id+".html");	

		Element pre = doc.select("pre").first();

		String text = pre.text();
		
		String[] split = text.split("\n \n");
		//System.out.println(split.length);
		String classificazioni_rif = split[1];
		
		String[] classificazioniSplit = classificazioni_rif.split(" \n-");
		String classificazioni = classificazioniSplit[0];
		//System.out.println(classificazioni);

		return classificazioni.replaceAll("\n", " ");
	}
	
	
	

	
	
	private  void savePage(String pageName, String content){

		BufferedWriter writer = null;
		try
		{
			//  writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Config.GU_PAGES_SG+"/"+pageName+".html")/*, "Cp1252"*/));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pageName)/*, "Cp1252"*/));
			writer.write(content);
		}
		catch ( IOException e)
		{
		}
		finally
		{
			try
			{
				if ( writer != null)
					writer.close( );
			}
			catch ( IOException e)
			{
			}
		}
	}

	private String getTitolettiCOST_OLD(String titolo_provv_gazzetta) throws Exception {
		
		
		String titoletti = titolo_provv_gazzetta.substring(titolo_provv_gazzetta.indexOf("-"), titolo_provv_gazzetta.length());
		int firstIndex = titolo_provv_gazzetta.indexOf("-");
		
		String descriptor="";
		int k=firstIndex-1;
		do {
			
			   char current = titolo_provv_gazzetta.charAt(k);
			   descriptor = ""+current+descriptor;
			   k--;
		}while(titolo_provv_gazzetta.charAt(k)!='.');
		
		
		return descriptor+" "+titoletti;
	}
	
	
	
	
	private String cutEstremoText(String titolo_provv_gazzetta) {
		
		String estremoText ="";
		if(!titolo_provv_gazzetta.contains("G.I.P.") && !titolo_provv_gazzetta.contains("G.U.P.") && !titolo_provv_gazzetta.contains("Sez.")) {
			 estremoText = titolo_provv_gazzetta.substring(0, titolo_provv_gazzetta.indexOf("."));
		}
		else {
			 String token1 = titolo_provv_gazzetta.substring(0, titolo_provv_gazzetta.indexOf(".")+5);
			 String token2 = titolo_provv_gazzetta.substring(titolo_provv_gazzetta.indexOf(".")+5,titolo_provv_gazzetta.length());
			 String token3 = token2.substring(0,token2.indexOf("."));
			 estremoText = token1+token3;
		}
		
		if(estremoText.indexOf("nel procedimento")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("nel procedimento"));
		if(estremoText.indexOf("nei procedimenti")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("nei procedimenti"));
		
		
		if(estremoText.indexOf("sul ricorso proposto")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sul ricorso proposto"));
		if(estremoText.indexOf("sull'istanza proposta")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sull'istanza proposta"));
		if(estremoText.indexOf("sul reclamo proposto")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sul reclamo proposto"));
		if(estremoText.indexOf("sul ricorso poposto")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sul ricorso poposto"));
		if(estremoText.indexOf("nel giudizio di responsabilit")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("nel giudizio di responsabilit"));
		if(estremoText.indexOf("sul giudizio relativo")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sul giudizio relativo"));
		if(estremoText.indexOf("sulle istanze proposte")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sulle istanze proposte"));
		if(estremoText.indexOf("sui ricorsi proposti")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sui ricorsi proposti"));
		if(estremoText.indexOf("sulle istanze riunite")!=-1)
			estremoText=estremoText.substring(0,estremoText.indexOf("sulle istanze riunite"));
	
		
		return estremoText.trim();
	}
	
	
	private String getLocalitaUfficioFromEstremo(String estremoText) {
		
		String localita ="";
		estremoText = estremoText+" ";
		if(estremoText.indexOf("sorveglianza di")!=-1) {
			estremoText = estremoText.substring(estremoText.indexOf("sorveglianza di")+16);
			localita= estremoText.substring(0, estremoText.indexOf(" ")).trim();
		}
		else if(estremoText.indexOf("Tribunale di")!=-1) {
			estremoText = estremoText.substring(estremoText.indexOf("Tribunale di")+13);
			localita= estremoText.substring(0, estremoText.indexOf(" ")).trim();
		}
		else if(estremoText.indexOf("Tribuanle di")!=-1) {
			estremoText = estremoText.substring(estremoText.indexOf("Tribuanle")+13);
			localita= estremoText.substring(0, estremoText.indexOf(" ")).trim();
		}else {
			localita = "unknown";
		}
		
		if(localita.trim().equalsIgnoreCase("vibo"))
			localita = "Vibo Valentia";
		if(localita.trim().equalsIgnoreCase("torre"))
			localita = "Torre Annunziata";
		if(localita.trim().equalsIgnoreCase("busto"))
			localita = "Busto Arsizio";
		if(localita.trim().equalsIgnoreCase("nocera"))
			localita = "Nocera Inferiore";
		
		return localita;
	}


	private String getGuessedSubject(String titolo) {

		
		if(titolo.toLowerCase().contains("procedimento civile")|| titolo.toLowerCase().contains("procedimenti civili") )
			return "CIVILE";
		if(titolo.toLowerCase().contains("procedimento penale")|| titolo.toLowerCase().contains("procedimenti penali") )
			return "PENALE";
		if(titolo.toLowerCase().contains("procedimento di sorv"))
			//return "SORVEGLIANZA";
			return "PENALE";
		if(titolo.toLowerCase().contains("procedimento di esecuzione")) {
			if(titolo.toLowerCase().contains("esecuzione civile") || titolo.toLowerCase().contains("pignoramento"))
				return "CIVILE";
			else
				return "PENALE";
		}
			
		
		if(titolo.toLowerCase().contains("ricorso proposto")|| titolo.toLowerCase().contains("istanza proposta") 
				|| titolo.toLowerCase().contains("reclamo proposto")
				|| titolo.toLowerCase().contains("ricorso poposto")
				||  titolo.toLowerCase().contains("nel giudizio di responsabilit")
				|| titolo.toLowerCase().contains("sul giudizio relativo")
				|| titolo.toLowerCase().contains("sulle istanze proposte")
				||  titolo.toLowerCase().contains("sui ricorsi proposti")
				||  titolo.toLowerCase().contains("sulle istanze riunite")
				
				 
				)
			return "CHECK";
		
		return "CHECK??";

	}
	
	private String textToDate(String data) {
		
		String giorno = data.split(" ")[0].trim();
		String mese = data.split(" ")[1].trim();
		String anno = data.split(" ")[2].trim();


		if(giorno.trim().length()==1)
			giorno="0"+giorno;
		
		switch(mese.toLowerCase()) {
		case "gennaio": mese="01";
		break;
		case "febbraio": mese="02";
		break;
		case "marzo": mese="03";
		break;
		case "aprile": mese="04";
		break;
		case "maggio": mese="05";
		break;
		case "giugno": mese="06";
		break;
		case "luglio": mese="07";
		break;
		case "agosto": mese="08";
		break;
		case "settembre": mese="09";
		break;
		case "ottobre": mese="10";
		break;
		case "novembre": mese="11";
		break;
		case "dicembre": mese="12";
		break;
		default: mese=" ";
		break;
	}
		
		return anno+"-"+mese+"-"+giorno;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public boolean printCivileDeJureGennusoStandard() {
		
		
		System.err.println(Config.HEADER_GENNUSO);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printGennusoSingleRowProvvedimento();
		
		}
		
		return true;
	}
	
	
	public HashMap<String,Provvedimento> getProvvedimentiCOST(){
		return this.provvedimenti;
	}
	
	
	public boolean printSingleRowStandard() {
		
		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printSingleRowProvvedimento();
		
		}
		return true;
	}
	
	
	public boolean printCivileDeJureSingleRowOmeka() {
		
	
		System.err.println(Config.HEADER_OMEKA);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printOMEKASingleRowProvvedimento();
		
		}
		return true;
	}
	
	
	public boolean printCivileDeJureSingleRow() {
		
		
		String header ="idITTIG"+Config.SEP
				+"isDuplicato"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"classeDeJure"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
//				+"giudici"+Config.SEP
//				+"parti"+Config.SEP
//				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"titolo"+Config.SEP
				+"testoMassima"+Config.SEP
				+"idContributo"+Config.SEP
				+"url_contributo"+Config.SEP
				+"fonte"+Config.SEP
				+"editore"+Config.SEP
				+"classificazioniTOP"+Config.SEP
				+"classificazioni"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		System.err.println(header);
		int count =0;
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			
			
				//printSingleRowDeJureCivileCSV(p);
				count++;
			
//			printExplodedLEXADOCDeJureCivileCSV(p);
		}
		
		System.err.println(count+" PROVVEDIMENTI in SELEZIONE");
		return true;
	}
	
	
	public void alphaOrderForMultipleCat() {
				
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getDeJureClasse().indexOf("#")!=-1)
				p.setDeJureClasse(alphaOrder(p.getDeJureClasse()));
			
		}
	}
	
	

	private String alphaOrder(String multipleCat) {
		String[] cat=multipleCat.split("#");
		String ret="";
		Arrays.sort(cat);
		for(int i = 0; i < cat.length-1; i++)
           ret+=cat[i]+"#";
		ret+=cat[cat.length-1];
		
		return ret;
	}
	
	
	
	private String getAutorita(String authority) {
		String ret="";
		if(authority==null)
			return null;
		switch(authority) {
			case "IT_TRB": 
				ret="TRIBUNALE";
				break;
			case "IT_TSV": 
				ret="TRIBUNALE DI SORVEGLIANZA";
				break;
			case "IT_CPP":
				ret="CORTE APPELLO";
				break;
// FIXME come sta la storia delle corti d'assise ?  
			case "IT_CSS":
				ret="CORTE ASSISE";
				//ret="CORTE APPELLO";
				break;
			case "IT_TMN":
				ret="TRIBUNALE DEI MINORI";
				break;
			case "IT_GPC":
				ret="GIUDICE DI PACE";
				break;
			case "IT_CASS":
				ret="CORTE DI CASSAZIONE";
				break;
			case "IT_COST":
				ret="CORTE COSTITUZIONALE";
				break;
			default:
				ret = null;
		}
		return ret;
	}
	
	
	private String printList(String[] list){
		
		String ret="";

		if(list!=null){
			for(int i=0;i<list.length;i++){
				ret+=list[i]+";";
			}
			if(ret.length()>0)
				ret=ret.substring(0, ret.length()-1).trim();
		}
		return ret;
	}
	
	public List<Provvedimento> getProvvedimenti() {
		List<Provvedimento> filtered_provv_list = new ArrayList<Provvedimento>();
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			if(p.getProvv_year()==null )
				filtered_provv_list.add(p);
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					filtered_provv_list.add(p);
			}
			
		}	
		return filtered_provv_list;
	}

	
	
	private String getGiudiciFromFullTextLOCAL(String id) throws Exception{

		File page = new File(Config.CACHE_ORDINANZE_COST+"/"+id+".html");
		Document doc = Jsoup.parse(page, "UTF-8", "http://www.gazzettaufficiale.it");	


		//Document doc = Jsoup.parse(id+".html");	

		Element pre = doc.select("pre").get(2);
		String stringa;

		String text = pre.text();
		String giudici ="";
		String giudice ="";
		String sep = "; ";
		if(text.indexOf("Il Presidente ed il Magistrato di Sorveglianza coestensori")!=-1) {
			stringa = text.substring(text.indexOf("Il Presidente ed il Magistrato di Sorveglianza coestensori"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente ed il Magistrato di Sorveglianza coestensori"), stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Presidente di sezione:")!=-1) {
			stringa = text.substring(text.indexOf("Il Presidente di sezione:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente di sezione:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice tutelare:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice tutelare:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice tutelare:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il consigliere estensore:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il consigliere estensore:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il consigliere estensore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		// PRESIDENTI
		if(text.indexOf("Il Presidente est.:")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente est.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente est.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il presidente est:")!=-1) {
			stringa = text.substring(text.indexOf("Il presidente est:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il presidente est:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il Presidente estensore:")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente estensore:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente estensore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il Presidente dott.")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente dott."), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente dott."), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il Presidente Rel. Est.:")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente Rel. Est.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente Rel. Est.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il Presidente ed il consigliere a latere estensori")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente ed il consigliere a latere estensori"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente ed il consigliere a latere estensori"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.indexOf("Il Presidente di sezione:")!=-1) {
			stringa=text.substring(text.indexOf("Il Presidente di sezione:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Presidente di sezione:"),stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}else if(text.toLowerCase().indexOf("il presidente")!=-1) {
			stringa=text.substring(text.toLowerCase().indexOf("il presidente"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il presidente"),stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		
		if(text.indexOf("Il giudice designato:")!=-1) {
			stringa = text.substring(text.indexOf("Il giudice designato:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il giudice designato:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Giudice delegato:")!=-1) {
			stringa = text.substring(text.indexOf("Il Giudice delegato:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Giudice delegato:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("I giudici est:")!=-1) {
			stringa = text.substring(text.indexOf("I giudici est:"), text.length());
			giudice=stringa.substring(stringa.indexOf("I giudici est:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Consigliere estensore:")!=-1) {
			stringa = text.substring(text.indexOf("Il Consigliere estensore:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Consigliere estensore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il giudice relatore:")!=-1) {
			stringa = text.substring(text.indexOf("Il giudice relatore:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il giudice relatore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("I Consiglieri:")!=-1) {
			stringa = text.substring(text.indexOf("I Consiglieri:"), text.length());
			giudice=stringa.substring(stringa.indexOf("I Consiglieri:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il G.U.P.:")!=-1) {
			stringa = text.substring(text.indexOf("Il G.U.P.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il G.U.P.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il GUP:")!=-1) {
			stringa = text.substring(text.indexOf("Il GUP:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il GUP:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il gip:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il gip:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il gip:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il g.i.p.:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il g.i.p.:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il g.i.p.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il funzionario giudiziario:")!=-1) {
			stringa = text.substring(text.indexOf("Il funzionario giudiziario:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il funzionario giudiziario:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice unico:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice unico:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice unico:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice del lavoro:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice del lavoro:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice del lavoro:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il GOT:")!=-1) {
			stringa = text.substring(text.indexOf("Il GOT:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il GOT:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il G.O.T.:")!=-1) {
			stringa = text.substring(text.indexOf("Il G.O.T.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il G.O.T.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il G.O.P.:")!=-1) {
			stringa = text.substring(text.indexOf("Il G.O.P.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il G.O.P.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice estensore:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice estensore:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice estensore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Giudice per le indagini preliminari:")!=-1) {
			stringa = text.substring(text.indexOf("Il Giudice per le indagini preliminari:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Giudice per le indagini preliminari:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Consigliere designato:")!=-1) {
			stringa = text.substring(text.indexOf("Il Consigliere designato:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Consigliere designato:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il Giudice a Latere:")!=-1) {
			stringa = text.substring(text.indexOf("Il Giudice a Latere:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Giudice a Latere:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
	
		if(text.indexOf("Il Consigliere relatore:")!=-1) {
			stringa = text.substring(text.indexOf("Il Consigliere relatore:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il Consigliere relatore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il g.e.:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il g.e.:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il g.e.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		
		if(text.toLowerCase().indexOf("il g. e.:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il g. e.:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il g. e.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("la giudice:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("la giudice:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("la giudice:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("la presidente:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("la presidente:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("la presidente:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice dell'udienza preliminare:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice dell'udienza preliminare:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice dell'udienza preliminare:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice u. des.:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice u. des.:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice u. des.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice on.:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice on.:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice on.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice/die richterin:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice/die richterin:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice/die richterin:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		
		if(text.toLowerCase().indexOf("il g.e. avv.")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il g.e. avv."), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il g.e. avv."), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("Il g.u.:")!=-1) {
			stringa = text.substring(text.indexOf("Il g.u.:"), text.length());
			giudice=stringa.substring(stringa.indexOf("Il g.u.:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice onorario:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice onorario:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice onorario:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice istruttore:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice istruttore:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice istruttore:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice onorario del tribunale di lecco:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice onorario del tribunale di lecco:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice onorario del tribunale di lecco:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.indexOf("La Consigliera designata:")!=-1) {
			stringa = text.substring(text.indexOf("La Consigliera designata:"), text.length());
			giudice=stringa.substring(stringa.indexOf("La Consigliera designata:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il magistrato di sorveglianza:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il magistrato di sorveglianza:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il magistrato di sorveglianza:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il consigliere designato:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il consigliere designato:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il consigliere designato:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
		if(text.toLowerCase().indexOf("il giudice dell'esecuzione:")!=-1) {
			stringa = text.substring(text.toLowerCase().indexOf("il giudice dell'esecuzione:"), text.length());
			giudice=stringa.substring(stringa.toLowerCase().indexOf("il giudice dell'esecuzione:"), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();	
		}
		if(text.indexOf("Il giudice \n dott.")!=-1) {
			stringa = text.substring(text.indexOf("Il giudice \n dott."), text.length());
			giudice=stringa.substring(stringa.indexOf("Il giudice \n dott."), stringa.indexOf("\n")!=-1?stringa.indexOf("\n"):stringa.length());
			if(!giudici.contains(giudice.trim()))		
				giudici=giudici+sep+giudice.trim();
		}
//		Il Giudice dell'esecuzione:
		// Il Magistrato di Sorveglianza: Cesaro
//		Il GOT: (1)
//		Il G.O.T.:
//		Il Giudice estensore:
//		Il Giudice per le indagini preliminari:
//		Il Consigliere designato:
//		Il Giudice a Latere:
//		Il Consigliere relatore:
//		Il G.E.:
//		Il g.u.:
//		Il giudice \n dott.
//		Il Giudice onorario del Tribunale di Lecco:
//		Il Giudice onorario:
//		La Consigliera designata:
		//Il Giudice:  (285)
	//	Il presidente (120)
		//Il giudice designato: (1)
		//Il Giudice delegato: (1)
//		Il presidente est: (3)
//		Il Presidente est.: (19)
		//I giudici est: (1)
		//Il Consigliere estensore: (9)
		//Il giudice relatore: (8)
		//I Consiglieri: (8)
		//Il G.U.P.: (5)
		//Il GUP:
		//Il G.I.P.: 
		//Il funzionario giudiziario: (7)
//		Il Presidente estensore: (19)
		//Il Giudice unico:
		//Il Giudice del lavoro: (23)
//		Il Presidente dott.
//		Il Presidente Rel. Est.:
//		Il Presidente ed il consigliere a latere estensori
//		Il Presidente di sezione: 	
		//Il Presidente ed il Magistrato di Sorveglianza coestensori 
//		Il GOT: (1)
//		Il G.O.T.:
//		Il Giudice estensore:
//		Il Giudice per le indagini preliminari:
//		Il Consigliere designato:
//		Il Giudice a Latere:
//		Il Consigliere relatore:
//		Il G.E.:
//		Il g.u.:
//		Il giudice \n dott.
//		Il Giudice onorario del Tribunale di Lecco:
//		Il Giudice per le indagini preliminari: 
//		Il Giudice onorario:
//		La Consigliera designata:
//		La Giudice: Jacqueline Monica Magi
//		Il Giudice/Die Richterin: Covi
//		La Presidente: Maffiodo 
//		Il Giudice u. des.: Braccialini
//		Il Giudice dell'udienza preliminare: Terzi
//		Il Giudice On.: Lanza
//		XIl giudice onorario del Tribunale di Lecco: Dott. Enrico Marradi
//		XIl Giudice istruttore: Orlandi
//		XIl Giudice Onorario: Accogli
//		XIl giudice unico: Carra
//		XIl Consigliere Designato: 
//		XIl GIP: De Robbio
//		X Il Giudice Unico: Beghini
//		X IL G.E.: Sisto
//		X Il Giudice Unico: Beghini
//		X Il G.O.P.: Serafini
//		X Il G.E. Avv. Luisa Sisto
//		X Il Consigliere estensore: Erlicher
//		XIl Giudice dell'Esecuzione: Pavich
//		X Il g.i.p.: Morello
//		X Il Giudice dell'Esecuzione: Di Paoli Paulovich
//		X Il giudice estensore: Reynaud
		
		
		if(giudici.startsWith(sep))
			giudici=giudici.substring(1,giudici.length()).trim();
		if(giudici.endsWith(sep))
			giudici=giudici.substring(0,giudici.length()-1).trim();
		
//		if(giudici.trim().length()>0) {	
//			System.out.println(giudici);
//			System.out.println("*****");
//		}
//		else {	
//			System.out.println(text);
//			System.out.println("*****");
//		}
		
		return giudici.replaceAll("\n", "");

//		String[] split = text.split("\n \n");
//		//System.out.println(split.length);
//		String classificazioni_rif = split[1];
//
//		String[] classificazioniSplit = classificazioni_rif.split(" \n-");
//		String classificazioni = classificazioniSplit[0];
//		//System.out.println(classificazioni);
//
//		return classificazioni.replaceAll("\n", " ");
	}

	
	
}

