import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;

import config.Config;
import deJureDataReader.DeJureCivileDataReader;
import provvedimento.Provvedimento;


public class RunCleanCivileDeJure {


	public static void main (String[] args) {
	
		runCleanCivileDeJure();
				
	}
	

	private static void runCleanCivileDeJure() {
		DeJureCivileDataReader djCiviledr = new DeJureCivileDataReader();

		System.out.println("-- START READ CIVILE TSV --");
		djCiviledr.readLookupComuni_TSV();
		djCiviledr.readLookupDistrettiCA_TSV();
		

		// LOAD DATASET PROVVEDIMENTI PRINCIPALE (mergeProvvMAP)
		HashMap<String,Provvedimento> datasetMerge = null;
		File prov_Map_serial = new File(Config.serializedProvvMap);
		if(!prov_Map_serial.exists()) {
			System.out.println("DATASET provvedimenti PRINCIPALE non trovato ");
		}else {
			try {
				datasetMerge = readProvvMapFromFile(Config.serializedProvvMap);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		
		//String tsvFilePath=Config.DATA_INPUT_DEJURE_CIVILE_FAMIGLIA;
		//String tsvFilePath=Config.DATA_INPUT_DEJURE_CIVILE_IMMOBILI_CONDOMINIO;
//		String tsvFilePath=Config.DATA_INPUT_DEJURE_CIVILE_DANNO_RESPONSABILITA;
//		djCiviledr.readDeJureCivileTSV(tsvFilePath);
		///////////////////
		// SCRAPE LEXADOC
		///////////////////
		
//		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1;
//		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
//		tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_2;
		
		
		
		
		// ================================================
		//
		//					1-2-3
		//
		// ================================================
		
		// MEGLIO FARLO DA FILE UNICO...
		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1_2_3;
		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
		djCiviledr.alphaOrderForMultipleCat();
		// CERCA I PROVVEDIMENTI SCRAPATI da DEJURE nel dataset principale & CHECK se già presi
		if(datasetMerge!=null) {
			djCiviledr.flagDuplicate(datasetMerge);
			//djCiviledr.flagProbablyDuplicate(serializedProvvMap);
		}
		HashMap<String,Provvedimento> provvedimenti1_2_3 = djCiviledr.getProvvedimentiScrapeDeJure();

		
		// ================================================
		//
		//					4
		//
		// ================================================
		
		// MEGLIO FARLO DA FILE UNICO...
		//String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1_2_3;
		tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_4;

		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
		djCiviledr.alphaOrderForMultipleCat();
		

		// CERCA I PROVVEDIMENTI SCRAPATI da DEJURE nel dataset principale & CHECK se già presi
		if(datasetMerge!=null) {
			djCiviledr.flagDuplicate(datasetMerge);
			//djCiviledr.flagProbablyDuplicate(serializedProvvMap);
		}
		
		if(provvedimenti1_2_3!=null) {
			djCiviledr.flagVisited(provvedimenti1_2_3);
			//djCiviledr.flagProbablyDuplicate(serializedProvvMap);
		}
		
		// FORMATO SPECIFICO scrape Dejure
		djCiviledr.printCivileDeJureSingleRow();
		

		
		// FORMATO COMUNE ALTRI RECORD
		//djCiviledr.printCivileDeJureGennusoStandard();
		//djCiviledr.printCivileDeJureSingleRowStandard();
		//djCiviledr.printCivileDeJureSingleRowOmeka();


		
	}
	
	
		
	private static HashMap<String, Provvedimento> readProvvMapFromFile(String filePath) throws IOException{
		
		ObjectInputStream objectinputstream = null;
		try {
		    FileInputStream streamIn = new FileInputStream(filePath);
		    objectinputstream = new ObjectInputStream(streamIn);
		    HashMap<String, Provvedimento> readMap = (HashMap<String, Provvedimento>) objectinputstream.readObject();
			return readMap;
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if(objectinputstream != null){
		        objectinputstream .close();
		    } 
		}
		return null;
	}
	
}



