package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import config.Config;
import provvedimento.Provvedimento;

public class DistrettiCA {

	HashMap<String,List<String>> lookupComuni = new HashMap<String,List<String>>();
	HashMap<String,String> lookupDistrettiCA = new HashMap<String,String>();
	HashMap<String,String> lookupDistrettiCA_Fix = new HashMap<String,String>();

	
	 public DistrettiCA() {
		 readLookupComuni_TSV();
		 readLookupDistrettiCA_TSV();	  
		 readLookupDistrettiCA_Fix_TSV();
	}
	
	
	
	public void readLookupComuni_TSV() {
		String tsvFilePath=Config.TAB_COMUNI;
		File comuniTSV = new File(tsvFilePath);
		if(!comuniTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(comuniTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String comune = fields[0];
				String cittaMetropolitana = fields[3];
				String capoluogoProvincia = fields[4];
				String capoluogoRegione = fields[8];

				String label = comune;

		
				if(lookupComuni.get(label)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(cittaMetropolitana);
					lista.add(capoluogoProvincia);
					lista.add(capoluogoRegione);
					lookupComuni.put(label, lista);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	
	
	public void readLookupDistrettiCA_Fix_TSV() {

		String tsvFilePath=Config.TAB_DISTRETTI_CA_FIX;
		File distrettiTSV = new File(tsvFilePath);
		if(!distrettiTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(distrettiTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String distretto = fields[0];
				String nomeCorte = fields[1];
				String siglaProvincia = fields[2];

				String label = distretto;

				if(lookupDistrettiCA_Fix.get(label)==null){		
					lookupDistrettiCA_Fix.put(label,nomeCorte+":"+siglaProvincia);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	public void readLookupDistrettiCA_TSV() {

		String tsvFilePath=Config.TAB_DISTRETTI_CA;
		File distrettiTSV = new File(tsvFilePath);
		if(!distrettiTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(distrettiTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String distretto = fields[0];
				String nomeCorte = fields[1];
				String siglaProvincia = fields[2];

				String label = distretto;

				if(lookupDistrettiCA.get(label)==null){		
					lookupDistrettiCA.put(label,nomeCorte+":"+siglaProvincia);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	public Provvedimento setDistrettoAppello_FIX(Provvedimento p) {
		String geoName = p.getGeo_name();  
		
//		geo_name = court_city + court_municipality
//		String courtCode = p.getCourt_city();
//		String catastale = p.getCourt_municipality();
//		System.err.println(geo_Name+Config.SEP+courtCode+Config.SEP+catastale);
		
		List<String> infoComuni;
		String cittaMetropolitana = "";
		String capoluogoProvincia = "";
		String capoluogoRegione = "";

		String distrettoCA_FIX = "";
		String distrettoCA_OLD = "";


		
		// NORMALIZZA I NOMI DELLE LOCALITA'
		geoName = Util.normalizeLocalitaUfficio(geoName);
		//p.setGeo_name(geoName);
	
		
		// MODIFICA TEMPORANEAMENTE I NOMI PER IL LOOKUP DEL CAPOLUOGO
		if(geoName.equalsIgnoreCase("Porretta Terme"))
			geoName="Alto Reno Terme";
		else if(geoName.equalsIgnoreCase("Reggio Calabria"))
			geoName="Reggio di Calabria";
		else if(geoName.equalsIgnoreCase("Reggio Emilia"))
			geoName="Reggio nell'Emilia";
		else if(geoName.equalsIgnoreCase("Napoli Nord"))
			geoName="Napoli";
		
		
		// 1 - dalla sede del Trib. risali a Capoluogo Provincia - Capoluogo Regione
		if(geoName.trim().length()>1) {

			geoName = geoName.substring(0,1).toUpperCase() + geoName.substring(1);
			infoComuni = lookupComuni.get(geoName);
			if(infoComuni!=null) {
				cittaMetropolitana = infoComuni.get(0).trim();
				capoluogoProvincia = infoComuni.get(1).trim();
				capoluogoRegione  = infoComuni.get(2).trim();

				//System.err.println("----------"+geoName+Config.SEP+capoluogoRegione+Config.SEP+capoluogoProvincia+Config.SEP+cittaMetropolitana);

			}else {
				if(geoName.indexOf(" ")!=-1) {
					String municipality = geoName.substring(geoName.indexOf(" "), geoName.length()).trim();
					infoComuni = lookupComuni.get(municipality);

					if(infoComuni!=null) {
						cittaMetropolitana = infoComuni.get(0).trim();
						capoluogoProvincia = infoComuni.get(1).trim();
						capoluogoRegione  = infoComuni.get(2).trim();

						//System.err.println("----------"+geoName+Config.SEP+capoluogoRegione+Config.SEP+capoluogoProvincia+Config.SEP+cittaMetropolitana);

					}else

						 System.err.println("*****  CANNOT FIND COMUNE "+geoName);
				}
				else
					System.err.println("*****  CANNOT FIND COMUNE "+geoName);

			}


			// FIND DISTRETTO CA in base a sede Trib
			if(capoluogoProvincia.length()<3)
				capoluogoProvincia = cittaMetropolitana;
			// ECCEZIONI
			if(capoluogoRegione.equalsIgnoreCase("Aosta"))
				capoluogoRegione = "Torino";
			if(capoluogoProvincia.equalsIgnoreCase("Massa-Carrara")) {
				capoluogoProvincia = "Genova";
				capoluogoRegione = "Genova";
			}
			if(geoName.equalsIgnoreCase("Lanusei")) {
				capoluogoProvincia = "Cagliari";
				capoluogoRegione = "Cagliari";
			}

			// FIXed Version
			distrettoCA_FIX = lookupDistrettiCA_Fix.get(capoluogoProvincia);
			if(distrettoCA_FIX==null) {
				distrettoCA_FIX = lookupDistrettiCA_Fix.get(capoluogoRegione);
			}

			// OLD Version  (backward compatibility per ID Persistente)
			distrettoCA_OLD = lookupDistrettiCA.get(capoluogoProvincia);
			if(distrettoCA_OLD==null) {
				distrettoCA_OLD = lookupDistrettiCA.get(capoluogoRegione);
			}
			
			if(distrettoCA_FIX!=null) {
				//System.err.println("--D--"+geoName+Config.SEP+distrettoCA);
			}else {
				System.err.println("*****  CANNOT FIND DISTRETTO FOR "+geoName);
			}

		}


		// SETTA NELL'OGGETTO PROVVEDIMENTO - il distretto CA
		
		String distrettoCA_esteso ="";
		String distrettoCA_sigla = "";
		if(distrettoCA_FIX!=null && distrettoCA_FIX.length()>0) {
			distrettoCA_esteso = distrettoCA_FIX.split(":")[0];
			//distrettoCA_sigla = distrettoCA_FIX.split(":")[1];
		}
		if(distrettoCA_OLD!=null && distrettoCA_OLD.length()>0) {
			//distrettoCA_esteso = distrettoCA_OLD.split(":")[0];
			distrettoCA_sigla = distrettoCA_OLD.split(":")[1];
			
		}
		
		p.setDistrettoCorteAppello(distrettoCA_esteso);
		p.setDistrettoCorteAppello_SIGLA(distrettoCA_sigla);
		return p;
	}

	
	public Provvedimento setDistrettoAppello(Provvedimento p) {
		return setDistrettoAppello_FIX(p);
	}
	
	
	public Provvedimento setDistrettoAppello_OLD(Provvedimento p) {
		String geoName = p.getGeo_name();  // geo_name = court_city + court_municipality
		String courtCode = p.getCourt_city();
		String catastale = p.getCourt_municipality();

		//	System.err.println(geo_Name+Config.SEP+courtCode+Config.SEP+catastale);
		List<String> infoComuni;
		String cittaMetropolitana = "";
		String capoluogoProvincia = "";
		String capoluogoRegione = "";

		String distrettoCA = "";



		if(geoName.equalsIgnoreCase("Aquila"))
			geoName="L'Aquila";
		if(geoName.equalsIgnoreCase("Reggio Calabria"))
			geoName="Reggio di Calabria";
		if(geoName.equalsIgnoreCase("Reggio Emilia"))
			geoName="Reggio nell'Emilia";

		if(geoName.equals("Lecce Taranto")) {
			geoName = "Taranto";
		}
		
		if(geoName.equals("Trento Bolzano")) {
			geoName = "Bolzano";
		}
		
		if(geoName.equals("Cagliari Sassari")) {
			geoName = "Sassari";
		}

		
		// DEJURE
		
		if(geoName.equalsIgnoreCase("S.Maria Capua V."))
			geoName="Santa Maria Capua Vetere";
		if(geoName.equalsIgnoreCase("Napoli Nord"))
			geoName="Napoli";
		if(geoName.equalsIgnoreCase("Forli'"))
			geoName="Forlì";
		if(geoName.equalsIgnoreCase("Bassano Grappa"))
			geoName="Bassano del Grappa";
		if(geoName.equalsIgnoreCase("Vallo Lucania"))
			geoName="Vallo della Lucania";
		if(geoName.equalsIgnoreCase("Barcellona P.G."))
			geoName="Barcellona Pozzo di Gotto";
		if(geoName.equalsIgnoreCase("Grumello Monte"))
			geoName="Grumello del Monte";
		if(geoName.equalsIgnoreCase("Paterno'"))
			geoName="Paternò";
		if(geoName.equalsIgnoreCase("Porretta Terme"))
			geoName="Alto Reno Terme";
		
		// 1 - dalla sede del Trib. risali a Capoluogo Provincia - Capoluogo Regione
		if(geoName.trim().length()>1) {

			geoName = geoName.substring(0,1).toUpperCase() + geoName.substring(1);
			infoComuni = lookupComuni.get(geoName);
			if(infoComuni!=null) {
				cittaMetropolitana = infoComuni.get(0).trim();
				capoluogoProvincia = infoComuni.get(1).trim();
				capoluogoRegione  = infoComuni.get(2).trim();

				//System.err.println("----------"+geoName+Config.SEP+capoluogoRegione+Config.SEP+capoluogoProvincia+Config.SEP+cittaMetropolitana);

			}else {
				if(geoName.indexOf(" ")!=-1) {
					String municipality = geoName.substring(geoName.indexOf(" "), geoName.length()).trim();
					infoComuni = lookupComuni.get(municipality);

					if(infoComuni!=null) {
						cittaMetropolitana = infoComuni.get(0).trim();
						capoluogoProvincia = infoComuni.get(1).trim();
						capoluogoRegione  = infoComuni.get(2).trim();

						//System.err.println("----------"+geoName+Config.SEP+capoluogoRegione+Config.SEP+capoluogoProvincia+Config.SEP+cittaMetropolitana);

					}else

						 System.err.println("*****  CANNOT FIND COMUNE "+geoName);
				}
				else
					System.err.println("*****  CANNOT FIND COMUNE "+geoName);

			}


			// FIND DISTRETTO CA in base a sede Trib
			if(capoluogoProvincia.length()<3)
				capoluogoProvincia = cittaMetropolitana;
			// ECCEZIONI
			if(capoluogoRegione.equalsIgnoreCase("Aosta"))
				capoluogoRegione = "Torino";
			if(capoluogoProvincia.equalsIgnoreCase("Massa-Carrara")) {
				capoluogoProvincia = "Genova";
				capoluogoRegione = "Genova";

			}

			distrettoCA = lookupDistrettiCA.get(capoluogoProvincia);
			if(distrettoCA==null) {
				distrettoCA = lookupDistrettiCA.get(capoluogoRegione);
			}

			if(distrettoCA!=null) {
				//System.err.println("--D--"+geoName+Config.SEP+distrettoCA);
			}else {
				System.err.println("*****  CANNOT FIND DISTRETTO FOR "+geoName);
			}

		}


		// SETTA NELL'OGGETTO PROVVEDIMENTO - il distretto CA
		
		String distrettoCA_esteso ="";
		String distrettoCA_sigla = "";
		if(distrettoCA!=null && distrettoCA.length()>0) {
			distrettoCA_esteso = distrettoCA.split(":")[0];
			distrettoCA_sigla = distrettoCA.split(":")[1];
			
		}
		
		
		
		p.setDistrettoCorteAppello(distrettoCA_esteso);
		p.setDistrettoCorteAppello_SIGLA(distrettoCA_sigla);
		return p;
	}
	
	
	
	
	
}
