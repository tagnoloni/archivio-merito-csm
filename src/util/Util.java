package util;

import provvedimento.Provvedimento;
import provvedimento.ProvvedimentoFinal;

public class Util {

	
	
	public static String computeNewID (Provvedimento p) {
		
		String authority = "";
		if(p.getAuthority()!=null && p.getAuthority().indexOf("_")!=-1)
			authority = p.getAuthority().substring(p.getAuthority().indexOf("_")+1, p.getAuthority().length());
		
		int hash = Math.abs(p.getEstremoText().hashCode());
		
		String newIdProvvedimento = "IT"+":"+p.getDistrettoCorteAppello_SIGLA()+":"+authority+":"+p.getProvv_year()+":"+hash;
		
		return newIdProvvedimento;
	}
	
	
	public static String computeNewID_COST (Provvedimento p) {
		
		String redazionaleGU = p.getIdProvvedimento();
		String authority = "";
		if(p.getAuthority()!=null && p.getAuthority().indexOf("_")!=-1)
			authority = p.getAuthority().substring(p.getAuthority().indexOf("_")+1, p.getAuthority().length());
		
		
		String newIdProvvedimento = "IT"+":"+p.getDistrettoCorteAppello_SIGLA()+":"+authority+":"+p.getProvv_year()+":"+redazionaleGU;
		
		return newIdProvvedimento;
	}
	
	
	public static String normalizeLocalitaUfficio(String geoName) {
		
		
		if(geoName.equals("bologna")) {
			geoName = "Bologna";
		}
		else if(geoName.equals("Lecce Taranto")) {
			geoName = "Taranto";
		}
		else if(geoName.equals("Cagliari Sassari")) {
			geoName = "Sassari";
		}
		else if(geoName.equals("Trento Bolzano")) {
			geoName = "Bolzano";
		}
		else if(geoName.equals("Cagliari Sassari")) {
			geoName = "Sassari";
		}
		else if(geoName.toLowerCase().indexOf("forl")!=-1) {
			geoName = "Forlì";
		}
		else if(geoName.equalsIgnoreCase("Aquila"))
			geoName="L'Aquila";
		else if(geoName.equalsIgnoreCase("S.Maria Capua V."))
			geoName="Santa Maria Capua Vetere";
		else if(geoName.equalsIgnoreCase("Bassano Grappa"))
			geoName="Bassano del Grappa";
		else if(geoName.equalsIgnoreCase("Vallo Lucania"))
			geoName="Vallo della Lucania";
		else if(geoName.equalsIgnoreCase("Barcellona P.G."))
			geoName="Barcellona Pozzo di Gotto";
		else if(geoName.equalsIgnoreCase("Grumello Monte"))
			geoName="Grumello del Monte";
		else if(geoName.equalsIgnoreCase("Paterno'"))
			geoName="Paternò";
		else if(geoName.equalsIgnoreCase("Aversa"))
			geoName="Napoli Nord";
		else if(geoName.equalsIgnoreCase("Napoli Casoria"))
			geoName="Casoria";
		else if(geoName.equalsIgnoreCase("Venezia San Donà di Piave"))
			geoName="San Donà di Piave";
		
		
//		Vallo della Lucania	1
//		Vallo Lucania	2
//		S.Maria Capua V.	23
//		Santa Maria Capua Vetere	10
//		Forlì	5
//		Forli’	6

		
//		else if(geoName.equalsIgnoreCase("Porretta Terme"))
//			geoName="Alto Reno Terme";
//		else if(geoName.equalsIgnoreCase("Reggio Calabria"))
//			geoName="Reggio di Calabria";
//		else if(geoName.equalsIgnoreCase("Reggio Emilia"))
//			geoName="Reggio nell'Emilia";
//		else if(geoName.equalsIgnoreCase("Napoli Nord"))
//			geoName="Napoli";
		
		return geoName;
	}
	
	
	public static String arabicToRoman(String number) {
		
		number=number.trim();
		
		switch(number) {
		case "1": number="I";
				break;
		case "2": number="II";
			break;
		case "3": number="III";
			break;
		case "4": number="IV";
			break;
		case "5": number="V";
			break;
		case "6": number="VI";
			break;
		case "7": number="VII";
			break;
		case "8": number="VIII";
			break;
		case "9": number="IX";
			break;
		case "10": number="X";
			break;
		case "11": number="XI";
			break;
		case "12": number="XII";
			break;
		case "13": number="XIII";
			break;
		case "14": number="XIV";
			break;
		case "15": number="XV";
			break;
		case "16": number="XVI";
			break;
		default:return number;
			
		}
		return number;
	}
	
public static String romanToArabic(String number) {
		
		number=number.trim();
		
		switch(number) {
		case "I": number="1";
				break;
		case "II": number="2";
			break;
		case "III": number="3";
			break;
		case "IV": number="4";
			break;
		case "V": number="5";
			break;
		case "VI": number="6";
			break;
		case "VII": number="7";
			break;
		case "VIII": number="8";
			break;
		case "IX": number="9";
			break;
		case "X": number="10";
			break;
		case "XI": number="11";
			break;
		case "XII": number="12";
			break;
		case "XIII": number="13";
			break;
		case "XIV": number="14";
			break;
		case "XV": number="15";
			break;
		case "XVI": number="16";
			break;
		case "XVII": number="17";
			break;
		case "XVIII": number="18";
			break;
		case "XIX": number="19";
			break;
		case "XX": number="20";
			break;
		default:return number;
			
		}
		return number;
	}



//	X F	3   --> Feriale
//	X fer.	4

//	X L	177
//	X lav.	706

//	X minorenni	1
//	X minori	1
//	X persona, famiglia, minori	1
//	X sezione per i minorenni	3

//	X U	1
//	X un.	4

//	X fallimentare	88
//	X Ufficio Fallimenti	1

//	X giurisd.	1

//	X Sezione specializzata in materia d'impresa	1
//	X spec. Impresa	289

//	X spec. Impresa - B	37
//	X spec. Impresa spec. Impresa - B	1

//  NON NORMALIZZATI (ok così) : 
//	agraria	4
//	civile	2
//	Diritti della persona e immigrazione	1
//	equa riparazione	2
//	famiglia	16
//	sez. provvedimenti speciali	1
//	sezione distaccata di Sassari	1
//	Sezione Specializzata in materia di Immigrazione	1
//	spec. propr. ind.	3
//	Tribunale Regionale delle Acque Pubbliche	3

	


	public static String normalizeSezione_CEDStyle(String sezione) {
		
		if(sezione==null || sezione.trim().length()==0)
			return sezione.trim();
		
		sezione = romanToArabic(sezione.trim());
		
		if(sezione.trim().equalsIgnoreCase("L") || sezione.trim().equalsIgnoreCase("lav."))
			sezione = "lavoro";
		
		if(sezione.trim().equalsIgnoreCase("U") || sezione.trim().equalsIgnoreCase("un."))
			sezione = "unite";
		
		if(sezione.trim().equalsIgnoreCase("F") || sezione.trim().equalsIgnoreCase("fer."))
			sezione = "feriale";
		
		if(sezione.trim().equalsIgnoreCase("minorenni") || sezione.trim().equalsIgnoreCase("minori") 
				|| sezione.trim().equalsIgnoreCase("sezione per i minorenni")
				|| sezione.trim().equalsIgnoreCase("persona, famiglia, minori"))
			sezione = "minorennni";

		if(sezione.trim().equalsIgnoreCase("Ufficio Fallimenti"))
			sezione = "fallimentare";
		
		// fix caso singolo IT:MI:TRB:2018:1461494512
		if(sezione.trim().equalsIgnoreCase("giurisd."))
			sezione = "civile";
		
		if(sezione.trim().equalsIgnoreCase("Sezione specializzata in materia d'impresa"))
			sezione = "spec. Impresa";
		
		if(sezione.trim().equalsIgnoreCase("spec. Impresa spec. Impresa - B"))
			sezione = "spec. Impresa - B";
		
		return sezione;
	}
	
	
	
	
	public static String dataToText(String data) throws Exception{
		
		
		String anno = data.split("-")[0];
		String mese = data.split("-")[1];
		String giorno = data.split("-")[2];
		
		if(giorno.startsWith("0"))
			giorno=giorno.substring(1);
		if(mese.startsWith("0"))
			mese=mese.substring(1);
		
		// ammetti date : 2014-00-00  (giorno e mese non specificati)
		if(giorno.equals("0"))
			giorno = "";
		
		switch(mese) {
			case "1": mese="gennaio";
			break;
			case "2": mese="febbraio";
			break;
			case "3": mese="marzo";
			break;
			case "4": mese="aprile";
			break;
			case "5": mese="maggio";
			break;
			case "6": mese="giugno";
			break;
			case "7": mese="luglio";
			break;
			case "8": mese="agosto";
			break;
			case "9": mese="settembre";
			break;
			case "10": mese="ottobre";
			break;
			case "11": mese="novembre";
			break;
			case "12": mese="dicembre";
			break;
			default: mese=" ";
			break;
		}
		
		return giorno+" "+mese+" "+anno;
	}
	
	
	
	public static String getEstremoUniformeFinal(ProvvedimentoFinal provv) {

		
		
		String tipoDoc = provv.getDocType();
		String autorita = provv.getAuthority();
		String localita = provv.getGeo_name();
		String sezione = provv.getNormalized_court_section();
		String area = provv.getGuessed_subject();
		String data = provv.getDataPubblicazione();
		String numero = provv.getProvv_number();

		String estremoUniforme = "";
		
		////////////////
		// ECCEZIONI
		// con Estremo Uniforme perdo potenzialmente info su sezione
		// nel caso in cui la sezione non sia riconosciuta come campo strutturato
		
		if(autorita!=null && autorita.equalsIgnoreCase("IT_TSV"))
			return provv.getEstremoUniforme();
		
		if(provv.getEstremoUniforme().toLowerCase().indexOf("assise")!=-1 ||
				provv.getEstremoUniforme().toLowerCase().indexOf("minori")!=-1)
			return provv.getEstremoUniforme();
		
		//////////////////

		
		String tipoDocText ="";
		if(tipoDoc!=null) {
			if(tipoDoc.equalsIgnoreCase("JUDGMENT"))
				tipoDocText = "sentenza";
			else if (tipoDoc.equalsIgnoreCase("ORDER"))
				tipoDocText = "ordinanza";
			else if (tipoDoc.equalsIgnoreCase("DECREE"))
				tipoDocText = "decreto";
		}

		String autoritaText ="";
		if(autorita!=null) {
			if(autorita.equalsIgnoreCase("IT_TRB"))
				autoritaText = "Tribunale";
			else if (autorita.equalsIgnoreCase("IT_CPP"))
				autoritaText = "Corte Appello";
			else if (autorita.equalsIgnoreCase("IT_TMN"))
				autoritaText = "Tribunale dei minori";
			else if (autorita.equalsIgnoreCase("IT_TSV"))
				autoritaText = "Tribunale di sorveglianza";
			else if (autorita.equalsIgnoreCase("IT_GPC"))
				autoritaText = "Giudice di pace";
		}

		String areaText="";
		if(area!=null) {
			if(area.equalsIgnoreCase("civile"))
				areaText = "civ.";
			else if(area.equalsIgnoreCase("penale"))
				areaText = "pen.";
		}

		String sezioneText ="";
		if(sezione!=null) {
			if(sezione.equalsIgnoreCase("lavoro"))
				sezioneText = "lav.";
			else if(sezione.toLowerCase().startsWith("unica"))
				sezioneText = "un.";
			else if(sezione.equalsIgnoreCase("feriale"))
				sezioneText = "fer.";
			else if(sezione.toLowerCase().startsWith("fallimentare"))
				sezioneText = "fall.";
			else if(sezione.toLowerCase().startsWith("agraria"))
				sezioneText = "agr.";
			else if(sezione.toLowerCase().startsWith("famiglia"))
				sezioneText = "fam.";
			else	
				sezioneText = arabicToRoman(sezione);			
		}
		if(sezioneText.trim().length()>0 && !sezioneText.startsWith("sez"))
			sezioneText = "sez. "+sezioneText;

		String localitaText ="";
		if(localita!=null)
			localitaText= localita.trim();

		String dataText = "";
		if(data!=null)
			try {
			dataText=dataToText(data);
			}catch(Exception ex) {
				System.out.println("exception per data "+data+"in provv "+provv.getPersistentID());
			}

		String numeroText ="";
		if(numero!=null) {
			if(!numero.toLowerCase().startsWith("n"))
				numeroText="n. "+numero;
			else
				numeroText = numero;
		}


		estremoUniforme+=tipoDocText;
		estremoUniforme=estremoUniforme+" "+autoritaText+" "+localitaText;
		if(sezione.trim().length()>0)
			estremoUniforme=estremoUniforme+" "+sezioneText;
		if(sezione.trim().length()>0 && areaText.trim().length()>0) {
			// aggiunge civ. o pen. solo per le sezioni numeriche
			if(!sezione.trim().toLowerCase().startsWith("u") && !sezione.trim().toLowerCase().startsWith("f") && !sezione.trim().toLowerCase().startsWith("tribunale") && !sezione.trim().toLowerCase().startsWith("equa") && !sezione.trim().toLowerCase().startsWith("agr") && !sezione.trim().toLowerCase().startsWith("sezione") && sezione.trim().toLowerCase().indexOf("impr")==-1 )
				estremoUniforme=estremoUniforme+" "+areaText;
		}
		estremoUniforme=estremoUniforme+" "+dataText;
		if(numero.trim().length()>0)
			estremoUniforme = estremoUniforme+", "+numeroText;

		estremoUniforme = estremoUniforme.trim();


		//		sentenza Tribunale Ravenna sez. lav. 22 settembre 2015
		//		ord. Corte Appello Genova sez. F 6 settembre 2016, n. 51
		//		sentenza  Corte Appello Genova sez. I civ. 2 luglio 2014
		//		sentenza Tribunale Matera 7 marzo 2018, n. 250
		//		Tribunale  Milano, 19 maggio 2015, sez. II
		//		Tribunale  Torino, 13 ottobre 2016, n.1743
		//		Tribunale , Roma , sez. VI , 13/03/2018 , n. 5521
		return estremoUniforme;
	}
	
	
	
	
	public static String getEstremoUniforme(Provvedimento provv) {

		
		
		String tipoDoc = provv.getDocType();
		String autorita = provv.getAuthority();
		String localita = provv.getGeo_name();
		String sezione = provv.getCourt_section();
		String area = provv.getGuessed_subject();
		String data = provv.getProvv_date();
		String numero = provv.getProvv_number();

		String estremoUniforme = "";
		
		////////////////
		// ECCEZIONI
		// con Estremo Uniforme perdo potenzialmente info su sezione
		// nel caso in cui la sezione non sia riconosciuta come campo strutturato
		
		if(autorita!=null && autorita.equalsIgnoreCase("IT_TSV"))
			return provv.getEstremoText();
		
		if(provv.getEstremoText().toLowerCase().indexOf("assise")!=-1 ||
				provv.getEstremoText().toLowerCase().indexOf("minori")!=-1)
			return provv.getEstremoText();
		
		//////////////////

		
		String tipoDocText ="";
		if(tipoDoc!=null) {
			if(tipoDoc.equalsIgnoreCase("JUDGMENT"))
				tipoDocText = "sentenza";
			else if (tipoDoc.equalsIgnoreCase("ORDER"))
				tipoDocText = "ordinanza";
			else if (tipoDoc.equalsIgnoreCase("DECREE"))
				tipoDocText = "decreto";
		}

		String autoritaText ="";
		if(autorita!=null) {
			if(autorita.equalsIgnoreCase("IT_TRB"))
				autoritaText = "Tribunale";
			else if (autorita.equalsIgnoreCase("IT_CPP"))
				autoritaText = "Corte Appello";
			else if (autorita.equalsIgnoreCase("IT_TMN"))
				autoritaText = "Tribunale dei minori";
			else if (autorita.equalsIgnoreCase("IT_TSV"))
				autoritaText = "Tribunale di sorveglianza";
			else if (autorita.equalsIgnoreCase("IT_GPC"))
				autoritaText = "Giudice di pace";
		}

		String areaText="";
		if(area!=null) {
			if(area.equalsIgnoreCase("civile"))
				areaText = "civ.";
			else if(area.equalsIgnoreCase("penale"))
				areaText = "pen.";
		}

		String sezioneText ="";
		if(sezione!=null) {
			if(sezione.equalsIgnoreCase("L"))
				sezioneText = "lav.";
			else if(sezione.toLowerCase().startsWith("un"))
				sezioneText = "un.";
			else if(sezione.equalsIgnoreCase("F"))
				sezioneText = "fer.";
			else if(sezione.toLowerCase().startsWith("fall"))
				sezioneText = "fall.";
			else	
				sezioneText = arabicToRoman(sezione);			
		}
		if(sezioneText.trim().length()>0 && !sezioneText.startsWith("sez"))
			sezioneText = "sez. "+sezioneText;

		String localitaText ="";
		if(localita!=null)
			localitaText= localita.trim();

		String dataText = "";
		if(data!=null)
			try {
			dataText=dataToText(data);
			}catch(Exception ex) {
				System.out.println("exception per data "+data+"in provv "+provv.getPersistentID());
			}

		String numeroText ="";
		if(numero!=null) {
			if(!numero.toLowerCase().startsWith("n"))
				numeroText="n. "+numero;
			else
				numeroText = numero;
		}


		estremoUniforme+=tipoDocText;
		estremoUniforme=estremoUniforme+" "+autoritaText+" "+localitaText;
		if(sezione.trim().length()>0)
			estremoUniforme=estremoUniforme+" "+sezioneText;
		if(sezione.trim().length()>0 && areaText.trim().length()>0) {
			// aggiunge civ. o pen. solo per le sezioni numeriche
			if(!sezione.trim().toLowerCase().startsWith("u") && !sezione.trim().toLowerCase().startsWith("f") && !sezione.trim().toLowerCase().startsWith("tribunale") && !sezione.trim().toLowerCase().startsWith("equa") && sezione.trim().toLowerCase().indexOf("impr")==-1 )
				estremoUniforme=estremoUniforme+" "+areaText;
		}
		estremoUniforme=estremoUniforme+" "+dataText;
		if(numero.trim().length()>0)
			estremoUniforme = estremoUniforme+", "+numeroText;

		estremoUniforme = estremoUniforme.trim();


		//		sentenza Tribunale Ravenna sez. lav. 22 settembre 2015
		//		ord. Corte Appello Genova sez. F 6 settembre 2016, n. 51
		//		sentenza  Corte Appello Genova sez. I civ. 2 luglio 2014
		//		sentenza Tribunale Matera 7 marzo 2018, n. 250
		//		Tribunale  Milano, 19 maggio 2015, sez. II
		//		Tribunale  Torino, 13 ottobre 2016, n.1743
		//		Tribunale , Roma , sez. VI , 13/03/2018 , n. 5521
		return estremoUniforme;
	}
	
	
	// RIVISTE before normalization: 163 choices
	
	public static String normalizeTitoloRivista(String titoloRivista) {
		
		titoloRivista = titoloRivista.trim();
		
		if(titoloRivista.equalsIgnoreCase("Rivista dei Dottori Commercialisti"))
			return "Rivista dei dottori commercialisti";
			
		if(titoloRivista.equalsIgnoreCase("Rivista del notariato"))
			return "Rivista del notariato";
		
		if(titoloRivista.equalsIgnoreCase("Rivista dell'arbitrato"))
			return "Rivista dell'arbitrato";
		
		if(titoloRivista.equalsIgnoreCase("Cassazione penale"))
			return "Cassazione penale";
		
		if(titoloRivista.equalsIgnoreCase("Banca Borsa Titoli di Credito"))
			return "Banca borsa e titoli di credito";
		
		if(titoloRivista.equalsIgnoreCase("giustiziacivile.com"))
			return "GiustiziaCivile.com";
		
		if(titoloRivista.equalsIgnoreCase("Diritto d'Autore (Il)"))
			return "Il Diritto d'autore";
		
		if(titoloRivista.equalsIgnoreCase("Diritto dell'Informazione e dell'Informatica (Il)"))
			return "Il Diritto dell'informazione e dell'informatica";
		
		if(titoloRivista.equalsIgnoreCase("Diritto di Famiglia e delle Persone (Il)"))
			return "Il Diritto di famiglia e delle persone";
		
		if(titoloRivista.equalsIgnoreCase("Diritto delle relazioni industriali"))
			return "Diritto delle relazioni industriali";
	
		if(titoloRivista.equalsIgnoreCase("Giurisprudenza commerciale"))
			return "Giurisprudenza commerciale";
		
		if(titoloRivista.equalsIgnoreCase("Rivista di diritto industriale"))
			return "Rivista di diritto industriale";
		
		if(titoloRivista.equalsIgnoreCase("Responsabilita' Civile e Previdenza"))
			return "Responsabilità civile e previdenza";
		
		if(titoloRivista.equalsIgnoreCase("Rivista di diritto tributario"))
			return "Rivista di diritto tributario";
		
		if(titoloRivista.equalsIgnoreCase("Rivista italiana di diritto del lavoro"))
			return "Rivista italiana di diritto del lavoro";
		
		if(titoloRivista.equalsIgnoreCase("Rivista Italiana di Medicina Legale (e del Diritto in campo sanitario)"))
			return "Rivista italiana di medicina legale e del diritto in campo sanitario";
		
		return titoloRivista;
		
	}
	
	
	
	public static String normalizeEditore(String editore) {
		return editore;
	}
	
	
	public static boolean isRivistaTelematica(String titoloRivista) {
		
		if(titoloRivista.equalsIgnoreCase("Condominioelocazione.it")||
		   titoloRivista.equalsIgnoreCase("giustiziacivile.com")||
		   titoloRivista.equalsIgnoreCase("Ilfallimentarista.it")||
		   titoloRivista.equalsIgnoreCase("Ilfamiliarista.it")||
		   titoloRivista.equalsIgnoreCase("Ilgiuslavorista.it")||
		   titoloRivista.equalsIgnoreCase("Ilpenalista.it")||
		   titoloRivista.equalsIgnoreCase("Ilprocessocivile.it")||
		   titoloRivista.equalsIgnoreCase("Ilprocessotelematico.it")||
		   titoloRivista.equalsIgnoreCase("Ilsocietario.it")||
		   titoloRivista.equalsIgnoreCase("Iltributario.it")||
		   titoloRivista.equalsIgnoreCase("Ridare.it"))
			return true;
		return false;

	}
	
}


//Rivista dei Dottori Commercialisti	5
//*Rivista dei dottori commercialisti	22
//
//
//
//Rivista del Notariato	12
//Rivista del notariato	18
//
//Rivista dell'arbitrato	8
//Rivista dell'Arbitrato	4
//
//Cassazione penale	11
//Cassazione Penale	10
//
//Banca borsa e titoli di credito	81
//Banca Borsa Titoli di Credito	24
//
//giustiziacivile.com	18
//GiustiziaCivile.com	12
//
//Diritto d'Autore (Il)	1
//Il Diritto d'autore	10
//
//Diritto dell'Informazione e dell'Informatica (Il)	1
//Il Diritto dell'informazione e dell'informatica	10
//
//Il Diritto di famiglia e delle persone	30
//Diritto di Famiglia e delle Persone (Il)	7
//
//Diritto delle Relazioni Industriali	5
//Diritto delle relazioni industriali	43
//
//Giurisprudenza commerciale	92
//Giurisprudenza Commerciale	9
//
//
//Rivista di Diritto Industriale	8
//Rivista di diritto industriale	37
//
//Responsabilità civile e previdenza	62
//Responsabilita' Civile e Previdenza	2
//
//Rivista di Diritto Tributario	1
//Rivista di diritto tributario	5
//
//Rivista Italiana di Diritto del Lavoro	9
//Rivista italiana di diritto del lavoro	106
//
//Rivista Italiana di Medicina Legale (e del Diritto in campo sanitario)	15
//Rivista italiana di medicina legale e del diritto in campo sanitario	38
