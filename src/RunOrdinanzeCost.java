import config.Config;
import ordinanzeCostDataReader.OrdinanzeCostDataReader;


public class RunOrdinanzeCost {


	public static void main (String[] args) {
	
		runOrdinanzeCost();
				
	}
	

	private static void runOrdinanzeCost() {
		OrdinanzeCostDataReader OrdCostDr = new OrdinanzeCostDataReader();

		System.out.println("-- START READ CIVILE TSV --");
	
		
		String tsvFilePath=Config.DATA_ORDINANZE_COST;
		OrdCostDr.readOrdinanzeCostTSV(tsvFilePath);	
		
	
		OrdCostDr.printSingleRowStandard();
		//OrdCostDr.printCivileDeJureGennusoStandard();

		
	}
	
	
		
	
	
}



