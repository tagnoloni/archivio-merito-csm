package nota;

public class Nota {
	
	

	private String idContributo;
	private String ISSN;
	private String estremo;
	private String titoloContributo;
	private String rivista;
	private String editore;
	private String nomeAutore;
	private String cognomeAutore;
	private String autori;
	private String dataFascicolo;
	private String pagIniziale;
	private String numFascicolo;
	


	
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}
	
	public String getIdContributo() {
		return idContributo;
	}
	public void setIdContributo(String idContributo) {
		this.idContributo = idContributo;
	}
	public String getISSN() {
		return ISSN;
	}
	public void setISSN(String iSSN) {
		ISSN = iSSN;
	}
	public String getEstremo() {
		return estremo;
	}
	public void setEstremo(String estremo) {
		this.estremo = estremo;
	}
	public String getTitoloContributo() {
		return titoloContributo;
	}
	public void setTitoloContributo(String titoloContributo) {
		this.titoloContributo = titoloContributo;
	}
	public String getRivista() {
		return rivista;
	}
	public void setRivista(String rivista) {
		this.rivista = rivista;
	}
	public String getEditore() {
		return editore;
	}
	public void setEditore(String editore) {
		this.editore = editore;
	}
	public String getNomeAutore() {
		return nomeAutore;
	}
	public void setNomeAutore(String nomeAutore) {
		this.nomeAutore = nomeAutore;
	}
	public String getCognomeAutore() {
		return cognomeAutore;
	}
	public void setCognomeAutore(String cognomeAutore) {
		this.cognomeAutore = cognomeAutore;
	}
	public String getDataFascicolo() {
		return dataFascicolo;
	}
	public void setDataFascicolo(String dataFascicolo) {
		this.dataFascicolo = dataFascicolo;
	}
	public String getPagIniziale() {
		return pagIniziale;
	}
	public void setPagIniziale(String pagIniziale) {
		this.pagIniziale = pagIniziale;
	}
	public String getNumFascicolo() {
		return numFascicolo;
	}
	public void setNumFascicolo(String numFascicolo) {
		this.numFascicolo = numFascicolo;
	}
	
	
	public String toString() { 
		String ret="";
        ret+="estremo: "+estremo +"\n";
        ret+="idContributo: "+idContributo +"\n";
  //      ret+="cognomeAutore: "+cognomeAutore+"\n";
  //      ret+="nomeAutore: "+nomeAutore+"\n";
        ret+="autori: "+autori+"\n";      
        ret+="titoloContributo: "+titoloContributo +"\n";
        ret+="rivista: "+rivista +"\n";
        ret+="editore: "+editore +"\n";
        ret+="ISSN: "+ISSN +"\n";
        ret+="numFascicolo: "+numFascicolo +"\n";
        ret+="dataFascicolo: "+dataFascicolo +"\n";
        ret+="pagIniziale: "+pagIniziale +"\n";
		ret+="*****************************"+"\n";

		return ret;
    } 
	
//	System.err.println("----------");
//	System.err.println("estremo: "+estremo);
//	System.err.println("idContributo: "+idContributo);
//	System.err.println("autore: "+cognomeAutore+" "+nomeAutore);
//	System.err.println("titoloContributo: "+titoloContributo);
//	System.err.println("rivista: "+rivista);
//	System.err.println("editore: "+editore);
//	System.err.println("ISSN: "+ISSN);
//	System.err.println("numFascicolo: "+numFascicolo);
//	System.err.println("dataFascicolo: "+dataFascicolo);
//	System.err.println("pagIniziale: "+pagIniziale);
//	System.err.println("");
	
}
