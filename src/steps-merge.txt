merge totale


AUTORI
SISTEMATICI
DESCRITTORI
FONTI


AUTORI (start)

126.	267252	0035-7022	Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286	Illiquidità involontaria dell'impresa e omesso versamento Iva e di ritenute certificate: la non punibilità dell'impresa all'epoca della crisi economica?	Rivista penale	La Tribuna	Miglio	Mattia	2014-03-00	318	3
127.	267252	0035-7022	Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286	Illiquidità involontaria dell'impresa e omesso versamento Iva e di ritenute certificate: la non punibilità dell'impresa all'epoca della crisi economica?	Rivista penale	La Tribuna	Ferri	Filippo	2014-03-00		



SISTEMATICI

267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",REATI TRIBUTARI (=>TRIB.2.9.1. VIOLAZIONI E SANZIONI PENALI IN MATERIA TRIBUTARIA)
267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",IMPOSTE INDIRETTE
267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",LAVORO
267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",IMPRENDITORE


DESCRITTORI

267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",Imposta sul valore aggiunto (IVA)
267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",Omesso versamento di ritenute e contributi assistenziali e previdenziali


FONTI

267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286","d.lg. 10 marzo 2000, n. 74",bis,10
267252,"Nota a Trib. Vigevano sez. pen. 11 aprile 2013, n. 286","Trib. Vigevano sez. pen. 11 aprile 2013, n. 286",,


------------------------------

MERGE...

(per valori multipli - uso id Contributo + collection oggetti multipli)

	key - id contributo
	value - collection di oggetti multipli

------------------------------

	
leggi tutte le tabelle come oggetti e poi genera output che serve 

------------------------------
MULTIPLI:

note a sentenze multiple (semicolon separated)   (contributo dottrinale che commenta più sentenze - vanno prese quelle di merito)
autori multipli

desc/sist/fonti multiple

------------------------------


