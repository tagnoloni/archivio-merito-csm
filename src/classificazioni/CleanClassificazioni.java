package classificazioni;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import config.Config;
import fonte.Fonte;
import provvedimento.Provvedimento;

public class CleanClassificazioni {
	
	
	HashMap<String,Provvedimento> provv_Merge_Map = new HashMap<String,Provvedimento>();
	
	HashMap<String,ArrayList<String>> classificazioniTOPMap = new HashMap<String,ArrayList<String>>();


	
	public void setProvv_Merge_Map(HashMap<String,Provvedimento> map) {
		this.provv_Merge_Map = map;
	}
	
	
	public HashMap<String,Provvedimento> getProvv_Merge_Map() {
		return this.provv_Merge_Map;
	}
	
	
	
	public void update_ProvvMergeMap() {
		
			
		for(Provvedimento provv : provv_Merge_Map.values()) {			
			
			ArrayList<String> classificazioniTop = classificazioniTOPMap.get(provv.getPersistentID());	
			provv.setClassificazioniTOP(classificazioniTop);
			
		}
		
	}
	
	
	
	
	
	public void cleanClassificazioniProvvMap() {

		for(Provvedimento provv : provv_Merge_Map.values()) {
			
			
			String idProvv = provv.getPersistentID();
			
			ArrayList<String> init = new ArrayList<>();

			
			if(classificazioniTOPMap.get(idProvv)==null)
				classificazioniTOPMap.put(idProvv,init);
				
			String descrittoreTop ="";
			List<Fonte> fonti = provv.getFonti();
			for(int i=0;i<fonti.size();i++) {
				if(fonti.get(i).getSistematici()!=null && fonti.get(i).getSistematici().length>0) {
					
					for(int k=0;k<fonti.get(i).getSistematici().length;k++) {
						descrittoreTop = fonti.get(i).getSistematici()[k];
						
						
						
						
						
						descrittoreTop = stripQuotes(descrittoreTop);
						descrittoreTop = stripSquareBrackets(descrittoreTop);
						descrittoreTop = descrittoreTop.replaceAll("A'", "À");
						
						if(descrittoreTop.indexOf("|")!=-1) {
							//System.out.println(descrittoreTop);
							String[] descrittoriSingoli = descrittoreTop.split("\\|");
							for(int index=0;index<descrittoriSingoli.length;index++) {
								String descrittoreSingolo = descrittoriSingoli[index];
								descrittoreSingolo = stripQuotes(descrittoreSingolo);
								//System.out.println(descrittoreSingolo);
								
								cleanClusterAndAddDescrittore(idProvv, descrittoreSingolo);
									//aggiungilo pulito alla mappa se non c'è
								
							}
						// VA AGGIUNTO CASO WKI
						}else {
									
							ArrayList<String>descrittoriTop = splitDescrittoreTopByCase(descrittoreTop);
							
							for(String descrittore:descrittoriTop) {
								cleanClusterAndAddDescrittore(idProvv, descrittore);
							}
						}
						// questo singolo descrittoreTop potrebbe essere a sua volta essere ; separated
						
						
						// 1) singola Stringa
						// es. DeJure
						// COSA GIUDICATA IN MATERIA PENALE" | "COSA GIUDICATA IN MATERIA PENALE" | "COSA GIUDICATA IN MATERIA PENALE" 
						// COSA GIUDICATA IN MATERIA PENALE - Irrevocabilità - - \"ne bis in idem\" e/o divieto di un secondo giudizio" | "COSA GIUDICATA IN MATERIA PENALE - Irrevocabilità - - \"ne bis in idem\" e/o divieto di un secondo giudizio" | "COSA GIUDICATA IN MATERIA PENALE - Irrevocabilità - - \"ne bis in idem\" e/o divieto di un secondo giudizio" 
					
					    // es. WKI
						// INCOLUMITA' PUBBLICA (REATI) SEQUESTRO PENALE Sequestro preventivo APPROPRIAZIONE INDEBITA DIBATTIMENTO PENALE Dibattimento in genere CAUSE DI NON PUNIBILITA' Cause di non punibilità in genere
						
						// 2) Array di stringhe
						// es. DoGi
						// PROCEDURE CONCORSUALI;CONCORDATO PREVENTIVO#PROCEDURE CONCORSUALI;CONCORDATO PREVENTIVO
						
					}
						
					
					
					//getSistematici() è un array di Stringhe / oppure una singola string con separatore ;
					// ogni singolo descriptor va pulito (")
					// aggiunto se non c'è
					
				}
			}
			
		}
	}
	
	
	private void cleanClusterAndAddDescrittore(String idProvv, String descrittore){
		
//		
//
//		*PROVVEDIMENTI D'URGENZA	3
//		PROVVEDIMENTI DI URGENZA (ART	3
//
//
//		*700	3
//
//		*PROVVEDIMENTI D'URGENZA	3
//		PROVVEDIMENTI DI URGENZA (ART	3
//
//		*C	1
//
//		*C.E.E.
//
//
//		��	1
		
		
		if(descrittore.endsWith("À")) {
			descrittore=descrittore.substring(0,descrittore.length()-1)+"A'";
		}
		
		
		if(descrittore.equals("APPALTO (Contratto di)"))
			descrittore = "APPALTO";
		else if(descrittore.equals("ASSICURAZIONE (CONTRATTO DI)") || descrittore.equals("ASSICURAZIONE (Contratto di)"))
			descrittore = "ASSICURAZIONE";
		else if(descrittore.equals("AVVOCATI"))
			descrittore = "AVVOCATO";
		else if(descrittore.equals("BANCA") || descrittore.equals("BANCA (Istituti di credito)"))
			descrittore = "BANCHE";
		else if(descrittore.equals("COMPETENZA CIVILE"))
			descrittore = "COMPETENZA E GIURISDIZIONE CIVILE";
		else if(descrittore.equals("COMUNIONE"))
			descrittore = "COMUNIONE DEI BENI";
		else if(descrittore.equals("CONCORRENZA (Disciplina della)"))
			descrittore = "CONCORRENZA";
		else if(descrittore.equals("CONTO CORRENTE") || descrittore.equals("CONTO CORRENTE (Contratto di)"))
			descrittore = "CONTO CORRENTE BANCARIO";
		else if(descrittore.equals("DANNI"))
			descrittore = "DANNO E RISARCIMENTO";
		else if(descrittore.equals("DELIBAZIONE (GIUDIZIO DI)"))
			descrittore = "DELIBAZIONE";
		else if(descrittore.equals("DIRITTI CONNESSI AL DIRITTO D'AUTORE") || descrittore.equals("DIRITTI D'AUTORE") || descrittore.equals("DIRITTI DELL'AUTORE"))
			descrittore = "DIRITTO D'AUTORE";
		else if(descrittore.equals("DIRITTO CIVILE IN GENERALE"))
			descrittore = "DIRITTO CIVILE";
		else if(descrittore.equals("DIRITTO PENALE IN GENERALE"))
			descrittore = "DIRITTO PENALE";
		else if(descrittore.equals("ESECUZIONE FORZATA DI OBBLIGHI DI FARE O DI NON FARE"))
			descrittore = "ESECUZIONE FORZATA";
		else if(descrittore.equals("ESPROPRIAZIONE"))
			descrittore = "ESPROPRIAZIONE FORZATA";
		else if(descrittore.equals("FALLIMENTO (Disposizioni penali)"))
			descrittore = "FALLIMENTO";
		else if(descrittore.equals("IMPOSTA REDDITO PERSONE FISICHE (I.R.P.E.F.)"))
			descrittore = "IMPOSTA REDDITO PERSONE FISICHE E GIURIDICHE";
		else if(descrittore.equals("IMPRENDITORE AGRICOLO"))
			descrittore = "IMPRESA AGRICOLA";
		else if(descrittore.equals("IMPRESA ED IMPRENDITORE"))
			descrittore = "IMPRESA E IMPRENDITORE";
		else if(descrittore.equals("INFORMATICA GIURIDICA DOCUMENTARIA"))
			descrittore = "INFORMATICA GIURIDICA E DIRITTO DELL'INFORMATICA";
		else if(descrittore.equals("INTERMEDIARI FINANZIARI (PROMOTORI FINANZIARI)") || descrittore.equals("INTERMEDIAZIONE FINANZIARIA") || descrittore.equals("PROMOTORI FINANZIARI"))
			descrittore = "INTERMEDIARI FINANZIARI";
		else if(descrittore.equals("INTERRUZIONE DELLA GRAVIDANZA (ABORTO)"))
			descrittore = "ABORTO";
		else if(descrittore.equals("ISTRUZIONE PUBBLICA E PRIVATA"))
			descrittore = "ISTRUZIONE";
		else if(descrittore.equals("LAVORO"))
			descrittore = "LAVORO (RAPPORTO DI)";
		else if(descrittore.equals("LAVORO SUBORDINATO (Contratto particolare)") || descrittore.equals("LAVORO SUBORDINATO (Controversie individuali di)") 
				|| descrittore.equals("LAVORO SUBORDINATO (RAPPORTO DI)") || descrittore.equals("LAVORO SUBORDINATO (Rapporto di)"))
			descrittore = "LAVORO SUBORDINATO";
		else if(descrittore.equals("LOCAZIONE"))
			descrittore = "LOCAZIONE DI COSE";
		else if(descrittore.equals("MARCHI") || descrittore.equals("MARCHIO E ALTRI SEGNI DISTINTIVI") 
				|| descrittore.equals("REGISTRAZIONE DEL MARCHIO") )
			descrittore = "MARCHIO";
		else if(descrittore.equals("MATRIMONIO E RAPPORTI TRA CONIUGI"))
			descrittore = "MATRIMONIO";
		else if(descrittore.equals("NOTAIO E ARCHIVI NOTARILI") || descrittore.equals("NOTAIO ED ATTO NOTARILE") 
				|| descrittore.equals("REGISTRAZIONE DEL MARCHIO") )
			descrittore = "NOTAI E NOTARIATO";
		else if(descrittore.equals("PARTITI POLITICI"))
			descrittore = "PARTITI E MOVIMENTI POLITICI";
		else if(descrittore.equals("PERSONA FISICA"))
			descrittore = "PERSONA FISICA E DIRITTI DELLA PERSONALITÀ";
		else if(descrittore.equals("PERSONE"))
			descrittore = "PERSONE FISICHE E GIURIDICHE";
		else if(descrittore.equals("PERSONE GIURIDICHE PRIVATE")|| descrittore.equals("PERSONE GIURIDICHE PUBBLICHE"))
			descrittore = "PERSONE GIURIDICHE";
		else if(descrittore.equals("PREVIDENZA ED ASSISTENZA")|| descrittore.equals("PREVIDENZA SOCIALE"))
			descrittore = "PREVIDENZA E ASSISTENZA SOCIALE";
		else if(descrittore.equals("PROVA IN GENERE IN MATERIA CIVILE"))
			descrittore = "PROVE NEL PROCESSO CIVILE";
		else if(descrittore.equals("REATO IN GENERE"))
			descrittore = "REATO";
		else if(descrittore.equals("REGIME AMMINISTRATIVO DELLA NAVE E DELL'AEROMOBILE"))
			descrittore = "REGIME GIURIDICO DELLA NAVE E DELL'AEROMOBILE";
		else if(descrittore.equals("RENDITA PERPETUA"))
			descrittore = "RENDITA VITALIZIA";
		else if(descrittore.equals("SEPARAZIONE DEI CONIUGI")|| descrittore.equals("SCIOGLIMENTO DEL MATRIMONIO"))
			descrittore = "SEPARAZIONE PERSONALE DEI CONIUGI";
		else if(descrittore.equals("SEQUESTRO CONSERVATIVO AZIENDA"))
			descrittore = "SEQUESTRO CONSERVATIVO";
		else if(descrittore.equals("ASSOCIAZIONI SINDACALI"))
			descrittore = "SINDACATO";
		else if(descrittore.equals("SPESE GIUDIZIALI IN MATERIA CIVILE"))
			descrittore = "SPESE GIUDIZIALI CIVILI";
		else if(descrittore.equals("SPORT"))
			descrittore = "SPORT E TEMPO LIBERO";
		else if(descrittore.equals("STRANIERO"))
			descrittore = "STRANIERI E APOLIDI";
		else if(descrittore.equals("SUCCESSIONE LEGITTIMA") || descrittore.equals("SUCCESSIONE NECESSARIA"))
			descrittore = "SUCCESSIONE LEGITTIMA E NECESSARIA";
		else if(descrittore.equals("SUCCESSIONI PER CAUSA DI MORTE"))
			descrittore = "SUCCESSIONI MORTIS_CAUSA";
		else if(descrittore.equals("TERMINE"))
			descrittore = "TERMINI";
		else if(descrittore.equals("TRASPORTI") || descrittore.equals("TRASPORTO (CONTRATTO DI)"))
			descrittore = "TRASPORTO";
		else if(descrittore.equals("TRATTAMENTO DEI DATI PERSONALI") || descrittore.equals("TUTELA DELLA RISERVATEZZA (PRIVACY)"))
			descrittore = "TRATTAMENTO DEI DATI PERSONALI E TUTELA DELLA RISERVATEZZA (PRIVACY)";
		else if(descrittore.equals("PROVVEDIMENTI D'URGENZA"))
			descrittore = "PROVVEDIMENTI DI URGENZA (ART. 700 c.p.c.)";
		
		
		if(!classificazioniTOPMap.get(idProvv).contains(descrittore)) {
			classificazioniTOPMap.get(idProvv).add(descrittore);
		}
		
	}
	
	
	private ArrayList<String> splitDescrittoreTopByCase(String descrittoreTop){
		
		
		

		ArrayList<String>descrittoriTop = new ArrayList<String>();
		
		if(isStringUpperCase(descrittoreTop)) {
			descrittoriTop.add(descrittoreTop);
			return descrittoriTop;
		}
		
		
		if(descrittoreTop.startsWith("PROVVEDIMENTI DI URGENZA (ART. 700 c.p.c.)")){
			descrittoriTop.add(descrittoreTop);
			return descrittoriTop;
		}
		
		if(descrittoreTop.contains("C.E.E."))
			descrittoreTop=descrittoreTop.replace("C.E.E.", "CEE");
			
		
		//convert String to char array
		String descrittoreUpperCase ="";
	    char[] charArray = descrittoreTop.toCharArray();
		for(int i=0; i < charArray.length; i++){
	        
			
	        if(!Character.isLowerCase( charArray[i] ) /*&& !(charArray[i]==')')*/ && !(charArray[i]==';')  && !(charArray[i]=='.') && !(charArray[i]==':')/*&& !(charArray[i]=='(')*/) {
	        	if(i<charArray.length-1 && !Character.isLowerCase(charArray[i+1])) {
	        		descrittoreUpperCase +=""+charArray[i];
	        	}
	        }else {
	        	descrittoreUpperCase=descrittoreUpperCase.trim();
	        	if(descrittoreUpperCase.startsWith(")") && descrittoreUpperCase.length()>1)
		        	descrittoreUpperCase=descrittoreUpperCase.substring(1).trim();
	        	if(descrittoreUpperCase.endsWith("(") && descrittoreUpperCase.length()>1)
		        	descrittoreUpperCase=descrittoreUpperCase.substring(0,descrittoreUpperCase.length()-2).trim();
	        
	        	
	        	if(!descrittoriTop.contains(descrittoreUpperCase) && 
	        	  !descrittoreUpperCase.equals("(") && !descrittoreUpperCase.equals(")")  && !descrittoreUpperCase.equals(",")
	        	  && !descrittoreUpperCase.equals("IVA") && !descrittoreUpperCase.equals("-") && !descrittoreUpperCase.equals(":")) {
	        		descrittoriTop.add(descrittoreUpperCase);
	        	}
	        	
	        	descrittoreUpperCase ="";
	        }
	            
	    }
		
		// RIFACCIO OPERAZIONI PRECEDENTI PER ULTIMO DESCRITTORE
		descrittoreUpperCase=descrittoreUpperCase.trim();
		if(descrittoreUpperCase.length()>0 ) {
			if(!Character.isLowerCase(charArray[charArray.length-1])) {
				descrittoreUpperCase +=""+charArray[charArray.length-1];
			}
		}
		descrittoreUpperCase=descrittoreUpperCase.trim();
    	if(descrittoreUpperCase.startsWith(")") && descrittoreUpperCase.length()>1)
        	descrittoreUpperCase=descrittoreUpperCase.substring(1).trim();
    	if(descrittoreUpperCase.endsWith("(") && descrittoreUpperCase.length()>1)
        	descrittoreUpperCase=descrittoreUpperCase.substring(0,descrittoreUpperCase.length()-2).trim();
		if(!descrittoriTop.contains(descrittoreUpperCase) && 
	        	  !descrittoreUpperCase.equals("(") && !descrittoreUpperCase.equals(")")  && !descrittoreUpperCase.equals(",")
	        	  && !descrittoreUpperCase.equals("IVA") && !descrittoreUpperCase.equals("-")) {
	        		descrittoriTop.add(descrittoreUpperCase);
	    }
		
		return descrittoriTop;
	}
	
	
	
	private static boolean isStringUpperCase(String str){
	    
	    //convert String to char array
	    char[] charArray = str.toCharArray();
	    
	    for(int i=0; i < charArray.length; i++){
	        
	        //if any character is in lower case, return false
	        if( Character.isLowerCase( charArray[i] ))
	            return false;
	    }
	    
	    return true;
	}

	
	private String stripQuotes(String title) {
		
		
		return title.replaceAll("\"", "").trim();
		
//		if(title.startsWith("\\\""))
//			title=title.substring(2,title.length());
//		if(title.startsWith("\""))
//			title=title.substring(1,title.length());
//		if(title.endsWith("\\\""))
//			title=title.substring(0,title.length()-2);
//		if(title.endsWith("\""))
//			title=title.substring(0,title.length()-1);
//		
//		//System.out.println(title);
//		return title.trim();
	}
	
	
	private String stripSquareBrackets(String title) {
		
		if(title.indexOf("[")!=-1) {
			title=title.substring(0,title.indexOf("[")-1).trim();
		}
		if(title.indexOf("=>")!=-1) {
			title=title.substring(0,title.indexOf("=>")-2).trim();
		}
		return title.trim();
	}
	
	public void printClassificazioniProvvMap() {
		String ret;
		for(Provvedimento provv : provv_Merge_Map.values()) {			
			ret="";
			ret+=provv.getPersistentID()+Config.SEP
					//+provv.isEstremoCompleto()+Config.SEP
					//+provv.getProvv_year()+Config.SEP
					//+provv.getDistrettoCorteAppello()+Config.SEP
					//+provv.getGuessed_subject()+Config.SEP
					//+provv.getEstremoUniforme()+Config.SEP
					+classificazioniTopCSV(provv.getPersistentID())+Config.SEP
					+sistematiciCSV(provv.getFonti())+Config.SEP
					//+descrittoriCSV(provv.getFonti())+Config.SEP
					+provv.getProvenance();
			System.err.println(ret);
		}
	}
	
	

	
	public void printSingleColumnClassificazioniTOP_Civile() {

		for(Provvedimento provv : provv_Merge_Map.values()) {
			if(!provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.") && !provv.getGuessed_subject().equalsIgnoreCase("PENALE")) {
				if(provv.getClassificazioniTOP().size()>0) {
					for(int i=0;i<provv.getClassificazioniTOP().size();i++) {
						if(provv.getClassificazioniTOP().get(i).trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniTOP().get(i)+Config.SEP+provv.getProvenance());
					}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}

	
	public void printSingleColumnClassificazioniTOP_Penale() {

		for(Provvedimento provv : provv_Merge_Map.values()) {
			if(!provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.") && provv.getGuessed_subject().equalsIgnoreCase("PENALE")) {
				if(provv.getClassificazioniTOP().size()>0) {
					for(int i=0;i<provv.getClassificazioniTOP().size();i++) {
						if(provv.getClassificazioniTOP().get(i).trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniTOP().get(i)+Config.SEP+provv.getProvenance());
					}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	public void printSingleColumnClassificazioniCSMFinal() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(!provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")) {
				if(provv.getClassificazioniCSMFinal()!=null && provv.getClassificazioniCSMFinal().length>0) {
					for(int i=0;i<provv.getClassificazioniCSMFinal().length;i++) {
						if(provv.getClassificazioniCSMFinal()[i].trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniCSMFinal()[i].trim()+Config.SEP+provv.getProvenance());
					}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	
	public void printSingleColumnClassiDeJurePenale() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(!provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.") && provv.getGuessed_subject().equalsIgnoreCase("PENALE")) {
				if(provv.getClassificazioniCSMFinal()!=null && provv.getClassificazioniCSMFinal().length>0) {
					for(int i=0;i<provv.getClassificazioniCSMFinal().length;i++) {
						if(provv.getClassificazioniCSMFinal()[i].trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniCSMFinal()[i].trim()+Config.SEP+provv.getProvenance());
					}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	
	public void printSingleColumnClassificazioniCOSTPrimoLiv() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")) {
				if(provv.getClassificazioniCSMFinal().length>0) {
					//for(int i=0;i<provv.getClassificazioniCSMFinal().length;i++) {
						if(provv.getClassificazioniCSMFinal()[0].trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniCSMFinal()[0].trim()+Config.SEP+provv.getProvenance()+Config.SEP+provv.getGuessed_subject());
					//}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	
	public void printSingleColumnClassificazioniCOSTSecondoLiv() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")) {
				if(provv.getClassificazioniCSMFinal().length>0) {
					//for(int i=0;i<provv.getClassificazioniCSMFinal().length;i++) {
						if(provv.getClassificazioniCSMFinal()[1].trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniCSMFinal()[1].trim()+Config.SEP+provv.getProvenance()+Config.SEP+provv.getGuessed_subject());
					//}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	
	public void printSingleColumnClassificazioniCOST() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")) {
				if(provv.getClassificazioniCSMFinal().length>0) {
					for(int i=0;i<provv.getClassificazioniCSMFinal().length;i++) {
						if(provv.getClassificazioniCSMFinal()[i].trim().length()>0)
							System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniCSMFinal()[i].trim()+Config.SEP+provv.getProvenance());
					}
				}else {
					System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
				}
			}
		}
	}
	
	
	
	public void printSingleColumnClassiDeJure() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			if(provv.getClassificazioniTOP().size()>0) {
				for(int i=0;i<provv.getClassificazioniTOP().size();i++) {
					if(provv.getClassificazioniTOP().get(i).trim().length()>0)
						System.err.println(provv.getPersistentID()+Config.SEP+provv.getClassificazioniTOP().get(i)+Config.SEP+provv.getProvenance());
				}
			}else {
				System.err.println(provv.getPersistentID()+Config.SEP+""+Config.SEP+provv.getProvenance());
			}
		}
	}
	
	
	
	private String classificazioniTopCSV(String id) {
		String ret="";
		List<String> classificazioniTop = classificazioniTOPMap.get(id);
		for(int i=0;i<classificazioniTop.size();i++) {
			if(classificazioniTop.get(i).trim().length()>0)
				ret+=classificazioniTop.get(i)+"#";
			
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	
	private String sistematiciCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getSistematici().length>0)
				ret+=printList(fonti.get(i).getSistematici())+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String descrittoriCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=printList(fonti.get(i).getDescrittori())+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String printList(String[] list){
		
		String ret="";

		if(list!=null){
			for(int i=0;i<list.length;i++){
				ret+=list[i]+";";
			}
			if(ret.length()>0)
				ret=ret.substring(0, ret.length()-1).trim();
		}
		return ret;
	}

	
	
}
