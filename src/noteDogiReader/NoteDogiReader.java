package noteDogiReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import config.Config;
import fonte.Fonte;
import nota.Nota;
import provvedimento.Provvedimento;
import util.DistrettiCA;
import util.Util;

public class NoteDogiReader {


	HashMap<String,Nota> uniqueNote = new HashMap<String,Nota>();
	HashMap<String,Fonte> uniqueFonti = new HashMap<String,Fonte>();
	HashMap<String,Provvedimento> provvedimenti = new HashMap<String,Provvedimento>();


	HashMap<String,List<String>> tabSistematici = new HashMap<String,List<String>>();
	HashMap<String,List<String>> tabDescrittori = new HashMap<String,List<String>>();
	HashMap<String,List<String>> tabFonti = new HashMap<String,List<String>>();

	HashMap<String,List<String>> lookupSistematici = new HashMap<String,List<String>>();

	
	
	HashMap<String,String> lookupFindabilityEditori = new HashMap<String,String>();



	
	public void readLookupFindabilityEditori_TSV() {

		String tsvFilePath=Config.TAB_FINDABILITY_EDITORI;
		File editoriTSV = new File(tsvFilePath);
		if(!editoriTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(editoriTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String editore = fields[0];
				String publisher = fields[2];

				String label = editore;

				if(lookupFindabilityEditori.get(label)==null){		
					lookupFindabilityEditori.put(label,publisher);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}


	



	// TODO  - da qui risalire al Sistematico ROOT dato il Sistematico
	public void readLookupSistematici_DoGi_TSV(){


		String tsvFilePath=Config.TAB_SISTEMATICI;
		File sistematiciTSV = new File(tsvFilePath);
		if(!sistematiciTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(sistematiciTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String codiceSistematico = fields[0];
				if(codiceSistematico.indexOf(".")!=-1)
					codiceSistematico= codiceSistematico.substring(0, fields[0].indexOf(".")).trim();
				String label = fields[3].trim();

				// log
				//System.err.println("-- ***lookupSistematici - ADDING "+label);

				if(lookupSistematici.get(label)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(codiceSistematico);
					lookupSistematici.put(label, lista);
				}else{
					List<String> lista = lookupSistematici.get(label);   
					lista.add(codiceSistematico);
					lookupSistematici.put(label, lista);
				}

			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void readSistematiciDogi_TSV(){


		String tsvFilePath=Config.DATA_INPUT_SISTEMATICI;
		File sistematiciTSV = new File(tsvFilePath);
		if(!sistematiciTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(sistematiciTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String idContributoDogi = fields[0].trim();
				String sistematico = fields[2].trim();

				if(tabSistematici.get(idContributoDogi)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(sistematico);
					tabSistematici.put(idContributoDogi, lista);
				}else{
					List<String> lista = tabSistematici.get(idContributoDogi);   
					if(!lista.contains(sistematico)){
						lista.add(sistematico);
						tabSistematici.put(idContributoDogi, lista);
					}

				}

			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}





	public void  readDescrittoriDogi_TSV(){


		String tsvFilePath=Config.DATA_INPUT_DESCRITTORI;
		File descrittoriTSV = new File(tsvFilePath);
		if(!descrittoriTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(descrittoriTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String idContributoDogi = fields[0].trim();
				String descrittore = fields[2].trim();

				if(tabDescrittori.get(idContributoDogi)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(descrittore);
					tabDescrittori.put(idContributoDogi, lista);
				}else{
					List<String> lista = tabDescrittori.get(idContributoDogi);   
					if(!lista.contains(descrittore)){
						lista.add(descrittore);
						tabDescrittori.put(idContributoDogi, lista);
					}

				}

			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void  readFontiJur_Leg_Dogi_TSV(){


		String tsvFilePath=Config.DATA_INPUT_FONTI;
		File fontiTSV = new File(tsvFilePath);
		if(!fontiTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(fontiTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String idContributoDogi = fields[0].trim();
				String fonte = fields[2].trim();

				if(tabFonti.get(idContributoDogi)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(fonte);
					tabFonti.put(idContributoDogi, lista);
				}else{
					List<String> lista = tabFonti.get(idContributoDogi);   
					if(!lista.contains(fonte)){
						lista.add(fonte);
						tabFonti.put(idContributoDogi, lista);
					}

				}

			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}






	// _ - id	_ - ISSN	_ - Testo	_ - Titolo	_ - Rivista	_ - editore	_ - NomeAutore	_ - CognomeAutore	_ - DataFascicolo	_ - PaginaIniziale	_ - NumeroFascicolo


	// 1' passaggio note ripetute in caso di autori multipli, una riga per ogni autore con stesso id contributo  --> nota unica con autore multiplo, stesso id
	public boolean readNoteDogiTSV() {

		String tsvFilePath=Config.DATA_INPUT_NOTE;
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

			String estremoForParser ="";


			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				String[] fields = line.split("\t");


				String idContributo = fields[0].trim();
				String ISSN = fields[1].trim();
				String estremo = fields[2].trim();
				String titoloContributo = stripQuotes(fields[3].trim());
				String rivista = fields[4].trim();
				String editore = fields[5].trim();
				String nomeAutore = fields[7].trim();
				String cognomeAutore = fields[6].trim();
				String dataFascicolo = fields[8].trim();
				String pagIniziale = fields[9].trim();
				String numFascicolo = fields.length>10?fields[10].trim():"";


				Nota n = new Nota();
				n.setIdContributo(idContributo);
				n.setISSN(ISSN);
				n.setEstremo(estremo);
				n.setTitoloContributo(titoloContributo);
				n.setRivista(rivista);
				n.setEditore(editore);
				n.setCognomeAutore(cognomeAutore);
				n.setNomeAutore(nomeAutore);
				n.setDataFascicolo(dataFascicolo);
				n.setPagIniziale(pagIniziale);
				n.setNumFascicolo(numFascicolo);
				n.setAutori(cognomeAutore+" "+nomeAutore);


				if(uniqueNote.get(idContributo)==null){
					uniqueNote.put(idContributo, n);
				}else{    // CONTRIBUTO CON AUTORI MULTIPLI - li separo con "|"
					Nota temp = uniqueNote.get(idContributo);
					temp.setCognomeAutore(temp.getCognomeAutore()+" | "+cognomeAutore);
					temp.setNomeAutore(temp.getNomeAutore()+" | "+nomeAutore);
					temp.setAutori(temp.getAutori()+" | "+cognomeAutore+" "+nomeAutore);
					uniqueNote.put(idContributo, temp);

				}
			}


			//		    2 passaggi: 
			//		    	1 - accorpare autori dello stesso contributo (stesso idContributo, concatena autori - oggetto NOTA?)
			//				2- creare altra lista splitted - 
			//		    	2-  splittare contributi per più sentenze commentate  


			// DELLA LISTA CONTRIB UNICI CREARNE UN'ALTRA SPLITTATA PER PROVVEDIMENTO ANNOTATO
			// oggetto provvedimento - fonteDottrina - fontilegislative - Descrittori - Sistematici


		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}


	private String stripQuotes(String title) {
		
		if(title.startsWith("\""))
			title=title.substring(1,title.length());
		if(title.endsWith("\""))
			title=title.substring(0,title.length()-1);
		//System.out.println(title);
		return title;
	}


	/**
	 * 	ISTANZIA I PROVVEDIMENTI A PARTIRE DALLA LISTA DELLE NOTE A SENTENZA (uniche)
	 * @return
	 */
	public boolean istanziaProvvedimenti() {

		int idProvvedimento =1;
		// QUI SETTA ID PROVVEDIMENTO..
		
		for(String idContributoDogi : uniqueNote.keySet()){

			// istanzia provvedimento completo (con estremi parsati) e idProvvedimento 
			// istanzia fonte con classificazioni e fonti leg  (classificazione e fonti leg vanno recuperati tramite idContributo)
			// su provvedimento assegna oggetto fonte
			// serializzazione completa lista dei provvedimenti


			// 1 solo provvedimento annotato es. 
			// Nota a Trib. Milano 6 febbraio 2014, n. 419
			if(uniqueNote.get(idContributoDogi).getEstremo().indexOf(";")==-1){
				String dogiCommentedWith = "";
				Provvedimento p =  createProvvedimento(""+idProvvedimento,uniqueNote.get(idContributoDogi).getEstremo(),dogiCommentedWith,idContributoDogi);
				// qui in teoria se il provvedimento c'è già commentato gli andrebbe aggiunta "SOLO" la nuova fonte
				

				String estremo = p.getEstremoText();



				if(skipProvvedimento(estremo.trim()))
					continue;



				String found ="";
				if((found = findEstremoInProvvedimenti(estremo))==null) {
					provvedimenti.put(""+idProvvedimento, p);
				}else {
					Fonte newFonte = p.getFonti().get(0);
					provvedimenti.get(found).addFonte(newFonte);
				}
				idProvvedimento++;
			}else{
				// Più provvedimenti annotati es.
				// Nota a ord. Trib. Palermo 10 giugno 2013; ord. Cass. sez. VI 18 febbraio 2014, n. 3838

				String provvList[] = uniqueNote.get(idContributoDogi).getEstremo().split(";");


				for(int j=0;j<provvList.length;j++){


					// qui aggiungere la proprietà (dogiCommentedWith..)
					String dogiCommentedWith = "";
					for(int k=0;k<j;k++) {
						dogiCommentedWith+=provvList[k]+";";
					}
					for(int k=j+1;k<provvList.length;k++) {
						dogiCommentedWith+=provvList[k]+";";
					}
					if(dogiCommentedWith.length()>1)
						dogiCommentedWith = dogiCommentedWith.substring(0, dogiCommentedWith.length()-1).trim();

					Provvedimento p =  createProvvedimento(""+idProvvedimento,provvList[j],dogiCommentedWith,idContributoDogi);
					String estremo = p.getEstremoText();


					if(skipProvvedimento(estremo.trim()))
						continue;

					String found ="";
					if((found = findEstremoInProvvedimenti(estremo))==null) {
						provvedimenti.put(""+idProvvedimento, p);
					}else {
						Fonte newFonte = p.getFonti().get(0);
						provvedimenti.get(found).addFonte(newFonte);
					}
					idProvvedimento++;
				}	
			}
		}
		return true;
	}



	private boolean skipProvvedimento(String estremo) {


		if(estremo.endsWith(")")) {    // SALTA TUTTE LE NOTE A GIURISP STRANIERA ?
			//System.err.println("******   SKIPPING "+estremo);
			return true;
		}
		if(estremo.indexOf("Comm. Reg.")!=-1 || 
				estremo.indexOf("Comm. Prov.")!=-1 || 
				estremo.indexOf("Giust. Amm.")!=-1  || 
				estremo.indexOf("arbitr.")!=-1 || 
				estremo.indexOf("Commissione garanzia")!=-1 || 
				estremo.indexOf("conciliatore")!=-1 || 
				estremo.indexOf("garante protezione dati")!=-1 || 
				estremo.indexOf("sentenza _")!=-1 || 
				estremo.indexOf("Tribunale Mil.")!=-1 || 
				estremo.indexOf("Tribunale intern")!=-1 || 
				estremo.indexOf(" TAR ")!=-1 || 
				estremo.indexOf("C. Conti")!=-1 || 
				estremo.indexOf("Cons. Stato")!=-1 || 
				estremo.indexOf("Acque")!=-1) {
			//System.err.println("******   SKIPPING "+estremo);
			return true;
		}

		if(estremo.equalsIgnoreCase("sentenza") || estremo.indexOf("c.c")!=-1 ) {
			System.err.println("******   SKIPPING "+estremo);
			return true;
		}

		return false;
	}

	private String findEstremoInProvvedimenti(String estremo) {

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getEstremoText().trim().equalsIgnoreCase(estremo.toLowerCase().trim())) {
				//System.out.println("FOUND DUPLICATE - "+estremo + "    "+p.getEstremoText());
				return p.getIdProvvedimento();
			}
		}
		return null;
	}
	
	
	private String getPublisherForEditore(String editore) {
		String publisher =lookupFindabilityEditori.get(editore);
		if(publisher==null) {
			return "altro";
		}
		return publisher;
	}
	

	private Provvedimento createProvvedimento(String idProvvedimento, String estremoProvvedimento, String dogiCommentedWith, String idContributoDogi){


		Nota n = uniqueNote.get(idContributoDogi);	
		Provvedimento p = new Provvedimento();
		p.setIdProvvedimento(idProvvedimento);
		// QUI spacchetta l'estremo con LINKOLN
		p.setEstremoDogi(estremoProvvedimento);

		// A QUESTO PUNTO GLI ESTREMI DEL PROVVEDIMENTO SONO SETTATI CON CHIAMATA LINKOLN
		DistrettiCA distretti = new DistrettiCA();
		p = distretti.setDistrettoAppello(p);

		p.setCommentedWith(dogiCommentedWith);   // conserva gli altri provvedimenti annotati dalla stessa nota a sentenza
		p.setProvenance("DoGi");


		// idContributoDogi già processato  (caso di più provvedimenti annotati dalla stessa fonte dottrinale)
		if(uniqueFonti.get(idContributoDogi)!=null){
			p.addFonte(uniqueFonti.get(idContributoDogi));
		}else{
			Fonte f = new Fonte();
			f.setIdContributo(idContributoDogi);
			f.setAutori(n.getAutori());
			f.setCognomeAutore(n.getCognomeAutore());
			f.setNomeAutore(n.getNomeAutore());
			f.setNumFascicolo(n.getNumFascicolo());
			f.setDataFascicolo(n.getDataFascicolo());
			f.setEditore(n.getEditore());
			
			f.setFindableIn(getPublisherForEditore(f.getEditore()));
			
			f.setPagIniziale(n.getPagIniziale());
			f.setRivista(n.getRivista());
			f.setTitoloContributo(n.getTitoloContributo());
			f.setISSN(n.getISSN());

			// settare Classificazioni, Fonti - leg
			if(tabSistematici.get(idContributoDogi)!=null)
				f.setSistematici(tabSistematici.get(idContributoDogi).toArray(new String[0]));
			if(tabDescrittori.get(idContributoDogi)!=null)
				f.setDescrittori(tabDescrittori.get(idContributoDogi).toArray(new String[0]));
			if(tabFonti.get(idContributoDogi)!=null)
				f.setFonti_Jur_Leg(tabFonti.get(idContributoDogi).toArray(new String[0]));

			// qui settare grande voce (grandi voci)  - in base a SISTEMATICI
			// risale alla ROOT dei Sistematici associati alla fonte 
			String[] labelSistematici = tabSistematici.get(idContributoDogi).toArray(new String[0]);
			List<String> grandiVociDogi = new ArrayList<String>();

			for(int i=0;i<labelSistematici.length;i++){
				String findLabel = labelSistematici[i];
				List<String> rootSistematici = lookupSistematici.get(findLabel);
				if(rootSistematici==null){
					System.out.println("UNABLE TO FIND ROOT SISTEMATICO per "+findLabel);
				}else{
					for (int k = 0; k < rootSistematici.size(); k++){
						if(!grandiVociDogi.contains(rootSistematici.get(k)))
							grandiVociDogi.add(rootSistematici.get(k));
					}

				}

			}
			String grandiVoci = "";
			for (int i = 0; i < grandiVociDogi.size(); i++){
				grandiVoci+=grandiVociDogi.get(i)+";";

			}
			if(grandiVoci.length()>0){
				grandiVoci=grandiVoci.substring(0, grandiVoci.length()-1);
			}


			f.setGrandeVoceDogi(grandiVoci);

			p.addFonte(f);
			uniqueFonti.put(idContributoDogi, f);
		}

		// PROVA UN GUESS SU AREA: CIVILE/PENALE/LAVORO in base a sez. negli estremi e ROOT Sistematici Dogi
		p = guessAreaCivilePenale(p);
		
		
		// attribuisci ID persistente basato su ESTREMI del provvedimento
		String newID = Util.computeNewID(p);
		//System.err.println("****   "+newID+"\t"+p.getIdProvvedimento());
		p.setPersistentID(newID);
		
		return p;
	}
	
	
	private String grandeVoceDogiCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getGrandeVoceDogi()+";";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	private Provvedimento guessAreaCivilePenale(Provvedimento p) {
		
		String guessedSubject = "";
		
		String classiDogi = grandeVoceDogiCSV(p.getFonti());
		String linkolnCourtSubject = p.getCourt_subject();
		String estremoTextLower = p.getEstremoText().toLowerCase();
		
		
		//System.out.println(p.getEstremoText()+Config.SEP+linkolnCourtSubject+Config.SEP+classiDogi);


		// CASO AREA PRESA DAGLI ESTREMI DEL PROVVEDIMENTO
		if(linkolnCourtSubject!=null) {
			if(linkolnCourtSubject.equalsIgnoreCase("CIVIL"))
				guessedSubject = "CIVILE";
			if(linkolnCourtSubject.equalsIgnoreCase("CRIMINAL"))
				guessedSubject = "PENALE";
		}else {
			if(estremoTextLower.indexOf(" gip ")!=-1 || estremoTextLower.indexOf(" gup ")!=-1)
				guessedSubject = "PENALE";
			else if(classiDogi.indexOf("LAV")!=-1)
				guessedSubject = "LAVORO";
			else if(classiDogi.indexOf("CIV")!=-1 && classiDogi.indexOf("PEN")==-1 )
				guessedSubject = "CIVILE";
			else if(classiDogi.indexOf("PEN")!=-1 && classiDogi.indexOf("CIV")==-1 )
				guessedSubject = "PENALE";
			else if(classiDogi.indexOf("COMM")!=-1)
				guessedSubject = "COMMERCIALE";
			else if(classiDogi.indexOf("IND")!=-1)
				guessedSubject = "INDUSTRIALE";
			else if(classiDogi.indexOf("NAV")!=-1)
				guessedSubject = "CIVILE";
			else if(classiDogi.indexOf("TRIB")!=-1)
				guessedSubject = "CIVILE";
			else
				guessedSubject = "N.D.";	
		}
		
		// SETTA NELL'OGGETTO PROVVEDIMENTO - il distretto CA
		p.setGuessed_subject(guessedSubject);
		return p;
	}
	
	
	

	
	
	
	
	public boolean printEditori() {
		
		String header =  "id"+Config.SEP+"distretto"+Config.SEP+"anno"+Config.SEP+"editore"; 



		System.err.println(header);
		//System.err.println(header);

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			//			p.printCsv();
			if(p.getProvv_year()==null )
				p.printListEditoriCSV();
			else{
				//System.out.println(p.getProvv_year());
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					p.printListEditoriCSV();
			}
		}
		return true;
	}
	
	
	
	

	public boolean printProvvedimenti() {


		//String header_old_old =  "dcterms:identifier"+Config.SEP+"dcterms:title"+Config.SEP+"dcterms:type"+Config.SEP+"bibo:court"+Config.SEP+"dcterms:coverage"+Config.SEP+"dcterms:coverage"+Config.SEP+"dcterms:spatial"+Config.SEP+"bibo:identifier"+Config.SEP+"dcterms:subject"+Config.SEP+"dcterms:tableOfContents"+Config.SEP+"dcterms:references"+Config.SEP+"dcterms:source"+Config.SEP+"bibo:authorList"+Config.SEP+"dcterms:bibliographicCitation"+Config.SEP+"dcterms:publisher";

		//String header_old =  "dcterms:identifier"+Config.SEP+"dcterms:issued"+Config.SEP+"dcterms:title"+Config.SEP+"dcterms:type"+Config.SEP+"bibo:court"+Config.SEP+"dcterms:coverage"+Config.SEP+"dcterms:coverage"+Config.SEP+"dcterms:spatial"+Config.SEP+"bibo:identifier"+Config.SEP+"dcterms:subject"+Config.SEP+"dcterms:tableOfContents"+Config.SEP+"dcterms:references"+Config.SEP+"dcterms:source"+Config.SEP+"dcterms:bibliographicCitation"+Config.SEP+"dcterms:publisher"+Config.SEP+"dogi:commentedWith";

		
//		String header =  "id"+Config.SEP+"distretto"+Config.SEP+"anno"+Config.SEP+"tipo_ufficio"+Config.SEP+"localita_ufficio"+Config.SEP+"tipo_provvedimento"+Config.SEP+"estremi"+Config.SEP+"settore"+Config.SEP+"fonte"+Config.SEP+"editore"+Config.SEP+"provenienza"+Config.SEP+"versione"; 
//
//		String header_omeka =  "dcterms:identifier"+Config.SEP+"bibo:court"+Config.SEP+"dcterms:issued"+Config.SEP+"dcterms:creator"+Config.SEP+"dcterms:coverage"+Config.SEP+"dcterms:type"+Config.SEP+"dcterms:title"+Config.SEP+"dcterms:subject"+Config.SEP+"dcterms:bibliographicCitation"+Config.SEP+"dcterms:publisher"+Config.SEP+"dcterms:provenance"+Config.SEP+"dcterms:hasVersion"; 

		
		
		
		String header ="persistentID"+Config.SEP+"idProvvedimento"+Config.SEP+"distretto"+Config.SEP+"anno"+Config.SEP+
				"tipo_ufficio"+Config.SEP+"localita_ufficio"+Config.SEP+"tipo_provvedimento"+Config.SEP+"provv_data"+Config.SEP+
				"provv_numero"+Config.SEP+"provv_sezione"+Config.SEP+"settore"+Config.SEP+
				"presidente"+Config.SEP+"relatore"+Config.SEP+"giudice"+Config.SEP+"estensore"+Config.SEP+"parti"+Config.SEP+
				"NumRG"+Config.SEP+"dataUdienza"+Config.SEP+"hasFullText"+Config.SEP+"isMassimaRedazionale"+Config.SEP+"isAbstractRivista"+Config.SEP+
				"nota"+Config.SEP+"url"+Config.SEP+"completato"+Config.SEP+
				"estremi"+Config.SEP+"fonte"+Config.SEP+"editore"+Config.SEP+"findable_IN"+Config.SEP+
				"provenance"+Config.SEP+"versione";
		
		
		
		System.err.println(header);
		//System.err.println(header_omeka);

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			//			p.printCsv();
			if(p.getProvv_year()==null )
				p.printExplodedProvvedimentoCSV();
			else{
				//System.out.println(p.getProvv_year());
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					p.printExplodedProvvedimentoCSV();
			}
		}
		return true;
	}
	
	
	
	public boolean printExplodedProvvedimenti() {

		
		String header ="persistentID"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"pageNum"+Config.SEP
				+"estremo_completo"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
				+"giudici"+Config.SEP
				+"parti"+Config.SEP
				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP			
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"idContributo"+Config.SEP
				+"titolo"+Config.SEP
				+"autori"+Config.SEP
				+"url_contributo"+Config.SEP
				+"rivista"+Config.SEP
				+"editore"+Config.SEP
				+"findable_IN"+Config.SEP
				+"fonte"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		System.err.println(header);
		//System.err.println(header_omeka);

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			
			if(p.getProvv_year()==null )
				p.printExplodedProvvedimentoCSV();
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					p.printExplodedProvvedimentoCSV();
			}
		}
		return true;
	}
	
	
	
	public List<Provvedimento> getProvvedimenti() {
		List<Provvedimento> filtered_provv_list = new ArrayList<Provvedimento>();
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			
			if(p.getProvv_year()==null )
				filtered_provv_list.add(p);
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					filtered_provv_list.add(p);
			}
		}
		
		return filtered_provv_list;
	}
	
	
}

