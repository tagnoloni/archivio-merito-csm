import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import classificazioni.CleanClassificazioni;
import config.Config;
import consegne.ConsegneReader;
import deJureDataReader.DeJureCivileDataReader;
import deJureDataReader.DeJureDataReader;
import deJureDataReader.DeJurePenaleDataReader;
import deJureDataReader.DeJureSpogliDataReader;
import editManualiDataReader.EditManualiDataReader;
import fonte.Fonte;
import merge.mergeProvvedimenti;
import noteDogiReader.NoteDogiReader;
import ordinanzeCostDataReader.OrdinanzeCostDataReader;
import provvedimento.Provvedimento;
import wkiDataReader.WKIDataReader;
import wkiDataReader.WKISpogliDataReader;



// duplicati Dogi:
// 1 provvedimento --> N Fonti


// duplicati DeJure
// provvedimenti sia in NAS che in Commentata (prendi commentata - compare per titoloContributo)

// altri duplicati DeJure ?  1 provvedimento --> N Fonti


// merge/duplicati Dogi in DeJure (prendi deJure con estremo (e campi?) Dogi )



// poi: recupero editing manuale ?? 

public class Run {


	public static void main (String[] args) {
	//	runDeJure();
	//	runDogi();
	//	runWKI();
		
		
	//	runDeJureSpogli();
		
	//	testConsegneReader();
		
		mergeProvvedimenti();
		
	//	testReadSameAS();
		
	}
	
	
	private static void testConsegneReader() {
		ConsegneReader cR = new ConsegneReader();
		//cR.readConsegnati();
		//cR.readScartati();
		cR.readRisposte();
	}
	
	private static void testReadSameAS() {
		////////////////
		// MERGE SAME_AS
		////////////////
		mergeProvvedimenti mp = new merge.mergeProvvedimenti();

		String tsvSAME_AS_FilePath=Config.DATA_SAME_AS;
		mp.mergeSAME_AS(tsvSAME_AS_FilePath);

	}
	
	
	private static void runDeJureSpogli() {
		DeJureSpogliDataReader djSpoglidr = new DeJureSpogliDataReader();
		
		String tsvFilePath_1=Config.DATA_SPOGLI_DEJURE_COMMENTATA_APPELLO;
		String tsvFilePath_2=Config.DATA_SPOGLI_DEJURE_COMMENTATA_TRIBUNALI;
 		String tsvFilePath_3=Config.DATA_SPOGLI_DEJURE_NOTE_SENTENZA_APPELLO;
		
 		// MISSING 23-2
 		String tsvFilePath_4=Config.DATA_INPUT_DEJURE_NOTE_SENTENZA_TRIBUNALI;

		djSpoglidr.readDeJureSpogliTSV(tsvFilePath_1);	
		djSpoglidr.readDeJureSpogliTSV(tsvFilePath_2);	
		djSpoglidr.readDeJureSpogliTSV(tsvFilePath_3);	
		djSpoglidr.readDeJureSpogliTSV(tsvFilePath_4);	

		System.err.println("TOT SPOGLI: "+djSpoglidr.getProvvedimenti().size());
		for(Provvedimento provv : djSpoglidr.getProvvedimenti()) {
			provv.printSpogliSingleRowProvvedimento();
		}
		
	}
	
	
	
	private  static HashMap<String,Provvedimento> readAndCleanScrapeCivileDeJure() {
		
		DeJureCivileDataReader djCiviledr = new DeJureCivileDataReader();

		System.out.println("-- START READ CIVILE TSV --");
		djCiviledr.readLookupComuni_TSV();
		djCiviledr.readLookupDistrettiCA_TSV();
		
		///////////////////
		// SCRAPE LEXADOC
		///////////////////
	
		
//		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1;
//		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
//		tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_2;
		
		////////////
		//
		// CIVILE
		//
		////////////
		// MEGLIO FARLO DA FILE UNICO...
		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1_2_3;
		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
		djCiviledr.alphaOrderForMultipleCat();
	
		List<String> idSelezionati = djCiviledr.readProvvedimentiSelezionati(Config.FOLDER_SELEZIONE_LEXADOC_CIVILE); 
		
		return djCiviledr.getProvvedimentiScrapeDeJureSelezionati(idSelezionati);
		
		
		
	}
	
	
	private  static HashMap<String,Provvedimento> readAndCleanScrapeLavoroDeJure() {
		
		DeJureCivileDataReader djCiviledr = new DeJureCivileDataReader();

		System.out.println("-- START READ CIVILE TSV --");
		djCiviledr.readLookupComuni_TSV();
		djCiviledr.readLookupDistrettiCA_TSV();
		

		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_4;
		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
		djCiviledr.alphaOrderForMultipleCat();
	
		List<String> idSelezionati = djCiviledr.readProvvedimentiSelezionati(Config.FOLDER_SELEZIONE_LEXADOC_LAVORO); 
		
		return djCiviledr.getProvvedimentiScrapeDeJureSelezionati(idSelezionati);
		
		
		
	}
	
	
	private  static HashMap<String,Provvedimento> readAndCleanScrapePenaleDeJure() {
		
		DeJurePenaleDataReader djPenaledr = new DeJurePenaleDataReader();

		System.out.println("-- START READ PENALE TSV --");
	
		///////////////////
		// SCRAPE LEXADOC
		///////////////////
	
		
//		String tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_1;
//		djCiviledr.readDeJureCivileLexaDocTSV(tsvFilePath);	
//		tsvFilePath=Config.DATA_INPUT_DEJURE_LEXADOC_2;
		
		////////////
		//
		// PENALE
		//
		////////////
		String tsvFilePath=Config.DATA_INPUT_DEJURE_PENALE_TRIB_APP_ASSISE;
		djPenaledr.readDeJurePenaleLexaDocTSV(tsvFilePath);
		djPenaledr.alphaOrderForMultipleCat();
		

		List<String> idSelezionati = djPenaledr.readProvvedimentiSelezionati(Config.FOLDER_SELEZIONE_LEXADOC_PENALE); 
		
		return djPenaledr.getProvvedimentiScrapeDeJureSelezionati(idSelezionati);
		
	}
	
	
	private  static HashMap<String,Provvedimento> readAndCleanScrapeCOST() {
		

		OrdinanzeCostDataReader OrdCostDr = new OrdinanzeCostDataReader();
	
		String tsvFilePath=Config.DATA_ORDINANZE_COST;
		OrdCostDr.readOrdinanzeCostTSV(tsvFilePath);
				
		return OrdCostDr.getProvvedimentiCOST();
	}
	
	
	private static void mergeProvvedimenti() {
		
		
		HashMap<String,Provvedimento> serializedProvvMap = null;
		
		
		File prov_Map_serial = new File(Config.serializedProvvMap);
		if(!prov_Map_serial.exists()) {

			
			
			mergeProvvedimenti mp = new merge.mergeProvvedimenti();

			//////////////
			// DE JURE
			//////////////
			DeJureDataReader djdr = new DeJureDataReader();

			System.err.println("-- START READ TSV --");
			String tsvFilePath_1=Config.DATA_INPUT_DEJURE_NOTE_SENTENZA_APPELLO;
			String tsvFilePath_2=Config.DATA_INPUT_DEJURE_NOTE_SENTENZA_TRIBUNALI;
			String tsvFilePath_3=Config.DATA_INPUT_DEJURE_COMMENTATA_APPELLO;
			String tsvFilePath_4=Config.DATA_INPUT_DEJURE_COMMENTATA_TRIBUNALI;


			djdr.readDeJureTSV(tsvFilePath_1);	
			djdr.readDeJureTSV(tsvFilePath_2);	
			djdr.readDeJureTSV(tsvFilePath_3);	
			djdr.readDeJureTSV(tsvFilePath_4);	


			mp.setProvv_deJure(djdr.getProvvedimenti());



			//////////////
			// WKI
			//////////////
			WKIDataReader wkidr = new WKIDataReader();

			System.err.println("-- START READ TSV --");
			String tsvFilePath_WKI=Config.DATA_INPUT_WKI_NOTE_SENTENZA;

			wkidr.readWKITSV(tsvFilePath_WKI);	
			mp.setProvv_WKI(wkidr.getProvvedimenti());




			//////////////
			// DOGI
			//////////////
			NoteDogiReader ndr = new NoteDogiReader();

			System.err.println("-- START READ TSV --");
			ndr.readLookupSistematici_DoGi_TSV();
			ndr.readSistematiciDogi_TSV();
			ndr.readDescrittoriDogi_TSV();
			ndr.readFontiJur_Leg_Dogi_TSV();

			
			ndr.readLookupFindabilityEditori_TSV();
			System.err.println("-- END READ TSV --");

			ndr.readNoteDogiTSV();
			ndr.istanziaProvvedimenti();

			mp.setProvv_Dogi(ndr.getProvvedimenti());

			////////////////////////////
			// MERGE  DEJURE-DOGI-WKI //
			////////////////////////////

			mp.mergeProvvedimentiALL();

			System.err.println();
			System.err.println("PROVV DOGI: "+mp.getProvv_Dogi().size());
			System.err.println("PROVV DEJURE: "+mp.getProvv_deJure().size());
			System.err.println("PROVV WKI: "+mp.getProvv_WKI().size());

			System.err.println("PROVV MERGE: "+mp.getProvv_Merge().size());
			System.err.println();


			System.err.println("+++ INTEGRA SPOGLI MANUALI ++++");



			///////////////////////////////////////
			// MERGE INFO DA SPOGLI MANUALI DEJURE
			///////////////////////////////////////

			DeJureSpogliDataReader djSpoglidr = new DeJureSpogliDataReader();

			String tsvFilePathDJ_1=Config.DATA_SPOGLI_DEJURE_COMMENTATA_APPELLO;
			String tsvFilePathDJ_2=Config.DATA_SPOGLI_DEJURE_COMMENTATA_TRIBUNALI;
			String tsvFilePathDJ_3=Config.DATA_SPOGLI_DEJURE_NOTE_SENTENZA_APPELLO;
			// MISSING 23-2
			String tsvFilePathDJ_4=Config.DATA_SPOGLI_DEJURE_NOTE_SENTENZA_TRIBUNALI;

			djSpoglidr.readDeJureSpogliTSV(tsvFilePathDJ_1);	
			djSpoglidr.readDeJureSpogliTSV(tsvFilePathDJ_2);	
			djSpoglidr.readDeJureSpogliTSV(tsvFilePathDJ_3);	
			djSpoglidr.readDeJureSpogliTSV_v2(tsvFilePathDJ_4);	// versione Tribunali_NAS - matchano tramite IDIttig

			mp.setSpogliManualiDeJure(djSpoglidr.getProvvedimenti());

			// merge delle info da Spoglio in provvedimenti_merge
			mp.mergeEditedInfoDeJure();


			////////////////
			// MERGE SAME_AS
			////////////////
			mp.mergeSAME_AS(Config.DATA_SAME_AS);

			// da ora in poi i provvedimenti stanno sulla mappa provv_Merge_Map


			///////////////////////////////////////
			// MERGE INFO DA SPOGLI MANUALI WKI
			///////////////////////////////////////

			//TODO  da QUI merge 1160 spogli WKI 
			WKISpogliDataReader wkiSpoglidr = new WKISpogliDataReader();
			wkiSpoglidr.readWKISpogliTSV(Config.DATA_SPOGLI_WKI_1160);	
			mp.setSpogliManualiWKI(wkiSpoglidr.getProvvedimenti());
			mp.mergeEditedInfoWKI();


			///////////////////////////////////////
			// MERGE INFO DA EDIT MANUALI 
			///////////////////////////////////////
			
			// EDIT_MANUALI_CLEAN1 = "edit-manuali/spogli-clean#.tsv";
			// EDIT_MANUALI_CLEAN2 ="edit-manuali/fix-revisione-consegna-29-aprile.tsv";
			// SPOGLI_DOGI_HOPELESS_1 = "edit-manuali/Hopeless_DOGI_fatti_20190506.tsv";
			// SPOGLI_GIUFFRE_VENANZI_1 = "edit-manuali/spogliVenanzi_TABLE_clean_20190506.tsv";
			// SPOGLI_DOGI_HOPELESS_2 ="edit-manuali/spogli-fatti-20190507.tsv";
			// SPOGLI_HOPELESS_WKI_17_5 ="edit-manuali/Fatti-Integrati17maggio.tsv";

			
			mp.resetIsEditManualeMap();

			
			//TODO QUI merge 1160 spogli WKI 
			EditManualiDataReader editManualidr = new EditManualiDataReader();
			
			
			
			editManualidr.readEditManualiTSV(Config.EDIT_MANUALI_CLEAN1);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			// QUI MERGE Edit manuali revisione consegna 29 aprile
			editManualidr.readEditManualiTSV(Config.EDIT_MANUALI_CLEAN2);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			///////////////////////////////////////////////////////
			//  6 maggio - Merge Spogli Venanzi + Dogi_HOPELESS_1
			///////////////////////////////////////////////////////
			
//			// QUI MERGE Edit manuali SPOGLI_GIUFFRE_VENANZI e DOGI_HOPELESS_1
			
			
			
			editManualidr.readEditManualiTSV(Config.SPOGLI_DOGI_HOPELESS_1);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			editManualidr.readEditManualiTSV(Config.SPOGLI_GIUFFRE_VENANZI_1);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			editManualidr.readEditManualiTSV(Config.SPOGLI_DOGI_HOPELESS_2);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			// 17 maggio - spogli da Tabelle HOPELESS e WKI
			editManualidr.readEditManualiTSV(Config.SPOGLI_HOPELESS_WKI_17_5);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			// REVISIONE CONSEGNA 1_7 (27 maggio) revisione FARO
			editManualidr.readEditManualiTSV(Config.EDIT_MANUALI_FIX_CONSEGNA_27_MAGGIO);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			
			// 6 giugno - spogli da Tabelle HOPELESS e WKI
			editManualidr.readEditManualiTSV(Config.SPOGLI_HOPELESS_6_6);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
			editManualidr.readEditManualiTSV(Config.SPOGLI_WKI_6_6);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManuali();
			
		
	
			////////////////////////////////////////////////
			// POI BISOGNERA' AGGIUNGERE IL PENALE; TMN_TSV
			////////////////////////////////////////////////

			
			//////////////////////////////////////////////
			//  28/5/2019
			//  SCRAPE DEJURE MASSIME/SENTENZE - LEXADOC
			//
			//////////////////////////////////////////////
			
			HashMap<String,Provvedimento> provvedimentiSelezionatiDeJureScrape = readAndCleanScrapeCivileDeJure();
			mp.addSelezioneLexadoc(provvedimentiSelezionatiDeJureScrape);
			editManualidr.readEditManualiTSVLexadoc(Config.EDIT_CONSEGNA_LEXADOC_1);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManualiLEXADOC();
			
			//////////////////////////////////////////////
			//  27/6/2019
			//  SCRAPE DEJURE MASSIME/SENTENZE - LEXADOC-2-CIVILE
			//
			//////////////////////////////////////////////
			editManualidr.readEditManualiTSVLexadoc(Config.EDIT_CONSEGNA_LEXADOC_3);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManualiLEXADOC();
			
			
			
			//////////////////////////////////////////////
			//  28/6/2019
			//  SCRAPE DEJURE MASSIME/SENTENZE - LEXADOC - LAVORO
			//
			//////////////////////////////////////////////
			
			HashMap<String,Provvedimento> provvedimentiSelezionatiDeJureLavoro = readAndCleanScrapeLavoroDeJure();
			mp.addSelezioneLexadoc(provvedimentiSelezionatiDeJureLavoro);
			editManualidr.readEditManualiTSVLexadoc(Config.EDIT_CONSEGNA_LEXADOC_LAVORO);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManualiLEXADOC();
			
			
			//////////////////////////////////////////////
			//  17/6/2019
			//  SCRAPE DEJURE LEXADOC - PENALE
			//
			//////////////////////////////////////////////

			HashMap<String,Provvedimento> provvedimentiSelezionatiDeJurePenale = readAndCleanScrapePenaleDeJure();
			mp.addSelezioneLexadoc(provvedimentiSelezionatiDeJurePenale);
			System.out.println("SELEZIONE PENALE - ADD "+provvedimentiSelezionatiDeJurePenale.size());
			editManualidr.readEditManualiTSVLexadoc(Config.EDIT_CONSEGNA_LEXADOC_2_PENALE);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoSpogliManualiLEXADOC();
			
		
			
			
			//////////////////////////////////////////////
			//  9/6/2019
			//  ADD ORDINANZE COST - da Gazzetta Ufficiale
			//
			//////////////////////////////////////////////
			
			HashMap<String,Provvedimento> ordinanzeCOST = readAndCleanScrapeCOST();
			mp.addProvvedimentiCOST(ordinanzeCOST);
			// FIXME CHECK lettura giusta
			editManualidr.readEditManualiCOSTTSV(Config.EDIT_ORDINANZE_COST);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoOrdinanzeCOST();
			
			//////////////////////////////////////////
			//
			//		DATI CONSEGNE DEFINITIVE 
			//
			//////////////////////////////////////////

			editManualidr.readEditConsegneDefinitiveTSV(Config.DATA_CONSEGNA_DEFINITIVA_1);
			mp.setEditManuali(editManualidr.getProvvedimenti());
			mp.mergeEditedInfoConsegneDefinitive();
			
			
			/////////////////
			// OUTPUT MERGE
			/////////////////

			//		PRINT EXPLODED
			//		System.err.println(Config.HEADER_EXPLODED_PROVV);
			//		for(Provvedimento provv : mp.getProvv_Merge()) {
			//			provv.printExplodedProvvedimentoCSV();
			//		}

			mp.updateIsEstremoCompletoMap();
			mp.normalizeLocalitaUfficio();			
			mp.forceAREAbySection();
			mp.generaTestoEstremoUniformeMap();



			// CLEAN scartati, update richiesti e consegnati

			ConsegneReader cR = new ConsegneReader();
			mp.removeScartatiFromProvvedimentiMap(cR.readScartati());
			mp.flagProvvedimentoConsegnato(cR.readConsegnati());
			mp.flagProvvedimentoTrovato(cR.readRisposte());

			
			CleanClassificazioni cC = new CleanClassificazioni();
			cC.setProvv_Merge_Map(mp.getProvv_Merge_Map());
			cC.cleanClassificazioniProvvMap();
			cC.update_ProvvMergeMap();
			mp.setProvv_Merge_Map(cC.getProvv_Merge_Map());
			
			//cC.printClassificazioniProvvMap();

			// SAVE MAP

			
			if(!prov_Map_serial.exists()) {
				try {
					saveProvvMap(mp.getProvv_Merge_Map(),Config.serializedProvvMap);
					serializedProvvMap = mp.getProvv_Merge_Map();
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}	
		}else {   // MAPPA ESISTE SERIALIZZATA
			try {
				serializedProvvMap = readProvvMapFromFile(Config.serializedProvvMap);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		
		
//		////////////////////////////////////////////////
//		// ATTIVA PER LAVORARE SOLO SU CLASSIFICAZIONI
//		////////////////////////////////////////////////
//		
//		CleanClassificazioni cC = new CleanClassificazioni();
//		cC.setProvv_Merge_Map(serializedProvvMap);
//		cC.cleanClassificazioniProvvMap();
//		cC.update_ProvvMergeMap();
//		//cC.printClassificazioniProvvMap();
//		
//		// TIPI DI CLASSIFICAZIONE
////		cC.printSingleColumnClassificazioniTOP_Penale();
////		cC.printSingleColumnClassificazioniTOP_Civile();
////		cC.printSingleColumnClassificazioniCOSTPrimoLiv();
//		cC.printSingleColumnClassificazioniCOSTSecondoLiv();
////		cC.printSingleColumnClassiDeJurePenale();
		
		//cC.printSingleColumnClassificazioniCSMFinal();
	
		//cC.printSingleColumnClassificazioniCOST();
		//cC.printSingleColumnClassiDeJure();

		
//		COMMENT / UNCOMMENT PRINT
//		From NOW --> ON
	
		System.err.println();
		System.err.println("****************");
		System.err.println();

//		PRINT SINGLE ROW (Provvedimenti Unici, Fonti multiple)
		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
		for(Provvedimento provv : serializedProvvMap.values()) {
			provv.printSingleRowProvvedimento();
		}


		System.err.println();
		System.err.println("****************");
		System.err.println();


		//	PRINT GENNUSO SINGLE ROW (Provvedimenti Unici, Fonti multiple)	
		System.err.println(Config.HEADER_GENNUSO);
		for(Provvedimento provv : serializedProvvMap.values()) {
			provv.printGennusoSingleRowProvvedimento();
		}
	
		
//		System.err.println();
//		System.err.println("****************");
//		System.err.println();
//
		
//		//	PRINT OMEKA SINGLE ROW (Provvedimenti Unici, Fonti multiple)	
//		System.err.println(Config.HEADER_OMEKA);
//		for(Provvedimento provv : serializedProvvMap.values()) {
//			provv.printOMEKASingleRowProvvedimento();
//		}

//		//	PRINT OMEKA SINGLE ROW 1.8 + (Provvedimenti Unici, Fonti multiple)	
//		System.err.println(Config.HEADER_OMEKA_1_8);
//		for(Provvedimento provv : serializedProvvMap.values()) {
//			provv.printOMEKA_1_8_SingleRowProvvedimento();
//		}
		
		//////////////////////////////////////////////////////////////////////
		//  
		//    PRINT CONSEGNA FINALE
		//
		//////////////////////////////////////////////////////////////////////
		
//		System.err.println();
//		System.err.println("****************");
//		System.err.println();
//		
//		//	PRINT SINGLE ROW (Provvedimenti Unici, Fonti multiple)
//			System.err.println(Config.HEADER_SINGLE_ROW_PROVV_FINAL);
//			for(Provvedimento provv : serializedProvvMap.values()) {
//				provv.printSingleRowProvvedimentoCONSEGNA_FINALE();
//			}
//		
//		System.err.println();
//		System.err.println("****************");
//		System.err.println();
//			
////			PRINT GENNUSO SINGLE ROW (Provvedimenti Unici, Fonti multiple)	
//			System.err.println(Config.HEADER_GENNUSO_FINAL);
//			for(Provvedimento provv : serializedProvvMap.values()) {
//				provv.printGennusoSingleRowProvvedimentoCONSEGNA_FINALE();
//			}
		
		
		
//      ///////////////////////////////////////////////////////////////////////
//		
//		///////////////////////////////////////////////////////////////////////
		
//		//	PRINT EXPLOED PROVVEDIMENTI (Una riga per Fonte)
//		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
//		for(Provvedimento provv : serializedProvvMap.values()) {
//			provv.printExplodedProvvedimentoCSV();
//		}
		
		
//		PRINT CLASSIFICAZIONI
//			//System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
//			for(Provvedimento provv : serializedProvvMap.values()) {
//				provv.printSingleColumnClassificazioni();
//			}
		
		
		
//		//////////////////////////////////////////////////
//		// STAMPA SPOGLI RESIDUI WKI
//		//////////////////////////////////////////////////
//
//		
//		//	PRINT FILTRATO SingleRow (spogli residui WKI)
//		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
//		for(Provvedimento provv : serializedProvvMap.values()) {
//			
//			if(!provv.isEstremoCompleto() && !provv.isEditManuale() 
//				&& !provv.getDocType().contains("RIMESSIONE")/*&& isUrlContributoinWKI(provv.getFonti())*/ 
//				&& isEditoreContributoinWKI(provv.getFonti())) {
//				if(!(provv.getProvv_year().equals("2019") || 
//					provv.getProvv_year().equals("2018") ||
//					provv.getProvv_year().equals("2017") ||
//					provv.getProvv_year().equals("2016")
//					))
//					
//				provv.printSingleRowProvvedimento();
//			}
//		}
//
//		
//		System.err.println();
//		System.err.println("****************");
//		System.err.println();
//		
//		
//		
////		PRINT (RIVISTE )FILTRATO SingleRow (spogli residui WKI)
//			System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
//			for(Provvedimento provv : serializedProvvMap.values()) {
//
//				if(!provv.isEstremoCompleto() && !provv.isEditManuale() 
//					&& !provv.getDocType().contains("RIMESSIONE")/*&& isUrlContributoinWKI(provv.getFonti())*/ 
//					&& isEditoreContributoinWKI(provv.getFonti())) {
//
////				if(!provv.isEstremoCompleto() && isUrlContributoinWKI(provv.getFonti()) && isEditoreContributoinWKI(provv.getFonti())) {
//					if(!(provv.getProvv_year().equals("2019") || 
//						provv.getProvv_year().equals("2018") ||
//						provv.getProvv_year().equals("2017") ||
//						provv.getProvv_year().equals("2016")
//						))
//						
//				
//						for(Fonte f:provv.getFonti()) {
//							if(f.getEditore().contains("Wolters Kluwer")) {
//								String nomeRivista = "";
//								if(f.getRivista().startsWith("Riv"))
//									nomeRivista=f.getRivista()+", "+f.getEditore();
//								else
//									nomeRivista="Riv. "+f.getRivista();
//								System.err.println(provv.getPersistentID()+Config.SEP+nomeRivista);
//							}
//						}
//					//provv.printSingleRowProvvedimento();
//				}
//			}
		
		
		
//		// PRINT SPOGLI_VENANZI_TABLE
//		
//		List<String> spogliVenanzi =  readSpogliVenanzi();
//		//	PRINT SINGLE ROW (Provvedimenti Unici, Fonti multiple)
//		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
//		for(Provvedimento provv : serializedProvvMap.values()) {
//			if(spogliVenanzi.contains(provv.getPersistentID())) {
//				provv.printSingleRowProvvedimento();
//			}
//		}
		
		
	}
	
	
	
	public static List<String> readSpogliVenanzi(){
		
		List<String> spogliVenanzi = new ArrayList<String>();
		
		
				
				System.err.println("SPOGLI VENANZI");

				
				try {
					BufferedReader reader = new BufferedReader( new FileReader(/*Config.SPOGLI_VENANZI_FAILED*/Config.SPOGLI_VENANZI));
					String line  = null;

					while( ( line = reader.readLine() ) != null) {
						
						// SKIP COMMENTS
						if(line.startsWith("idITTIG"))
							continue;
						
						String idVenanzi = line.trim();
						System.err.println("\t"+idVenanzi);
						spogliVenanzi.add(idVenanzi);
					}
				}catch(Exception x) {
					x.printStackTrace();
				}
			
		
		return spogliVenanzi;
	}
	
	
	private static boolean isUrlContributoinWKI(List<Fonte> fonti) {
			
		for(Fonte f:fonti) {
			if(f.getUrl().contains("studiolegale.leggiditalia"))
				return true;
		}
		return false;
	}
	
	private static boolean isEditoreContributoinWKI(List<Fonte> fonti) {
		
		for(Fonte f:fonti) {
			if(f.getEditore().contains("Wolters Kluwer"))
				return true;
		}
		return false;
	}

	private static void runWKI() {
		//////////////
		// WKI
		//////////////
		WKIDataReader wkidr = new WKIDataReader();

		System.out.println("-- START READ TSV --");
		String tsvFilePath_WKI=Config.DATA_INPUT_WKI_NOTE_SENTENZA;

		wkidr.readWKITSV(tsvFilePath_WKI);	
		wkidr.printExplodedProvvedimenti();
	}
	
	
	
	private static void saveProvvMap(HashMap<String, Provvedimento> map, String filePath) throws IOException{
		
		ObjectOutputStream oos = null;
		FileOutputStream fout = null;
		try{
		    fout = new FileOutputStream(filePath, true);
		    oos = new ObjectOutputStream(fout);
		    oos.writeObject(map);
		} catch (Exception ex) {
		    ex.printStackTrace();
		} finally {
		    if(oos != null){
		        oos.close();
		    } 
		}
	}
	
	private static HashMap<String, Provvedimento> readProvvMapFromFile(String filePath) throws IOException{
		
		ObjectInputStream objectinputstream = null;
		try {
		    FileInputStream streamIn = new FileInputStream(filePath);
		    objectinputstream = new ObjectInputStream(streamIn);
		    HashMap<String, Provvedimento> readMap = (HashMap<String, Provvedimento>) objectinputstream.readObject();
			return readMap;
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if(objectinputstream != null){
		        objectinputstream .close();
		    } 
		}
		return null;
	}
	
	private static void runDeJure() {
		DeJureDataReader djdr = new DeJureDataReader();

		System.out.println("-- START READ TSV --");

		
		
		// BASIC - 1 provvedimento 1 fonte
		// Tabella unica scrape DeJure
		
		// obbiettivo: rappresentaz - uniforme Dogi/DeJure/Tutto
		// merge Dogi/DeJure (togli da Dogi provv già in DeJure)
		
		// duplicati ? fonti multiple ? leva, leva dopo, usa rows/records, bo 
		
		// output per spoglio ulteriore  (estremo_completo, link, altro)
		// output per consegna (?)  -- farlo come subset ? 
		// nel mezzo c'è recupero righe editate a mano
		
		// obiettivo immediato: record già pronti per acquisizione (quanti)
		// fare un listone unico (mappa) Dogi+DeJure
		
		String tsvFilePath_1=Config.DATA_INPUT_DEJURE_NOTE_SENTENZA_APPELLO;
		String tsvFilePath_2=Config.DATA_INPUT_DEJURE_NOTE_SENTENZA_TRIBUNALI;
 		String tsvFilePath_3=Config.DATA_INPUT_DEJURE_COMMENTATA_APPELLO;
		String tsvFilePath_4=Config.DATA_INPUT_DEJURE_COMMENTATA_TRIBUNALI;


		djdr.readDeJureTSV(tsvFilePath_1);	
		djdr.readDeJureTSV(tsvFilePath_2);	
		djdr.readDeJureTSV(tsvFilePath_3);	
		djdr.readDeJureTSV(tsvFilePath_4);	

		djdr.printExplodedProvvedimenti();
	}
	
	
	
	
	private static void runDogi() {
		NoteDogiReader ndr = new NoteDogiReader();

		System.out.println("-- START READ TSV --");
		ndr.readLookupSistematici_DoGi_TSV();
		ndr.readSistematiciDogi_TSV();
		ndr.readDescrittoriDogi_TSV();
		ndr.readFontiJur_Leg_Dogi_TSV();

		
		ndr.readLookupFindabilityEditori_TSV();
		System.out.println("-- END READ TSV --");

		ndr.readNoteDogiTSV();
		ndr.istanziaProvvedimenti();

		//	ndr.printProvvedimenti();
		// a fonti dottrinali multiple più righe e provvedimenti duplicati 
		ndr.printExplodedProvvedimenti();
		//	ndr.printEditori();

		//String text = "sentenza Corte Assise Appello Messina 18 dicembre 2013";
		//ndr.testLinkoln(text);
	}
	
	
	
	///////////////////////////////////
	//
	// PRINT PROVVEDIMENTI OLD
	//
	///////////////////////////////////
	//
	////PRINT SINGLE ROW (Provvedimenti Unici, Fonti multiple)
	//System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
	//for(Provvedimento provv : mp.getProvv_Merge()) {
	//	provv.printSingleRowProvvedimento();
	//}
	//
	//
	//System.err.println();
	//System.err.println("****************");
	//System.err.println();
	//
	//
	////PRINT GENNUSO SINGLE ROW (Provvedimenti Unici, Fonti multiple)	
	//System.err.println(Config.HEADER_GENNUSO);
	//for(Provvedimento provv : mp.getProvv_Merge()) {
	//	provv.printGennusoSingleRowProvvedimento();
	//}
	
	
}



