package deJureDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import config.Config;
import fonte.Fonte;
import provvedimento.Provvedimento;
import util.DistrettiCA;
import util.Util;

public class DeJureDataReader {



	HashMap<String,Provvedimento> provvedimenti = new HashMap<String,Provvedimento>();

	HashMap<String,List<String>> lookupComuni = new HashMap<String,List<String>>();
	HashMap<String,String> lookupDistrettiCA = new HashMap<String,String>();
	
	int idProvvedimento =1;



	

	public boolean readDeJureTSV(String tsvFilePath) {

		// 1 oggetto provvedimento per uno; 1 oggetto fonte
		// completa con distretto CA; IDPermanente
		// tabella unica DeJure
		// FIX se import diretto da export JSON ci sono duplicati; rispetto a quale feature li vedo? estremo ? 
		
		
		// a finale: 
		// 1 tebella Dogi
		// 1 tabella DeJure
		
		// trova 1 tabella merge con provvedimento unico "at its best :)"
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

			//  COMMENTATA	
			//	pagina	ufficio	localita	dataProvv	sezione	numero	estremo	urlSentenza	titolo	autori	fonte	urlScheda	rivista	dataNota	provenance

			//	NOTE_A_SENT
			//	pagina	ufficio	localita	dataProvv	sezione	numero	estremo	urlSentenza	titolo	autori	fonte	urlScheda	rivista	dataNota	classificazioniTOP	classificazioni	provenance


			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				//SKIP HEADER
				if(line.startsWith("pagina"))
					continue;
				
				String[] fields = line.split("\t");

				String paginaDejure = fields[0].trim();
				String tipo_ufficio  = fields[1].trim();
				String localita_ufficio = fields[2].trim();
				String provv_data = fields[3].trim();  // trasforma in aaaa-mm-gg
				String provv_sezione = fields[4].trim();
				String provv_numero = fields[5].trim();
				String provv_estremo = fields[6].trim();
				String provv_url = fields[7].trim();
				String contrib_titolo = fields[8].trim();
				String contrib_autori = fields[9].trim();
				String contrib_fonte = fields[10].trim(); 
				String contrib_url = fields[11].trim();
				String contrib_rivista = fields[12].trim();
				String contrib_data = fields[13].trim();

				
				String idProvvDeJure = "";
				if(provv_url!=null && provv_url.indexOf("idDocMaster")!=-1){
					idProvvDeJure = provv_url.substring(provv_url.indexOf("idDocMaster=")+12, provv_url.length());
					idProvvDeJure = idProvvDeJure.substring(0, idProvvDeJure.indexOf("&"));
					//System.err.println("idProvvDeJure: "+idProvvDeJure);
				}
				
				//https://dejure.it/#/ricerca/dottrine_documento?idDatabank=99&idDocMaster=7015206&idUnitaDoc=0&nVigUnitaDoc=1&docIdx=0&semantica=0&isPdf=false&isCorrelazioniSearch=false
				String idFonteDeJure = "";
				if(contrib_url!=null && contrib_url.indexOf("idDocMaster")!=-1){
					idFonteDeJure = contrib_url.substring(contrib_url.indexOf("idDocMaster=")+12, contrib_url.length());
					idFonteDeJure = idFonteDeJure.substring(0,idFonteDeJure.indexOf("&"));
					//System.err.println("idFonteDeJure: "+idFonteDeJure);
				}else {
					System.err.println("********  idDocMaster NOT FOUND  *********");
				}
				
				// se non c'è link alla sentenza prendo id del contributo
				if(idProvvDeJure.trim().length()==0)
					idProvvDeJure = idFonteDeJure;
				
				
				String classificazioniTOP = "";
				String classificazioni="";
				String provenance = "";
				if(tsvFilePath.contains("NOTE-SENT")) {
					classificazioniTOP = fields[14].trim();
					classificazioni=fields[15].trim();
					provenance = translateProvenance(fields[16].trim());
				}else {
					classificazioniTOP = "";
					classificazioni="";
					provenance = translateProvenance(fields[14].trim());
				}

				
				String normalizedDate = getNormalizedDate(provv_data);
				String authority_Code ="";
				if(tipo_ufficio.toLowerCase().indexOf("tribunal")!=-1)
					authority_Code="IT_TRB";
				else if(tipo_ufficio.toLowerCase().indexOf("assise")!=-1)
					authority_Code="IT_CSS";
				else if(tipo_ufficio.toLowerCase().indexOf("appell")!=-1)
					authority_Code="IT_CPP";
				if(tipo_ufficio.toLowerCase().indexOf("cassaz")!=-1)
					authority_Code="IT_CASS";
				
				
				Provvedimento p = new Provvedimento();
				
				// AGGIUNGERE CAMPI PER SPOGLIO: estremo completo, giudici, parti, numRG
				
				p.setScrapedPageNumber(paginaDejure);
				p.setIdProvvedimento(idProvvDeJure);   				
				p.setEstremoText(provv_estremo);
				p.setCommentedWith("");
				p.setGeo_name(localita_ufficio);
				p.setCourt_section(provv_sezione);
				p.setProvv_number(provv_numero);
				p.setProvv_date(normalizedDate);
				p.setProvv_year(normalizedDate.substring(0, 4));
				p.setDocType("");
				p.setAuthority(authority_Code);
				p.setUrl(provv_url);
				
				p.setProvenance(provenance);
				DistrettiCA distretti = new DistrettiCA();
				p = distretti.setDistrettoAppello(p);
				// metti l'idPersistente
				p.setPersistentID(Util.computeNewID(p));
				
				
				Fonte f = new Fonte();
				f.setIdContributo(idFonteDeJure);					
				f.setAutori(contrib_autori);
				f.setTitoloContributo(contrib_titolo);
				f.setRivista(contrib_rivista);
				f.setDataFascicolo(contrib_data);   	
				f.setEditore("Giuffrè Francis Lefebvre");
				f.setFindableIn("GFL");
				f.setEstremoText(contrib_fonte);     	// gli estremi fonte deJure sono fatti così:  Rivista Italiana di Diritto del Lavoro, fasc.3, 2017, pag. 509
				f.setUrl(contrib_url);
				f.setSistematici(new String[]{classificazioniTOP});
				f.setDescrittori(new String[]{classificazioni});
				
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
				//uniqueFonti.put(idContributoDogi, f);
				
				//Provvedimento p =  createProvvedimentoDeJure(""+idProvvedimento,estremo,dogiCommentedWith,idContributoDogi);
				// qui in teoria se il provvedimento c'è già commentato gli andrebbe aggiunta "SOLO" la nuova fonte
				
				// CON QUESTI DATI
				// - istanzia oggetti provvedimento e fonte
				// - normalizza date nel formato aaaa-mm-gg
				// - assegna persistentId
				// - assegna distretto Appello
				
				// mettili da qualche parte (mappa)
				// restituisci mappa
				
				provvedimenti.put(""+idProvvedimento, p);			
				provvedimenti.put(""+idProvvedimento, p);				

//				String found ="";
//				if((found = findEstremoInProvvedimenti(estremo))==null) {
//					provvedimenti.put(""+idProvvedimento, p);
//				}else {
//					// provvedimento commentato in più contributi; gli aggiungo solo il contributo
//					Fonte newFonte = p.getFonti().get(0);
//					provvedimenti.get(found).addFonte(newFonte);
//				}
				idProvvedimento++;
				
				
			}
			System.err.println("processed "+i+" rows");
			System.err.println("idProvvedimento fino a "+idProvvedimento);

		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	
	
	private String getNormalizedDate(String date) {

		//	in 21/09/2018
		//	in 04 settembre 2018
		//	out 2016-04-28		
		try {
		if(date.indexOf("/")!=-1) {
			return date.split("/")[2]+"-"+date.split("/")[1]+"-"+date.split("/")[0];
		}else {
			String day = date.split(" ")[0].trim();
			String month = date.split(" ")[1].trim();
			String year = date.split(" ")[2].trim();

			switch(month.toLowerCase()) {
			case "gennaio": month="01";
			break;
			case "febbraio": month="02";
			break;
			case "marzo": month="03";
			break;
			case "aprile": month="04";
			break;
			case "maggio": month="05";
			break;
			case "giugno": month="06";
			break;
			case "luglio": month="07";
			break;
			case "agosto": month="08";
			break;
			case "settembre": month="09";
			break;
			case "ottobre": month="10";
			break;
			case "novembre": month="11";
			break;
			case "dicembre": month="12";
			break;
			}
			return year+"-"+month+"-"+day;
		}
		}catch(Exception ex) {
			System.out.println("+++++++++++++ exc for "+date);
			return date;
		}
	}
	

	

	
	
	public boolean printExplodedProvvedimenti() {

			
		
		
		String header ="persistentID"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"pageNum"+Config.SEP
				+"estremo_completo"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
				+"giudici"+Config.SEP
				+"parti"+Config.SEP
				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"idContributo"+Config.SEP
				+"titolo"+Config.SEP
				+"autori"+Config.SEP
				+"url_contributo"+Config.SEP
				+"rivista"+Config.SEP
				+"editore"+Config.SEP
				+"findable_IN"+Config.SEP
				+"fonte"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		System.err.println(header);
	

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printExplodedProvvedimentoCSV();
//			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
//				continue;
//			
//			if(p.getProvv_year()==null )
//				p.printGennusoCSV_1_3();
//			else{
//				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
//					p.printGennusoCSV_1_3();
//			}
		}
		return true;
	}
	
	
	public List<Provvedimento> getProvvedimenti() {
		List<Provvedimento> filtered_provv_list = new ArrayList<Provvedimento>();
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			if(p.getProvv_year()==null )
				filtered_provv_list.add(p);
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					filtered_provv_list.add(p);
			}
			
		}	
		return filtered_provv_list;
	}



	private String translateProvenance(String provenance) {
		switch(provenance) {
		case "DeJure-Giurisp. commentata-Appello-2014-2019": return Config.DEJURE_COMM_APP;
		case "DeJure-Giurisp. commentata-Tribunali-2014-2019": return Config.DEJURE_COMM_TRIB;
		case "DeJure-Note a sentenza-Appello-2014-2019": return Config.DEJURE_NAS_APP;
		case "DeJure-Note a sentenza-Tribunali-2014-2019": return Config.DEJURE_NAS_TRIB;
		case "WKI_NAS_APPELLO_2016_2019": return Config.WKI_NAS_APP;
		case "WKI_NAS_TRIBUNALI_2016_2019": return Config.WKI_NAS_TRIB;
		default: return "";
		}
	}
	
	
	
	
}

