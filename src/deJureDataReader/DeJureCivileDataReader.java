package deJureDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import config.Config;
import fonte.Fonte;
import provvedimento.Provvedimento;
import util.DistrettiCA;
import util.Util;

public class DeJureCivileDataReader {



	HashMap<String,Provvedimento> provvedimenti = new HashMap<String,Provvedimento>();
	HashMap<String,Provvedimento> provvedimentiMassimati = new HashMap<String,Provvedimento>();
	HashMap<String,Provvedimento> provvedimentiSentenza = new HashMap<String,Provvedimento>();



	HashMap<String,List<String>> lookupComuni = new HashMap<String,List<String>>();
	HashMap<String,String> lookupDistrettiCA = new HashMap<String,String>();
	
	int idProvvedimento =1;



	public void readLookupComuni_TSV() {
		String tsvFilePath=Config.TAB_COMUNI;
		File comuniTSV = new File(tsvFilePath);
		if(!comuniTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(comuniTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String comune = fields[0];
				String cittaMetropolitana = fields[3];
				String capoluogoProvincia = fields[4];
				String capoluogoRegione = fields[8];

				String label = comune;

				if(lookupComuni.get(label)==null){
					List<String> lista = new ArrayList<String>();
					lista.add(cittaMetropolitana);
					lista.add(capoluogoProvincia);
					lista.add(capoluogoRegione);
					lookupComuni.put(label, lista);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	
	public void readLookupDistrettiCA_TSV() {

		String tsvFilePath=Config.TAB_DISTRETTI_CA;
		File distrettiTSV = new File(tsvFilePath);
		if(!distrettiTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}

		try{
			BufferedReader reader = new BufferedReader( new FileReader(distrettiTSV));
			String line  = null;

			while( ( line = reader.readLine() ) != null) {
				String[] fields = line.split("\t");
				String distretto = fields[0];
				String nomeCorte = fields[1];
				String siglaProvincia = fields[2];

				String label = distretto;

				if(lookupDistrettiCA.get(label)==null){		
					lookupDistrettiCA.put(label,nomeCorte+":"+siglaProvincia);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	public boolean readDeJureCivileTSV(String tsvFilePath) {
		
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

			
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				//SKIP HEADER
				if(line.startsWith("num_classe"))
					continue;
				
				String[] fields = line.split("\t");
		
				
				String classe_penale = fields[1].trim();
				String tipo_ufficio  = fields[2].trim();
				String localita_ufficio = fields[3].trim();
				String tipo_doc = fields[4].trim();
				String provv_data = fields[5].trim();  // trasforma in aaaa-mm-gg
				String provv_sezione = fields[6].trim();
				String provv_numero = fields[7].trim();
				String idDocMasterSentenza = fields[8].trim();
				String provv_estremo = fields[9].trim();
				String provv_url = fields[10].trim();
				String idDocMasterMassima = fields[11].trim();
				String contrib_titolo = fields[12].trim();
				//String contrib_autori = fields[9].trim();
				String contrib_fonte = fields[13].trim(); 
				String testoMassima = fields[14].trim();
				String contrib_url = fields[15].trim();
				String classificazioniTOP = fields[16].trim();
				String classificazioni = fields[17].trim();
				String provenance = fields[18].trim();

				String idFonteDeJure = idDocMasterMassima;
				String idProvvDeJure = idDocMasterSentenza;
			
				// se non c'è link alla sentenza prendo id del contributo
				if(idProvvDeJure.trim().length()==0)
					idProvvDeJure = idFonteDeJure;
				
			
				
				String normalizedDate = provv_data;
				String authority_Code ="";
				if(tipo_ufficio.toLowerCase().indexOf("tribunal")!=-1)
					authority_Code="IT_TRB";
				else if(tipo_ufficio.toLowerCase().indexOf("assise")!=-1)
					authority_Code="IT_CSS";
				else if(tipo_ufficio.toLowerCase().indexOf("appell")!=-1)
					authority_Code="IT_CPP";
				if(tipo_ufficio.toLowerCase().indexOf("cassaz")!=-1)
					authority_Code="IT_CASS";
				
				
				Provvedimento p = new Provvedimento();
				
				// AGGIUNGERE CAMPI PER SPOGLIO: estremo completo, giudici, parti, numRG
				
				p.setIdProvvedimento(idProvvDeJure); 
				p.setDocType(tipo_doc);
				p.setEstremoText(provv_estremo);
				p.setDeJureClasse(classe_penale);
				p.setCommentedWith("");
				p.setGeo_name(Util.normalizeLocalitaUfficio(localita_ufficio));
								
				p.setCourt_section(provv_sezione);
				p.setProvv_number(provv_numero);
				p.setProvv_date(normalizedDate);
				p.setProvv_year(normalizedDate.substring(0, 4));
				p.setAuthority(authority_Code);
				p.setUrl(provv_url);
				p.setGuessed_subject("CIVILE");
				
				p.setProvenance(provenance);
				DistrettiCA distretti = new DistrettiCA();
				p = distretti.setDistrettoAppello(p);
				
				// metti l'idPersistente
				p.setPersistentID(Util.computeNewID(p));
				
				
				Fonte f = new Fonte();
				f.setIdContributo(idFonteDeJure);					
				//f.setAutori(contrib_autori);
				f.setTitoloContributo(contrib_titolo);
				//f.setRivista(contrib_rivista);
				//f.setDataFascicolo(contrib_data);   	
				f.setEditore("Giuffrè Francis Lefebvre");
				f.setFindableIn("GFL");
				f.setEstremoText(contrib_fonte);     	
				f.setUrl(contrib_url);
				
				// FIXME normalizzare e deduplicare i descrittori TOP e di secondo livello
				
				f.setSistematici(new String[]{classificazioniTOP});
				f.setDescrittori(new String[]{classificazioni});
				f.setDeJureTestoMassima(testoMassima);
				
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
				//uniqueFonti.put(idContributoDogi, f);
				
		
				
				// grossolanamente provvedimenti PENALI  UNICI
				// FIXME
				// fare prima una lista dei provvedimenti massimati unici
				// poi aggiungi le sole sentenze pubblicate
				// TipoDoc NOT FOUND o VUOTO
				// Sistema print Fonti per questo tipo di Fonti
				// Sistema Oggetti Provvedimento buoni per:
				// 		1)ulteriore spoglio e selezione
				// 		2)successivo merge nel dataset esteso
				// CHECK IT_CSS / IT_CPP
				
				
				// MASSIME
				if(provenance.indexOf("MASS")!=-1) {

					if(provvedimentiMassimati.get(p.getPersistentID())==null) {
						provvedimentiMassimati.put(p.getPersistentID(), p);			
					}else {
						// FIXME
						// Aggiungi nuova fonte solo se non c'è già nella lista
						if(!isFonteinList(f, provvedimentiMassimati.get(p.getPersistentID()).getFonti())) {
							//System.out.println("Add Fonte multipla "+f.getIdContributo());
							provvedimentiMassimati.get(p.getPersistentID()).addFonte(f);
						}else {
							System.out.println("Fonte is already there");
						}
						if(provvedimentiMassimati.get(p.getPersistentID()).getUrl().trim().length()==0)
							provvedimentiMassimati.get(p.getPersistentID()).setUrl(p.getUrl());
					}
				}else {
					if(provvedimentiSentenza.get(p.getPersistentID())==null) {
						provvedimentiSentenza.put(p.getPersistentID(), p);			
					}else {
						// in teoria le sentenze da sè non hanno fonte
						//provvedimentiSentenza.get(p.getPersistentID()).addFonte(f);
						if(provvedimentiSentenza.get(p.getPersistentID()).getUrl().trim().length()==0)
							provvedimentiSentenza.get(p.getPersistentID()).setUrl(p.getUrl());
					}
				}


				
				
			}
			System.err.println("processed "+i+" rows");

		}catch(Exception e){
			e.printStackTrace();
		}

		mergeMassimeSentenze();
		
		return true;
			
		
	}
	
	
	private String encodeTipoDoc(String tipoDoc) {

		String ret ="";
		switch(tipoDoc.toLowerCase()) {
		case "sentenza": 
			ret="JUDGMENT";
			break;
		case "ordinanza": 
			ret="ORDER";
			break;
		case "decreto":
			ret="DECREE";
			break;
		default:
			ret = tipoDoc;
		}

		return ret;
	}
	
	public boolean readDeJureCivileLexaDocTSV(String tsvFilePath) {
		
		
		provvedimenti = new HashMap<String,Provvedimento>();
		provvedimentiMassimati = new HashMap<String,Provvedimento>();
		provvedimentiSentenza = new HashMap<String,Provvedimento>();
		
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;

			
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
				//System.err.println(line);

				//SKIP HEADER
				if(line.startsWith("num_classe"))
					continue;
				
				String[] fields = line.split("\t");
		
				
				String classe_penale = fields[1].trim();
				String tipo_ufficio  = fields[2].trim();
				String localita_ufficio = fields[3].trim();
				String tipo_doc = fields[4].trim();
				String provv_data = fields[5].trim();  // trasforma in aaaa-mm-gg
				String provv_sezione = fields[6].trim();
				String provv_numero = fields[7].trim();
				String idDocMasterSentenza = fields[8].trim();
				String provv_estremo = fields[9].trim();
				String provv_url = fields[10].trim();
				String idDocMasterMassima = fields[11].trim();
				String contrib_titolo = fields[12].trim();
				//String contrib_autori = fields[9].trim();
				String contrib_fonte = fields[13].trim(); 
				contrib_fonte = contrib_fonte.trim().replaceAll("\\s+", " ");
				
				line = line.replaceAll("( )+"," ").trim();
				String testoMassima = fields[14].trim();
				String contrib_url = fields[15].trim();
				String classificazioniTOP = fields[16].trim();
				String classificazioni = fields[17].trim();
				String provenance = fields[18].trim();

				String idFonteDeJure = idDocMasterMassima;
				String idProvvDeJure = idDocMasterSentenza;
			
				// se non c'è link alla sentenza prendo id del contributo
				if(idProvvDeJure.trim().length()==0)
					idProvvDeJure = idFonteDeJure;
				
			
				
				String normalizedDate = provv_data;
				String authority_Code ="";
				if(tipo_ufficio.toLowerCase().indexOf("tribunal")!=-1)
					authority_Code="IT_TRB";
				else if(tipo_ufficio.toLowerCase().indexOf("assise")!=-1)
					authority_Code="IT_CSS";
				else if(tipo_ufficio.toLowerCase().indexOf("appell")!=-1)
					authority_Code="IT_CPP";
				if(tipo_ufficio.toLowerCase().indexOf("cassaz")!=-1)
					authority_Code="IT_CASS";
				
				
				Provvedimento p = new Provvedimento();
				
				// AGGIUNGERE CAMPI PER SPOGLIO: estremo completo, giudici, parti, numRG
				
				p.setIdProvvedimento(idProvvDeJure); 
				p.setDocType(tipo_doc);
				// per MERGE: encode
				//p.setDocType(encodeTipoDoc(tipo_doc));
				p.setEstremoText(provv_estremo);
				p.setDeJureClasse(classe_penale);
				p.setClassificazioniCSM(classe_penale);
				p.setCommentedWith("");
				p.setGeo_name(Util.normalizeLocalitaUfficio(localita_ufficio));
				p.setCourt_section(provv_sezione);
				p.setProvv_number(provv_numero);
				p.setProvv_date(normalizedDate);
				p.setProvv_year(normalizedDate.substring(0, 4));
				p.setAuthority(authority_Code);
				p.setUrl(provv_url);
				p.setGuessed_subject("CIVILE");
				
				p.setProvenance(provenance);
				DistrettiCA distretti = new DistrettiCA();
				p = distretti.setDistrettoAppello(p);
				
				// metti l'idPersistente
				p.setPersistentID(Util.computeNewID(p));
				
				
				Fonte f = new Fonte();
				f.setIdContributo(idFonteDeJure);					
				f.setTitoloContributo(contrib_titolo);
				
				// PROVVEDIMENTI NON MASSIMATI
				if(contrib_fonte.trim().length()==0)
					contrib_fonte = "DeJure, Giuffrè Francis Lefebvre";
				
				f.setRivista(contrib_fonte);
			
				f.setFindableIn("GFL");
				f.setEstremoText(contrib_fonte);     	
				f.setUrl(contrib_url);
				f.setEditore("Giuffrè Francis Lefebvre");

				
				//f.setAutori(contrib_autori);
				//f.setDataFascicolo(contrib_data);   	
				//f.setEditore("Giuffrè");
				
				// FIXME normalizzare e deduplicare i descrittori TOP e di secondo livello
				
				f.setSistematici(new String[]{classificazioniTOP});
				f.setDescrittori(new String[]{classificazioni});
				f.setDeJureTestoMassima(testoMassima);
				
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
			
				
				
				// MASSIME
				if(provenance.indexOf("MASS")!=-1) {

					if(provvedimentiMassimati.get(p.getPersistentID())==null) {
						provvedimentiMassimati.put(p.getPersistentID(), p);			
					}else {
						// FIXME
						// Aggiungi nuova fonte solo se non c'è già nella lista
						if(!isFonteinList(f, provvedimentiMassimati.get(p.getPersistentID()).getFonti())) {
							//System.out.println("Add Fonte multipla "+f.getIdContributo());
							provvedimentiMassimati.get(p.getPersistentID()).addFonte(f);
						}else {
							//System.out.println("Fonte is already there");
						}
												
						if(provvedimentiMassimati.get(p.getPersistentID()).getUrl().trim().length()==0)
							provvedimentiMassimati.get(p.getPersistentID()).setUrl(p.getUrl());
						
						// se appartiene a più categorie aggiungi categoria
						if(!provvedimentiMassimati.get(p.getPersistentID()).getDeJureClasse().contains(p.getDeJureClasse())) {
							provvedimentiMassimati.get(p.getPersistentID()).setDeJureClasse(provvedimentiMassimati.get(p.getPersistentID()).getDeJureClasse()+"#"+p.getDeJureClasse());
						}
						
						
					}
				}else {
					if(provvedimentiSentenza.get(p.getPersistentID())==null) {
						provvedimentiSentenza.put(p.getPersistentID(), p);			
					}else {
						// in teoria le sentenze da sè non hanno fonte
						//provvedimentiSentenza.get(p.getPersistentID()).addFonte(f);
						if(provvedimentiSentenza.get(p.getPersistentID()).getUrl().trim().length()==0)
							provvedimentiSentenza.get(p.getPersistentID()).setUrl(p.getUrl());
						
						// se appartiene a più categorie aggiungi categoria
						if(!provvedimentiSentenza.get(p.getPersistentID()).getDeJureClasse().contains(p.getDeJureClasse())) {
							provvedimentiSentenza.get(p.getPersistentID()).setDeJureClasse(provvedimentiSentenza.get(p.getPersistentID()).getDeJureClasse()+"#"+p.getDeJureClasse());
						}
						
					}
				}


				
				
			}
			System.err.println("processed "+i+" rows");

		}catch(Exception e){
			e.printStackTrace();
		}

		
		mergeMassimeSentenze();
		
		
		return true;
			
		
	}


	
	private boolean isFonteinList(Fonte f, List<Fonte> listFonti) {
		// FIXME
		// CHECK: strano non ci sono massime duplicate (?)
		for(Fonte fonteItem:listFonti) {
			if(fonteItem.getIdContributo().trim().equals(f.getIdContributo().trim()))
				return true;
		}
		return false;
	}
	
		
	public void mergeMassimeSentenze() {
		for(String idProvvedimento : provvedimentiMassimati.keySet()){
			provvedimenti.put(idProvvedimento, provvedimentiMassimati.get(idProvvedimento));
		}
		
		for(String idProvvedimento : provvedimentiSentenza.keySet()){
			if(provvedimenti.get(idProvvedimento)==null) {
				provvedimenti.put(idProvvedimento, provvedimentiSentenza.get(idProvvedimento));
			}else {
				// Aggiungi la url se non c'era nel provv massimato
				if(provvedimenti.get(idProvvedimento).getUrl().trim().length()==0)
					provvedimenti.get(idProvvedimento).setUrl(provvedimentiSentenza.get(idProvvedimento).getUrl());
				
				String existingCategories = provvedimenti.get(idProvvedimento).getDeJureClasse();
				String newCategory = provvedimentiSentenza.get(idProvvedimento).getDeJureClasse();
				//if not newCategory in existing category
				String[] array = newCategory.split("#");
				for(int j=0;j<array.length;j++) {
					if(!existingCategories.contains(array[j]))
						existingCategories=existingCategories+"#"+array[j];
				}
				provvedimenti.get(idProvvedimento).setDeJureClasse(existingCategories);
				
			}
		}
		
	}

	
	
	public List<String> readProvvedimentiSelezionati(String filePath){
		
		List<String> selezionati = new ArrayList<String>();
		
		File folderSelezionati = new File(filePath);
		File[] listOfFiles = folderSelezionati.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()  && file.getName().startsWith("selezione") && file.getName().endsWith("txt")) {
				
				System.err.println("Selezione LEXADOC: ");

				
				try {
					BufferedReader reader = new BufferedReader( new FileReader(file));
					String line  = null;

					while( ( line = reader.readLine() ) != null) {
						
						// SKIP COMMENTS
						if(line.startsWith("#"))
							continue;
						
						String idSelezionato = line.trim();
						//System.err.println("LD \t"+idSelezionato);
						selezionati.add(idSelezionato);
					}
				}catch(Exception x) {
					x.printStackTrace();
				}
			}
		}
		return selezionati;
	}
	
	
	public boolean printCivileDeJureGennusoStandard() {
		
		
		System.err.println(Config.HEADER_GENNUSO);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printGennusoSingleRowProvvedimento();
		
		}
		
		return true;
	}
	
	
	public HashMap<String,Provvedimento> getProvvedimentiScrapeDeJure(){
		return this.provvedimenti;
	}
	
	
	public HashMap<String,Provvedimento> getProvvedimentiScrapeDeJureSelezionati(List<String> idList){
		
		HashMap<String,Provvedimento> selection = new HashMap<String,Provvedimento>();
		
		
		for(String idSelezionato : idList) {	
			if(provvedimenti.get(idSelezionato)!=null) {
				selection.put(idSelezionato, provvedimenti.get(idSelezionato));
			}else {
				System.err.println("+++WARNING Provv selezionat NOT FOUND su mappa DeJureScrape "+idSelezionato);
			}	
		}
	
		return selection;
	}
	
	
	public boolean printCivileDeJureSingleRowStandard() {
		
		System.err.println(Config.HEADER_SINGLE_ROW_PROVV);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printSingleRowProvvedimento();
		
		}
		return true;
	}
	
	
	public boolean printCivileDeJureSingleRowOmeka() {
		
	
		System.err.println(Config.HEADER_OMEKA);
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			p.printOMEKASingleRowProvvedimento();
		
		}
		return true;
	}
	
	
	public boolean printCivileDeJureSingleRow() {
		
		
		String header ="idITTIG"+Config.SEP
				+"isDuplicato"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"classeDeJure"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
//				+"giudici"+Config.SEP
//				+"parti"+Config.SEP
//				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"titolo"+Config.SEP
				+"testoMassima"+Config.SEP
				+"idContributo"+Config.SEP
				+"url_contributo"+Config.SEP
				+"fonte"+Config.SEP
				+"editore"+Config.SEP
				+"classificazioniTOP"+Config.SEP
				+"classificazioni"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		System.err.println(header);
		int count =0;
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			
			if(selezioneLexadoc_LAVORO_1(p)) {
				printSingleRowDeJureCivileCSV(p);
				count++;
			}
//			printExplodedLEXADOCDeJureCivileCSV(p);
		}
		
		System.err.println(count+" PROVVEDIMENTI in SELEZIONE");
		return true;
	}
	
	
	
	
	// DISTRETTI GRANDI - COMPLETI
	private boolean selezioneLexadoc_LAVORO_1(Provvedimento provv) {
		
		
		boolean condizione0 = true; //provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("L'Aquila") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && condizione1 && condizione2 && condizione3 /*&& condizione4*/)
			return true;
		
		return false;
	}
	
	// DISTRETTI GRANDI - incompleti per qualche motivo
	private boolean selezioneLexadoc_LAVORO_2(Provvedimento provv) {
		
		boolean condizione0 = true; //provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");

		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("L'Aquila") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && condizione1 && (!condizione2 || !condizione3 /*|| !condizione4*/))
			return true;
		
		return false;
	}
	
	
	// DISTRETTI PICCOLI - PROVV NUMERATI
	private boolean selezioneLexadoc_LAVORO_3(Provvedimento provv) {
		
		
		boolean condizione0 = true; //provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("L'Aquila") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		//boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && !condizione1 && condizione3)
			return true;
		
		return false;
	}
	
	// DISTRETTI PICCOLI - RESIDUO (non numerati)
	private boolean selezioneLexadoc_LAVORO_4(Provvedimento provv) {
		
		
		boolean condizione0 = true; //provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("L'Aquila") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		//boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && !condizione1 && !condizione3)
			return true;
		
		return false;
	}
	
	
	
	// DISTRETTI GRANDI - COMPLETI
	private boolean selezioneLexadoc1(Provvedimento provv) {
		
		
		boolean condizione0 = provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && condizione1 && condizione2 && condizione3 && condizione4)
			return true;
		
		return false;
	}
	
	// DISTRETTI GRANDI - incompleti per qualche motivo
	private boolean selezioneLexadoc2(Provvedimento provv) {
		
		boolean condizione0 = provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");

		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && condizione1 && (!condizione2 || !condizione3 || !condizione4))
			return true;
		
		return false;
	}
	
	
	// DISTRETTI PICCOLI - PROVV NUMERATI
	private boolean selezioneLexadoc3(Provvedimento provv) {
		
		
		boolean condizione0 = provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		//boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && !condizione1 && condizione3)
			return true;
		
		return false;
	}
	
	// DISTRETTI PICCOLI - RESIDUO (non numerati)
	private boolean selezioneLexadoc4(Provvedimento provv) {
		
		
		boolean condizione0 = provv.getFlagDeJureDuplicate().equalsIgnoreCase("NON_DUPLICATO") || provv.getFlagDeJureDuplicate().equalsIgnoreCase("DA_CONTROLLARE");
		
		boolean condizione1 = provv.getDistrettoCorteAppello().equalsIgnoreCase("Roma") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Milano") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Firenze") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bari") ||
				provv.getDistrettoCorteAppello().equalsIgnoreCase("Bologna");
		
		//boolean condizione2 = provv.getDocType().trim().length()>0 && !provv.getDocType().equalsIgnoreCase("NOT_FOUND");
		
		boolean condizione3 = provv.getProvv_number().trim().length()>0;
		
		//boolean condizione4 = provv.getProvenance().contains("MASS");

		
		
		if(condizione0 && !condizione1 && !condizione3)
			return true;
		
		return false;
	}
	
	public void alphaOrderForMultipleCat() {
				
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			if(p.getDeJureClasse().indexOf("#")!=-1)
				p.setDeJureClasse(alphaOrder(p.getDeJureClasse()));
			
		}
	}
	
	

	private String alphaOrder(String multipleCat) {
		String[] cat=multipleCat.split("#");
		String ret="";
		Arrays.sort(cat);
		for(int i = 0; i < cat.length-1; i++)
           ret+=cat[i]+"#";
		ret+=cat[cat.length-1];
		
		return ret;
	}
	
	
	
	public void flagDuplicate(HashMap<String,Provvedimento> datasetPrincipale) {
		
		for(String idProvvedimento : provvedimenti.keySet()){
		
			provvedimenti.get(idProvvedimento).setFlagDeJureDuplicate(isDeJureProvvInDataset(provvedimenti.get(idProvvedimento), datasetPrincipale));
	
		}
	}
	
	
	
	public void flagVisited(HashMap<String,Provvedimento> datasetPrecedente) {
		
		for(String idProvvedimento : provvedimenti.keySet()){
		
			provvedimenti.get(idProvvedimento).setFlagDeJureDuplicate(isDeJureProvvAlreadyVisited(provvedimenti.get(idProvvedimento), datasetPrecedente));
	
		}
	}
	
//  TOTALI FINORA: 9919
//	DUPLICATO_ID	178     (c'è con lo stesso identico ID)
//	DUPLICATO:	101  		(c'è con lo stesso AUTORITA_LOCALITA_DATA_NUMERO)
	
		// TOT DUPLICATI:   610/9919
		// PER QUELLI DUPLICATI SI POTREBBERO PRENDERE DA SELEZIONE DEJURE --> Le Classificazioni della Griglia Cassazione
	
//	NON_DUPLICATO:	8965    (o non c'è o c'è con lo stesso AUTORITA_LOCALITA_DATA ma NUMERI_DIVERSI) 
//	DA_CONTROLLARE:	675     (c'è con lo stesso AUTORITA_LOCALITA_DATA ma almeno uno dei due non è numerato)
	
	

	
	private String isDeJureProvvInDataset(Provvedimento deJureProvv,HashMap<String,Provvedimento> datasetPrincipale ) {
		
		String flag = "NON_DUPLICATO";
		if(datasetPrincipale.get(deJureProvv.getPersistentID())!=null) 
			return "DUPLICATO_ID";
		
		for(Provvedimento p : datasetPrincipale.values()){
			
			if(deJureProvv.getAuthority().equals(p.getAuthority()) &&
			   deJureProvv.getProvv_date().equals(p.getProvv_date())&&
			   deJureProvv.getGeo_name().equals(p.getGeo_name())){
				
				// se entrambi sono numerati e i numeri sono uguale --> SONO UGUALI
				if(deJureProvv.getProvv_number().trim().length()>0 && 
						p.getProvv_number().trim().length()>0 &&
					deJureProvv.getProvv_number().equals(p.getProvv_number()))
					return "DUPLICATO"; 
				// se entrambi sono numerati e i numeri sono diversi --> SONO DIVERSI
				if(deJureProvv.getProvv_number().trim().length()>0 && 
						p.getProvv_number().trim().length()>0 &&
					!deJureProvv.getProvv_number().equals(p.getProvv_number()))
					return "NON_DUPLICATO"; 
				
				flag = "DA_CONTROLLARE";

			}
			
		}
		return flag;
	}
	
	
	private String isDeJureProvvAlreadyVisited(Provvedimento deJureProvv,HashMap<String,Provvedimento> datasetPrincipale ) {
		
		String flag = "NON_DUPLICATO";
		if(datasetPrincipale.get(deJureProvv.getPersistentID())!=null) { 
			if(deJureProvv.getFlagDeJureDuplicate().equals("NON_DUPLICATO"))
			return "VISITATO";
		}
		
		for(Provvedimento p : datasetPrincipale.values()){
			
			if(deJureProvv.getAuthority().equals(p.getAuthority()) &&
			   deJureProvv.getProvv_date().equals(p.getProvv_date())&&
			   deJureProvv.getGeo_name().equals(p.getGeo_name())){
				
				// se entrambi sono numerati e i numeri sono uguale --> SONO UGUALI
				if(deJureProvv.getProvv_number().trim().length()>0 && 
						p.getProvv_number().trim().length()>0 &&
					deJureProvv.getProvv_number().equals(p.getProvv_number())) {
						if(deJureProvv.getFlagDeJureDuplicate().equals("NON_DUPLICATO"))
							return "VISITATO";	
					}
				// se entrambi sono numerati e i numeri sono diversi --> SONO DIVERSI
				if(deJureProvv.getProvv_number().trim().length()>0 && 
						p.getProvv_number().trim().length()>0 &&
					!deJureProvv.getProvv_number().equals(p.getProvv_number()))
					return "NON_DUPLICATO"; 
				
				flag = "DA_CONTROLLARE";

			}
			
		}
		return flag;
	}
	
	public void printSingleRowDeJureCivileCSV(Provvedimento p){
		String ret="";
	

			ret+=p.getPersistentID()+Config.SEP
					+p.getFlagDeJureDuplicate()+Config.SEP
					+p.getIdProvvedimento()+Config.SEP
					+p.getDeJureClasse()+Config.SEP
				
					/*+p.isEstremoCompletoAuto()+Config.SEP*/
					/*+p.scrapedPageNumber+Config.SEP*/
					+p.getDistrettoCorteAppello()+Config.SEP
					+p.getProvv_year()+Config.SEP
					+getAutorita(p.getAuthority())+Config.SEP
					+p.getGeo_name()+Config.SEP
					+p.getDocType()+Config.SEP
					+p.getGuessed_subject()+Config.SEP
					+p.getProvv_date()+Config.SEP
					+p.getCourt_section()+Config.SEP
					+p.getProvv_number()+Config.SEP
//					+p.giudici+Config.SEP
//					+p.parti+Config.SEP
//					+p.NumRG+Config.SEP
//					+p.estremoUniforme+Config.SEP
					+p.getEstremoText()+Config.SEP
					+p.getUrl()+Config.SEP
					//+printCitazioneFonteCSV(p.getFonti())+Config.SEP
					+titoloContributoCSV(p.getFonti())+Config.SEP
					+DeJureTestoMassimaCSV(p.getFonti())+Config.SEP
					+idContributoCSV(p.getFonti())+Config.SEP
					+urlContributoCSV(p.getFonti())+Config.SEP
					+estremoFonteCSV(p.getFonti())+Config.SEP
					+editoreCSV(p.getFonti())+Config.SEP
					/*+findableInCSV(p.getFonti())+Config.SEP*/
					+sistematiciCSV(p.getFonti())+Config.SEP
					+descrittoriCSV(p.getFonti())+Config.SEP
					+p.getProvenance()+Config.SEP
					+Config.DATASET_VERSION;
		

		
		System.err.println(ret);
	}
	
	
	public void printExplodedLEXADOCDeJureCivileCSV(Provvedimento p){
		String ret="";
	
		String[] categorie = p.getDeJureClasse().split("#");
		for(int i=0; i<categorie.length;i++) {
			ret+=p.getPersistentID()+Config.SEP
					+p.getFlagDeJureDuplicate()+Config.SEP
					+p.getIdProvvedimento()+Config.SEP
					//+p.getDeJureClasse()+Config.SEP
					+categorie[i]+Config.SEP
					/*+p.isEstremoCompletoAuto()+Config.SEP*/
					/*+p.scrapedPageNumber+Config.SEP*/
					+p.getDistrettoCorteAppello()+Config.SEP
					+p.getProvv_year()+Config.SEP
					+getAutorita(p.getAuthority())+Config.SEP
					+p.getGeo_name()+Config.SEP
					+p.getDocType()+Config.SEP
					+p.getGuessed_subject()+Config.SEP
					+p.getProvv_date()+Config.SEP
					+p.getCourt_section()+Config.SEP
					+p.getProvv_number()+Config.SEP
//					+p.giudici+Config.SEP
//					+p.parti+Config.SEP
//					+p.NumRG+Config.SEP
//					+p.estremoUniforme+Config.SEP
					+p.getEstremoText()+Config.SEP
					+p.getUrl()+Config.SEP
					//+printCitazioneFonteCSV(p.getFonti())+Config.SEP
					+titoloContributoCSV(p.getFonti())+Config.SEP
					+DeJureTestoMassimaCSV(p.getFonti())+Config.SEP
					+idContributoCSV(p.getFonti())+Config.SEP
					+urlContributoCSV(p.getFonti())+Config.SEP
					+estremoFonteCSV(p.getFonti())+Config.SEP
					+editoreCSV(p.getFonti())+Config.SEP
					/*+findableInCSV(p.getFonti())+Config.SEP*/
					+sistematiciCSV(p.getFonti())+Config.SEP
					+descrittoriCSV(p.getFonti())+Config.SEP
					+p.getProvenance()+Config.SEP
					+Config.DATASET_VERSION;
			if(categorie.length>1 && i<categorie.length-1)
				ret+="\n";
		}

		
		System.err.println(ret);
	}
	
	public boolean printCivileDeJureExploded() {

		
		
		String header ="persistentID"+Config.SEP
				+"idProvvedimento"+Config.SEP
				+"pageNum"+Config.SEP
				+"estremo_completo"+Config.SEP
				+"distretto"+Config.SEP
				+"anno"+Config.SEP
				+"tipo_ufficio"+Config.SEP
				+"localita_ufficio"+Config.SEP
				+"tipo_provvedimento"+Config.SEP
				+"area"+Config.SEP		
				+"giudici"+Config.SEP
				+"parti"+Config.SEP
				+"num_RG"+Config.SEP
				+"provv_data"+Config.SEP
				+"provv_sezione"+Config.SEP
				+"provv_numero"+Config.SEP
				+"estremi"+Config.SEP
				+"url_provv"+Config.SEP
				+"idContributo"+Config.SEP
				+"titolo"+Config.SEP
				+"autori"+Config.SEP
				+"url_contributo"+Config.SEP
				+"rivista"+Config.SEP
				+"editore"+Config.SEP
				+"findable_IN"+Config.SEP
				+"fonte"+Config.SEP
				+"provenance"+Config.SEP
				+"versione";
		

		
		//System.err.println(header);
	

		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			printExplodedDeJureCivileCSV(p);
//			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
//				continue;
//			
//			if(p.getProvv_year()==null )
//				p.printGennusoCSV_1_3();
//			else{
//				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
//					p.printGennusoCSV_1_3();
//			}
		}
		return true;
	}
	
	
	
	
	
	
	
	
	public void printExplodedDeJureCivileCSV(Provvedimento p){
		String ret="";
		
	
		for(int i=0;i<p.getFonti().size();i++) {
			ret+=p.getPersistentID()+Config.SEP
					+p.getIdProvvedimento()+Config.SEP
					+p.getDeJureClasse()+Config.SEP
					/*+p.isEstremoCompletoAuto()+Config.SEP*/
					/*+p.scrapedPageNumber+Config.SEP*/
					+p.getDistrettoCorteAppello()+Config.SEP
					+p.getProvv_year()+Config.SEP
					+getAutorita(p.getAuthority())+Config.SEP
					+p.getGeo_name()+Config.SEP
					+p.getDocType()+Config.SEP
					+p.getGuessed_subject()+Config.SEP
					+p.getProvv_date()+Config.SEP
					+p.getProvv_number()+Config.SEP
					+p.getCourt_section()+Config.SEP
//					+p.giudici+Config.SEP
//					+p.parti+Config.SEP
//					+p.NumRG+Config.SEP
//					+p.estremoUniforme+Config.SEP
					+p.getEstremoText()+Config.SEP
					+p.getUrl()+Config.SEP
					+p.getFonti().get(i).getIdContributo()+Config.SEP
					/*+p.getFonti().get(i).printCitazioneFonte()+Config.SEP*/
					+p.getFonti().get(i).getUrl()+Config.SEP
					+p.getFonti().get(i).getTitoloContributo()+Config.SEP
					+p.getFonti().get(i).getDeJureTestoMassima()+Config.SEP
					+p.getFonti().get(i).getAutori()+Config.SEP
					+p.getFonti().get(i).getRivista()+Config.SEP
					+p.getFonti().get(i).getEditore()+Config.SEP
					+p.getFonti().get(i).getFindableIn()+Config.SEP
					+printList(p.getFonti().get(i).getSistematici())+Config.SEP
					+printList(p.getFonti().get(i).getDescrittori())+Config.SEP
//					+p.nota+Config.SEP
					+p.getProvenance()+Config.SEP
					+Config.DATASET_VERSION;
			if(p.getFonti().size()>1 && i<p.getFonti().size()-1)
				ret+="\n";
		}
		System.err.println(ret);
	}

	
	
	
	private String getAutorita(String authority) {
		String ret="";
		if(authority==null)
			return null;
		switch(authority) {
			case "IT_TRB": 
				ret="TRIBUNALE";
				break;
			case "IT_TSV": 
				ret="TRIBUNALE DI SORVEGLIANZA";
				break;
			case "IT_CPP":
				ret="CORTE APPELLO";
				break;
// FIXME come sta la storia delle corti d'assise ?  
			case "IT_CSS":
				ret="CORTE ASSISE";
				//ret="CORTE APPELLO";
				break;
			case "IT_TMN":
				ret="TRIBUNALE DEI MINORI";
				break;
			case "IT_GPC":
				ret="GIUDICE DI PACE";
				break;
			case "IT_CASS":
				ret="CORTE DI CASSAZIONE";
				break;
			case "IT_COST":
				ret="CORTE COSTITUZIONALE";
				break;
			default:
				ret = null;
		}
		return ret;
	}
	
	
	private String printList(String[] list){
		
		String ret="";

		if(list!=null){
			for(int i=0;i<list.length;i++){
				ret+=list[i]+";";
			}
			if(ret.length()>0)
				ret=ret.substring(0, ret.length()-1).trim();
		}
		return ret;
	}
	
	public List<Provvedimento> getProvvedimenti() {
		List<Provvedimento> filtered_provv_list = new ArrayList<Provvedimento>();
		for(String idProvvedimento : provvedimenti.keySet()){
			Provvedimento p = provvedimenti.get(idProvvedimento);
			
			if(p.getAuthority()!=null && (p.getAuthority().equals("IT_CASS")||p.getAuthority().equals("IT_COST")))
				continue;
			if(p.getProvv_year()==null )
				filtered_provv_list.add(p);
			else{
				if(Integer.parseInt(p.getProvv_year().trim())>=2014)
					filtered_provv_list.add(p);
			}
			
		}	
		return filtered_provv_list;
	}

	
	
	
	
	
	
	
	
	
	
	

	
	
	// ==================================================================
	// 			UTIL
	// ==================================================================
	
		
	
	
	private String DeJureTestoMassimaCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getDeJureTestoMassima()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String sistematiciCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getSistematici().length>0)
				ret+=printList(fonti.get(i).getSistematici())+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String urlContributoCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getUrl()!=null && fonti.get(i).getUrl().trim().length()>0)
				ret+=fonti.get(i).getUrl()+" | ";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-2).trim();
		return ret;
	}
	
	private String estremoFonteCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getEstremoText()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String titoloContributoCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getTitoloContributo()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String findableInCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getFindableIn()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	private String editoreCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getEditore()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	private String idContributoCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getIdContributo()+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String descrittoriCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=printList(fonti.get(i).getDescrittori())+"#";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
		
	

	
	
}

