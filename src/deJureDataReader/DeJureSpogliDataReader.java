package deJureDataReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fonte.Fonte;
import provvedimento.Provvedimento;

public class DeJureSpogliDataReader {
	
	
	List<Provvedimento> provvedimenti = new ArrayList<Provvedimento>();
	
	
	
	
	public boolean readDeJureSpogliTSV(String tsvFilePath) {
				
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		boolean isNAS = false;
		boolean isCommentata = false;
		if(tsvFilePath.indexOf("nas")!=-1)
			isNAS = true;
		if(tsvFilePath.indexOf("commentata")!=-1)
			isCommentata = true;
		
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
			String line  = null;			
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;

				//SKIP HEADER
				if(line.startsWith("pagina") || line.startsWith("in_") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
				
				//leggi le info (di interesse)
				//	estremo_completo 
				//	esito
				//	tipo_doc *
				//	area *
				//	giudici *
				//	parti *
				//	num_reg
				//	sezione
				//	numero
				//	linkProvvedimento (dà idDeJure per match)
				//	linkScheda (dà altro id per match)
				
				String paginaDejure = "";
				String estremoCompleto = "";
				String esito = "";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				String provv_url = "";
				String contrib_titolo = "";
				String contrib_autori = "";
				String contrib_fonte = ""; 
				String contrib_url = "";
				String contrib_rivista = "";
				String contrib_data = "";
				String provenance = "";
				
				// 1  GIURISP_COMM_TRIB
				//pagina	estremo_completo	ESITO	TIPO_DOC	AREA	Giudici	Parti	N. Registro	ufficio	localita	dataProvv	sezione	numero	estremo	linkSentenza	urlSentenza	titolo	autori	fonte	linkScheda	urlScheda	rivista	dataNota	provenance

				// 2  GIURISP_COMM_APP
				//pagina	estremo_completo	ESITO	TIPO_DOC	AREA	Giudici	Parti	N. Registro	ufficio	localita	dataProvv	sezione	numero	estremo	linkSentenza	urlSentenza	titolo	autori	fonte	linkScheda	urlScheda	rivista	dataNota	provenance

				if(isCommentata) {
					 paginaDejure = fields[0].trim();
					 estremoCompleto = fields[1].trim();
					 esito = fields[2].trim();
					 tipoDoc = fields[3].trim();
					 area = fields[4].trim();
					 giudici = fields[5].trim();
					 parti = fields[6].trim();
					 num_registro = fields[7].trim();
					 tipo_ufficio  = fields[8].trim();
					 localita_ufficio = fields[9].trim();
					 provv_data = fields[10].trim();  // trasforma in aaaa-mm-gg
					 provv_sezione = fields[11].trim();
					 provv_numero = fields[12].trim();
					 provv_estremo = fields[13].trim();
					 provv_url = fields[15].trim();
					 contrib_titolo = fields[16].trim();
					 contrib_autori = fields[17].trim();
					 contrib_fonte = fields[18].trim(); 
					 contrib_url = fields[20].trim();
					 contrib_rivista = fields[21].trim();
					 contrib_data = fields[22].trim();
					 provenance = fields[23].trim();
				}
				// 3  NOTE_SENTENZA_APP
				//in_GIURISP_COMMENTATA	pagina	estremo_completo	ESITO	TIPO_DOC	AREA	Giudici	Parti	N. Registro	ufficio	
				//localita	dataProvv	sezione	numero	estremo	urlSentenza	linkScheda	titolo	autori	fonte	urlScheda	rivista	dataNota	classificazioniTOP	classificazioni	provenance


				
				//0	5	X	si	sentenza	civile	dr. Giuseppe De Rosa, Presidente dr. Vincenzo Colarieti, Consigliere dr. Francesco Iaderosa, G. A. Relatore		NRG. 57/2017 VG,	Corte appello	Trieste	 06 giugno 2017	I	207	Corte appello  Trieste, 06 giugno 2017, n.207, sez. I		VAI	Negoziazione assistita e trascrizione nei registri immobiliari: è necessario il notaio	Catania Laura	Ilfamiliarista.it, fasc., 23 AGOSTO    2017, pag. 	https://dejure.it/#/ricerca/dottrine_documento?idDatabank=99&idDocMaster=6307030&idUnitaDoc=0&nVigUnitaDoc=1&docIdx=0&semantica=0&isPdf=false&isCorrelazioniSearch=false	Ilfamiliarista.it	23 AGOSTO    2017	"SEPARAZIONE DEI CONIUGI"" | ""SEPARAZIONE DEI CONIUGI"" "	"SEPARAZIONE DEI CONIUGI - Consensuale"" | ""SEPARAZIONE DEI CONIUGI - Consensuale"" "	DeJure-Note a sentenza-Appello-2014-2019

				
				if(isNAS) {
					 paginaDejure = fields[1].trim();
					 estremoCompleto = fields[2].trim();
					 esito = fields[3].trim();
					 tipoDoc = fields[4].trim();
					 area = fields[5].trim();
					 giudici = fields[6].trim();
					 parti = fields[7].trim();
					 num_registro = fields[8].trim();
					 tipo_ufficio  = fields[9].trim();
					 localita_ufficio = fields[10].trim();
					 provv_data = fields[11].trim();  // trasforma in aaaa-mm-gg
					 provv_sezione = fields[12].trim();
					 provv_numero = fields[13].trim();
					 provv_estremo = fields[14].trim();
					 contrib_titolo = fields[17].trim();
					 contrib_autori = fields[18].trim();
					 contrib_fonte = fields[19].trim(); 
					 contrib_url = fields[20].trim();
					 contrib_rivista = fields[21].trim();
					 contrib_data = fields[22].trim();
					 provenance = fields[25].trim();
				}
			
				
				String idProvvDeJure = "";
				if(provv_url!=null && provv_url.trim().length()>0 && provv_url.indexOf("idDocMaster")!=-1){
					idProvvDeJure = provv_url.substring(provv_url.indexOf("idDocMaster=")+12, provv_url.length());
					idProvvDeJure = idProvvDeJure.substring(0, idProvvDeJure.indexOf("&"));
					//System.err.println("idProvvDeJure: "+idProvvDeJure);
				}
				
				//https://dejure.it/#/ricerca/dottrine_documento?idDatabank=99&idDocMaster=7015206&idUnitaDoc=0&nVigUnitaDoc=1&docIdx=0&semantica=0&isPdf=false&isCorrelazioniSearch=false
				String idFonteDeJure = "";
				if(contrib_url!=null && contrib_url.trim().length()>0 && contrib_url.indexOf("idDocMaster")!=-1){
					idFonteDeJure = contrib_url.substring(contrib_url.indexOf("idDocMaster=")+12, contrib_url.length());
					idFonteDeJure = idFonteDeJure.substring(0,idFonteDeJure.indexOf("&"));
					//System.err.println("idFonteDeJure: "+idFonteDeJure);
				}else {
					System.err.println("********  idDocMaster NOT FOUND  *********");
				}
				
				// se non c'è link alla sentenza prendo id del contributo
				if(idProvvDeJure.trim().length()==0)
					idProvvDeJure = idFonteDeJure;
				
				
				
				String normalizedDate = getNormalizedDate(provv_data);
				
				Provvedimento p = new Provvedimento();
				
				
				//leggi le info (di interesse)
				//	estremo_completo 
				//	esito
				//	tipo_doc *
				//	area *
				//	giudici *
				//	parti *
				//	num_reg
				//	sezione
				//	numero
				//	linkProvvedimento (dà idDeJure per match)
				//	linkScheda (dà altro id per match)
				
			
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
				
				
				p.setScrapedPageNumber(paginaDejure);
				p.setEditManuale(true);
				p.setEsitoEdit(esito.toLowerCase().startsWith("s")?true:false);
				p.setEstremoCompletoEdit(esito.toLowerCase().startsWith("s")?true:false);
				p.setEstremoCompletoAuto(estremoCompleto.toLowerCase().equals("x")?true:false);

				
				p.setDocType(tipoDoc);
				p.setLegalArea(area.toUpperCase().toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(normalizedDate);
				p.setCourt_section(provv_sezione);
				p.setIdProvvedimento(idProvvDeJure);   				
				p.setEstremoText(provv_estremo);
				p.setUrl(provv_url);
				p.setCommentedWith("");
				p.setProvv_number(provv_numero);
				p.setProvv_year(normalizedDate.substring(0, 4));				
				p.setProvenance(provenance);
			
				
				Fonte f = new Fonte();
				f.setIdContributo(idFonteDeJure);					
				f.setAutori(contrib_autori);
				f.setTitoloContributo(contrib_titolo);
				f.setRivista(contrib_rivista);
				f.setDataFascicolo(contrib_data);   	
				f.setEditore("Giuffrè Francis Lefebvre");
				f.setFindableIn("GFL");
				f.setEstremoText(contrib_fonte);     	
				f.setUrl(contrib_url);
				f.setSistematici(new String[]{""});
				
				
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
				provvedimenti.add(p);			
				
			
				
			}
			System.err.println("processed "+i+" rows");

		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	
	public boolean readDeJureSpogliTSV_v2(String tsvFilePath) {
		
		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;

				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("omissis", "");
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");
				
				
				//leggi le info (di interesse)
				//	estremo_completo 
				//	esito
				//	tipo_doc *
				//	area *
				//	giudici *
				//	parti *
				//	num_reg
				//	sezione
				//	numero
				//	linkProvvedimento (dà idDeJure per match)
				//	linkScheda (dà altro id per match)
				
				String idITTIG ="";
				String distretto ="";
				String provv_year="";
				String estremoCompleto = "";
				String esito = "";
				String tipoDoc = "";
				String area = "";
				String giudici = "";
				String parti = "";
				String num_registro = "";
				String tipo_ufficio  = "";
				String localita_ufficio = "";
				String provv_data = "";
				String provv_sezione = "";
				String provv_numero = "";
				String provv_estremo = "";
				String provv_url = "";
				String contrib_titolo = "";
				String contrib_autori = "";
				String contrib_fonte = ""; 
				String contrib_url = "";
				String contrib_rivista = "";
				String contrib_data = "";
				String provenance = "";
				
				
				//
				// idITTIG	isEstremoCompleto	ESITO	distretto	anno	TIPO_DOC	AREA	giudici	parti	num_registro	
				// tipo_ufficio	localita_ufficio	provv_data	provv_numero	provv_sez	estremo_provv	link_provv	url_provv	
				// fonte	link_contributo	url_contributo	rivista	editore	classificazioneTOP	nota	provenance	versione
				idITTIG = fields[0].trim();
				estremoCompleto = fields[1].trim();
				esito = fields[2].trim();
				distretto = fields[3].trim();
				provv_year = fields[4].trim();
				tipoDoc = fields[5].trim();
				area = fields[6].trim();
				giudici = fields[7].trim();
				parti = fields[8].trim();
				num_registro = fields[9].trim();
				tipo_ufficio  = fields[10].trim();
				localita_ufficio = fields[11].trim();
				provv_data = fields[12].trim();  // trasforma in aaaa-mm-gg
				provv_sezione = fields[14].trim();
				provv_numero = fields[13].trim();
				provv_estremo = fields[15].trim();
				provv_url = fields[17].trim();
				contrib_fonte = fields[18].trim();
				contrib_url = fields[20].trim();
				contrib_rivista = fields[21].trim();
				provenance = fields[25].trim();
				
				
				String idProvvDeJure = "";
				if(provv_url!=null && provv_url.trim().length()>0 && provv_url.indexOf("idDocMaster")!=-1){
					idProvvDeJure = provv_url.substring(provv_url.indexOf("idDocMaster=")+12, provv_url.length());
					idProvvDeJure = idProvvDeJure.substring(0, idProvvDeJure.indexOf("&"));
					//System.err.println("idProvvDeJure: "+idProvvDeJure);
				}
				
				//https://dejure.it/#/ricerca/dottrine_documento?idDatabank=99&idDocMaster=7015206&idUnitaDoc=0&nVigUnitaDoc=1&docIdx=0&semantica=0&isPdf=false&isCorrelazioniSearch=false
				String idFonteDeJure = "";
				if(contrib_url!=null && contrib_url.trim().length()>0 && contrib_url.indexOf("idDocMaster")!=-1){
					idFonteDeJure = contrib_url.substring(contrib_url.indexOf("idDocMaster=")+12, contrib_url.length());
					idFonteDeJure = idFonteDeJure.substring(0,idFonteDeJure.indexOf("&"));
					//System.err.println("idFonteDeJure: "+idFonteDeJure);
				}else {
					System.err.println("********  idDocMaster NOT FOUND  *********");
				}
				
				// se non c'è link alla sentenza prendo id del contributo
				if(idProvvDeJure.trim().length()==0)
					idProvvDeJure = idFonteDeJure;
				
				
				String normalizedDate = provv_data;
				
				Provvedimento p = new Provvedimento();
				
				
				//leggi le info (di interesse)
				//	estremo_completo 
				//	esito
				//	tipo_doc *
				//	area *
				//	giudici *
				//	parti *
				//	num_reg
				//	sezione
				//	numero
				//	linkProvvedimento (dà idDeJure per match)
				//	linkScheda (dà altro id per match)
				
			
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
		
				p.setEditManuale(true);
				p.setEsitoEdit(esito.toLowerCase().startsWith("s")?true:false);
				p.setEstremoCompletoEdit(esito.toLowerCase().startsWith("s")?true:false);
				p.setEstremoCompletoAuto(estremoCompleto.toLowerCase().equals("true")?true:false);

				p.setPersistentID(idITTIG);
				p.setDocType(tipoDoc);
				p.setLegalArea(area.toUpperCase().toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setGiudici(giudici);
				p.setParti(parti);
				p.setNumRG(num_registro);
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setProvv_date(normalizedDate);
				p.setCourt_section(provv_sezione);
				p.setIdProvvedimento(idProvvDeJure);   				
				p.setEstremoText(provv_estremo);
				p.setUrl(provv_url);
				p.setCommentedWith("");
				p.setProvv_number(provv_numero);
				p.setProvv_year(provv_year);				
				p.setProvenance(provenance);
			
				
				Fonte f = new Fonte();
				f.setIdContributo(idFonteDeJure);					
				f.setAutori(contrib_autori);
				f.setTitoloContributo(contrib_titolo);
				f.setRivista(contrib_rivista);
				f.setDataFascicolo(contrib_data);   	
				f.setEditore("Giuffrè Francis Lefebvre");
				f.setFindableIn("GFL");
				f.setEstremoText(contrib_fonte);     	
				f.setUrl(contrib_url);
				f.setSistematici(new String[]{""});
				
				
				// 1 provvedimento / 1 fonte
				p.addFonte(f);
				provvedimenti.add(p);			
				
			
				
			}
			System.err.println("processed "+i+" rows");

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}

	
	
	private String getNormalizedDate(String date) {

		//	in 21/09/2018
		//	in 04 settembre 2018
		//	out 2016-04-28		
		try {
		if(date.indexOf("/")!=-1) {
			return date.split("/")[2]+"-"+date.split("/")[1]+"-"+date.split("/")[0];
		}else {
			String day = date.split(" ")[0].trim();
			String month = date.split(" ")[1].trim();
			String year = date.split(" ")[2].trim();

			switch(month.toLowerCase()) {
			case "gennaio": month="01";
			break;
			case "febbraio": month="02";
			break;
			case "marzo": month="03";
			break;
			case "aprile": month="04";
			break;
			case "maggio": month="05";
			break;
			case "giugno": month="06";
			break;
			case "luglio": month="07";
			break;
			case "agosto": month="08";
			break;
			case "settembre": month="09";
			break;
			case "ottobre": month="10";
			break;
			case "novembre": month="11";
			break;
			case "dicembre": month="12";
			break;
			}
			return year+"-"+month+"-"+day;
		}
		}catch(Exception ex) {
			System.out.println("+++++++++++++ exc for "+date);
			return date;
		}
	}
	
	public List<Provvedimento> getProvvedimenti() {
		return this.provvedimenti;
	}
	

}
