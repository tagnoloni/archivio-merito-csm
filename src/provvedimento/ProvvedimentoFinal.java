package provvedimento;

import java.util.ArrayList;

import config.Config;

public class ProvvedimentoFinal extends Provvedimento{

	
	private String presidente= "";
	private String relatore_estensore= "";
	private boolean spogliato = false;
	private String normalized_court_section ="";






	private static final long serialVersionUID = -628787308975888036L;

	ArrayList<String> classificazioniFinal;

	public ArrayList<String> getClassificazioniFinal() {
		return classificazioniFinal;
	}

	public void setClassificazioniFinal(ArrayList<String> classificazioniFinal) {
		this.classificazioniFinal = classificazioniFinal;
	} 
	
	public String getPresidente() {
		return presidente;
	}

	public void setPresidente(String presidente) {
		this.presidente = presidente;
	}

	public String getRelatore_estensore() {
		return relatore_estensore;
	}

	public void setRelatore_estensore(String relatore_estensore) {
		this.relatore_estensore = relatore_estensore;
	}

	
	public boolean isSpogliato() {
		return spogliato;
	}

	public void setSpogliato(boolean spogliato) {
		this.spogliato = spogliato;
	}

	
	public String getNormalized_court_section() {
		return normalized_court_section;
	}

	public void setNormalized_court_section(String normalized_court_section) {
		this.normalized_court_section = normalized_court_section;
	}

	
	public void printSingleRowProvvedimentoFinal(){
		String ret="";

		ret+=this.getPersistentID()+Config.SEP
				//+this.getPersistentIDFix()+Config.SEP
				//+this.isEstremoCompleto()+Config.SEP
				//+this.isProvvedimentoConsegnato()+Config.SEP
				+this.getDataConsegnaProvvedimento()+Config.SEP
				+this.isProvvedimentoTrovato()+Config.SEP
				+this.getDistrettoCorteAppello()+Config.SEP
				+this.getProvv_year()+Config.SEP
				+getAutorita(this.getAuthority())+Config.SEP
				+this.getGeo_name()+Config.SEP
				+getTipoDoc(this.getDocType())+Config.SEP
				+this.getGuessed_subject()+Config.SEP
				+this.getProvv_date()+Config.SEP
				+this.getProvv_number()+Config.SEP
				+this.getNormalized_court_section()+Config.SEP
				+this.getGiudici()+Config.SEP
				+this.getParti()+Config.SEP
				+this.getNumRG()+Config.SEP
				+this.getEstremoUniforme()+Config.SEP
				+this.getUrl()+Config.SEP
				+printCitazioneFonteCSV(this.getFonti())+Config.SEP
				+urlContributoCSV(this.getFonti())+Config.SEP
				+rivistaCSV(this.getFonti())+Config.SEP
				+editoreCSV(this.getFonti())+Config.SEP
				+this.getNota()+Config.SEP
				+this.getProvenance()+Config.SEP
				+this.getLocalFilePath()+Config.SEP
				+this.getNotaRispostaRichiesta()+Config.SEP
				+classificazioniTopCSV(this.getClassificazioniTOP())+Config.SEP
				+classificazioniCSMFinalCSV(this.getClassificazioniCSMFinal())+Config.SEP
				+classificazioniFINAL_CSV(this.getClassificazioniFinal())+Config.SEP
				+Config.DATASET_VERSION;
				
		
		System.err.println(ret);
	}
	

	

	
	public void printSingleRowTabellaSpogliFinale(){
		String ret="";

		ret+=this.getPersistentID()+Config.SEP
				+this.isRestituireDefinitivo()+Config.SEP
				+this.getNota()+Config.SEP
				//+this.isSpogliato()+Config.SEP
				+this.getDataConsegnaProvvedimento()+Config.SEP
				+this.isProvvedimentoTrovato()+Config.SEP
				+this.getNotaRispostaRichiesta()+Config.SEP
				+this.getEstremoUniforme()+Config.SEP
				+this.getLocalFilePath()+Config.SEP
				+titoloContributoCSV(this.getFonti())+Config.SEP
				+printFonteGennusoCSV(this.getFonti())+Config.SEP
				//+printCitazioneFonteCSV(this.getFonti())+Config.SEP
				+this.getDistrettoCorteAppello()+Config.SEP
				+this.getProvv_year()+Config.SEP
				+getAutorita(this.getAuthority())+Config.SEP
				+this.getGeo_name()+Config.SEP
				+getTipoDoc(this.getDocType())+Config.SEP
				+this.getGuessed_subject()+Config.SEP
				+this.getNormalized_court_section()+Config.SEP
				+this.getProvv_number()+Config.SEP
				+this.getNumRG()+Config.SEP
				+this.getProvv_date()+Config.SEP
				+this.getDataDecisione()+Config.SEP
				+this.getDataPubblicazione()+Config.SEP
				+this.getGiudici()+Config.SEP
				+this.getPresidente()+Config.SEP
				+this.getRelatore_estensore()+Config.SEP
				+classificazioniFINAL_CSV(this.getClassificazioniFinal())+Config.SEP
				+classificazioniTopCSV(this.getClassificazioniTOP())+Config.SEP
				+classificazioniCSMFinalCSV(this.getClassificazioniCSMFinal())+Config.SEP
				+this.getUrl()+Config.SEP
				+urlContributoCSV(this.getFonti())+Config.SEP
				+rivistaCSV(this.getFonti())+Config.SEP
				+editoreCSV(this.getFonti())+Config.SEP
				+this.getProvenance()+Config.SEP
				+Config.DATASET_VERSION;
		
		System.err.println(ret);
	}
	
	private String fixLocalUrl(String url) {
		
		//http://nash.ittig.cnr.it/risposte-csm/trieste/ittstrb20191479240425/documento.pdf
		if(url.indexOf("http://nash.ittig.cnr.it/risposte-csm")!=-1)
			return url.replace("http://nash.ittig.cnr.it/risposte-csm", "").trim();
		return url;
	}
	
	
	public void printSingleRowProvvedimentoCONSEGNA_FINALE(){
		String ret="";

		ret+=this.getPersistentID()+Config.SEP
				+this.getDistrettoCorteAppello()+Config.SEP
				+this.getProvv_year()+Config.SEP
				+getAutorita(this.getAuthority())+Config.SEP
				+this.getGeo_name()+Config.SEP
				+getTipoDoc(this.getDocType())+Config.SEP
				+this.getGuessed_subject()+Config.SEP
				+this.getDataDecisione()+Config.SEP
				+this.getDataPubblicazione()+Config.SEP
				+this.getProvv_number()+Config.SEP
				+this.getNumRG()+Config.SEP
				+this.getNormalized_court_section()+Config.SEP
				+this.getPresidente()+Config.SEP
				+this.getRelatore_estensore()+Config.SEP
				+classificazioniFINAL_CSV(this.getClassificazioniFinal())+Config.SEP
				+this.getEstremoUniforme()+Config.SEP
				+titoloContributoCSV(this.getFonti())+Config.SEP
				+printFonteGennusoCSV(this.getFonti())+Config.SEP
				//+this.getNota()+Config.SEP
				+simplifiedProvenance(this.getProvenance())+Config.SEP
				+Config.DATASET_VERSION+Config.SEP
				+fixLocalUrl(this.getLocalFilePath());

				
		
		System.err.println(ret);
	}
	
	
	
	public void printSingleRowProvvedimentoCONSEGNA_FINALE_PRE_CONSEGNA(){
		String ret="";

		ret+=this.getPersistentID()+Config.SEP
				+this.isSpogliato()+Config.SEP
				+this.isRestituireDefinitivo()+Config.SEP
				+this.getNota()+Config.SEP
				+this.getDistrettoCorteAppello()+Config.SEP
				+this.getProvv_year()+Config.SEP
				+getAutorita(this.getAuthority())+Config.SEP
				+this.getGeo_name()+Config.SEP
				+getTipoDoc(this.getDocType())+Config.SEP
				+this.getGuessed_subject()+Config.SEP
				+this.getDataDecisione()+Config.SEP
				+this.getDataPubblicazione()+Config.SEP
				+this.getProvv_number()+Config.SEP
				+this.getNumRG()+Config.SEP
				+this.getNormalized_court_section()+Config.SEP
				+this.getPresidente()+Config.SEP
				+this.getRelatore_estensore()+Config.SEP
				+classificazioniFINAL_CSV(this.getClassificazioniFinal())+Config.SEP
				+this.getEstremoUniforme()+Config.SEP
				+titoloContributoCSV(this.getFonti())+Config.SEP
				+printFonteGennusoCSV(this.getFonti())+Config.SEP
				//+this.getNota()+Config.SEP
				+simplifiedProvenance(this.getProvenance())+Config.SEP
				+Config.DATASET_VERSION+Config.SEP
				+fixLocalUrl(this.getLocalFilePath());

				
		
		System.err.println(ret);
	}
	
	
	private String simplifiedProvenance(String provenance) {
		
		
		if(provenance.toLowerCase().indexOf("lexadoc")!=-1)
			return "DeJure_Giur";
		if(provenance.toLowerCase().indexOf("dejure")!=-1)
			return "DeJure";
		if(provenance.toLowerCase().indexOf("wki")!=-1)
			return "WKI";
		if(provenance.toLowerCase().indexOf("dogi")!=-1)
			return "DoGi";
	
		return provenance;
	}
	
	
	public String classificazioniFINAL_CSV(ArrayList<String> classificazioniFinal) {
		
		if(classificazioniFinal == null)
			return "";
		String ret="";
		//if(!this.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.") && !this.getGuessed_subject().equalsIgnoreCase("PENALE")) {
		
		for(String descCSM:classificazioniFinal) {
			if(descCSM.trim().length()>0)
				ret+=descCSM+Config.MULTI_VALUE_SEP;
		}
		
		//}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
}
