package provvedimento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import config.Config;
import fonte.Fonte;
import it.cnr.ittig.linkoln.Linkoln;
import it.cnr.ittig.linkoln.LinkolnDocument;
import it.cnr.ittig.linkoln.LinkolnDocumentFactory;
import it.cnr.ittig.linkoln.entity.AnnotationEntity;
import it.cnr.ittig.linkoln.entity.CaseLawAuthority;
import it.cnr.ittig.linkoln.entity.CaseLawAuthoritySection;
import it.cnr.ittig.linkoln.entity.CaseLawReference;
import it.cnr.ittig.linkoln.entity.City;
import it.cnr.ittig.linkoln.entity.Date;
import it.cnr.ittig.linkoln.entity.DocumentType;
import it.cnr.ittig.linkoln.entity.Municipality;
import it.cnr.ittig.linkoln.entity.Subject;
import it.cnr.ittig.linkoln.entity.Year;



public class Provvedimento implements Serializable {

	private static final long serialVersionUID = -628789568975888036L;
	
	private String idProvvedimento;
	
	//[] fonte
		
	private List<Fonte> fonti = new ArrayList<Fonte>();	
	private String estremoDogi;
	private String estremoText;
	private String estremoUniforme;
	private String commentedWith;
	private boolean linkolnFoundRef;
	private String legalArea;   // istanziato sulla base della classificazione della fonte ? 
	private String docType;
	private String authority;
	private String provv_year;
	private String provv_date;
	private String provv_number ="";
	private String court_city;
	private String court_municipality; 
	private String geo_name;
	private String court_subject;
	private String guessed_subject = "";
	private String court_section ="";
	private String provenance;
	private String distrettoCorteAppello;
	private String distrettoCorteAppello_SIGLA;
	private String persistentID; 
	private String persistentIDFix; 

	

	private String scrapedPageNumber ="";
	
	private String deJureClasse ="";
	private String flagDeJureDuplicate ="";
	
	
	private String dataDecisione ="";
	private String dataPubblicazione ="";
	private boolean restituireDefinitivo;
	private String filePath ="";

	private String[] classificazioniCSMFinal;

	private String classificazioniCSM ="";

	private String eli_GU_Pdf ="";
	
	
	
	///////////////////////////////////////
	// CONSEGNE DEFINITIVE SPOGLI AGO 2019
	///////////////////////////////////////
//	ArrayList<String> classificazioniFinal;
//
//	public ArrayList<String> getClassificazioniFinal() {
//		return classificazioniFinal;
//	}
//
//	public void setClassificazioniFinal(ArrayList<String> classificazioniFinal) {
//		this.classificazioniFinal = classificazioniFinal;
//	} 
//	
	
	//////////////////////////
	// CONSEGNE DEFINITIVE
	//////////////////////////

	public String getPersistentIDFix() {
		return persistentIDFix;
	}


	
	public void setPersistentIDFix(String persistentIDFix) {
		this.persistentIDFix = persistentIDFix;
	}
	
	
	public String getDataDecisione() {
		return dataDecisione;
	}


	public void setDataDecisione(String dataDecisione) {
		this.dataDecisione = dataDecisione;
	}


	public String getDataPubblicazione() {
		return dataPubblicazione;
	}


	public void setDataPubblicazione(String dataPubblicazione) {
		this.dataPubblicazione = dataPubblicazione;
	}


	public boolean isRestituireDefinitivo() {
		return restituireDefinitivo;
	}


	public void setRestituireDefinitivo(boolean restituireDefinitivo) {
		this.restituireDefinitivo = restituireDefinitivo;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String[] getClassificazioniCSMFinal() {
		return classificazioniCSMFinal;
	}


	public void setClassificazioniCSMFinal(String[] classificazioniCSMFinal) {
		this.classificazioniCSMFinal = classificazioniCSMFinal;
	}

	
	public String getEli_GU_Pdf() {
		return eli_GU_Pdf;
	}


	public void setEli_GU_Pdf(String eli_GU_Pdf) {
		this.eli_GU_Pdf = eli_GU_Pdf;
	}
	
	//////////////////////////
	// END - CONSEGNE DEFINITIVE
	//////////////////////////
	
	public String getClassificazioniCSM() {
		return classificazioniCSM;
	}


	public void setClassificazioniCSM(String classificazioniCSM) {
		this.classificazioniCSM = classificazioniCSM;
	}


	public String getFlagDeJureDuplicate() {
		return flagDeJureDuplicate;
	}


	public void setFlagDeJureDuplicate(String flagDeJureDuplicate) {
		this.flagDeJureDuplicate = flagDeJureDuplicate;
	}

	private ArrayList<String> classificazioniTOP = null;
	


	private String nota="";
	
	
	private boolean editManuale = false;
	private boolean esitoEdit = false;
	private boolean estremoCompletoAuto= false;
	private boolean estremoCompletoEdit= false;
	private boolean estremoCompleto= false;
	
	private boolean provvedimentoConsegnato = false;
	private String dataConsegnaProvvedimento ="";
	

	
	private String isProvvedimentoTrovato = "";
	private String localFilePath="";
	private String notaRispostaRichiesta="";
	


	private String giudici= "";
	private String parti= "";
 	private String NumRG= "";
	private String dataUdienza= "";
	private String url= "";
	
	
	public ArrayList<String> getClassificazioniTOP() {
		return classificazioniTOP;
	}


	public void setClassificazioniTOP(ArrayList<String> classificazioniTOP) {
		this.classificazioniTOP = classificazioniTOP;
	}
	
	public String isProvvedimentoTrovato() {
		return isProvvedimentoTrovato;
	}


	public void setProvvedimentoTrovato(String isProvvedimentoTrovato) {
		this.isProvvedimentoTrovato = isProvvedimentoTrovato;
	}


	public String getLocalFilePath() {
		return localFilePath;
	}


	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}


	public String getNotaRispostaRichiesta() {
		return notaRispostaRichiesta;
	}


	public void setNotaRispostaRichiesta(String notaRispostaRichiesta) {
		this.notaRispostaRichiesta = notaRispostaRichiesta;
	}
	
	public boolean isProvvedimentoConsegnato() {
		return provvedimentoConsegnato;
	}


	public void setProvvedimentoConsegnato(boolean provvedimentoConsegnato) {
		this.provvedimentoConsegnato = provvedimentoConsegnato;
	}


	public String getDataConsegnaProvvedimento() {
		return dataConsegnaProvvedimento;
	}


	public void setDataConsegnaProvvedimento(String dataConsegnaProvvedimento) {
		this.dataConsegnaProvvedimento = dataConsegnaProvvedimento;
	}
	

	public String getDeJureClasse() {
		return deJureClasse;
	}


	public void setDeJureClasse(String deJureClasse) {
		this.deJureClasse = deJureClasse;
	}

	public boolean isEstremoCompleto() {
		return estremoCompleto;
	}


	public void setEstremoCompleto(boolean estremoCompleto) {
		this.estremoCompleto = estremoCompleto;
	}
	
	
	public boolean isEditManuale() {
		return editManuale;
	}


	public void setEditManuale(boolean editManuale) {
		this.editManuale = editManuale;
	}



	public boolean isEsitoEdit() {
		return esitoEdit;
	}


	public void setEsitoEdit(boolean esitoEdit) {
		this.esitoEdit = esitoEdit;
	}


	public boolean isEstremoCompletoAuto() {
		return estremoCompletoAuto;
	}


	public void setEstremoCompletoAuto(boolean estremoCompletoAuto) {
		this.estremoCompletoAuto = estremoCompletoAuto;
	}


	public boolean isEstremoCompletoEdit() {
		return estremoCompletoEdit;
	}


	public void setEstremoCompletoEdit(boolean estremoCompletoEdit) {
		this.estremoCompletoEdit = estremoCompletoEdit;
	}
	
	
	public String getEstremoUniforme() {
		return estremoUniforme;
	}


	public void setEstremoUniforme(String estremoUniforme) {
		this.estremoUniforme = estremoUniforme;
	}
	

	public String getNota() {
		return nota;
	}

	
	
	public void setNota(String nota) {
		this.nota = nota;
	}


	public String getGiudici() {
		return giudici;
	}



	public void setGiudici(String giudici) {
		this.giudici = giudici;
	}





	public String getParti() {
		return parti;
	}



	public void setParti(String parti) {
		this.parti = parti;
	}



	public String getNumRG() {
		return NumRG;
	}



	public void setNumRG(String numRG) {
		NumRG = numRG;
	}


	public String getDataUdienza() {
		return dataUdienza;
	}


	public void setDataUdienza(String dataUdienza) {
		this.dataUdienza = dataUdienza;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	
	//////////////////////////////////////////////

	
	public String getScrapedPageNumber() {
		return scrapedPageNumber;
	}
	public void setScrapedPageNumber(String scrapedPageNumber) {
		this.scrapedPageNumber = scrapedPageNumber;
	}
	
	

	public void setFonti(List<Fonte> fonti) {
		this.fonti = fonti;
	}




	public String getPersistentID() {
		return persistentID;
	}



	public void setPersistentID(String persistentID) {
		this.persistentID = persistentID;
	}



	public String getDistrettoCorteAppello() {
		return distrettoCorteAppello;
	}

	

	public void setDistrettoCorteAppello(String distrettoCorteAppello) {
		this.distrettoCorteAppello = distrettoCorteAppello;
	}

	
	public String getDistrettoCorteAppello_SIGLA() {
		return distrettoCorteAppello_SIGLA;
	}
	
	public void setDistrettoCorteAppello_SIGLA(String distrettoCorteAppello_SIGLA) {
		this.distrettoCorteAppello_SIGLA = distrettoCorteAppello_SIGLA;
	}
	
	
	public String getGuessed_subject() {
		return guessed_subject;
	}


	public void setGuessed_subject(String guessed_subject) {
		this.guessed_subject = guessed_subject;
	}
	

	public String getEstremoDogi() {
		return estremoDogi;
	}


	public void setEstremoDogi(String estremoDogi) {
		this.estremoDogi = estremoDogi;
		this.estremoText = prepareForParser(estremoDogi);
		
		// completa i meta del provvedimento applicando Linkoln su testo estremo citazione
		linkolnCall(this.estremoText);
	}

		
	
	private String prepareForParser(String estremi){		

		//Ass. App. Torino 27 maggio 2003
		//
		//ord. Ass. App. Messina 18 dicembre 2013
		
		if(estremi.startsWith("Nota a")){
			estremi=estremi.substring(6, estremi.length()).trim();
		}

		if((!estremi.trim().toLowerCase().startsWith("ord.") && ! estremi.trim().toLowerCase().startsWith("decr.")))
			estremi = "sentenza "+estremi;
		estremi=estremi.replaceAll("Trib.", "Tribunale");
		estremi=estremi.replaceAll("sez. civ", "civ");
		if(estremi.indexOf("Ass. App.")!=-1){
			estremi=estremi.replaceAll("Ass. App.", "Corte Assise Appello");
		}else{
			estremi=estremi.replaceAll("App.", "Corte Appello");
		}
		estremi=estremi.replaceAll("decr.", "decreto");

		return estremi.trim();
	}
	
	
	
	private void linkolnCall (String text) {


		// FIX
		
		if(text.indexOf("Magistrato sorveglianza")!=-1) {
			text = text.replace("Magistrato", "Tribunale");
			//System.err.println("***** LINKOLN parse replacement - "+text );
		}
		
		
		
		String inputEcli = "";
		LinkolnDocument linkolnDocument = null;

		inputEcli = ""; // "ECLI:IT:COST:2016:250";
		linkolnDocument = LinkolnDocumentFactory.getDocument(inputEcli);//(text, inputEcli, language, jurisdiction); 
		linkolnDocument.setText(text);

		this.geo_name="";


			
			Linkoln.run(linkolnDocument);
			this.linkolnFoundRef = false;
			
			


			for(AnnotationEntity entity : linkolnDocument.getAnnotationEntities()) {


				if(entity instanceof CaseLawAuthority){
					this.authority = entity.getValue();
				}

				if(entity instanceof CaseLawReference){
					this.linkolnFoundRef = true;
				}

				if(entity instanceof DocumentType){
					this.docType = entity.getValue();
				}

				if(entity instanceof City){
					this.court_city = entity.getValue();
					this.geo_name =(this.geo_name.trim()+" "+entity.getText()).trim();
				}
				
				if(entity instanceof Municipality){
					this.court_municipality = entity.getValue();
					this.geo_name = (this.geo_name.trim()+" "+entity.getText()).trim();
				}

				if(entity instanceof Subject){
					this.court_subject = entity.getValue();
					//System.err.println(text+"--SUBJ: "+this.court_subject);
				}

				if(entity instanceof CaseLawAuthoritySection){
					this.court_section = entity.getValue();
				}

//				if(entity instanceof CaseLawDetachedSection){
//					distacco = entity.getValue();
//				}

				if(entity instanceof Date){
					this.provv_date = entity.getValue();
				}

				if(entity instanceof Year){
					this.provv_year = entity.getValue();
				}

				if(entity instanceof it.cnr.ittig.linkoln.entity.Number){
					setProvv_number(entity.getValue());
				}

//				if(entity instanceof Party){
//					applicant = entity.getValue();
//				}
			}
		
			
			// LOCALITa non riconosciute
			
			if(this.court_section==null || this.court_section.trim().length()==0) {
				if(text.toLowerCase().indexOf("impresa - b")!=-1) {
					this.court_section = "spec. Impresa - B";
				}
				else if(text.toLowerCase().indexOf("impresa")!=-1) {
					this.court_section = "spec. Impresa";
				}
				if(text.toLowerCase().indexOf("fall")!=-1) {
					this.court_section = "fallimentare";
				}
			}
			
			
			if(this.geo_name.equals("bologna")) {
				this.geo_name = "Bologna";
			}
			
			if(this.geo_name.equals("Lecce Taranto")) {
				this.geo_name = "Taranto";
			}
			
			
			if(this.geo_name.equals("Cagliari Sassari")) {
				this.geo_name = "Sassari";
			}
			
			if(this.geo_name.equals("Trento Bolzano")) {
				this.geo_name = "Bolzano";
			}
			
			
			if(this.geo_name.equals("Cagliari Sassari")) {
				this.geo_name = "Sassari";
			}
			
			if(this.geo_name.trim().length()==0) {
				if(text.toLowerCase().indexOf("reggio emilia")!=-1) {
					this.geo_name = "Reggio Emilia";
				}
				if(text.toLowerCase().indexOf("civitavecchia")!=-1) {
					this.geo_name = "Civitavecchia";
				}
				if(text.toLowerCase().indexOf("spezia")!=-1) {
					this.geo_name = "La Spezia";
				}
				if(text.toLowerCase().indexOf("mestre")!=-1) {
					this.geo_name = "Venezia";
				}
				if(text.toLowerCase().indexOf("termini imerese")!=-1) {
					this.geo_name = "Termini Imerese";
				}
				if(text.toLowerCase().indexOf("vercelli")!=-1) {
					this.geo_name = "Vercelli";
				}
				if(text.toLowerCase().indexOf("varese")!=-1) {
					this.geo_name = "Varese";
				}
				if(text.toLowerCase().indexOf("modena")!=-1) {
					this.geo_name = "Modena";
				}
				if(text.toLowerCase().indexOf("lucca")!=-1) {
					this.geo_name = "Lucca";
				}
				if(text.toLowerCase().indexOf("barra")!=-1) {
					this.geo_name = "Barra";
				}
				if(text.toLowerCase().indexOf("forl")!=-1) {
					this.geo_name = "Forlì";
				}

			}
			
			if(this.authority==null || this.authority.trim().length()==0) {
				if(text.toLowerCase().indexOf("tribunale")!=-1) {
					this.authority = "IT_TRB";
				}
				else if(text.toLowerCase().indexOf("giudice tutelare")!=-1) {
					this.authority = "IT_TRB";
				}
			}
			
			// FIX BRUTALE di: 
			//297471	1826-3534	Nota a Trib. Roma sez. II civ. 2016, n. 2948	
			//Nota a sentenza del Tribunale di Roma, in materia di affidamento unitario delle infrastrutture strategiche	federalismi.it	Società Editoriale Federalismi s.r.l.	Di Costanzo	Daniela	2016-04-20	4	8
			
			if(this.provv_year==null || this.provv_year.trim().length()==0) {
				if(text.toLowerCase().indexOf("2016")!=-1) {
					this.provv_year = "2016";
				}
			}
			
			
	}
	
	
	
	public String getIdProvvedimento() {
		return idProvvedimento;
	}



	public void setIdProvvedimento(String idProvvedimento) {
		this.idProvvedimento = idProvvedimento;
	}



	public List<Fonte> getFonti() {
		return fonti;
	}



	public void addFonte(Fonte fonte) {
		//this.fonte = fonte;
		this.fonti.add(fonte);

	}
	
	
	public String getEstremoText() {
		return estremoText;
	}


	public void setEstremoText(String estremoText) {
		this.estremoText = estremoText;
	}


	public boolean isLinkolnFoundRef() {
		return linkolnFoundRef;
	}



	public void setLinkolnFoundRef(boolean linkolnFoundRef) {
		this.linkolnFoundRef = linkolnFoundRef;
	}



	public String getLegalArea() {
		return legalArea;
	}



	public void setLegalArea(String legalArea) {
		this.legalArea = legalArea;
	}



	public String getDocType() {
		return docType;
	}



	public void setDocType(String docType) {
		this.docType = docType;
	}



	public String getAuthority() {
		return authority;
	}



	public void setAuthority(String authority) {
		this.authority = authority;
	}



	public String getProvv_year() {
		return provv_year;
	}



	public void setProvv_year(String provv_year) {
		this.provv_year = provv_year;
	}



	public String getProvv_date() {
		return provv_date;
	}



	public void setProvv_date(String provv_date) {
		this.provv_date = provv_date;
	}



	public String getProvv_number() {
		return provv_number;
	}



	public void setProvv_number(String provv_number) {
		
		this.provv_number = provv_number;
		if(provv_number!=null && provv_number.trim().length()>0)
			setEstremoCompletoAuto(true);
			
	}



	public String getCourt_city() {
		return court_city;
	}



	public void setCourt_city(String court_city) {
		this.court_city = court_city;
	}



	public String getCourt_municipality() {
		return court_municipality;
	}



	public void setCourt_municipality(String court_municipality) {
		this.court_municipality = court_municipality;
	}



	public String getGeo_name() {
		return geo_name;
	}



	public void setGeo_name(String geo_name) {
		
		// converte primo carattere in UpperCase
		if(geo_name.trim().length()>0 && Character.isLowerCase(geo_name.charAt(0))){
			geo_name=(""+geo_name.charAt(0)).toUpperCase()+geo_name.substring(1, geo_name.length());
		}
	
		this.geo_name = geo_name;
	}



	public String getCourt_subject() {
		return court_subject;
	}



	public void setCourt_subject(String court_subject) {
		this.court_subject = court_subject;
	}



	public String getCourt_section() {
		return court_section;
	}



	public void setCourt_section(String court_section) {
		this.court_section = court_section;
	}



	public String getProvenance() {
		return provenance;
	}



	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}

	
	
	
	public String getCommentedWith() {
		return commentedWith;
	}



	public void setCommentedWith(String commentedWith) {
		this.commentedWith = commentedWith;
	}
	
	
	
	
	public void printListEditoriCSV(){
		
		
		//String header =  "id"+Config.SEP+Config.SEP+"distretto"+Config.SEP+"anno"+Config.SEP+"editore"; 

		for(int i=0;i<this.fonti.size();i++) {
			System.err.println(this.idProvvedimento+Config.SEP+this.distrettoCorteAppello+Config.SEP+this.provv_year+Config.SEP+this.fonti.get(i).getEditore());
		}
	}

	private String simplifiedProvenance(String provenance) {
		
		
		if(provenance.toLowerCase().indexOf("lexadoc")!=-1)
			return "DeJure_Giur";
		if(provenance.toLowerCase().indexOf("dejure")!=-1)
			return "DeJure";
		if(provenance.toLowerCase().indexOf("wki")!=-1)
			return "WKI";
		if(provenance.toLowerCase().indexOf("dogi")!=-1)
			return "DoGi";
	
		return provenance;
	}
	
	public void printGennusoSingleRowProvvedimento(){
		
			String ret="";

			ret+=this.persistentID+Config.SEP
					//+this.idProvvedimento+Config.SEP
					//+this.scrapedPageNumber+Config.SEP
					+this.isEstremoCompleto()+Config.SEP
					+this.isProvvedimentoConsegnato()+Config.SEP
					+this.isProvvedimentoTrovato+Config.SEP
					//+this.isEstremoCompletoAuto()+Config.SEP
					//+this.isEstremoCompletoEdit()+Config.SEP
					+this.distrettoCorteAppello+Config.SEP
					+this.provv_year+Config.SEP
					+getAutorita(this.authority)+Config.SEP
					+this.geo_name+Config.SEP
					+getTipoDoc(this.docType)+Config.SEP
					+this.guessed_subject+Config.SEP
					+this.provv_date+Config.SEP
					+this.provv_number+Config.SEP
					+this.court_section+Config.SEP
					+this.giudici+Config.SEP
					+this.parti+Config.SEP
					+this.NumRG+Config.SEP
					+this.estremoUniforme+Config.SEP
					//+this.url+Config.SEP
					+titoloContributoCSV(this.fonti)+Config.SEP
					+printFonteGennusoCSV(this.fonti)+Config.SEP
					//+urlContributoCSV(this.fonti)+Config.SEP
					//+rivistaCSV(this.fonti)+Config.SEP
					//+editoreCSV(this.fonti)+Config.SEP
					//+findableInCSV(this.fonti)+Config.SEP
					//+sistematiciCSV(this.fonti)+Config.SEP
				
					// note varie
					+this.nota+Config.SEP
					+simplifiedProvenance(this.getProvenance())+Config.SEP
					+Config.DATASET_VERSION;
			
			
			System.err.println(ret);
		
		
//		HEADER vers. 1.0
//		id: identificativo numerico del record;
//		distretto: distretto di Corte d’appello dell’ufficio che emana il provvedimento;
//		anno: anno del provvedimento;
//		tipo_ufficio: tipo di ufficio che emana il provvedimento (cfr. Tab. 4);
//		localita_ufficio: città sede dell’ufficio che emana il provvedimento;
//		tipo_provvedimento: sentenza, ordinanza o decreto;
		
		// provv_data
		// provv_numero
		// provv_sez_trib

//		settore: civile/penale/lavoro/commerciale/industriale come acquisito dalla citazione del
//			provvedimento, dal settore della rivista in cui è pubblicato il provvedimento o dalla
//			classificazione DoGi;
//		estremi: citazione in forma testuale del provvedimento da acquisire;
//		fonte: la citazione bibliografica della nota a sentenza a commento del provvedimento
//			selezionato, scritta nella forma concordata con le case editrici e riportata nell’allegato 1 del rapporto n. 1. La cella può contenere la concatenazione delle citazioni bibliografiche di più fonti dottrinali che annotano il provvedimento;
//		editore: l’editore della rivista che pubblica la nota a sentenza. La cella conterrà una lista di più editori nel caso di provvedimento annotato in più contributi dottrinali;
//		provenienza: archivio da cui sono ottenuti i record;
//		versione: versione del dataset.
		
		
//		Per quanto riguarda il Numero della sentenza andrebbe inserita in una voce a parte
//		Stessa cosa per la data di Deposito della sentenza.
		
// 		AGGIUNTE: 
//		provv_data nel formato aaaa-mm-gg  (deposito?)
//		provv_numero
//		provv_sez_trib
//		giudici
//		parti
//		numero_registro
		
		
		
//		MODIFICA RICHIESTA GENNUSO mail 6/12/2018
//		Per esempio la Fonte "Insussistenza del giustificato motivo oggettivo di licenziamento e discriminazione su base sindacale, Riv. Diritto e lavoro nelle Marche, Fondazione Cassa di Risparmio di Ascoli Piceno, 2016, 1-2, 79, nota di GIULIANI ALESSANDRO" va convertita in ->
//        Estremi = "Insussistenza del giustificato motivo oggettivo di licenziamento e discriminazione su base sindacale"
//        Fonte="Riv. Diritto e lavoro nelle Marche, Fondazione Cassa di Risparmio di Ascoli Piceno, 2016, 1-2, 79, nota di GIULIANI ALESSANDRO"

		
		
		
	}
	
	

	
	
	public void printOMEKASingleRowProvvedimento(){

		
		String ret="";

		ret+=this.persistentID+Config.SEP
				+this.isEstremoCompleto()+Config.SEP
				+this.distrettoCorteAppello+Config.SEP
				+this.provv_year+Config.SEP
				+getAutorita(this.authority)+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.estremoUniforme+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				+this.parti+Config.SEP
				+this.NumRG+Config.SEP
				//+this.url+Config.SEP
				+printUrlProvvedimentoCSV(this.url)+Config.SEP
				+printCitazioneFonteCSV(this.fonti)+Config.SEP
				+rivistaCSV(this.fonti)+Config.SEP
				+editoreCSV(this.fonti)+Config.SEP
				+classificazioniTopCSV(this.classificazioniTOP)+Config.SEP
				//+sistematiciCSV(this.fonti)+Config.SEP
				+simplifiedProvenance(this.getProvenance())+Config.SEP
				+Config.DATASET_VERSION;
				
		System.err.println(ret);
	}
	
	
	// FIXME citazione fonte in formato Omeka 1.8+
	public void printOMEKA_1_8_SingleRowProvvedimento(){

		
		String ret="";

		ret+=this.persistentID+Config.SEP
				+this.isEstremoCompleto()+Config.SEP
				+this.distrettoCorteAppello+Config.SEP
				+this.provv_year+Config.SEP
				+getAutorita(this.authority)+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.estremoUniforme+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				//+this.parti+Config.SEP
				+this.NumRG+Config.SEP
				//+this.url+Config.SEP
				+printUrlProvvedimentoCSV(this.url)+Config.SEP
				+printCitazioneFonteOMEKA_CSV(this.fonti)+Config.SEP
				+rivistaCSV(this.fonti)+Config.SEP
				+editoreCSV(this.fonti)+Config.SEP
				+classificazioniTopCSV(this.classificazioniTOP)+Config.SEP
				+classificazioniCSMFinalCSV(this.classificazioniCSMFinal)+Config.SEP
				//+sistematiciCSV(this.fonti)+Config.SEP
				+this.dataConsegnaProvvedimento+Config.SEP
				+simplifiedProvenance(this.getProvenance())+Config.SEP
				+Config.DATASET_VERSION;
				
		System.err.println(ret);
	}
	
	
	private String printUrlProvvedimentoCSV(String url) {
		String ret ="";
		String[] urlList = url.split("\\|");
		if(urlList==null || urlList.length==0)
			return url.trim();
		
		for(int i=0;i<urlList.length;i++) {
			if(urlList[i]!=null && urlList[i].trim().length()>0)
				ret+=urlList[i].trim()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	
	public void printSingleRowProvvedimento(){
		String ret="";

			
		
		ret+=this.persistentID+Config.SEP
				+this.idProvvedimento+Config.SEP
				+this.isEstremoCompleto()+Config.SEP
				+this.isEditManuale()+Config.SEP
				+this.isProvvedimentoConsegnato()+Config.SEP
				+this.dataConsegnaProvvedimento+Config.SEP
				+this.isProvvedimentoTrovato+Config.SEP
				//+this.isEsitoEdit()+Config.SEP
				//+this.isEstremoCompletoAuto()+Config.SEP
				//+this.isEstremoCompletoEdit()+Config.SEP
				//+this.scrapedPageNumber+Config.SEP
				+this.distrettoCorteAppello+Config.SEP
				+this.provv_year+Config.SEP
				+getAutorita(this.authority)+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				+this.parti+Config.SEP
				+this.NumRG+Config.SEP
				+this.estremoUniforme+Config.SEP
				+this.estremoText+Config.SEP
				+this.url+Config.SEP
				+printCitazioneFonteCSV(this.fonti)+Config.SEP
				+urlContributoCSV(this.fonti)+Config.SEP
				+rivistaCSV(this.fonti)+Config.SEP
				+editoreCSV(this.fonti)+Config.SEP
				+findableInCSV(this.fonti)+Config.SEP
				+classificazioniTopCSV(this.classificazioniTOP)+Config.SEP
				+sistematiciCSV(this.fonti)+Config.SEP
				+this.nota+Config.SEP
				+this.getProvenance()+Config.SEP
				+this.getLocalFilePath()+Config.SEP
				+this.notaRispostaRichiesta+Config.SEP
				+classificazioniCSMFinalCSV(this.classificazioniCSMFinal)+Config.SEP
				+Config.DATASET_VERSION;
				
		
		System.err.println(ret);
	}
	
	
	public void printSingleRowProvvedimentoCONSEGNA_FINALE(){
		String ret="";

		ret+=this.persistentID+Config.SEP
				+this.restituireDefinitivo+Config.SEP
				+this.idProvvedimento+Config.SEP
				+this.isEstremoCompleto()+Config.SEP
				+this.isEditManuale()+Config.SEP
				+this.isProvvedimentoConsegnato()+Config.SEP
				+this.dataConsegnaProvvedimento+Config.SEP
				+this.isProvvedimentoTrovato+Config.SEP
				//+this.isEsitoEdit()+Config.SEP
				//+this.isEstremoCompletoAuto()+Config.SEP
				//+this.isEstremoCompletoEdit()+Config.SEP
				//+this.scrapedPageNumber+Config.SEP
				+this.distrettoCorteAppello+Config.SEP
				+this.provv_year+Config.SEP
				+getAutorita(this.authority)+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				+this.parti+Config.SEP
				+this.NumRG+Config.SEP
				+this.dataDecisione+Config.SEP
				+this.dataPubblicazione+Config.SEP
				+classificazioniCSMFinalCSV(this.classificazioniCSMFinal)+Config.SEP
				+this.estremoUniforme+Config.SEP
				+this.estremoText+Config.SEP
				+this.url+Config.SEP
				+printCitazioneFonteCSV(this.fonti)+Config.SEP
				+urlContributoCSV(this.fonti)+Config.SEP
				+rivistaCSV(this.fonti)+Config.SEP
				+editoreCSV(this.fonti)+Config.SEP
				+findableInCSV(this.fonti)+Config.SEP
				+classificazioniTopCSV(this.classificazioniTOP)+Config.SEP
				+sistematiciCSV(this.fonti)+Config.SEP
				+this.nota+Config.SEP
				+this.getProvenance()+Config.SEP
				+this.getLocalFilePath()+Config.SEP
				+this.filePath+Config.SEP
				+this.notaRispostaRichiesta+Config.SEP
				+Config.DATASET_VERSION;
				
		
		System.err.println(ret);
	}
	
	
	
	public void printGennusoSingleRowProvvedimentoCONSEGNA_FINALE(){
		
		String ret="";

		ret+=this.persistentID+Config.SEP
				+this.restituireDefinitivo+Config.SEP
				+this.isEstremoCompleto()+Config.SEP
				+this.isProvvedimentoConsegnato()+Config.SEP
				+this.isProvvedimentoTrovato+Config.SEP
				+this.distrettoCorteAppello+Config.SEP
				+this.provv_year+Config.SEP
				+getAutorita(this.authority)+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.dataDecisione+Config.SEP
				+this.dataPubblicazione+Config.SEP
				//+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				/*+this.parti+Config.SEP*/
				+this.NumRG+Config.SEP
				+classificazioniCSMFinalCSV(this.classificazioniCSMFinal)+Config.SEP
				+this.estremoUniforme+Config.SEP
				+titoloContributoCSV(this.fonti)+Config.SEP
				+printFonteGennusoCSV(this.fonti)+Config.SEP
				// note varie
				+this.nota+Config.SEP
				+simplifiedProvenance(this.getProvenance())+Config.SEP
				+Config.DATASET_VERSION+Config.SEP
				+this.filePath+Config.SEP
				+this.getUrl()+Config.SEP
				+this.eli_GU_Pdf;
		
		
		System.err.println(ret);
	
}
	
	// TODO aggiungi in printSingleRow & OmekaPrint
	public String classificazioniCSMFinalCSV(String[] classificazioniCSMFinal) {
		
		if(classificazioniCSMFinal == null)
			return "";
		String ret="";
		if(!this.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.") && !this.getGuessed_subject().equalsIgnoreCase("PENALE")) {
			for(int i=0;i<classificazioniCSMFinal.length;i++) {
				if(classificazioniCSMFinal[i].trim().length()>0)
					ret+=classificazioniCSMFinal[i]+Config.MULTI_VALUE_SEP;

			}
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	

	
	public String classificazioniTopCSV(ArrayList<String> classificazioniTop) {

		if(classificazioniTop == null)
			return "";
		String ret="";
		if(!this.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")){
			for(int i=0;i<classificazioniTop.size();i++) {
				if(classificazioniTop.get(i).trim().length()>0)
					ret+=classificazioniTop.get(i)+Config.MULTI_VALUE_SEP;

			}
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	public void printSingleColumnClassificazioni() {
		String ret="";
		
		if(this.classificazioniTOP.size()>0) {
			for(int i=0;i<this.classificazioniTOP.size();i++) {
				if(this.classificazioniTOP.get(i).trim().length()>0)
					System.err.println(this.persistentID+Config.SEP+this.classificazioniTOP.get(i));
			}
		}else {
			System.err.println(this.persistentID+Config.SEP);
		}
		
		
			
	}
	
	
	
	public void printSpogliSingleRowProvvedimento(){
		String ret="";
	
		
//		p.setEditManuale(true);
//		p.setEsitoEdit(esito.toLowerCase().startsWith("s")?true:false);
//		p.setEstremoCompletoEdit(esito.toLowerCase().startsWith("s")?true:false);
//		p.setEstremoCompletoAuto(estremoCompleto.toLowerCase().equals("x")?true:false);
		
		ret+=this.idProvvedimento+Config.SEP
				+this.isEditManuale()+Config.SEP
				+this.isEsitoEdit()+Config.SEP
				+this.isEstremoCompletoAuto()+Config.SEP
				+this.isEstremoCompletoEdit()+Config.SEP
				+this.provv_year+Config.SEP
				+this.authority+Config.SEP
				+this.geo_name+Config.SEP
				+getTipoDoc(this.docType)+Config.SEP
				+this.guessed_subject+Config.SEP
				+this.provv_date+Config.SEP
				+this.provv_number+Config.SEP
				+this.court_section+Config.SEP
				+this.giudici+Config.SEP
				+this.parti+Config.SEP
				+this.NumRG+Config.SEP
				+this.estremoText+Config.SEP
				+this.url+Config.SEP
				+printCitazioneFonteCSV(this.fonti)+Config.SEP
				+urlContributoCSV(this.fonti)+Config.SEP
				+rivistaCSV(this.fonti)+Config.SEP
				+editoreCSV(this.fonti)+Config.SEP
				+this.getProvenance()+Config.SEP
				+Config.DATASET_VERSION;
		
		
		System.err.println(ret);
	}
	
	
	
	// exploded items 1 provvedimento - 1 fonte ---> duplicherà 208 provvedimenti commentati da più fonti
	public void printExplodedProvvedimentoCSV(){
		String ret="";
		for(int i=0;i<this.fonti.size();i++) {
			ret+=this.persistentID+Config.SEP
					+this.idProvvedimento+Config.SEP
					+this.isEstremoCompleto()+Config.SEP
					+this.isEditManuale()+Config.SEP
					+this.isProvvedimentoConsegnato()+Config.SEP
					+this.dataConsegnaProvvedimento+Config.SEP
					+this.isProvvedimentoTrovato+Config.SEP
					//+this.isEsitoEdit()+Config.SEP
					//+this.isEstremoCompletoAuto()+Config.SEP
					//+this.isEstremoCompletoEdit()+Config.SEP
					//+this.scrapedPageNumber+Config.SEP
					+this.distrettoCorteAppello+Config.SEP
					+this.provv_year+Config.SEP
					+getAutorita(this.authority)+Config.SEP
					+this.geo_name+Config.SEP
					+getTipoDoc(this.docType)+Config.SEP
					+this.guessed_subject+Config.SEP
					+this.provv_date+Config.SEP
					+this.provv_number+Config.SEP
					+this.court_section+Config.SEP
					+this.giudici+Config.SEP
					+this.parti+Config.SEP
					+this.NumRG+Config.SEP
					+this.estremoUniforme+Config.SEP
					/*+this.estremoText+Config.SEP*/
					+this.url+Config.SEP		
					//+this.fonti.get(i).getIdContributo()+Config.SEP
					+this.fonti.get(i).printCitazioneFonte()+Config.SEP
					+this.fonti.get(i).getUrl()+Config.SEP
					+this.fonti.get(i).getTitoloContributo()+Config.SEP
					+this.fonti.get(i).getAutori()+Config.SEP
					+this.fonti.get(i).getRivista()+Config.SEP
					+this.fonti.get(i).getEditore()+Config.SEP
					+this.fonti.get(i).getFindableIn()+Config.SEP
					+classificazioniTopCSV(this.classificazioniTOP)+Config.SEP
					//+printList(this.fonti.get(i).getSistematici())+Config.SEP
//					+printList(this.fonti.get(i).getDescrittori())+Config.SEP
					+this.nota+Config.SEP
					+this.getProvenance()+Config.SEP
					+this.getLocalFilePath()+Config.SEP
					+this.notaRispostaRichiesta+Config.SEP
					+Config.DATASET_VERSION;
			if(this.fonti.size()>1 && i<this.fonti.size()-1)
				ret+="\n";
		}
	
		// itera sulle fonti
		
		System.err.println(ret);
	}
	
	
	
	public String getTipoDoc(String docType) {
		String ret="";
		switch(docType) {
			case "JUDGMENT": 
				ret="SENTENZA";
				break;
			case "ORDER": 
				ret="ORDINANZA";
				break;
			case "DECREE":
				ret="DECRETO";
				break;
			default:
				ret = docType;
		}
		return ret;
	}
	
	public String getAutorita(String authority) {
		String ret="";
		if(authority==null)
			return null;
		switch(authority) {
			case "IT_TRB": 
				ret="TRIBUNALE";
				break;
			case "IT_TSV": 
				ret="TRIBUNALE DI SORVEGLIANZA";
				break;
			case "IT_CPP":
				ret="CORTE APPELLO";
				break;
			case "IT_CSS":
				//ret="CORTE ASSISE APPELLO";
				ret="CORTE APPELLO";
				break;
			case "IT_TMN":
				ret="TRIBUNALE DEI MINORI";
				break;
			case "IT_GPC":
				ret="GIUDICE DI PACE";
				break;
			case "IT_CASS":
				ret="CORTE DI CASSAZIONE";
				break;
			case "IT_COST":
				ret="CORTE COSTITUZIONALE";
				break;
			default:
				ret = null;
		}
		return ret;
	}
	
	private String idContributoDogiCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getIdContributo()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String grandeVoceDogiCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getGrandeVoceDogi()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	private String findabilityCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getFindableIn()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	public String editoreCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getEditore()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	private String printCitazioneFonteOMEKA_CSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		if(!simplifiedProvenance(this.provenance).equalsIgnoreCase("Dejure_giur") && !simplifiedProvenance(this.provenance).equalsIgnoreCase("GAZZETTA_UFFICIALE")) {
			for(int i=0;i<fonti.size();i++) {
				ret+=fonti.get(i).printCitazioneFonte()+Config.MULTI_VALUE_SEP;   
			}
		}else {
			for(int i=0;i<fonti.size();i++) {
				if(fonti.get(i).getTitoloContributo().trim().length()>0)
					ret+=fonti.get(i).getTitoloContributo()+", ";
				ret+=fonti.get(i).printFonteGennuso()+Config.MULTI_VALUE_SEP;   
			}
		}
		
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	

	
	public String printCitazioneFonteCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).printCitazioneFonte()+Config.MULTI_VALUE_SEP;   
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	public String printFonteGennusoCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).printFonteGennuso()+Config.MULTI_VALUE_SEP;   
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String sistematiciCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getSistematici()!=null && fonti.get(i).getSistematici().length>0)
				ret+=printList(fonti.get(i).getSistematici())+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	public String urlContributoCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getUrl()!=null && fonti.get(i).getUrl().trim().length()>0)
				ret+=fonti.get(i).getUrl()+" | ";
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-2).trim();
		return ret;
	}
	
	
	
	public String rivistaCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getRivista()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	public String titoloContributoCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			if(fonti.get(i).getTitoloContributo().trim().length()>0)
				ret+=fonti.get(i).getTitoloContributo()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String findableInCSV(List<Fonte> fonti) {
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=fonti.get(i).getFindableIn()+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	
	
	private String descrittoriCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=printList(fonti.get(i).getDescrittori())+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String fontiJur_LegCSV(List<Fonte> fonti) {
		//this.fonte.idContributoDogi
		String ret="";
		for(int i=0;i<fonti.size();i++) {
			ret+=printList(fonti.get(i).getFonti_Jur_Leg())+Config.MULTI_VALUE_SEP;
		}
		if(ret.length()>1)
			ret=ret.substring(0,ret.length()-1).trim();
		return ret;
	}
	
	private String printList(String[] list){
		
		String ret="";

		if(list!=null){
			for(int i=0;i<list.length;i++){
				ret+=list[i]+";";
			}
			if(ret.length()>0)
				ret=ret.substring(0, ret.length()-1).trim();
		}
		return ret;
	}
	
	
}
