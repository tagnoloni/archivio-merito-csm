package spogliFinale;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fonte.Fonte;
import provvedimento.Provvedimento;
import provvedimento.ProvvedimentoFinal;

public class EditSpogliDataReader {
	
	
	List<ProvvedimentoFinal> provvedimenti;
	
	HashMap<String,String> PENLookup;
	HashMap<String,String> CIVLookup;
	HashMap<String,String> PROCIVLookup;
	
	
	public boolean readEditSpogliDefinitiviTSV(String tsvFilePath) {
		
		provvedimenti = new ArrayList<ProvvedimentoFinal>();

		File noteTSV = new File(tsvFilePath);
		if(!noteTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
	
		String line  = null;
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader(noteTSV));
					
			int i=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				i++;
	
				//SKIP HEADER
				if(line.startsWith("idITTIG") || line.trim().length()==0)
					continue;
				
				line = line.replaceAll("( )+"," ").trim();
				//value.replace(/\s+/,' ') 
				
				String[] fields = line.split("\t");						
				
//				0 xidITTIG	
//				1 xrestituire_definitivo	
//				2 xnota_spoglio_ITTIG	
//				3 dataConsegnaProvvedimento	
//				4 isProvvedimentoTrovato	
//				5 notaRisposta	
//				6 estremo_uniforme	
//				7 local_pdf	
//				8 xtitolo_contributo	
//				9 xfonte	
//				10 xdistretto	
//				11 xanno	
//				12 xtipo_ufficio	
//				13 xlocalita_ufficio	
//				14 xtipo_provvedimento	
//				15 xsettore	
//				16 xprovv_sez	
//				17 xprovv_numero	
//				18 xnum_registro	
//				19 provv_data	
//				20 x*data_decisione	
//				21 x*data_pubblicazione	
//				22 xgiudici	
//				23 x*presidente	
//				24 x*relatore_estensore	
//				25 x*voci_classificazione_finale	
				
//				26 descrittoriTOP	
//				27 classiCSM	
//				28 url_provv	
//				29 url_contributo	
//				30 rivista	
//				31 editore	
//				32 provenance	
//				33 versione

				
				String idITTIG ="";
				String restituireDefinitivo; 
				String nota_spoglio_ITTIG="";
				String titolo_contributo ="";
				String fonte = "";
				String distretto ="";
				String anno="";
				String tipo_ufficio ="";
				String localita_ufficio ="";
				String tipoDoc = "";
				String area = "";
				String provv_sezione = "";
				String provv_numero ="";
				String num_registro = "";
				String data_decisione="";
				String data_pubblicazione ="";
				String giudici = "";
				String presidente = "";
				String relatore_estensore = "";
				String voci_classificazione_finale ="";
				
				
				idITTIG = fields[0].trim();
				restituireDefinitivo = fields[1].trim();
				nota_spoglio_ITTIG = fields[2].trim();
				titolo_contributo = fields[8].trim();
				fonte = fields[9].trim();
				distretto = fields[10].trim();
				anno = fields[11].trim();
				tipo_ufficio  = fields[12].trim();
				localita_ufficio = fields[13].trim();
				tipoDoc = fields[14].trim();
				area = fields[15].trim();
				provv_sezione = fields[16].trim();
				provv_numero = fields[17].trim();
				num_registro = fields[18].trim();
				data_decisione = fields[20].trim();
				data_pubblicazione = fields[21].trim();
				giudici = fields[22].trim();
				presidente = fields[23].trim();
				relatore_estensore = fields[24].trim();
				voci_classificazione_finale = fields[25].trim();
				
				
				ProvvedimentoFinal p = new ProvvedimentoFinal();
		
				if(tipo_ufficio.equalsIgnoreCase("tribunale"))
					tipo_ufficio = "IT_TRB";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("corte appello"))
					tipo_ufficio = "IT_CPP";
				else if(tipo_ufficio.equalsIgnoreCase("corte assise appello"))
					tipo_ufficio = "IT_CSS";
				else if(tipo_ufficio.equalsIgnoreCase("tribunale dei minori"))
					tipo_ufficio = "IT_TMN";
				else if(tipo_ufficio.equalsIgnoreCase("giudice di pace"))
					tipo_ufficio = "IT_GPC";
				else if(tipo_ufficio.equalsIgnoreCase("cassazione"))
					tipo_ufficio = "IT_CSS";
				
			
				p.setSpogliato(true);
				p.setPersistentID(idITTIG);
				p.setRestituireDefinitivo(restituireDefinitivo.trim().toLowerCase().startsWith("true")?true:false);
				p.setNota(nota_spoglio_ITTIG);
				p.setDistrettoCorteAppello(distretto);
				p.setProvv_year(anno);	
				p.setAuthority(tipo_ufficio);
				p.setGeo_name(localita_ufficio);
				p.setDocType(tipoDoc);
				p.setLegalArea(area.toUpperCase().trim());
				p.setGuessed_subject(area.toUpperCase().trim());
				p.setNormalized_court_section(provv_sezione); 				

				p.setProvv_number(provv_numero);
				p.setNumRG(num_registro);
				p.setDataDecisione(data_decisione);
				p.setDataPubblicazione(data_pubblicazione);
				p.setGiudici(giudici);
				p.setPresidente(presidente);
				p.setRelatore_estensore(relatore_estensore);
				// FIXME - leggi i codici
				p.setClassificazioniFinal(getClassificazioniDefinitive(voci_classificazione_finale));

				
				// FIXME è corretto??
				// titolo_contributo / fonte
				if(titolo_contributo.trim().length()==0) {
					//System.out.println("----- EDIT FINALE - Sganciare fonte");
					Fonte f = new Fonte();
					f.setEstremoText("");
					p.addFonte(f);
				}
			
				provvedimenti.add(p);			
				
			}
			System.err.println("+++CONSEGNE DEFINITIVE - ADDED "+i);

		}catch(Exception e){
			System.err.println("error in line");
			System.err.println(line);

			e.printStackTrace();
		}

		return true;
	}
	
	
	
	// QUI FARE LOOKUP CODICI ?
	private ArrayList<String> getClassificazioniDefinitive(String vociClassificazione) {
		ArrayList<String> ret = new ArrayList<String>();
		String[] internal;
		if(vociClassificazione.contains("#"))
			internal = vociClassificazione.split("#");
		else
			internal = new String[]{vociClassificazione};
		
		String descLabel="";
		for(int i=0;i<internal.length;i++) {
			
			descLabel = internal[i];
			
			if(descLabel.startsWith("PEN.")) {
				if(PENLookup.get(descLabel).trim()!=null)
					descLabel= PENLookup.get(descLabel);
				else
					System.out.println("++++++ WARNING ++++ DESC LABEL not found for "+descLabel);
			}else if (descLabel.startsWith("CIV.")) {
				if(CIVLookup.get(descLabel).trim()!=null)
					descLabel= CIVLookup.get(descLabel);
				else
					System.out.println("++++++ WARNING ++++ DESC LABEL not found for "+descLabel);	
			}else if (descLabel.startsWith("PROCIV.")) {
				if(PROCIVLookup.get(descLabel).trim()!=null)
					descLabel= PROCIVLookup.get(descLabel);
				else
					System.out.println("++++++ WARNING ++++ DESC LABEL not found for "+descLabel);
			}
			
			ret.add(descLabel);
		}
		
		return ret;
	}
	
	
	
	
	public void setPENLookup(HashMap<String, String> pENLookup) {
		PENLookup = pENLookup;
	}



	public void setCIVLookup(HashMap<String, String> cIVLookup) {
		CIVLookup = cIVLookup;
	}



	public void setPROCIVLookup(HashMap<String, String> pROCIVLookup) {
		PROCIVLookup = pROCIVLookup;
	}
		
	
	
	
	
	public List<ProvvedimentoFinal> getProvvedimentiFinali() {
		return this.provvedimenti;
	}
	

}
