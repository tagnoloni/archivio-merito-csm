package spogliFinale;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fonte.Fonte;
import provvedimento.Provvedimento;
import provvedimento.ProvvedimentoFinal;
import util.Util;

public class MergeEditSpogli {
	
	HashMap<String,ProvvedimentoFinal> provv_Map_Final = new HashMap<String,ProvvedimentoFinal>();
	List<ProvvedimentoFinal> provv_EditManuale;
	
	

	public void generaTestoEstremoUniformeFinal() {
		
		for(ProvvedimentoFinal provv : provv_Map_Final.values()) {
			if(provv.getDocType().equalsIgnoreCase("ORDINANZA RIMESSIONE COST."))
				provv.setEstremoUniforme(provv.getEstremoUniforme());
			else {
				provv.setEstremoUniforme(Util.getEstremoUniformeFinal(provv));
			}
			
			
			// PER LA CONSEGNA FINALE MANTENGO SOLO CIVILE/PENALE/LAVORO
			if(provv.getGuessed_subject().toLowerCase().startsWith("commerc") || provv.getGuessed_subject().toLowerCase().startsWith("fallim"))
				provv.setGuessed_subject("CIVILE");
			
		}
	}
	
	
	public  void mergeEditedInfoSpogliManuali() {
		
		for(ProvvedimentoFinal editedProvv:provv_EditManuale) {
			
			ProvvedimentoFinal found = provv_Map_Final.get(editedProvv.getPersistentID().trim());
			
			if(found!=null) {
				
				found.setSpogliato(true);
				found.setRestituireDefinitivo(editedProvv.isRestituireDefinitivo());
				found.setNota(editedProvv.getNota());
				
			
				// FIXME
				// manca SGANCIA FONTE!
				
				// se le edited info sono non vuote, rimpiazza le info esistenti	
					
				if(editedProvv.getDocType()!=null && editedProvv.getDocType().trim().length()>0) {
					if(!encodeTipoDoc(editedProvv.getDocType()).equalsIgnoreCase(found.getDocType()))
						found.setDocType(encodeTipoDoc(editedProvv.getDocType()));
				}
				
				if(editedProvv.getLegalArea()!=null && editedProvv.getLegalArea().trim().length()>0) {
					if(!editedProvv.getGuessed_subject().equalsIgnoreCase(found.getGuessed_subject())) {
						found.setLegalArea(editedProvv.getLegalArea());
						found.setGuessed_subject(editedProvv.getLegalArea());
					}
				}
				
				if(editedProvv.getGiudici()!=null && editedProvv.getGiudici().trim().length()>0) {
					if(!editedProvv.getGiudici().equalsIgnoreCase(found.getGiudici()))
						found.setGiudici(editedProvv.getGiudici());
				}
				
				found.setPresidente(editedProvv.getPresidente());
				found.setRelatore_estensore(editedProvv.getRelatore_estensore());
				
				found.setProvv_number(editedProvv.getProvv_number());				
				found.setNumRG(editedProvv.getNumRG());
				
				
				// SEZIONE
				if(editedProvv.getNormalized_court_section()!=null /*&& editedProvv.getNormalized_court_section().trim().length()>0*/) {
					if(!editedProvv.getNormalized_court_section().equalsIgnoreCase(found.getNormalized_court_section()))
						found.setNormalized_court_section(editedProvv.getNormalized_court_section());
				}
									
				// DATA
				if(editedProvv.getProvv_date()!=null && editedProvv.getProvv_date().trim().length()>0) {
					if(!found.getProvv_date().trim().equalsIgnoreCase(editedProvv.getProvv_date().trim()))
						found.setProvv_date(editedProvv.getProvv_date().trim());
				}
				
				found.setDataDecisione(editedProvv.getDataDecisione());
				found.setDataPubblicazione(editedProvv.getDataPubblicazione());
				
	
				
				if(editedProvv.getGeo_name()!=null && editedProvv.getGeo_name().trim().length()>0) {
					if(!found.getGeo_name().trim().equalsIgnoreCase(editedProvv.getGeo_name().trim()))
						found.setGeo_name(editedProvv.getGeo_name().trim());
				}
				
				// FIXME - decodifica Codici
				found.setClassificazioniFinal(editedProvv.getClassificazioniFinal());
				
				
				
				if(editedProvv.getFonti()!=null && editedProvv.getFonti().size()>0) {
					if(editedProvv.getFonti().get(0)!=null && editedProvv.getFonti().get(0).getEstremoText().trim().length()==0) {
					
						// SE IL RECORD NON AVEVA GIA' TITOLO_CONTRIBUTO VUOTO -- allora è stato sganciato
						if(found.getFonti().get(0).getTitoloContributo().trim().length()>0) {
							System.out.println("++++ SGANCIA FONTE PER "+found.getPersistentID());
							String contrib_fonte = "DeJure, Giuffrè Francis Lefebvre";
							editedProvv.getFonti().get(0).setRivista(contrib_fonte);
							editedProvv.getFonti().get(0).setTitoloContributo("");
							editedProvv.getFonti().get(0).setEstremoText(contrib_fonte); 
							editedProvv.getFonti().get(0).setSistematici(new String[]{""});
							editedProvv.getFonti().get(0).setDescrittori(new String[]{""});

							ArrayList<Fonte> newFonti = new ArrayList<Fonte>();
							newFonti.add(editedProvv.getFonti().get(0));
							found.setFonti(newFonti);
						}else {
							//System.out.println("++++ NON SGANCIARE FONTE PER "+found.getPersistentID());
						}
					}

				}
				
				
				
				
			}	
			else {
				System.err.println("**** WARNING provvedimento NOT FOUND -- "+editedProvv.getPersistentID());
			}
			
		}
	}
	
	
	
	

	
	
	private String encodeTipoDoc(String tipoDoc) {
		String ret ="";
		switch(tipoDoc.toLowerCase()) {
		case "sentenza": 
			ret="JUDGMENT";
			break;
		case "ordinanza": 
			ret="ORDER";
			break;
		case "decreto":
			ret="DECREE";
			break;
		default:
			ret = tipoDoc;
		}
		return ret;
	}	
	
	public HashMap<String, ProvvedimentoFinal> getProvv_Map_Final() {
		return provv_Map_Final;
	}
	
	public void setProvv_Map_Final(HashMap<String, ProvvedimentoFinal> provv_Map_Final) {
		this.provv_Map_Final = provv_Map_Final;
	}
	
	public List<ProvvedimentoFinal> getProvv_EditManuale() {
		return provv_EditManuale;
	}
	
	public void setProvv_EditManuale(List<ProvvedimentoFinal> provv_EditManuale) {
		this.provv_EditManuale = provv_EditManuale;
	}

}
