package spogliFinale;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import config.Config;
import config.ConfigSpogli;
import fonte.Fonte;
import provvedimento.Provvedimento;
import provvedimento.ProvvedimentoFinal;
import util.Util;


// LEGGI MAPPE code2descrittore
// LEGGI MAPPE ALLINEAMENTO
// aggiorna i provvedimenti con le info prese dagli allinemanenti

public class SpogliFinale {
	
	
	HashMap<String,Provvedimento> provv_Merge_Map = new HashMap<String,Provvedimento>();
	//HashMap<String,ProvvedimentoFinal> provv_Map_Final = new HashMap<String,ProvvedimentoFinal>();
	HashMap<String,ProvvedimentoFinal> provv_Map_Final = new HashMap<String,ProvvedimentoFinal>();


	HashMap<String,ArrayList<String>> classificazioniTOPMap = new HashMap<String,ArrayList<String>>();

	HashMap<String,String> PENLookup;
	HashMap<String,String> CIVLookup;
	HashMap<String,String> PROCIVLookup;
	
	HashMap<String,ArrayList<String>> allineaCIV;
	HashMap<String,ArrayList<String>> allineaPEN;
	HashMap<String,ArrayList<String>> allineaPEN_DJ;

	
	

	public void setProvv_Merge_Map(HashMap<String,Provvedimento> map) {
		this.provv_Merge_Map = map;
	}
	
	
	public HashMap<String,Provvedimento> getProvv_Merge_Map() {
		return this.provv_Merge_Map;
	}
	
	
	public HashMap<String, ProvvedimentoFinal> getProvv_Map_Final() {
		return provv_Map_Final;
	}
	
	public void update_ProvvMergeMap() {
		
		for(Provvedimento provv : provv_Merge_Map.values()) {			
			
			ArrayList<String> classificazioniTop = classificazioniTOPMap.get(provv.getPersistentID());	
			provv.setClassificazioniTOP(classificazioniTop);
			
		}
		
	}
	
	
	private ProvvedimentoFinal copyProvv(Provvedimento provv) {
		
		ProvvedimentoFinal provvFinal = new ProvvedimentoFinal();
		
		provvFinal.setPersistentID(provv.getPersistentID());
		provvFinal.setDistrettoCorteAppello(provv.getDistrettoCorteAppello());
		provvFinal.setNumRG(provv.getNumRG());
		provvFinal.setDataUdienza(provv.getDataUdienza());
		provvFinal.setUrl(provv.getUrl());
		provvFinal.setFonti(provv.getFonti());
		provvFinal.setGuessed_subject(provv.getGuessed_subject());
		provvFinal.setEstremoCompleto(provv.isEstremoCompleto());
		provvFinal.setEstremoUniforme(provv.getEstremoUniforme());
		provvFinal.setGiudici(provv.getGiudici());
		provvFinal.setParti(provv.getParti());
		provvFinal.setDocType(provv.getDocType());
		provvFinal.setAuthority(provv.getAuthority());
		provvFinal.setProvv_year(provv.getProvv_year());
		provvFinal.setProvv_date(provv.getProvv_date());
		provvFinal.setProvv_number(provv.getProvv_number());
		provvFinal.setGeo_name(provv.getGeo_name());
		provvFinal.setCourt_section(provv.getCourt_section());
		provvFinal.setProvenance(provv.getProvenance());

		
		// ATTRIBUTI CONSEGNE DEFINITIVE (di Giugno) ?
		provvFinal.setPersistentIDFix(provv.getPersistentIDFix());
		provvFinal.setDataDecisione(provv.getDataDecisione());
		provvFinal.setDataPubblicazione(provv.getDataPubblicazione());
		provvFinal.setRestituireDefinitivo(provv.isRestituireDefinitivo());
		provvFinal.setFilePath(provv.getFilePath());
		provvFinal.setClassificazioniCSMFinal(provv.getClassificazioniCSMFinal());
		provvFinal.setEli_GU_Pdf(provv.getEli_GU_Pdf());
		// END - CONSEGNE DEFINITIVE
		
	
		provvFinal.setClassificazioniCSM(provv.getClassificazioniCSM());
		provvFinal.setClassificazioniTOP(provv.getClassificazioniTOP());
		provvFinal.setProvvedimentoTrovato(provv.isProvvedimentoTrovato());
		provvFinal.setLocalFilePath(provv.getLocalFilePath());
		provvFinal.setNotaRispostaRichiesta(provv.getNotaRispostaRichiesta());
		provvFinal.setProvvedimentoConsegnato(provv.isProvvedimentoConsegnato());
		provvFinal.setDataConsegnaProvvedimento(provv.getDataConsegnaProvvedimento());
		provvFinal.setDeJureClasse(provv.getDeJureClasse());
		provvFinal.setNota(provv.getNota());
		
		
		return provvFinal;
	}
	


	public void updateProvvMapFinal() {
		for(ProvvedimentoFinal provvF : provv_Map_Final.values()) {
			
			
			// 1 - SET URL NASH
			if(provvF.isProvvedimentoTrovato().trim().length()>0 && provvF.isProvvedimentoTrovato().startsWith("TROVATO") ) {
				provvF.setLocalFilePath(provvF.getLocalFilePath().replace("file://"+Config.folderRISPOSTE+"/", ConfigSpogli.NASH_PREFIX));
			}
	
//			il campo provv_sez contiene sia numeri arabi, sia numeri romani, sia parole e abbreviazioni 
//			(es. civile, lavoro, lav., L, fallimentare, minori, …). Adottare la pratica di inserire al posto del numero 
//			romano il corrispondente arabo e quella di sostituire le abbreviazioni, ove possibile, 
//			con la corrispondente parola intera, ad esempio sostituire L o lav. con lavoro;  
//			Il dato sembra necessario ai tecnici Cassazione al fine di unificare il sistema a quello attualmente 
//			in uso presso il CED 
			
			// 2 - normalizza SEZIONI
			// FIXME la sezione normalizzara sarebbe da mettere in un campo a parte e tenuta quella originale per le citazioni testuali con abbreviazioni
			provvF.setNormalized_court_section(Util.normalizeSezione_CEDStyle(provvF.getCourt_section()));
			
		}		
	}
	
	
	public void setClassificazioniFinali_MergeMap() {

		for(Provvedimento provv : provv_Merge_Map.values()) {	
			
			
			ProvvedimentoFinal provvFinal = copyProvv(provv);
			// INIT
			provvFinal.setRestituireDefinitivo(true);
			provvFinal.setDataDecisione(provv.getProvv_date());
			provvFinal.setDataPubblicazione(provv.getProvv_date());


			// per ogni provv.
			// leggi le classificazioni TOP
			// percorri l'allineamento --> CODICE match
			// da CODICE get LABEL

			ArrayList<String> classificazioniTOP = provv.getClassificazioniTOP();
			String area = provv.getGuessed_subject();
			String tipoProvv = provv.getDocType();
			HashMap<String,ArrayList<String>> alignMap = null;


			if(tipoProvv.equalsIgnoreCase("ORDINANZA RIMESSIONE COST.")) {
				provv_Map_Final.put(provvFinal.getPersistentID(), provvFinal);
				continue;
			}

			
			ArrayList<String> matches = null;

			ArrayList<String> classi_prov_1 = new ArrayList<String>();
			ArrayList<String> classi_prov_2 = new ArrayList<String>();
			ArrayList<String> classi_prov_3 = new ArrayList<String>();
			
			// 1. CLASSIFICAZIONI DERIVANTI DA allineamento Classificazioni TOP

			if(area.equalsIgnoreCase("penale")) 
				alignMap = allineaPEN;
			else
				alignMap = allineaCIV;

			for(String desc :classificazioniTOP) {	
				System.err.println("TOP - "+desc+"  -- "+getMatchesForDesc(desc, alignMap));
				classi_prov_1.addAll(getMatchesForDesc(desc, alignMap));	
			}
			
			
			// 2. CLASSIFICAZIONI CSM Final preesistenti
			if(!provv.getGuessed_subject().equalsIgnoreCase("PENALE") && provv.getClassificazioniCSMFinal()!=null && provv.getClassificazioniCSMFinal().length>0) {
				
					System.err.println("FINAL - "+Arrays.asList(provv.getClassificazioniCSMFinal()));
					classi_prov_2.addAll(Arrays.asList(provv.getClassificazioniCSMFinal()));
				
			}
			
			// 3. CLASSIFICAZIONI DERIVANTI DA allineamento Classi Penali DeJure

			if(provv.getGuessed_subject().equalsIgnoreCase("PENALE") && provv.getClassificazioniCSMFinal()!=null && provv.getClassificazioniCSMFinal().length>0) {
				alignMap = allineaPEN_DJ;
				for(String desc :provv.getClassificazioniCSMFinal()) {	
					System.err.println("PENALE_DJ - "+desc+"  -- "+getMatchesForDesc(desc, alignMap));
					classi_prov_3.addAll(getMatchesForDesc(desc, alignMap));
				}
			}
			
			
			ArrayList<String> classi_prov_final = mergeClassiFinal(classi_prov_1,classi_prov_2,classi_prov_3);
			System.err.println(provv.getPersistentID()+"  -- "+classi_prov_final);
			System.err.println();
			
			
			provvFinal.setClassificazioniFinal(classi_prov_final);
			provv_Map_Final.put(provvFinal.getPersistentID(), provvFinal);
			// TODO
			// FIXME
			// ai descrittori testuali sarebbe preferibile aggiungere anche il codice (lookup da tabelle invertite?)
			
		}
	}
	
	
	
	private ArrayList<String> getMatchesForDesc(String desc, HashMap<String,ArrayList<String>> alignMap){
		
		// USA 
		// HashMap<String,String> PENLookup;
		// HashMap<String,String> CIVLookup;
		// HashMap<String,String> PROCIVLookup;

		// HashMap<String,ArrayList<String>> allineaCIV;
		// HashMap<String,ArrayList<String>> allineaPEN;
		// HashMap<String,ArrayList<String>> allineaPEN_DJ;
		
		ArrayList<String> returnMatches = new ArrayList<String>();
		ArrayList<String> matches = null;

		if(desc.trim().length()>0) {
			if(alignMap.get(desc)!=null) {
				//System.err.println("TROVATO ALLINEAMENTO PEN PER: "+desc + allineaPEN.get(desc));
				matches = alignMap.get(desc);
				// PIU ALLINEAMENTI PER DESCRITTORE
				for(String match:matches) {
					if(match!=null && match.trim().length()>0) {
						if(match.startsWith("CIV")) {
							if(CIVLookup.get(match)!=null) {
								//System.err.println(desc+"  -- "+CIVLookup.get(match));
								returnMatches.add(CIVLookup.get(match));
							}
							else
								System.err.println("++ WARNING failed lookup for "+match);
						}else if(match.startsWith("PEN")) {
							if(PENLookup.get(match)!=null) {
								//System.err.println(desc+"  -- "+PENLookup.get(match));
								returnMatches.add(PENLookup.get(match));
							}
							else
								System.err.println("++ WARNING failed lookup for "+match);
						}else if(match.startsWith("PROCIV")) {
							if(PROCIVLookup.get(match)!=null) {
								//System.err.println(desc+"  -- "+PROCIVLookup.get(match));
								returnMatches.add(PROCIVLookup.get(match));

							}
							else
								System.err.println("++ WARNING failed lookup for "+match);
						}
					}
				}

			}else {
				System.err.println("NON TROVATO ALLINEAMENTO PER: "+desc);
			}

		}
		return returnMatches; 
	}
	
	
	private boolean containsIgnoreCase(String desc, ArrayList<String> ret) {
		boolean found = false;
		for(String item:ret) {
			if(desc.trim().toLowerCase().equals(item.trim().toLowerCase()))
				return true;
		}
		return found;
	}
	
	public ArrayList<String> mergeClassiFinal(ArrayList<String> cp1,ArrayList<String> cp2,ArrayList<String> cp3 ) {
		
		ArrayList<String> ret = new ArrayList<String>();
		
		for(String desc:cp1) {
			if(desc.trim().length()>0 && !containsIgnoreCase(desc.trim(),ret))
				ret.add(desc);
		}
		
		for(String desc:cp2) {
			if(desc.trim().length()>0 && !containsIgnoreCase(desc.trim(),ret))
				ret.add(desc);
		}
		
		for(String desc:cp3) {
			if(desc.trim().length()>0 && !containsIgnoreCase(desc.trim(),ret))
				ret.add(desc);
		}
		
		return ret;
	}
	
	public HashMap<String,ArrayList<String>> readAllineamentiTSV(String tsvFilePath) {
		
		HashMap<String,ArrayList<String>> allineamenti = new HashMap<String,ArrayList<String>>();

		
		File allineamentiTSV = new File(tsvFilePath);
		if(!allineamentiTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(allineamentiTSV));
			String line  = null;
	
	
			int idAllineamento=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				
				//SKIP HEADER
				if(line.startsWith("DESCRITTORE"))
					continue;
				
				String[] fields = line.split("\t");
	
				String label = fields[0].trim();
				String match1  = "";
				if(fields.length>2)
					match1  = fields[2].trim();
				String match2  = "";
				if(fields.length>3)
					match2 = fields[3].trim();
				String match3  = "";
				if(fields.length>4)
					match3 = fields[4].trim();

				ArrayList<String> match = new ArrayList<>();
				if(match1.trim().length()>0)
					match.add(match1);
				if(match2.trim().length()>0)
					match.add(match2);
				if(match3.trim().length()>0)
					match.add(match3);

				if(allineamenti.get(label)==null) {
					allineamenti.put(label, match);
				}else {
					System.err.println("WARNING - duplicate label: "+label +"  in ALIGN");
				}
				
			
				idAllineamento++;
				
				
			}
			System.err.println("ALLINEAMENTI - processed "+idAllineamento+" rows");
	
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return allineamenti;
	}
	
	public HashMap<String,String> readDescrittoriLookupTSV(String tsvFilePath) {
		
		HashMap<String,String> descrittoriLookup = new HashMap<String,String>();

		
		File descrittoriTSV = new File(tsvFilePath);
		if(!descrittoriTSV.exists()){
			System.err.println(" PROBLEMS READING SOURCE FILE "+tsvFilePath);
		}
		try{
			BufferedReader reader = new BufferedReader( new FileReader(descrittoriTSV));
			String line  = null;
	
			//  COMMENTATA	
			//	pagina	ufficio	localita	dataProvv	sezione	numero	estremo	urlSentenza	titolo	autori	fonte	urlScheda	rivista	dataNota	provenance
	
			//	NOTE_A_SENT
			//	pagina	ufficio	localita	dataProvv	sezione	numero	estremo	urlSentenza	titolo	autori	fonte	urlScheda	rivista	dataNota	classificazioniTOP	classificazioni	provenance
	
	
			int idDescrittori=0;
			while( ( line = reader.readLine() ) != null  /*&& i<10*/) {
				
				//SKIP HEADER
				if(line.startsWith("url_scheda_persona"))
					continue;
				
				String[] fields = line.split("\t");
	
				String codice = fields[0].trim();
				String label  = fields[1].trim();
				
				
				descrittoriLookup.put(codice, label);
				
				
	
//				String found ="";
//				if((found = findEstremoInProvvedimenti(estremo))==null) {
//					provvedimenti.put(""+idProvvedimento, p);
//				}else {
//					// provvedimento commentato in più contributi; gli aggiungo solo il contributo
//					Fonte newFonte = p.getFonti().get(0);
//					provvedimenti.get(found).addFonte(newFonte);
//				}
				idDescrittori++;
				
				
			}
			System.err.println("CODE-LOOKUP - processed "+idDescrittori+" rows");
	
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return descrittoriLookup;
	}

	
	
	
	public HashMap<String, String> getPENLookup() {
		return PENLookup;
	}


	public void setPENLookup(HashMap<String, String> pENLookup) {
		PENLookup = pENLookup;
	}


	public HashMap<String, String> getCIVLookup() {
		return CIVLookup;
	}


	public void setCIVLookup(HashMap<String, String> cIVLookup) {
		CIVLookup = cIVLookup;
	}


	public HashMap<String, String> getPROCIVLookup() {
		return PROCIVLookup;
	}


	public void setPROCIVLookup(HashMap<String, String> pROCIVLookup) {
		PROCIVLookup = pROCIVLookup;
	}
		
	
	public HashMap<String, ArrayList<String>> getAllineaCIV() {
		return allineaCIV;
	}


	public void setAllineaCIV(HashMap<String, ArrayList<String>> allineaCIV) {
		this.allineaCIV = allineaCIV;
	}


	public HashMap<String, ArrayList<String>> getAllineaPEN() {
		return allineaPEN;
	}


	public void setAllineaPEN(HashMap<String, ArrayList<String>> allineaPEN) {
		this.allineaPEN = allineaPEN;
	}


	public HashMap<String, ArrayList<String>> getAllineaPEN_DJ() {
		return allineaPEN_DJ;
	}


	public void setAllineaPEN_DJ(HashMap<String, ArrayList<String>> allineaPEN_DJ) {
		this.allineaPEN_DJ = allineaPEN_DJ;
	}

	
	
}



