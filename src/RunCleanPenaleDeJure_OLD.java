import config.Config;
import deJureDataReader.DeJureCivileDataReader;
import deJureDataReader.DeJurePenaleDataReader_OLD;


public class RunCleanPenaleDeJure_OLD{


	public static void main (String[] args) {
	
		//runCleanPenaleDeJure();
		runCleanPenaleDeJureNew();
		//	runDeJure();
		//	runDeJureSpogli();
			
		
	}
	

	
	private static void runCleanPenaleDeJure() {
		DeJurePenaleDataReader_OLD djPenaledr = new DeJurePenaleDataReader_OLD();

		System.out.println("-- START READ PENALE TSV --");
		
		

		
		String tsvFilePath=Config.DATA_INPUT_DEJURE_PENALE_TRIB_APP_ASSISE;

		djPenaledr.readDeJurePenaleTSV(tsvFilePath);	
		//djPenaledr.printPenaliDeJureExploded();
		djPenaledr.printPenaliDeJureSingleRow();

		
	}
		
	private static void runCleanPenaleDeJureNew() {
		DeJureCivileDataReader djPenaledr = new DeJureCivileDataReader();

		System.out.println("-- START READ PENALE TSV --");
		djPenaledr.readLookupComuni_TSV();
		djPenaledr.readLookupDistrettiCA_TSV();
		

		
		String tsvFilePath=Config.DATA_INPUT_DEJURE_PENALE_TRIB_APP_ASSISE;

		djPenaledr.readDeJureCivileTSV(tsvFilePath);
		//djPenaledr.printCivileDeJureSingleRow();
		djPenaledr.printCivileDeJureSingleRowStandard();

		
		
		
		//djPenaledr.printPenaliDeJureExploded();
		//djPenaledr.printPenaliDeJureSingleRow();

		
	}
	
}



