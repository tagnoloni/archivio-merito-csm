# export dataset DoGi, WKI, GFL note a giurisprudenza di merito

# gli step:
--------------------

    - note Dogi (versione febbraio 2019)
    - spogli DeJure
        - deduplicate
    - spogli WKI
    - merge con Dogi & deduplicate
    - integra spogli manuali DeJure
    - identifica/elimina sameAS
    - integra spogli manuali WKI
    - etc.. 

# TODO Marzo 2019

ulteriori flag provvedimento

- consegnato
- scartato
- segnalato (particolare importanza)


nel merge provvedimenti Dogi-DeJure ci sono ancora alcuni duplicati
- 1 Provv : N fonti


- integra spogli TAB 4 DeJure (Frandi)
- Genera estremoTextReadable da Estremi Provvedimento (mantieni originale per IdITTIG)


- Export griglie nuovi spogli (WKI)
- Export tabella ESITI_NEGATIVI_DEJURE per Venanzi


# TODO: CSM 2.0


rewrite printCSV

espandi Objects con meta 

CSV2RDF - ? check 

addMetadata : fonte1/fonte2/fonte3  WKI/GFL/Altro
addMetadata: presidente, relatore, estensore, collegio, Numero, N. rg, Sezione, Area, Classificazione, Url, Note, tipoMenzionedelProvvedimento

manually added metadata: TestoSentenza, AbstractRivista, MassimaRedazionale
tipoFonte: NotaASentenza in rivista / Massima Redazionale WKI/GIUFFRE / Testo Provvedimento

esplodere e modellare le fonti


generateId from "estremi"  - clean first e lascia come era
keep prev. id

IT:MI:CPP:2015:hash

IT:TO:TRB:2012:hash

 
clean Text (Fonti)


Task 1.5
--------------------


documentare con numeri la casistica e la trovabilità di attributi di identificazione - presidente/estensore/parti :
 
provvedimenti con abstract rivista

provvedimenti con massima redazionale

full text / sì no


come incrociano questi casi con findability di attributi di identificazione

es: in una massima redazionale il provvedimento è citato solo con Trib, e data

Task 2: 
--------------------


scrape da WKI/GFL

dal set iniziale si possono filtrare le fonti accoppiate con fonti CASS



