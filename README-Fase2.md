# METADATA DEFINITIVI

- separa selezione da spoglio (leggi da dataset.ser)
- recupero mapping classificazioni / classificazioni pre-exist
- recuper mail commenti Mandalari a formato finale - normalizza sezioni
- output tabella spoglio (formato - quali colonne)
- regole spoglio
	- data pubblicazione
	- data decisione
	- giudici
	- classificazioni
	- check se è lui
	- revisione estremi (tipoDoc / autorità / area / numeri(?) / sezione)
	- includi/scarta
	- sameAs di qualcun altro



1. X tabella per lookup codice2label
2. --> leggi tabelle mapping
3. --> dataset classificato definitivo (e % trovati classificati)


0.1 lettura da .ser
0.2 lettura risposte/consegnati ftp